
#ifndef FONT_
#define FONT_
#include <stdint.h>
#include "geo.h"
/*struct font_type
{
 uint8_t x,y;
 const uint8_t *p;
};
extern struct font_type cur_font;
extern int disp_char_type;

struct disp_icon_type
{
 int x;int y;struct  font_type ic;
};*/
extern const unsigned char checkbox_[];
extern struct disp_icon_type checkbox;
extern const unsigned char checked_[];
extern struct disp_icon_type checked;


extern struct disp_icon_type sys_batt0_icon;
extern struct disp_icon_type sys_batt1_icon;
extern struct disp_icon_type sys_batt2_icon;
extern struct disp_icon_type sys_batt3_icon;
extern struct disp_icon_type sys_charge_icon;
extern struct disp_icon_type bluethooticon;
extern struct disp_icon_type logo;
extern struct disp_icon_type ico_1pl;
extern struct disp_icon_type ico_3pl;
extern struct disp_icon_type ico_3d;
extern struct disp_icon_type ico_2p;
extern struct disp_icon_type ico_map;
extern struct disp_icon_type ico_trail;
extern struct disp_icon_type ico_comp;
extern struct disp_icon_type ico_comp_rev;      //VLG2-14 reversed compass
extern struct disp_icon_type ico_none;
extern struct disp_icon_type ico_comppic;
extern struct disp_icon_type ico_unchecked;
extern struct disp_icon_type ico_checked;
extern struct disp_icon_type ico_gps;
extern struct disp_icon_type ico_mem;
extern struct disp_icon_type ico_compass;     //VLG-19 compass ikon
extern struct disp_icon_type ico_decl;
extern struct disp_icon_type ico_dang;
extern struct disp_icon_type ico_delta;
extern struct disp_icon_type ico_baf;   //VLG2-15 ny ikon BAF
extern struct disp_icon_type ico_memory;
extern struct disp_icon_type ico_diameter;      //VLG2-2 Ny ikon diametermätningen
extern struct disp_icon_type ico_diameter_fixed;

extern const struct  font_type font6x8;
extern const struct  font_type font8x6;//reverse
extern const struct  font_type font8x13;
extern const unsigned char font8x13p[];
extern const uint8_t font6x8p[];
extern const struct  font_type font13x8;
const extern struct font_type font8x13;
const extern struct  font_type font_grec_8x13;
const extern struct  font_type font_grec_7x10;
const extern struct font_type font_rus_8x13;

const extern struct font_type font_rus_6x8;
const extern struct  font_type font_20x32;
const extern struct font_type font7x10;
const extern struct font_type font_rus_7x10;
const extern struct font_type font_rus_14x20;
const extern struct font_type font_rus_12x16;
const extern struct  font_type font_rus_16x26;
const extern struct font_type symbfont28x27;

const extern struct font_type SymbExplorerFont28x21;
const extern struct  font_type SymbSettingsFont28x24;
const extern struct font_type SymbDiscFont28x27;


const extern struct font_type SymbYesNoFont32x21;
const extern struct font_type SymbFlagg48x28;


void set_font(struct font_type font);


#endif
