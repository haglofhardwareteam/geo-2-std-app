#ifndef HEIGHT__
#define HEIGHT__

int fDme();
int fhgt1pl(void);
int fhgt1pl_lineclear(void);
int fhgt1pl_relative();
int fhgt1pl_repeat(void);
int fhgt1pl_gps(void);
int fhgt1pl_compass(void);
int fhgt1pl_(void);
int fhgt1pl_compass_return(void);
int fhgt3pl(void);
int fhgt3pl_(void);
void HgtRedraw(void);
int fhgt2pdme(void);
struct xyz_type * f3d_data();
int f3d1pl(void);
int f3d(void);
void DrawCompass(int angle);
int ReadCompAndLaserOrDme(int laser_or_dme,int fix_or_relative_target, int loop_compass);      //VLG2-14 lagt till reversed compass
int fmeasure_compass(void);
int fmeasure_compass_rev(void);  //VLG2-14 Reversed compass
int fDangTreeFast(void);
int Show2plDeltaResult(void);
void Show2plFinalDeltaResult(void);
int fhgtdelta(void);

int fhgt1p_dme_repeat();
int fhgt1p_dme_relative();
int calc_hgt_1pdme(int tdistance,int talpha);
void redrawAngleResultDme(void);
void redrawAimDmeText(void);
int fhgt1p_dme_();
int fhgt_laser_dme_repeat();
int fhgt_laser_dme_relative(int type);

int fmeasure_baf();     //VLG2-15 BAf-funktion

#endif