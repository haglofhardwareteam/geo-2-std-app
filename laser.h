#ifndef LASER__
#define LASER__

#define LASERCMD_ID 0
#define LASERCMD_ON 1
#define LASERCMD_OFF 2
#define LASERCMD_DIST 3
#define LASERCMD_LASTSTATUS 4
#define LASERCMD_READ_SETTINGS 5
#define LASERCMD_SET_MODE 6
#define LASERCMD_GET_MODE 7
#define LASERCMD_STORE_SETTINGS 8
#define LASERCMD_AUTO_DIST 9
#define LASERCMD_POWER 10

int fS200Id(void);
int s200ReadDist();
int s200ReadDistCont(); //VLG-28 Skjuter med lasern tills sl�pper ON, tar sist m�tta v�rdet (bios)
int s200PowerOffReady(); //sl�r av str�mmen
int s200PowerOnReady(); //sl�r p� str�mmen till lasern, v�ntar p� READY meddelande
int s200PowerOnNoReady(); //sl�r p� str�mmen till lasern, v�ntar inte p� READY, g�r snabbare att ta nytt v�rde
int s200PowerOffNoReady(); //sl�r av str�mmen
int s200GetMode();
int s200SetMode(int mode);
int s200GetLaserSettings();
int s200SetLaserSettings();

int Laser(int cmd,int arg);

#define SHOW_ALPHA               0
#define SHOW_ALPHA_AND_H         1
#define SHOW_H                   2
#define SHOW_ALPHA_BOTTOM_OR_TOP 3      //anv�nds inte
#define SHOW_ALPHA_HANGN_BERG    4       //VLG-7
#define SHOW_ALPHA_HANGN_DAL     5

#define NORMALMETHOD        0	 //0=normal height measuring=>several heights possible

#define ONE_P_LASER_METHOD  5	 //0=1p Laser
#define TWO_P_LASER_METHOD  6	 //1=2p Laser
#define TRANSPONDER_METHOD  7	 //2=DME
#define TREE_P_METHOD       8    //3=3p Laser
#define ONE_P_3D_METHOD     9    //4=3D
#define COMPASS_METHOD      10    //5=compass
#define ANGLE_METHOD        11    //6=angle
#define DME_LASER_METHOD    12    //DME Laser
#define ONE_P_LASER_METHOD_RELATIVE  13	 //0=1p Laser relative measurment map
#define ONE_P_LASER_METHOD_REPEAT  14	 //0=1p Laser relative measurment map
#define ONE_P_LASER_METHOD_COMPASS 15 //trail
#define ONE_P_LASER_METHOD_LINECLEAR 16 //line clear stage 1
#define ONE_P_LASER_OR_DME_METHOD_LINECLEAR 17 // //line clear stage 2
#define ONE_P_LASER_METHOD_LINECLEAR_FINAL 18 //line clear final res
#define TREE_P_METHOD_FIRSTLASERSHOT       19   //first laser shot, do not store if press send
#define DELTA_METHOD_LASERSHOT       20   //delta hgt laser shot, do not store if press send
#define ONE_P_LASER_METHOD_GPS          21 //VLG-2 map target with gps
#define TREE_P_METHOD_TRAIL             22      //VLG-2 TRAILXY
#define ONE_P_LASER_METHOD_COMPASS_XY 23 //VLG-2 TRAILXY
#define ONE_P_LASER_METHOD_RELATIVE_XY 24 //VLG-2 TRAILXY
#define ANGLE_METHOD_TRAIL        25    //VLG-7 TRAILXY Slope angle

#define AREAMETHOD 30
#define GPSMETHOD 31
#define LENMETHOD 32 //trail
#define GPSSINGLEMETHOD 33 //store single pos

enum {SCAN_MODE=1};

#define PI 3.141592654F					// Pi

void redrawOnlyAngle(int hgt);
void redrawAngle(void);
void redrawAngleResult(void);
void redrawHgt(int show_compass);       //VLG-26 lagt till argument f�r visning av kompassriktningen i HEIGHT DME funktionen
void redrawAngleNum(int talpha,int ty);
void redrawOnlyAngleNum(int tangle);
void redrawHgtNum(int counter,int angle,int AddRefH);
void Redraw(int hgt);
void RedrawNum(int counter,int hgt,int angle,int AddRefH);

void InitDispBuff();    //funktioner f�r att scrolla h�jder i displayen
void ScrollDispBuff(int counter, char *str);
void PrintDispBuff(int counter);

int fKeyLeft(void);
int fKeyRight(void);
int fKeyEnter(void);

int measure_angle(void);
int fmeasure_angle(int hgt);
int measure_angle_(int counter,int hgt,int AddRefH);
int fKeyExit(void);
int fKeyLeftEnter(void);
int fKeyRightEnter(void);
void redrawAimLaserText(void);
int LaserShot(int show_aim_text, int copy_dist, int copy_ir, int meas_ang, int scan_mode);

#endif

