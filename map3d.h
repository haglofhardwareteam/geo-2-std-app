#ifndef MAP3D_
#define MAP3D_

int GetSystemKeyNoTimeLimit();
int GetSystemKeyNoTimeLimitAllKeys();
int fmeasure_map();
int fmeasure_map_laserVL();     //DME + Laser
int fmeasure_map_laserL();      //Enbart Laser
int fmeasure_map_gps();
int fmeasure_trailxy_();
int UseLaserOrDme();
int UseLaserOnly();
void PrintAreaLen();
int GpsLog();
#endif