#include "MyFuncs.h"

struct eedatatype *ee;
const struct MenuType MainMenu;

static char irstr[9*5+1];       //minst 45+1 orginal 9*5+1
static const char ir_str1[] = "1 0000\r\n2 0000\r\n3 0000\r\n4 0000\r\n5 +000\r\n";
static int max_main_menu_items;

__no_init uint8_t disp_data[LCDNUMPAGES+1][LCDMAXX];

/**********************************
*
*
***********************************/
void InitApp()
{
  //l�s ut om VL eller L-instrument fr�n bios
  use_dme = GetStatus()&STATUS_USE_DME;
  if(use_dme)
    max_main_menu_items=NUM_OF_VL_FUNCTIONS;
  else
    max_main_menu_items=NUM_OF_L_FUNCTIONS;

  //l�s in inst�llningar fr�n minnet
  ee=(struct eedatatype *)GetGeoSettings();
  if(ee->lastmenu>=max_main_menu_items-1)//ignore last menu (usb in this case)
    ee->lastmenu=0;

  //spr�kval
  if((main_lang = GetLanguage())>JAPAN)
    main_lang = ENGLISH;

  sd.id=getlastdataset()+1;//last ID+1;

  gHandle=-1;

  InitSysdata();
}

/**********************************
*
*
***********************************/
int InitSysdata()
{
  int handle=-1;
  char filename[FILENAMESIZE+1];

  memset(&g_Sysdata,0x00,sizeof(g_Sysdata));

  strcpy(filename,SETUPFILENAME);

  if((handle=open(filename,_LLIO_WRONLY))<0)
  {
    if((handle=create(filename,_LLIO_WRONLY))<0)
      return -1;
  }
  else
  {
    //l�s struct, kolla version, om fel version radera och skapa ny
    lseek(handle,0,SEEK_SET);
    read(handle,(unsigned char *)&g_Sysdata,sizeof(g_Sysdata));
    if(g_Sysdata.ver == SETUPVER)
    {
      CloseFile(&handle);
      return 1;
    }

    CloseFile(&handle);
    DeleteFile(filename);
    if((handle=create(filename,_LLIO_WRONLY))<0)
      return -1;
  }

  //init
  g_Sysdata.ver = SETUPVER;
  //default p�
  g_Sysdata.nSendFileType[0] = 1; //csv
  g_Sysdata.nSendFileType[1] = 1; //kml

  //Aktiva menyer i programmet fr�n start
  //3 sista ska inte g� att sl� av Memory, Settings, USB
  //blir olika index f�r kompass om VL eller L
  if(use_dme)
  {
  g_Sysdata.functions[VL_MENU_HEIGHT1PL] = 1;       //HEIGHT 1PL
  g_Sysdata.functions[VL_MENU_HEIGHT3P] = 1;       //HEIGHT 3PL
  g_Sysdata.functions[VL_MENU_HEIGHTDME] = 1;      //HEIGHT DME         //VLG2-13 �ven Height DME default p� om VL
  g_Sysdata.functions[VL_MENU_COMPASS] = 1;       //COMPASS             //underl�ttar f�r monteringen om caompass �r aktiverad d� kontrollerar enheten efter kalibreringen av kompass
  }
  else
  {
    g_Sysdata.functions[L_MENU_HEIGHT1PL] = 1;       //HEIGHT 1PL
  g_Sysdata.functions[L_MENU_HEIGHT3P] = 1;       //HEIGHT 3PL
  g_Sysdata.functions[L_MENU_COMPASS] = 1;       //COMPASS
  }

  //spara
  lseek(handle,0,SEEK_SET);
  write(handle,(unsigned char *)&g_Sysdata,sizeof(g_Sysdata));

  CloseFile(&handle);

  //g�m filen
  SetFileAttributes(filename,GetFileAttributes(filename)|ATTR_HIDDEN);
  return 1;
}

/**********************************
*
*
***********************************/
int SaveSysdata()
{
  int handle=-1;
  char filename[FILENAMESIZE+1];

  strcpy(filename,SETUPFILENAME);
  if((handle=open(filename,_LLIO_WRONLY))<0)
  {
    //Filen kan ha tagits bort via USB eller Settings-Memory
    if((handle=create(filename, _LLIO_WRONLY))<0)
    {
      Message(str_msg_Error_,""/*str_error_filehandle_*/);
      return 1;
    }

    //g�m filen
    SetFileAttributes(filename,GetFileAttributes(filename)|ATTR_HIDDEN);
  }

  lseek(handle,0,SEEK_SET);
  write(handle,(unsigned char *)&g_Sysdata,sizeof(g_Sysdata));

  CloseFile(&handle);

  return 1;
}

/**********************************
*
*
***********************************/
int My_Disp_Draw_Icon(struct disp_icon_type *p)
{
  return Draw_Icon(DISPLAY_GRAFIC_XY,p);
}

/**********************************
*
*
***********************************/
int My_YesNoExit(int x,int y,char *str,int answer)
{
  int ch,tanswer=answer;
  SetCharMode(0);
  putxy(x,y,str);
  ico_unchecked.y=ico_unchecked.ic.y*y;
  ico_checked.y=ico_checked.ic.y*y;

  SetCharMode(0);
  tanswer&=1;
  do
  {
    if(tanswer)
      My_Disp_Draw_Icon(&ico_checked);
    else
      My_Disp_Draw_Icon(&ico_unchecked);

    ch=GetSystemKey();
    if(ch==KEY_LEFT||ch==KEY_RIGHT)
      tanswer^=1;
    if(ch<=0)
      return -1;
  }
  while(ch!=KEY_ENTER&&ch!=KEY_EXIT);
  if(ch==KEY_EXIT)
    return -1;

  return tanswer;
}

/**********************************
*
*
***********************************/
int My_PutChar(char ch)
{
  return SendChar(DISP,ch);
}

/**********************************
Show batt.level on display
***********************************/
void My_battcheck()
{
  int tBatt=BattVoltage();
  int flag=GetStatus();

  PutCursor(0,3);
  if(flag&STATUS_CHARGING)
    My_PutChar('\037'); //ikonerna m�ste vridas 90 grader om det ska bli likt GEO
  else
    if(tBatt<34/*10*/){
      My_PutChar('\034');
    }
    else if(tBatt<36/*11*/){
      My_PutChar('\033');
    }
    else if(tBatt<38/*12*/){
      My_PutChar('\032');
    }
    else{
      My_PutChar('\031');
    }
}

/**********************************
*
*
***********************************/
int My_ch_clr_buff()
{
  return ClrBuff(DISP);
}

/**********************************
*
*
***********************************/
int My_ChRead()
{
  return GetChar(DISP);
}

/**********************************
*
*
***********************************/
int My_getc()
{
  return My_ChRead();
}

/**********************************
*
*
***********************************/
void My_SetFont(int font)
{
  if(main_lang == RUSKA)
  {
    if(font == 68)
      SetFont(GetFontRus6x8());
    else
       SetFont(GetFontRus8x13());
  }
  else if(main_lang == JAPAN)
  {
    if(font == 68)
      SetFont(GetFontJpn6x8());
    else
       SetFont(GetFontJpn8x13());
  }
  else
    {
    if(font == 68)
      SetFont(GetFont6x8());
    else
       SetFont(GetFont8x13());
  }
}

/**********************************
*
*
***********************************/
int ExecMeny(const struct MenuType *m,int n,char *act_, int IsMainMenu)
{
  int i=*act_,res;
  int flag;
  int oldch=KEY_RIGHT;

  m->Redraw();

  while(1)
  {
    if(IsMainMenu && i < max_main_menu_items-NUM_ALWAYS_ON && g_Sysdata.functions[i]==0)  //3 sista ska inte g� att sl� av Memory, Settings, USB
    {
      //ej aktiv, prova n�sta
      if(oldch==KEY_LEFT)
      {
        if(--i<0)
          i=max_main_menu_items-1;
      }
      else
      {
        if(++i>max_main_menu_items-1)
          i=0;
      }
    }
    else
    {
      flag=GetStatus();

      if(m->Menu[i].ico!=NULL)
        My_Disp_Draw_Icon(m->Menu[i].ico);
      putxy(0,4,(char *)m->Menu[i].s);

      if(flag&STATUS_USE_BLUETOOTH)
      {
        PutCursor(2,3);
        if(flag&STATUS_BLUE_CONNECTED)
          SetCharMode(DISP_REVERSE);
        My_PutChar('\035');
        SetCharMode(0);
      }
      else
        putxy(2,3," ");


      if(flag&STATUS_USE_GPS)
        My_Disp_Draw_Icon(&ico_gps);
      else
        putxy(3,3," ");

      if(flag&STATUS_USE_EESAVEMEM)
        My_Disp_Draw_Icon(&ico_mem);
      else
        putxy(4,3," ");

      if(flag&STATUS_USE_COMPAS_DECL)
        My_Disp_Draw_Icon(&ico_decl);
      else
        putxy(5,3," ");

      //kompassikon, kompassen kommer alltid vara aktiverad i GEO2
      /*
      if(force_compass || (ee->useGps && ee->EEsaveEEmem) || flag&STATUS_USE_COMPAS)
        My_Disp_Draw_Icon(&ico_compass);
      else
        putxy(6,3," ");
*/
      //VLG2-2 ikon diameterm�tning
      if(flag&STATUS_USE_DIAMETER)
      {
        if(ee->EEdiameter>0)
          My_Disp_Draw_Icon(&ico_diameter_fixed);
        else
        My_Disp_Draw_Icon(&ico_diameter);
      }
      else
        putxy(6,3," ");

      My_battcheck();
      switch((oldch=GetSystemKey()))
      {
      case KEY_EXIT:
      case 0:
        My_ch_clr_buff();
        return 0;
        break;
      case KEY_USB5V:
        fUsb();
        m->Redraw();
        break;

      case KEY_ENTER:
        if((i!=*act_)&&IsMainMenu)
        {
          if(i!=ee->lastmenu&&n<sizeof(m)/sizeof(m[0])-1)//ignore last menu, USB
          {
            ee->lastmenu=i;
            SetGeoSettings();//save ee
          }
        }

        *act_=i;
        if((res=(m->Menu[i].f)())<=0)
          return abs(res);
        cls_Oled();
        m->Redraw();
        //My_ChRead();
        My_ch_clr_buff(); //rensa bufferten ist�llet, kan finnas flera knapptryckningar d�r
        break;
      case KEY_LEFT:
        if(--i<0)
          i=n-1;
        break;

      case KEY_RIGHT:
        if(++i>=n)
          i=0;
        break;
      }
    }
  }
}

/**********************************
*
*
***********************************/
int fSettings(void)
{
  int lang=main_lang;
  int res=-1;
  Settings(str_menu_SelectFuncs_,&res);
  //Settings(NULL,NULL);

  //VLG2-5 Kunna v�lja funktioner i huvudmenyn
  if(res==0)
  {
    fSelectFunctions();
  }
  else
  {
    ee=(struct eedatatype *)GetGeoSettings();    //l�s in p� nytt ifall n�got �ndrats

    //ladda om spr�kval, kan ha �ndrats
    if((main_lang = GetLanguage())>JAPAN)
      main_lang = ENGLISH;

    if(lang != main_lang)
      return 0;   //beh�ver returnera 0 om �ndrat spr�k, s� hoppar ur ExecMeny och startar om
  }
  return 1;
}

/**********************************
*
*
***********************************/
int fUsb(void)
{
  Usb("");
  return 1;
}

/**********************************
*
*
***********************************/
void copy_to_ir_str(int n,int num)
{
  char str[15+1];

  format(str,num,4,0,'0');
  memcpy(irstr+(8*n)+2,str,strlen(str));
  if(n==4){
    if(num<0){
      irstr[8*n+2]='-';
    }
    else{
      irstr[8*n+2]='+';
    }
  }

  sdBlue.distance=sd.distance; //V2.0 save in blue send struct
  sdBlue.hdistance=sd.hdistance;
  sdBlue.height=sd.height;
  sdBlue.gPitch=sd.gPitch;
  sdBlue.gYaw=sd.gYaw;
  return;
}

/**********************************
*
*
***********************************/
void irset(void)
{
  strcpy(irstr,ir_str1);
  memset(&sdBlue,0,sizeof(sdBlue));
}

/**********************************
*
*
***********************************/
void ir_str_ram(void)
{
  char ch,*str=(char *)irstr;
  if(ee->EEsaveEEmem)
  {
    if(sd.SaveOk == 1)
    {
      MemStore();      //save in EE memory
    }
    return;
  }

  //ingen datalagring s�nd ist�llet
  if(sd.SaveOk == 1)
    SendDataBT();//send nmea data via bluetooth  , om connected

  if(sd.use_ir)//bara vid h�jdm�tning
  {
    while(ch=*str++)   //send via ir
    {
      SendChar(SERIE3,ch);//ir_send(ch);
    }
  }
  sd.OledDisplayDataValid=0;//used to blink sight/result in oled
  sd.stored=1;
  return;
}

/**********************************
*
*
***********************************/
//VLG-2 omvandla grader till nygrader
double fGradToGon(double deg)
{
  float gon = 0.0;
  gon = deg * 10.0 / 9.0;
  return gon;
}

/**********************************
Convert to gradients
***********************************/
int ConvToGrad(int deg)
{
  int res;
  res=rounddiv(1000L*deg,900);
  return res;
}

/**********************************
Convert to procent
***********************************/
int ConvToProc(int deg)
{
  int res;

  res=rounddiv(1000L*isin(deg),icos(deg));
  if(deg<0){
    res=-res;
  }
  return res;
}

/**********************************
*
*
***********************************/
int SaveDispContenz()
{
  memcpy(&disp_data,ScreenUpdate(),sizeof(disp_data));
  return 1;
}

/**********************************
*
*
***********************************/
int RestoreDispContenz()
{
  memcpy(ScreenUpdate(),&disp_data,sizeof(disp_data));
  ScreenUpdate();
  return 1;
}

/**********************************
*
*
***********************************/
void putxy2_Oled(int x,int y, char* str)
{
  PrintStrOled(x,y,(const char*)str);
  return;
}

/**********************************
*
*
***********************************/
void data_set()
{
  int tid=sd.id;
  memset(&sd,0,sizeof(sd));
  sd.id=tid%100000;
  sd.measmethod=-1;
  sd.sek=1;
  sd.handlecsv=-1;
  sd.handlekml=-1;
  sd.handlejson = -1;   //VLG2-10 lagt till GeoJsonutskrift
}

/**********************************
*
*
***********************************/
int dme_key()
{
  int ch;
  do
  {
    ch=GetSystemKey();

    if(ch==KEY_RIGHT)
    {
      ir_str_ram();
    }
  }
  while(ch==KEY_CHARGING||ch==KEY_USB5V||ch==KEY_RIGHT&&sd.use_ir&&!ee->EEsaveEEmem);//return if use mem store
  return ch;
}

/**********************************
*
*
***********************************/
int CopyFile(char *source, char *dest)
{
  char filenames[2*FILENAMESIZE+3];     //char *filenames=�Soure\nDest� //Aktuellt dir = \DATA,  Dest kan ha sub dir (m�ste finnas innan anrop)

  sprintf(filenames,"%s\n%s",source,dest);
  return Generics(GENERICFILEMANAGMENT,COPYFILE, filenames);
}


//VLG2-2 funktioner f�r diameterm�tningen
/**********************************
*
*
***********************************/
int ConvToInch(int mm)
{
  long temp;
  temp=(long)mm*3937L;
  return rounddiv(temp,10000);
}

/**********************************
*
*
***********************************/
int distance_to_cm(int dist)
{
  if(Metric())
  {
    return dist;        //�r redan i cm
  }
  else
  {
    //g�r om fr�n ftx10 till cm
    int temp;
    temp=dist*3048;
    return rounddiv(temp,1000);
  }
}

/**********************************
*
*
***********************************/
void PrintHeightOled()
{
  PrintIntOled(0,62,sd.height*10);
}

/**********************************
*
*
***********************************/
int redrawDiaHgt(int fixed_dia)
{
  int ch=KEY_ENTER,dia_inch;
  struct oled_print_int_data_type oled_dia={OLEDMAXX/2,8,0,3,0};
  struct oled_print_int_data_type oled_hgt={OLEDMAXX/2+2*6,16,0,5,1};

  //Displayen
  cls();
  putxy(0,0, (char*)str_Dia_Diameter_);
  putxy(0,2, (char*)str_Dia_Height_);

  if(Metric())
  {
    xprintf(12-2,1,0,12,"cm");        //cm
    xprintf(12-2,3,0,12,"m");         //m

    if(fixed_dia)
    {
      putint(12-6-2,1,ee->EEdiameter,6,1);
      sd.diameter = ee->EEdiameter;
    }
    else
    {
      putint(12-6-2,1,dia_res.dia_mm,6,1);   //mm
      sd.diameter = dia_res.dia_mm;
    }
  }
  else
  {
    //Om feet s� konvertera dia till inch
    xprintf(12-2,1,0,12,"\"");        //inch
    xprintf(12-2,3,0,12,"ft");        //ft

    if(fixed_dia)
    {
      putint(12-6-2,1,ee->EEdiameter,6,1);
      sd.diameter = ee->EEdiameter;
    }
    else
    {
      putint(12-6-2,1,ConvToInch(dia_res.dia_mm),6,1);   //inch, 1 decimal
      sd.diameter = ConvToInch(dia_res.dia_mm);
    }
  }

  putint(12-6-2,3,sd.height,6,1);

  //Oled
  cls_Oled();
  PrintStrRolledRigthOled(OLEDMAXX/2+4*6,8,str_oled_Dia_);
  PrintStrRolledRigthOled(OLEDMAXX/2+4*6,16,str_oled_H_);

  if(Metric())
  {
    if(fixed_dia)
    {
      oled_dia.num = roundi(ee->EEdiameter);    //cm
    }
    else
    {
      oled_dia.num = roundi(dia_res.dia_mm);        //cm
    }

    putint_RolledRigthOled(&oled_dia);
  }
  else
  {
    if(fixed_dia)
    {
      oled_dia.num = roundi(ee->EEdiameter);   //inch
    }
    else
    {
      //Om feet s� konvertera dia till inch
      dia_inch = ConvToInch(dia_res.dia_mm);
      oled_dia.num = roundi(dia_inch);
    }

    putint_RolledRigthOled(&oled_dia);
  }

  oled_hgt.num = sd.height;  //try use 1 decimal
  putint_RolledRigthOled(&oled_hgt);

  SetOledTimer(30);

  if(sd.SaveOk==0)
    sd.SaveOk=1;    //s� kan spara nytt v�rde �ven om inte m�tt om avst�ndet (blir d� samma avst�nd som f�reg�ende m�tning)

  do
  {
    if((ch=dme_key())==KEY_EXIT)        //Kommer inte hoppa ur h�r om inte tycker p� knapp, om blir problem f�r anv�ndarna s� f�r d� �ndra rutinen
    {
      goto end_dia;
    }
    else if(ch==KEY_RIGHT && sd.stored)    //om sparat s� hoppa �ver till h�jdm�tningen igen
    {
      ch=KEY_ENTER;
      goto end_dia;
    }
  }while(ch!=KEY_ENTER);
end_dia:
  sd.diameter = 0;      //Nollar diametern s� att den inte h�nger med till n�sta h�jd
  return ch;
}

/**********************************
*
* VLG2-2 Diameterm�tning
***********************************/
int My_Meas_Dia(int sd_dist_cm, int fix_dia_mm, int AddRefH)
{
  int ch;

  cls();
  if(fix_dia_mm > 0)        //Vill s�ka ut h�jd med viss diameter, annan text i displayen
  {
    putxy(0,0, (char*)str_Fixed_Dia_);
    if(Metric())
    {
      xprintf(12-2,1,0,12,"cm");        //cm
    }
    else
    {
      xprintf(12-2,1,0,12,"\"");        //inch
    }
    putint(12-6-2,1,ee->EEdiameter,6,1);
  }
  else
  {
    putxy(0,0, (char*)str_Dia_Diameter_);
    putxy(0,2, (char*)str_Dia_Bar1_);
    putxy(0,3, (char*)str_Dia_Bar2_);
    putxy(0,4, (char*)str_Dia_Bar3_);
  }

  Bell(200);
  if((ch=Meas_Dia(sd_dist_cm,fix_dia_mm,&dia_res))==KEY_EXIT)
  {
    return KEY_EXIT;
  }
  else if(ch==KEY_ENTER)
  {
    int angle;
    struct sensordatatype sensor;

    //sd.distance_cm kan ha �ndrats beroende p� hur vinklar lasern
    sd.distance_cm = dia_res.sd_cm;

    //l�sa ut vinkeln fr�n read_angle och r�kna ut/om h�jden
    memset(&sensor,0x00,sizeof(sensor));
    if(force_compass || (ee->useGps && ee->EEsaveEEmem) || GetStatus()&STATUS_USE_COMPAS)       //VLG2-2 Kompassen ska alltid anv�ndas, STATUS_USE_COMPAS returnerar alltid 1. Tagit bort ikonen fr�n huvudmenyn. Kontrollerna f�r vara kvar om �ndras i framtiden
      sensor.usesensor=1;       //anv�nd kompassen
    else
      sensor.usesensor=0;
    angle=read_angle(&sensor);
    sd.alpha=angle;
    sd.gPitch=sensor.gPitch;
    sd.gYaw=sensor.gYaw;        //f�r nu �ven kompassv�rdet fr�n read_angle-funktionen
      if(rev_compass)   //VLG2-14 lagt till reversed compass
      {
        //r�kna om v�rdet +180 mod 360
        float dAZ = sd.gYaw + 180.0;
        if(dAZ >= 360.0)
          dAZ -= 360.0;
        sd.gYaw = dAZ;
      }
    sd.gRoll=sensor.gRoll;

    //R�kna ut h�jden
    if(sd.measmethod==TREE_P_METHOD || sd.measmethod==TRANSPONDER_METHOD)       //3P eller 2P-DME
    {
      sd.height=calc_hgt_3(sd.distance,sd.gamma,sd.beta,angle,AddRefH);
    }
    else if(sd.measmethod==ONE_P_LASER_METHOD)       //1PL
    {
      if(Metric())
        sd.distance=roundi(sd.distance_cm);
      else
        sd.distance=ConvToFeet(sd.distance_cm);

      sd.height=calc_hgt(sd.distance,-sd.alpha,0,AddRefH);
    }
    else
      sd.height=0;   //ska inte kunna intr�ffa

    //Visa resultatet i Oled och display
    redrawDiaHgt(fix_dia_mm > 0? 1:0);    //visa resultatet och sparar v�rdena till sd-structen, olika visning om m�ter diametern eller s�ker ut fast diameter
  }

  cls_Oled();

  return ch;
}

/**********************************
*
* VLG2-5 Kunna v�lja funktioner i huvudmenyn
***********************************/
int fSelectFunctions()
{
  int val[MAX_NUM_FUNCTIONS];

  struct disp_menybox_type tbox={0,0,3,0,12,str_menu_Select_,str_sel_func_names_vl_,val};

  if(!use_dme)
  {
    //initiera om tbox f�r LGEO, inga DME-funktioner
    tbox.len = 12;
    tbox.str = str_sel_func_names_l_;
  }

  for(int j=0; j<tbox.len; j++)
    val[j] = g_Sysdata.functions[j];

  cls();
  CheckBox(&tbox);
  {
    Bell(200);

    for(int j=0; j<tbox.len; j++)
      g_Sysdata.functions[j] = val[j];

    //Spara settings
    SaveSysdata();
  }

  return 1;
}

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/

