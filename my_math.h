#ifndef MATH_H
#define MATH_H
#include <stdint.h>
#define MAXBUFFn		64
#define VINKELBUFFn 	(64-1)
#define UNVALID			(int16_t)32768
#define MA_PI 3.14159265359
#define MAX_DEG_ERR_AD  200//arctan(200/3276 (200mGaus) ca 3 deg
#define MAX_VINKEL_ERR_AD 1000 //arctan(1000/16384) ca 3 deg
int16_t rounddiv(int32_t,int16_t n);
uint32_t urounddiv(uint32_t div,uint32_t n);
int16_t roundi(int32_t div);
int clrbuff(void);
int storebuff(int data,int n);
int medelbuff(int n);
int filter(int data,int maxerror,int n);
int cntbuff(int n);
int readbuff(int n);
int calc_hgt(int tdistance,int talpha,int tbeta, int trp);
int isin(int deg);
int icos(int deg);
#pragma pack(1)
struct angle_type
{
 short int x;
 short int y;
 short int z;
};
struct angle_typef
{
 float x;
 float y;
 float z;
};

struct xyz_type
{
    int distance,pitch,yaw;//distx10, pitchx100, tawx100;
    int metric;         //metric=1
    struct angle_typef Cartesian;//m/f
};

#pragma pack()
#define MAG_DEV 0
#define ACC_DEV 1
//short int cntbuff_3d(int16_t device,int16_t n);
//short int clrbuff_3d(int16_t device);
//short int storebuff_3d(int16_t device,struct angle_type B,int16_t n);
struct angle_type readbuff_3d(short int device,int16_t n);
struct angle_type medelbuff_3d(short int device,short int n);
//int filter_3d(int16_t device,struct angle_type B,int16_t maxerror,int16_t n);
int ATanAlpha(int16_t x,int16_t y,int16_t z);
unsigned short crc_16(unsigned short sum, const unsigned char *p, unsigned long len);
uint16_t crc_16_8005(unsigned short sum, const unsigned char *data, unsigned long size);
//int CalcVectorAngle(struct angle_type angle);
int CalcVectorAngle(struct angle_type angle_a,struct angle_type angle_m);
int CalcVectorAngleFinal(struct angle_type angle_a,struct angle_type angle_m);
void Reset_6DOF_GB_BASIC();
void MultMatris3x3x3x1(float a[3][3],float b[3],float res[3]);
int ConvToFeet(int cm);
int Hdist(int tdistance);
struct angle_typef ConvertToCartesian(float dist,float pitch,float yaw);


void StoreStat(int index,int n,double x);
double StandardDeviation(int index);
int calc_hgt_3(int tdistance,int talpha,int tbeta,int gamma,int trp);
unsigned short slow_crc16(unsigned short sum, const unsigned char *p, unsigned long len);
unsigned short crc16__(unsigned short sum, unsigned char *p, unsigned int len);

int CalcHeightOptional(int d1,int h1,int d2,int h2);
#endif
