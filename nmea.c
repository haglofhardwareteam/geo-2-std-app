#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "nmea.h"

/*
unsigned char strhex(char *p)
{
  int i,ch,hex;
 hex=0;
 for(i=0;i<2;i++)
  {
    ch=p[i];
    hex*=0x10;
    if(ch<='9')
      hex+=(ch-'0');
    else
      if(ch>='A')
        hex+=(ch-'A'+10);

  }
 return hex;
}*/
int CalcNmeaSum(char *str)
 {
   char chk,i;
   chk=0;
   for(i=1;i<strlen(str)&&str[i]!='*';i++)
     chk^=str[i];
   return chk;
 }
/*
int Checksum(char *str)
 {
   char chk,*p;
   chk=CalcNmeaSum(str);
   if((p=strstr(str,"*"))!=NULL)
     if(chk==strhex(p+1))
       return 1;
   return 0;
 }*/
/*
#define MAXPACKETn 128
char *GetField(int arg,char *str,int n)
{
 int i;
 char *p,*p2;
 static char buff[MAXPACKETn+1];
 if(n>MAXPACKETn)
   n=MAXPACKETn;
 memcpy(buff,str,n);
 buff[n]=0;
 p=buff;
 i=0;
 while(arg)
  {
   for(;i<n&&buff[i]&&buff[i]!=',';i++);
    if(buff[i]!=',')
      return NULL;
    p=&buff[++i];
    arg--;
  }
 if((p2=strstr(p,","))!=NULL)
   *p2='\0';
 else if((p2=strstr(p,"*"))!=NULL)
   *p2='\0';
 return p;
}*/
int add_nmea_crc(char *str,int maxn)
{
  int chk;
  char sum[10];
  chk=CalcNmeaSum(str);
  sprintf(sum,"*%02X\r\n",chk);
  if((strlen(str)+strlen(sum))<maxn)
    strcat(str,sum);
  else
    return 0;
  return 1;
}
/*void send_nmea_packet(int channel,char *str)
{
  int chk;
  char sum[10];
  sPrint(SER_GPS,(uint8_t*)str);
  chk=CalcNmeaSum(str);
  sprintf(sum,"*%02X\r\n   ",chk);
  sPrint(SER_GPS,(uint8_t*)sum);
}*/