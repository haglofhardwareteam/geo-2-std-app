

#ifndef LANG_ENG_H
#define LANG_ENG_H


extern const char *str_menu_Select[];

extern  const char *str_Pin[];
extern  char *str_msg_NoDeviceFound[];
extern  char *str_msg_Searching[];
extern  char *str_msg_Disconnect[];
extern  char *str_msg_Connecting[];

//datamem.c
extern  const char *str_ID[];
extern  const char *str_Store[];
extern  const char *str_menu_Mem[];
extern  char *str_msg_Delete[];
extern  const char *str_SendFiles[];
extern  const char *str_PercentUsed[];
extern  char *str_msg_Error[];
extern  char *str_msg_MemFullQ[];
extern  const char *str_DataSaved[];
extern  char *str_msg_DeleteQ[];
extern  char *str_msg_DeleteQQ[];
extern  const char *str_menu_SendFiles[];
extern  const char *str_menu_EnableMem[];
extern  const char *str_menu_ClearMem[];
extern  const char *str_menu_Exit[];
extern const char *str_SaveDataIn[];
extern const char *str_Memory[];

//dme_tcc.c
extern  const char *dmestr3[];
extern  const char *dmestr6[];

//height.c
extern  const char *hgt_str_Deg[];
extern  const char *hgt_str_Grad[];
extern  const char *hgt_str_Percent[];
extern  const char *hgt_str_SD[];
extern  const char *hgt_str_HD[];
extern  const char *hgt_str_HD1[];
extern  const char *hgt_str_HD2[];
extern const char* str_oled_D[];
extern const char* str_oled_Hgt[];
extern  const char *hgt_str_H[];
extern  const char *hgt_str_H1[];
extern  const char *hgt_str_MDist[];
extern  const char *hgt_str_MDistKey[];
extern  const char *hgt_str_DmeKey[];
extern  const char *hgt_str_Height1[];
extern  const char *hgt_str_Height2[];
extern  const char *hgt_str_DeltaH[];
extern  const char *hgt_str_Diff[];
extern  const char *hgt_str_AZ[];

//main.c
extern  const char *str_menu_Height1PL[];
extern  const char *str_menu_Height3P[];
extern  const char *str_menu_HeightDme[];
extern  const char *str_menu_3D[];
extern  const char *str_menu_Compass[];
extern  const char *str_menu_CompassRev[];
extern  const char *str_CompRev[];
extern  const char *str_menu_Angle[];
extern  const char *str_menu_MapTarget[];
extern  const char *str_menu_MapTrail[];
extern  const char *str_menu_LineClear[];
extern  const char *str_menu_DeltaHeight[];
extern  const char *str_menu_Settings[];
extern  const char *str_menu_Usb[];
extern  const char *str_menu_About[];
extern  const char *str_turnoff1[];
extern  const char *str_turnoff2[];
extern  const char *str_turnoff3[];
extern  const char *str_closing[];

//settings.c
extern  const char *str_set_Deg[];
extern  const char *str_set_Grad[];
extern const char *str_set_Percent[];
extern  const char *str_Yes[];
extern  const char *str_No[];

//laser.c
extern const char *str_No_Laser[];
extern const char *hgt_str_Laser[];
extern const char *s200_strAim1[];
extern const char *s200_strAim2[];
extern const char *s200_strAim3[];
extern const char *s200_strAim4[];
extern const char *s200_strAim5[];
extern const char *s200_strAim6[];
extern const char *s200_strNoVal1[];
extern const char *s200_strNoVal2[];
extern const char *s200_strShotAgain1[];
extern const char *s200_strShotAgain2[];
extern const char *str_oled_LaserFirst1[];
extern const char *str_oled_LaserFirst2[];
extern const char *str_oled_LaserFirst3[];
extern const char *str_oled_LaserStrong1[];
extern const char *str_oled_LaserStrong2[];
extern const char *str_oled_LaserStrong3[];
extern const char *str_oled_LaserLast1[];
extern const char *str_oled_LaserLast2[];
extern const char *str_oled_LaserLast3[];
extern const char *str_Deleted[];
extern const char *str_oled_Deleted[];
extern const char *str_oled_Deleted2[];
extern  const char *str_Compass[];

//CalcDangerous.c
extern  const char *str_menu_MinDist[];
extern  const char *str_cd_ErrTrp1[];
extern  const char *str_cd_ErrTrp2[];
extern  const char *str_cd_ErrTrp3[];
extern  const char *str_cd_NotOk[];
extern  const char *str_cd_Ok[];
extern  const char *str_cd_Diff[];
extern  const char *str_cd_Diff2[];
extern  const char *str_cd_Dist[];
extern  const char *str_cd_Pitch[];
extern const char *str_unitM[];
extern const char *str_unitFT[];
extern const char *str_manual_object_hgt1[];    //VLG-30 Fast Dangerous Tree, Manual Line Height
extern const char *str_manual_object_hgt2[];

//datamem.c
extern const char *str_error_filehandle[];
extern const char *str_oled_Err[];
extern const char *str_oled_Err2[];
extern char *str_msg_TryAgainQ[];
extern const char *str_oled_Gps[];

//map3d.c
extern const char *str_menu_MapGps[];
extern const char *str_menu_GpsArea[];
extern const char *str_menu_Method[];
extern const char *str_Continue[];
extern const char *str_With[];
extern char *str_msg_FileOpen[];
extern const char *str_oled_Base1[];
extern const char *str_oled_Base2[];
extern const char *str_Dme[];
extern const char *str_Laser[];
extern const char *str_Baseline[];
extern const char *str_OnTarget[];
extern const char *str_oled_Target1[];
extern const char *str_oled_Target2[];
extern const char *str_StoreTargetQ[];
extern const char *str_Enter[];
extern const char *str_oled_StoreQ1[];
extern const char *str_oled_StoreQ2[];
extern const char *str_oled_StoreQ3[];
extern const char *str_menu_UseLaser[];
extern const char *str_menu_UseGps[];
extern const char *str_menu_UseDme[];
extern const char *str_menu_Finish[];
extern const char *str_menu_NewRef[];
extern const char *str_menu_NextTarget[];
extern const char *str_menu_Return[];
extern const char *str_oled_New[];
extern const char *str_oled_Ref[];
extern const char *str_oled_Move1[];
extern const char *str_oled_Move2[];
extern const char *str_MoveTo[];
extern const char *str_NewRef[];
extern const char *str_ThenEnter[];
extern const char *str_ToStartGps[];
extern const char *str_menu_NewTarget[];
extern const char *str_StoreQ[];
extern const char *str_PressEnter[];
extern const char *str_Target[];
extern const char *str_Seq[];
extern const char *str_Sum[];
extern const char *str_menu_LastTarget[];
extern const char *str_menu_Reference[];
extern const char *str_oled_Q[];
extern const char *str_Length[];
extern const char *str_MagnDecl[];
extern const char *str_TargetHeight[];
extern const char *str_menu_Height_XY[];
extern const char *str_menu_Offset[];
extern const char *str_menu_Laser[];
extern const char *str_menu_Dme[];
extern const char *str_menu_Target[];
extern const char *str_Area[];
extern const char *str_Autosave1[];
extern const char *str_Autosave2[];
extern char *str_msg_Log[];
extern char *str_msg_StartEnter[];
extern char *str_msg_SaveKmlData[];
extern char *str_msg_SaveCsvData[];
extern char *str_msg_SaveJsonData[];
extern char *str_msg_NoGps[];

extern const char *str_delete_all[];
extern const char *str_msg_SureQ[];
extern const char *str_msg_SureQQ[];

//VLG2-2 Diameter.c
extern const char *str_Dia_Diameter[];
extern const char *str_Dia_Height[];
extern const char *str_Fixed_Dia[];
extern const char *str_oled_Dia[];
extern const char *str_oled_H[];
extern const char *str_Dia_Bar1[];
extern const char *str_Dia_Bar2[];
extern const char *str_Dia_Bar3[];

//VLG2-5 Kunna v�lja funktioner i huvudmenyn
extern const char *str_sel_func_names_vl[][12+1];
extern const char *str_sel_func_names_l[][11+1];
extern const char *str_menu_SelectFuncs[];

//VLG2-15 Baf
extern const char *str_menu_BAF[];
extern char *str_msg_BAF[];
extern const char *str_Factor[];
extern const char *str_TotNum[];
extern const char *str_TotBA[];
extern char *str_msg_MaxBAF[];
extern char *str_msg_MaxBAFft[];
extern char *str_msg_MinBAF[];
extern const char *str_SightType[];

//VLG2-16
extern const char *str_menu_Bluetooth[];
extern char *str_msg_UpdateBios[];

extern int main_lang;
#define language main_lang      //ee->lang

//Blue.c
#define str_menu_Select_        str_menu_Select[language]

#define str_Pin_                str_Pin[language]
#define str_msg_NoDeviceFound_  str_msg_NoDeviceFound[language]
#define str_msg_Searching_      str_msg_Searching[language]
#define str_msg_Disconnect_     str_msg_Disconnect[language]
#define str_msg_Connecting_     str_msg_Connecting[language]

//datamem.c
#define str_ID_                 str_ID[language]
#define str_Store_              str_Store[language]
#define str_menu_Mem_           str_menu_Mem[language]
#define str_msg_Delete_         str_msg_Delete[language]
#define str_SendFiles_          str_SendFiles[language]
#define str_PressON_            str_PressON[language]
#define str_Start_              str_Start[language]
#define str_PercentUsed_        str_PercentUsed[language]
#define str_msg_Error_          str_msg_Error[language]
#define str_msg_MemFullQ_       str_msg_MemFullQ[language]
#define str_DataSaved_          str_DataSaved[language]
#define str_msg_DeleteQ_        str_msg_DeleteQ[language]
#define str_msg_DeleteQQ_       str_msg_DeleteQQ[language]
#define str_menu_SendFiles_     str_menu_SendFiles[language]
#define str_menu_EnableMem_     str_menu_EnableMem[language]
#define str_menu_ClearMem_      str_menu_ClearMem[language]
#define str_menu_Exit_          str_menu_Exit[language]
#define str_SaveDataIn_         str_SaveDataIn[language]
#define str_Memory_             str_Memory[language]

//dme_tcc.c
#define dmestr3_                dmestr3[language]
#define dmestr6_                dmestr6[language]

//height.c
#define hgt_str_Deg_            hgt_str_Deg[language]
#define hgt_str_Grad_           hgt_str_Grad[language]
#define hgt_str_Percent_        hgt_str_Percent[language]
#define hgt_str_SD_             hgt_str_SD[language]
#define hgt_str_HD_             hgt_str_HD[language]
#define hgt_str_H_              hgt_str_H[language]
#define str_oled_D_             str_oled_D[language]
#define hgt_str_H1_             hgt_str_H1[language]
#define str_oled_Hgt_           str_oled_Hgt[language]
#define hgt_str_MDist_          hgt_str_MDist[language]
#define hgt_str_MDistKey_       hgt_str_MDistKey[language]
#define hgt_str_DmeKey_         hgt_str_DmeKey[language]
#define hgt_str_HD1_            hgt_str_HD1[language]
#define hgt_str_HD2_            hgt_str_HD2[language]
#define hgt_str_Height1_        hgt_str_Height1[language]
#define hgt_str_Height2_        hgt_str_Height2[language]
#define hgt_str_DeltaH_         hgt_str_DeltaH[language]
#define hgt_str_Diff_           hgt_str_Diff[language]
#define str_Deleted_            str_Deleted[language]
#define hgt_str_AZ_             hgt_str_AZ[language]

//main.c
#define str_menu_Height1PL_     str_menu_Height1PL[language]
#define str_menu_Height3P_     str_menu_Height3P[language]
#define str_menu_HeightDme_     str_menu_HeightDme[language]
#define str_menu_3D_            str_menu_3D[language]
#define str_menu_Compass_         str_menu_Compass[language]
#define str_menu_CompassRev_         str_menu_CompassRev[language]
#define str_CompRev_         str_CompRev[language]
#define str_menu_Angle_         str_menu_Angle[language]
#define str_menu_MapTarget_         str_menu_MapTarget[language]
#define str_menu_MapTrail_         str_menu_MapTrail[language]
#define str_menu_LineClear_         str_menu_LineClear[language]
#define str_menu_DeltaHeight_         str_menu_DeltaHeight[language]
#define str_menu_Settings_      str_menu_Settings[language]
#define str_menu_Usb_         str_menu_Usb[language]
#define str_menu_About_         str_menu_About[language]

#define str_turnoff1_           str_turnoff1[language]
#define str_turnoff2_           str_turnoff2[language]
#define str_turnoff3_           str_turnoff3[language]
#define str_closing_            str_closing[language]

//settings.c
#define str_set_Deg_            str_set_Deg[language]
#define str_set_Grad_           str_set_Grad[language]
#define str_set_Percent_        str_set_Percent[language]
#define str_Yes_                str_Yes[language]
#define str_No_                 str_No[language]

//language.c
#define str_lang_                str_lang[language]

//laser.c
#define str_No_Laser_           str_No_Laser[language]
#define hgt_str_Laser_          hgt_str_Laser[language]
#define s200_strAim1_           s200_strAim1[language]
#define s200_strAim2_           s200_strAim2[language]
#define  s200_strAim3_          s200_strAim3[language]
#define  s200_strAim4_          s200_strAim4[language]
#define  s200_strAim5_          s200_strAim5[language]
#define  s200_strAim6_          s200_strAim6[language]
#define s200_strNoVal1_         s200_strNoVal1[language]
#define s200_strNoVal2_         s200_strNoVal2[language]
#define  s200_strShotAgain1_    s200_strShotAgain1[language]
#define s200_strShotAgain2_     s200_strShotAgain2[language]
#define str_oled_LaserFirst1_   str_oled_LaserFirst1[language]
#define str_oled_LaserFirst2_   str_oled_LaserFirst2[language]
#define str_oled_LaserFirst3_   str_oled_LaserFirst3[language]
#define str_oled_LaserStrong1_  str_oled_LaserStrong1[language]
#define str_oled_LaserStrong2_  str_oled_LaserStrong2[language]
#define str_oled_LaserStrong3_  str_oled_LaserStrong3[language]
#define str_oled_LaserLast1_    str_oled_LaserLast1[language]
#define str_oled_LaserLast2_    str_oled_LaserLast2[language]
#define str_oled_LaserLast3_    str_oled_LaserLast3[language]
#define str_Deleted_            str_Deleted[language]
#define str_oled_Deleted_       str_oled_Deleted[language]
#define str_oled_Deleted2_      str_oled_Deleted2[language]
#define str_Compass_            str_Compass[language]

//CalcDangerous.c
#define str_menu_MinDist_       str_menu_MinDist[language]
#define str_cd_ErrTrp1_         str_cd_ErrTrp1[language]
#define str_cd_ErrTrp2_         str_cd_ErrTrp2[language]
#define str_cd_ErrTrp3_         str_cd_ErrTrp3[language]
#define str_cd_NotOk_           str_cd_NotOk[language]
#define str_cd_Ok_              str_cd_Ok[language]
#define str_cd_Diff_            str_cd_Diff[language]
#define str_cd_Diff2_            str_cd_Diff2[language]
#define str_cd_Dist_            str_cd_Dist[language]
#define str_cd_Pitch_           str_cd_Pitch[language]
#define str_unitM_              str_unitM[language]
#define str_unitFT_             str_unitFT[language]
#define str_manual_object_hgt1_ str_manual_object_hgt1[language]
#define str_manual_object_hgt2_ str_manual_object_hgt2[language]

//datamem.c
#define str_error_filehandle_   str_error_filehandle[language]
#define str_oled_Err_           str_oled_Err[language]
#define str_oled_Err2_          str_oled_Err2[language]
#define str_msg_TryAgainQ_      str_msg_TryAgainQ[language]
#define str_oled_Gps_           str_oled_Gps[language]

//map3d.c
#define str_menu_MapGps_        str_menu_MapGps[language]
#define str_menu_GpsArea_       str_menu_GpsArea[language]
#define str_menu_Method_        str_menu_Method[language]
#define str_Continue_           str_Continue[language]
#define str_With_               str_With[language]
#define str_msg_FileOpen_       str_msg_FileOpen[language]
#define str_oled_Base1_         str_oled_Base1[language]
#define str_oled_Base2_         str_oled_Base2[language]
#define str_Dme_                str_Dme[language]
#define str_Laser_              str_Laser[language]
#define str_Baseline_           str_Baseline[language]
#define str_OnTarget_           str_OnTarget[language]
#define str_oled_Target1_       str_oled_Target1[language]
#define str_oled_Target2_       str_oled_Target2[language]
#define str_StoreTargetQ_       str_StoreTargetQ[language]
#define str_Enter_              str_Enter[language]
#define str_oled_StoreQ1_       str_oled_StoreQ1[language]
#define str_oled_StoreQ2_       str_oled_StoreQ2[language]
#define str_oled_StoreQ3_       str_oled_StoreQ3[language]
#define str_menu_UseLaser_      str_menu_UseLaser[language]
#define str_menu_UseGps_        str_menu_UseGps[language]
#define str_menu_UseDme_        str_menu_UseDme[language]
#define str_menu_Finish_        str_menu_Finish[language]
#define str_menu_NewRef_        str_menu_NewRef[language]
#define str_menu_NextTarget_    str_menu_NextTarget[language]
#define str_menu_Return_        str_menu_Return[language]
#define str_oled_New_           str_oled_New[language]
#define str_oled_Ref_           str_oled_Ref[language]
#define str_oled_Move1_         str_oled_Move1[language]
#define str_oled_Move2_         str_oled_Move2[language]
#define str_MoveTo_             str_MoveTo[language]
#define str_NewRef_             str_NewRef[language]
#define str_ThenEnter_          str_ThenEnter[language]
#define str_ToStartGps_         str_ToStartGps[language]
#define str_menu_NewTarget_     str_menu_NewTarget[language]
#define str_StoreQ_             str_StoreQ[language]
#define str_PressEnter_         str_PressEnter[language]
#define str_Target_             str_Target[language]
#define str_Seq_                str_Seq[language]
#define str_Sum_                str_Sum[language]
#define str_menu_LastTarget_    str_menu_LastTarget[language]
#define str_menu_Reference_     str_menu_Reference[language]
#define str_oled_Q_             str_oled_Q[language]
#define str_Length_             str_Length[language]
#define str_MagnDecl_           str_MagnDecl[language]
#define str_TargetHeight_       str_TargetHeight[language]
#define str_menu_Height_XY_     str_menu_Height_XY[language]
#define str_menu_Offset_        str_menu_Offset[language]
#define str_menu_Laser_         str_menu_Laser[language]
#define str_menu_Dme_           str_menu_Dme[language]
#define str_menu_Target_        str_menu_Target[language]
#define str_Area_               str_Area[language]
#define str_Autosave1_          str_Autosave1[language]
#define str_Autosave2_          str_Autosave2[language]
#define str_msg_Log_            str_msg_Log[language]
#define str_msg_StartEnter_     str_msg_StartEnter[language]
#define str_msg_SaveKmlData_    str_msg_SaveKmlData[language]
#define str_msg_SaveCsvData_    str_msg_SaveCsvData[language]
#define str_msg_SaveJsonData_    str_msg_SaveJsonData[language]
#define str_msg_NoGps_          str_msg_NoGps[language]

#define str_delete_all_         str_delete_all[language]
#define str_msg_SureQ_          str_msg_SureQ[language]
#define str_msg_SureQQ_         str_msg_SureQQ[language]

//VLG2-2 Diameter.c
#define str_Dia_Diameter_       str_Dia_Diameter[language]
#define str_Dia_Height_         str_Dia_Height[language]
#define str_Fixed_Dia_          str_Fixed_Dia[language]
#define str_oled_Dia_           str_oled_Dia[language]
#define str_oled_H_             str_oled_H[language]
#define str_Dia_Bar1_           str_Dia_Bar1[language]
#define str_Dia_Bar2_           str_Dia_Bar2[language]
#define str_Dia_Bar3_           str_Dia_Bar3[language]

//VLG2-5 Kunna v�lja funktioner i huvudmenyn
#define str_sel_func_names_vl_  str_sel_func_names_vl[language]
#define str_sel_func_names_l_   str_sel_func_names_l[language]
#define str_menu_SelectFuncs_   str_menu_SelectFuncs[language]

//VLG2-15 Baf
#define str_menu_BAF_   str_menu_BAF[language]
#define str_msg_BAF_   str_msg_BAF[language]
#define str_Factor_   str_Factor[language]
#define str_TotBA_   str_TotBA[language]
#define str_TotNum_   str_TotNum[language]
#define str_msg_MaxBAF_   str_msg_MaxBAF[language]
#define str_msg_MaxBAFft_   str_msg_MaxBAFft[language]
#define str_msg_MinBAF_   str_msg_MinBAF[language]
#define str_SightType_   str_SightType[language]

//VLG2-16 Obex
#define str_menu_Bluetooth_   str_menu_Bluetooth[language]
#define str_msg_UpdateBios_     str_msg_UpdateBios[language]

#endif