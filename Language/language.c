// '�' � � � ���U
#include "language.h"

// - g�tt igenom: cze, deu, eng, esp, por, ita, fra, swe

//Blue.c
const char *str_menu_Select[] =
{
  "SELECT ",//eng
  "VALINTA",//fin
  "CHOIX  ",//franc
  "SELECIONAR",//Port
  "�����  ",//Rus
  "SELECCIONAR",//Esp
  "V�LJ   ",//Swed
  "W�HLEN ",//Deu
  "VYBER  ",//Ceska
  "SELECT ",//Dan
  "SELECT ",//Est
  "SELECT ",//Latv
  "SELECT ",//Liet
  "SELECT ",//Nor
  "SELECT ",//Pol
  "SELEZIONARE",//Ita
  "SELECT "//Jpn
};

//datamem.c
const char *str_ID[] =
{
  "ID",//eng
  "ID",//fin
  "ID",//franc
  "ID",//Port
  "ID",//Rus
  "ID",//Esp
  "ID",//Swed
  "ID",//Deu
  "ID",//Ceska
  "ID",//Dan
  "ID",//Est
  "ID",//Latv
  "ID",//Liet
  "ID",//Nor
  "ID",//Pol
  "ID",//Ita
  "ID"//Jpn
};

const char *str_Store[] =
{
  "STORE",//eng
  "STORE",//fin
  "MEM.",//franc
  "SALVAR",//Port
  "STORE",//Rus
  "GUARDAR",//Esp
  "SPARA",//Swed
  "SPEICHER",//Deu
  "ULOZ",//Ceska
  "STORE",//Dan
  "STORE",//Est
  "STORE",//Latv
  "STORE",//Liet
  "STORE",//Nor
  "STORE",//Pol
  "SALVA",//Ita
  "STORE"//Jpn
};

char *str_msg_Delete[] =
{
  "DELETE",//eng
  "DELETE",//fin
  "EFFACE",//franc
  "APAGAR",//Port
  "��������",//Rus
  "BORRAR",//Esp
  "RADERA",//Swed
  "L�SCHEN",//Deu
  "SMAZAT",//Ceska
  "DELETE",//Dan
  "DELETE",//Est
  "DELETE",//Latv
  "DELETE",//Liet
  "DELETE",//Nor
  "DELETE",//Pol
  "CANCELLARE",//Ita
  "DELETE"//Jpn
};

const char *str_PercentUsed[] =
{
  "% USED",//eng
  "% USED",//fin
  "% UTIL",//franc
  "% USADO",//Port
  "% USED",//Rus
  "% USADO",//Esp
  "% ANV�NT",//Swed
  "% BENUTZT",//Deu
  "% VYUZITO",//Ceska
  "% USED",//Dan
  "% USED",//Est
  "% USED",//Latv
  "% USED",//Liet
  "% USED",//Nor
  "% USED",//Pol
  "% USATO",//Ita
  "% USED"//Jpn
};

char *str_msg_Error[] =
{
  "ERROR",//eng
  "ERROR",//fin
  "ERROR",//franc
  "ERRO",//Port
  "ERROR",//Rus
  "ERROR",//Esp
  "FEL",//Swed
  "FEHLER",//Deu
  "CHYBA",//Ceska
  "ERROR",//Dan
  "ERROR",//Est
  "ERROR",//Latv
  "ERROR",//Liet
  "ERROR",//Nor
  "ERROR",//Pol
  "ERRORE",//Ita
  "ERROR"//Jpn
};

char *str_msg_MemFullQ[] =
{
  "MEM FULL?",//eng
  "MEM FULL?",//fin
  "MEMPLEINE?",//franc
  "MEMORIA\nCHEIA?",//Port
  "MEM FULL?",//Rus
  "MEMORIA\nLLENA?",//Esp
  "MINNE FULLT?",//Swed
  "SPEICHER\nVOLL?",//Deu
  "PAMET\nPLNA?",//Ceska
  "MEM FULL?",//Dan
  "MEM FULL?",//Est
  "MEM FULL?",//Latv
  "MEM FULL?",//Liet
  "MEM FULL?",//Nor
  "MEM FULL?",//Pol
  "MEM PIENA?",//Ita
  "MEM FULL?"//Jpn
};

const char *str_DataSaved[] =
{
  "DATA SAVED",//eng
  "DATA SAVED",//fin
  "DATASAUV",//franc
  "DADO SALVO",//Port
  "DATA SAVED",//Rus
  "DATOS GUARD.",//Esp
  "DATA SPARAT",//Swed
  "GESICHERT",//Deu
  "DATA ULOZENA",//Ceska
  "DATA SAVED",//Dan
  "DATA SAVED",//Est
  "DATA SAVED",//Latv
  "DATA SAVED",//Liet
  "DATA SAVED",//Nor
  "DATA SAVED",//Pol
  "DATI SALVATI",//Ita
  "DATA SAVED"//Jpn
};

char *str_msg_DeleteQ[] =
{
  "DELETE?",//eng
  "DELETE?",//fin
  "EFFACE?",//franc
  "APAGAR?",//Port
  "DELETE?",//Rus
  "BORRAR?",//Esp
  "RADERA?",//Swed
  "L�SCHEN?",//Deu
  "SMAZAT?",//Ceska
  "DELETE?",//Dan
  "DELETE?",//Est
  "DELETE?",//Latv
  "DELETE?",//Liet
  "DELETE?",//Nor
  "DELETE?",//Pol
  "CANCELLARE?",//Ita
  "DELETE?"//Jpn
};

char *str_msg_DeleteQQ[] =
{
  "DELETE??",//eng
  "DELETE??",//fin
  "EFFACE??",//franc
  "APAGAR??",//Port
  "DELETE??",//Rus
  "BORRAR??",//Esp
  "RADERA??",//Swed
  "L�SCHEN??",//Deu
  "SMAZAT??",//Ceska
  "DELETE??",//Dan
  "DELETE??",//Est
  "DELETE??",//Latv
  "DELETE??",//Liet
  "DELETE??",//Nor
  "DELETE??",//Pol
  "CANCELLARE??",//Ita
  "DELETE??"//Jpn
};

const char *str_menu_SendFiles[] =
{
  "SEND FILE",//eng
  "SEND FILE",//fin
  "ENV. FICH.",//franc
  "ENVIAR DADO",//Port
  "����.����",//Rus
  "ENVIAR DATO",//Esp
  "S�ND FIL",//Swed
  "DAT. SENDEN",//Deu
  "POSLI SOUB.",//Ceska
  "SEND FILE",//Dan
  "SEND FILE",//Est
  "SEND FILE",//Latv
  "SEND FILE",//Liet
  "SEND FILE",//Nor
  "SEND FILE",//Pol
  "INVIARE  ",//Ita
  "SEND FILE"//Jpn
};

const char *str_menu_EnableMem[] =
{
  "ENABLE MEM ",//eng
  "AKTIVOI    ",//fin
  "ACTIVER    ",//franc
  "HABILITAR  ",//Port
  "ENABLE MEM ",//Rus
  "ACTIVAR MEM",//Esp
  "AKTIVERA   ",//Swed
  "AKTIVIEREN ",//Deu
  "AKTIVOVAT  ",//Ceska
  "ENABLE MEM ",//Dan
  "ENABLE MEM ",//Est
  "ENABLE MEM ",//Latv
  "ENABLE MEM ",//Liet
  "ENABLE MEM ",//Nor
  "ENABLE MEM ",//Pol
  "ATTIVARE   ",//Ita
  "ENABLE MEM ",//Jpn
};

const char *str_menu_ClearMem[] =
{
  "CLEAR MEM. ",//eng
  "TYHJE.MUIST",//fin
  "EFFACE     ",//franc
  "APAGAR MEM.",//Port
  "����.������",//Rus
  "VACIAR MEM.",//Esp
  "RADERA     ",//Swed
  "L�SCHEN    ",//Deu
  "SMAZ PAMET ",//Ceska
  "CLEAR MEM. ",//Dan
  "CLEAR MEM. ",//Est
  "CLEAR MEM. ",//Latv
  "CLEAR MEM. ",//Liet
  "CLEAR MEM. ",//Nor
  "CLEAR MEM. ",//Pol
  "CANCELLARE ",//Ita
  "CLEAR MEM. ",//Jpn
};

const char *str_menu_Exit[] =
{
  "EXIT      ",//eng
  "EXIT      ",//fin
  "EXIT      ",//franc
  "SAIR      ",//Port
  "�����     ",//Rus
  "EXIT      ",//Esp
  "EXIT      ",//Swed
  "EXIT      ",//Deu
  "EXIT      ",//Ceska
  "EXIT      ",//Dan
  "EXIT      ",//Est
  "EXIT      ",//Latv
  "EXIT      ",//Liet
  "EXIT      ",//Nor
  "EXIT      ",//Pol
  "EXIT      ",//Ita
  "EXIT      "//Jpn
};

const char *str_SaveDataIn[] =
{
  "SAVE DATA IN",//eng
  "TALLENNA",//fin
  "SAUV. EN",//franc
  "SALVAR EM",//Port
  "SAVE DATA IN",//Rus
  "GUARDAR EN",//Esp
  "SPARA DATA I",//Swed
  "SPEICHERN IN",//Deu
  "ULOZ DATA DO",//Ceska
  "SAVE DATA IN",//Dan
  "SAVE DATA IN",//Est
  "SAVE DATA IN",//Latv
  "SAVE DATA IN",//Liet
  "SAVE DATA IN",//Nor
  "SAVE DATA IN",//Pol
  "SALVA IN",//Ita
  "SAVE DATA IN",//Jpn
};

const char *str_Memory[] =
{
  "MEMORY",//eng
  "MUISTIA",//fin
  "M�MOIRE",//franc
  "MEMORIA",//Port
  "MEMORY",//Rus
  "MEMORIA",//Esp
  "MINNE",//Swed
  "GER�T",//Deu
  "PAMET",//Ceska
  "MEMORY",//Dan
  "MEMORY",//Est
  "MEMORY",//Latv
  "MEMORY",//Liet
  "MEMORY",//Nor
  "MEMORY",//Pol
  "MEMORIA",//Ita
  "MEMORY",//Jpn
};

//dme_tcc.c
const char *dmestr3[] =
{
  "DME",//eng
  "DME",//fin
  "DME",//franc
  "DME",//Port
  "DME",//Rus
  "DME",//Esp
  "DME",//Swed
  "DME",//Deu
  "DME",//Ceska
  "DME",//Dan
  "DME",//Est
  "DME",//Latv
  "DME",//Liet
  "DME",//Nor
  "DME",//Pol
  "DME",//Ita
  "DME"//Jpn
};

const char *dmestr6[] =
{
  "HDIST",//eng
  "HDIST",//fin
  "HDIST",//franc
  "DISTH",//Port
  "HDIST",//Rus
  "HDIST",//Esp
  "HDIST",//Swed
  "HDIST",//Deu
  "HDIST",//Ceska
  "HDIST",//Dan
  "HDIST",//Est
  "HDIST",//Latv
  "HDIST",//Liet
  "HDIST",//Nor
  "HDIST",//Pol
  "HDIST",//Ita
  "HDIST"//Jpn
};

//height.c
const char *hgt_str_Deg[] =
{
  "DEG",//eng
  "DEG",//fin
  "DEG",//franc
  "GRAU",//Port
  "DEG",//Rus
  "DEG",//Esp
  "DEG",//Swed
  "GRAD",//Deu
  "DEG",//Ceska
  "DEG",//Dan
  "DEG",//Est
  "DEG",//Latv
  "DEG",//Liet
  "DEG",//Nor
  "DEG",//Pol
  "DEG",//Ita
  "DEG"//Jpn
};

const char *hgt_str_Grad[] =
{
  "GRAD",//eng
  "GRAD",//fin
  "GRAD",//franc
  "GRAD",//Port
  "GRAD",//Rus
  "GRAD",//Esp
  "GRAD",//Swed
  "GON",//Deu
  "GRAD",//Ceska
  "GRAD",//Dan
  "GRAD",//Est
  "GRAD",//Latv
  "GRAD",//Liet
  "GRAD",//Nor
  "GRAD",//Pol
  "GRAD",//Ita
  "GRAD"//Jpn
};

const char *hgt_str_Percent[] =
{
  "%",//eng
  "%",//fin
  "%",//franc
  "%",//Port
  "%",//Rus
  "%",//Esp
  "%",//Swed
  "%",//Deu
  "%",//Ceska
  "%",//Dan
  "%",//Est
  "%",//Latv
  "%",//Liet
  "%",//Nor
  "%",//Pol
  "%",//Ita
  "%"//Jpn
};

const char *hgt_str_SD[] =
{
  "SD",//eng
  "SD",//fin
  "SD",//franc
  "SD",//Port
  "SD",//Rus
  "PD",//Esp
  "SD",//Swed
  "SD",//Deu
  "SD",//Ceska
  "SD",//Dan
  "SD",//Est
  "SD",//Latv
  "SD",//Liet
  "SD",//Nor
  "SD",//Pol
  "DO",//Ita
  "SD"//Jpn
};

const char *hgt_str_HD[] =
{
  "HD",//eng
  "HD",//fin
  "HD",//franc
  "DH",//Port
  "HD",//Rus
  "HD",//Esp
  "HD",//Swed
  "HD",//Deu
  "HD",//Ceska
  "HD",//Dan
  "HD",//Est
  "HD",//Latv
  "HD",//Liet
  "HD",//Nor
  "HD",//Pol
  "DH",//Ita
  "HD"//Jpn
};

const char *hgt_str_HD1[] =
{
  "HD1",//eng
  "HD1",//fin
  "HD1",//franc
  "DH1",//Port
  "HD1",//Rus
  "HD1",//Esp
  "HD1",//Swed
  "HD1",//Deu
  "HD1",//Ceska
  "HD1",//Dan
  "HD1",//Est
  "HD1",//Latv
  "HD1",//Liet
  "HD1",//Nor
  "HD1",//Pol
  "DH1",//Ita
  "HD1"//Jpn
};

const char *hgt_str_HD2[] =
{
  "HD2",//eng
  "HD2",//fin
  "HD2",//franc
  "DH2",//Port
  "HD2",//Rus
  "HD2",//Esp
  "HD2",//Swed
  "HD2",//Deu
  "HD2",//Ceska
  "HD2",//Dan
  "HD2",//Est
  "HD2",//Latv
  "HD2",//Liet
  "HD2",//Nor
  "HD2",//Pol
  "DH2",//Ita
  "HD2"//Jpn
};

const char *hgt_str_H[] =
{
  "H",//eng
  "H",//fin
  "H",//franc
  "ALT",//Port
  "H",//Rus
  "ALT",//Esp
  "H",//Swed
  "H",//Deu
  "V",//Ceska
  "H",//Dan
  "H",//Est
  "H",//Latv
  "H",//Liet
  "H",//Nor
  "H",//Pol
  "ALT",//Ita
  "H"//Jpn
};

const char *hgt_str_Height1[] =
{
  "HEIGHT1",//eng
  "HEIGHT1",//fin
  "HAUT1",//franc
  "ALTURA1",//Port
  "HEIGHT1",//Rus
  "ALTURA1",//Esp
  "H�JD1",//Swed
  "H�HE1",//Deu
  "VYSKA1",//Ceska
  "HEIGHT1",//Dan
  "HEIGHT1",//Est
  "HEIGHT1",//Latv
  "HEIGHT1",//Liet
  "HEIGHT1",//Nor
  "HEIGHT1",//Pol
  "ALTEZZA1",//Ita
  "HEIGHT1"//Jpn
};

const char *hgt_str_Height2[] =
{
  "HEIGHT2",//eng
  "HEIGHT2",//fin
  "HAUT2",//franc
  "ALTURA2",//Port
  "HEIGHT2",//Rus
  "ALTURA2",//Esp
  "H�JD2",//Swed
  "H�HE2",//Deu
  "VYSKA2",//Ceska
  "HEIGHT2",//Dan
  "HEIGHT2",//Est
  "HEIGHT2",//Latv
  "HEIGHT2",//Liet
  "HEIGHT2",//Nor
  "HEIGHT2",//Pol
  "ALTEZZA2",//Ita
  "HEIGHT2"//Jpn
};

const char *hgt_str_DeltaH[] =
{
  "DELTAH",//eng
  "DELTAH",//fin
  "DELTAH",//franc
  "FLECHA",//Port
  "DELTAH",//Rus
  "DELTA",//Esp
  "DELTAH",//Swed
  "DELTAH",//Deu
  "PRUVES",//Ceska
  "DELTAH",//Dan
  "DELTAH",//Est
  "DELTAH",//Latv
  "DELTAH",//Liet
  "DELTAH",//Nor
  "DELTAH",//Pol
  "DELTA",//Ita
  "DELTAH"//Jpn
};

const char *hgt_str_Diff[] =
{
  "DIFF",//eng
  "DIFF",//fin
  "DIFF",//franc
  "DIF",//Port
  "DIFF",//Rus
  "DIF",//Esp
  "DIFF",//Swed
  "DIFF",//Deu
  "DIF",//Ceska
  "DIFF",//Dan
  "DIFF",//Est
  "DIFF",//Latv
  "DIFF",//Liet
  "DIFF",//Nor
  "DIFF",//Pol
  "DIF",//Ita
  "DIFF"//Jpn
};

const char *str_oled_D[] =
{
  "D",//eng
  "D",//fin
  "D",//franc
  "D",//Port
  "D",//Rus
  "D",//Esp
  "D",//Swed
  "D",//Deu
  "D",//Ceska
  "D",//Dan
  "D",//Est
  "D",//Latv
  "D",//Liet
  "D",//Nor
  "D",//Pol
  "D",//Ita
  "D"//Jpn
};

const char *str_oled_Hgt[] =
{
  "HGT",//eng
  "HGT",//fin
  "HT",//franc
  "ALT",//Port
  "HGT",//Rus
  "ALT",//Esp
  "HJD",//Swed
  "H�H",//Deu
  "VYS",//Ceska
  "HGT",//Dan
  "HGT",//Est
  "HGT",//Latv
  "HGT",//Liet
  "HGT",//Nor
  "HGT",//Pol
  "ALT",//Ita
  "HGT"//Jpn
};

const char *hgt_str_H1[] =
{
  "H1",//eng
  "H1",//fin
  "H1",//franc
  "ALT1",//Port
  "H1",//Rus
  "ALT1",//Esp
  "H1",//Swed
  "H1",//Deu
  "V1",//Ceska
  "H1",//Dan
  "H1",//Est
  "H1",//Latv
  "H1",//Liet
  "H1",//Nor
  "H1",//Pol
  "ALT1",//Ita
  "H1"//Jpn
};

const char *hgt_str_MDist[] =
{
  "M.DIST",//eng
  "M.DIST",//fin
  "M.DIST",//franc
  "DIST.M",//Port
  "M.DIST",//Rus
  "DIST.M",//Esp
  "M.AVST",//Swed
  "M.DIST",//Deu
  "M.DIST",//Ceska
  "M.DIST",//Dan
  "M.DIST",//Est
  "M.DIST",//Latv
  "M.DIST",//Liet
  "M.DIST",//Nor
  "M.DIST",//Pol
  "DIST.M",//Ita
  "M.DIST"//Jpn
};

const char *hgt_str_MDistKey[] =
{
  "MDIST (SEND)",//eng
  "MDIST (SEND)",//fin
  "MDIST (SEND)",//franc
  "DISTM (SEND)",//Port
  "MDIST (SEND)",//Rus
  "DISTM (SEND)",//Esp
  "M.AVST(SEND)",//Swed
  "MDIST (SEND)",//Deu
  "MDIST (SEND)",//Ceska
  "MDIST (SEND)",//Dan
  "MDIST (SEND)",//Est
  "MDIST (SEND)",//Latv
  "MDIST (SEND)",//Liet
  "MDIST (SEND)",//Nor
  "MDIST (SEND)",//Pol
  "DISTM (SEND)",//Ita
  "MDIST (SEND)"//Jpn
};

const char *hgt_str_DmeKey[] =
{
  "DME     (ON)",//eng
  "DME     (ON)",//fin
  "DME     (ON)",//franc
  "DME     (ON)",//Port
  "DME     (ON)",//Rus
  "DME     (ON)",//Esp
  "DME     (ON)",//Swed
  "DME     (ON)",//Deu
  "DME     (ON)",//Ceska
  "DME     (ON)",//Dan
  "DME     (ON)",//Est
  "DME     (ON)",//Latv
  "DME     (ON)",//Liet
  "DME     (ON)",//Nor
  "DME     (ON)",//Pol
  "DME     (ON)",//Ita
  "DME     (ON)"//Jpn
};

const char *hgt_str_AZ[] =
{
  "AZ",//eng
  "AZ",//fin
  "AZ",//franc
  "AZ",//Port
  "AZ",//Rus
  "AZ",//Esp
  "AZ",//Swed
  "AZ",//Deu
  "AZ",//Ceska
  "AZ",//Dan
  "AZ",//Est
  "AZ",//Latv
  "AZ",//Liet
  "AZ",//Nor
  "AZ",//Pol
  "AZ",//Ita
  "AZ"//Jpn
};

//main.c
const char *str_menu_Height1PL[] =
{
  "HEIGHT 1PL",//eng
  "HEIGHT 1PL",//fin
  "HAUT 1PL  ",//franc
  "ALTURA 1PL",//Port
  "HEIGHT 1PL",//Rus
  "ALTURA 1PL",//Esp
  "H�JD 1PL  ",//Swed
  "H�HE 1PL  ",//Deu
  "VYSKA 1PL ",//Ceska
  "HEIGHT 1PL",//Dan
  "HEIGHT 1PL",//Est
  "HEIGHT 1PL",//Latv
  "HEIGHT 1PL",//Liet
  "HEIGHT 1PL",//Nor
  "HEIGHT 1PL",//Pol
  "ALT. 1PL  ",//Ita
  "HEIGHT 1PL"//Jpn
};

const char *str_menu_Height3P[] =
{
  "HEIGHT 3P ",//eng
  "HEIGHT 3P ",//fin
  "HAUT 3P   ",//franc
  "ALTURA 3P ",//Port
  "HEIGHT 3P ",//Rus
  "ALTURA 3P ",//Esp
  "H�JD 3P   ",//Swed
  "H�HE 3P   ",//Deu
  "VYSKA 3P  ",//Ceska
  "HEIGHT 3P ",//Dan
  "HEIGHT 3P ",//Est
  "HEIGHT 3P ",//Latv
  "HEIGHT 3P ",//Liet
  "HEIGHT 3P ",//Nor
  "HEIGHT 3P ",//Pol
  "ALTEZZA 3P",//Ita
  "HEIGHT 3P "//Jpn
};

const char *str_menu_HeightDme[] =
{
  "HEIGHT DME",//eng
  "HEIGHT DME",//fin
  "HAUT DME  ",//franc
  "ALTURA DME",//Port
  "HEIGHT DME",//Rus
  "ALTURA DME",//Esp
  "H�JD DME  ",//Swed
  "H�HE DME  ",//Deu
  "VYSKA DME ",//Ceska
  "HEIGHT DME",//Dan
  "HEIGHT DME",//Est
  "HEIGHT DME",//Latv
  "HEIGHT DME",//Liet
  "HEIGHT DME",//Nor
  "HEIGHT DME",//Pol
  "ALT. DME  ",//Ita
  "HEIGHT DME"//Jpn
};

const char *str_menu_3D[] =
{
  "3D VECTOR ",//eng
  "3D VECTOR ",//fin
  "3D VECTEUR",//franc
  "VETOR 3D  ",//Port
  "3D VECTOR ",//Rus
  "VECTOR 3D ",//Esp
  "3D VEKTOR ",//Swed
  "3D VEKTOR ",//Deu
  "3D VEKTOR ",//Ceska
  "3D VECTOR ",//Dan
  "3D VECTOR ",//Est
  "3D VECTOR ",//Latv
  "3D VECTOR ",//Liet
  "3D VECTOR ",//Nor
  "3D VECTOR ",//Pol
  "VETTORE 3D",//Ita
  "3D VECTOR "//Jpn
};

const char *str_menu_Compass[] =
{
  "COMPASS   ",//eng
  "COMPASS   ",//fin
  "BOUSSOLE  ",//franc
  "BUSSOLA   ",//Port
  "COMPASS   ",//Rus
  "BRUJULA   ",//Esp
  "KOMPASS   ",//Swed
  "KOMPASS   ",//Deu
  "KOMPAS    ",//Ceska
  "COMPASS   ",//Dan
  "COMPASS   ",//Est
  "COMPASS   ",//Latv
  "COMPASS   ",//Liet
  "COMPASS   ",//Nor
  "COMPASS   ",//Pol
  "BUSSOLA   ",//Ita
  "COMPASS   "//Jpn
};

const char *str_menu_CompassRev[] =     //VLG2-14 Compass Reversed, text i huvudmenyn
{
  "COMP. REV.",//eng
  "COMP. REV.",//fin
  "BOUSS. INV",//franc
  "BUSS. INV.",//Port
  "COMP. REV.",//Rus
  "BRUJ. INV.",//Esp
  "KOMP. REV.",//Swed
  "KOMP. INV.",//Deu
  "KOMP. OBR.",//Ceska
  "COMP. REV.",//Dan
  "COMP. REV.",//Est
  "COMP. REV.",//Latv
  "COMP. REV.",//Liet
  "COMP. REV.",//Nor
  "COMP. REV.",//Pol
  "BUSS. INV.",//Ita
  "COMP. REV "//Jpn
};

const char *str_CompRev[] =
{
"REV",//eng
"REV",//fin
"INV",//franc
"INV",//Port
"REV",//Rus
"INV",//Esp
"REV",//Swed
"INV",//Deu
"OBR",//Ceska
"REV",//Dan
"REV",//Est
"REV",//Latv
"REV",//Liet
"REV",//Nor
"REV",//Pol
"INV",//Ita
"REV"//Jpn
};

const char *str_menu_Angle[] =
{
  "ANGLE     ",//eng
  "ANGLE     ",//fin
  "ANGLE     ",//franc
  "ANGULO    ",//Port
  "ANGLE     ",//Rus
  "ANGULO    ",//Esp
  "VINKEL    ",//Swed
  "WINKEL    ",//Deu
  "SKLON     ",//Ceska
  "ANGLE     ",//Dan
  "ANGLE     ",//Est
  "ANGLE     ",//Latv
  "ANGLE     ",//Liet
  "ANGLE     ",//Nor
  "ANGLE     ",//Pol
  "ANGOLO    ",//Ita
  "ANGLE     "//Jpn
};

const char *str_menu_MapTarget[] =
{
  "MAP TARGET",//eng
  "MAP TARGET",//fin
  "CARTE OBJ.",//franc
  "LASER MAP ",//Port
  "MAP TARGET",//Rus
  "MAPEO OBJ.",//Esp
  "MAP TARGET",//Swed
  "OBJ.KART. ",//Deu
  "MAP CIL   ",//Ceska
  "MAP TARGET",//Dan
  "MAP TARGET",//Est
  "MAP TARGET",//Latv
  "MAP TARGET",//Liet
  "MAP TARGET",//Nor
  "MAP TARGET",//Pol
  "MAP OGGETT",//Ita
  "MAP TARGET"//Jpn
};

const char *str_menu_MapTrail[] =
{
  "MAP TRAIL ",//eng
  "MAP TRAIL ",//fin
  "CARTE RT. ",//franc
  "MAP.TRILHA",//Port
  "MAP TRAIL ",//Rus
  "MAPEO RUTA ",//Esp
  "MAP TRAIL ",//Swed
  "ROUTE KART",//Deu
  "MAP TRASA ",//Ceska
  "MAP TRAIL ",//Dan
  "MAP TRAIL ",//Est
  "MAP TRAIL ",//Latv
  "MAP TRAIL ",//Liet
  "MAP TRAIL ",//Nor
  "MAP TRAIL ",//Pol
  "MAP. ROTTA",//Ita
  "MAP TRAIL "//Jpn
};

const char *str_menu_LineClear[] =
{
  "LINE CLEAR",//eng
  "LINE CLEAR",//fin
  "ARB.RISQUE",//franc
  "RISCO EM LT",//Port
  "LINE CLEAR",//Rus
  "LINE CLEAR",//Esp
  "LINE CLEAR",//Swed
  "RISIKOBAUM",//Deu
  "RISK.STROM",//Ceska
  "LINE CLEAR",//Dan
  "LINE CLEAR",//Est
  "LINE CLEAR",//Latv
  "LINE CLEAR",//Liet
  "LINE CLEAR",//Nor
  "LINE CLEAR",//Pol
  "LINE CLEAR",//Ita
  "LINE CLEAR"//Jpn
};

const char *str_menu_DeltaHeight[] =
{
  "DELTA HGT ",//eng
  "DELTA HGT ",//fin
  "DELTA HAUT",//franc
  "ALT.FLECHA",//Port
  "DELTA HGT ",//Rus
  "ALT. DELTA",//Esp
  "DELTA HJD ",//Swed
  "DELTA H�HE",//Deu
  "PRUVES    ",//Ceska
  "DELTA HGT ",//Dan
  "DELTA HGT ",//Est
  "DELTA HGT ",//Latv
  "DELTA HGT ",//Liet
  "DELTA HGT ",//Nor
  "DELTA HGT ",//Pol
  "ALT. DELTA",//Ita
  "DELTA HGT "//Jpn
};

const char *str_menu_Mem[] =
{
  "MEMORY    ",//eng
  "MEMORY    ",//fin
  "M�MOIRE   ",//franc
  "MEMORIA   ",//Port
  "������    ",//Rus
  "MEMORIA   ",//Esp
  "MINNE     ",//Swed
  "SPEICHER  ",//Deu
  "PAMET     ",//Ceska
  "MEMORY    ",//Dan
  "MEMORY    ",//Est
  "MEMORY    ",//Latv
  "MEMORY    ",//Liet
  "MEMORY    ",//Nor
  "MEMORY    ",//Pol
  "MEMORIA   ",//Ita
  "MEMORY    "//Jpn
};

const char *str_menu_Settings[] =      //kan visa 12 tecken d� ingen ikon
{
  "SETTINGS    ",//eng
  "SETTINGS    ",//fin
  "REGLAG�S    ",//franc
  "AJUSTES     ",//Port
  "SETTINGS    ",//Rus
  "AJUSTES     ",//Esp
  "INST�LLNING ",//Swed
  "EINSTELLEN  ",//Deu
  "NASTAVENI   ",//Ceska
  "SETTINGS    ",//Dan
  "SETTINGS    ",//Est
  "SETTINGS    ",//Latv
  "SETTINGS    ",//Liet
  "SETTINGS    ",//Nor
  "SETTINGS    ",//Pol
  "IMPOSTAZIONE",//Ita
  "SETTINGS    "//Jpn
};

const char *str_menu_Usb[] =
{
  "USB       ",//eng
  "USB       ",//fin
  "USB       ",//franc
  "USB       ",//Port
  "USB       ",//Rus
  "USB       ",//Esp
  "USB       ",//Swed
  "USB       ",//Deu
  "USB       ",//Ceska
  "USB       ",//Dan
  "USB       ",//Est
  "USB       ",//Latv
  "USB       ",//Liet
  "USB       ",//Nor
  "USB       ",//Pol
  "USB       ",//Ita
  "USB       "//Jpn
};

const char *str_turnoff1[] =
{
  "TURNING OFF",//eng
  "TURNING OFF",//fin
  "ETEINDRE",//franc
  "DESLIGANDO",//Port
  "TURNING OFF",//Rus
  "APAGAR",//Esp
  "ST�NGER AV ",//Swed
  "ABSCHALTEN ",//Deu
  "VYPNE SE ",//Ceska
  "TURNING OFF ",//Dan
  "TURNING OFF ",//Est
  "TURNING OFF ",//Latv
  "TURNING OFF ",//Liet
  "TURNING OFF ",//Nor
  "TURNING OFF ",//Pol
  "SPEGNIMENTO",//Ita
  "TURNING OFF"//Jpn
};

const char *str_turnoff2[] =
{
  "AFTER ",//eng
  "AFTER ",//fin
  "APRES ",//franc
  "DEPOIS DE ",//Port
  "AFTER ",//Rus
  "DESPUES ",//Esp
  "OM ",//Swed
  "IN ",//Deu
  "PO ",//Ceska
  "AFTER ",//Dan
  "AFTER ",//Est
  "AFTER ",//Latv
  "AFTER ",//Liet
  "AFTER ",//Nor
  "AFTER ",//Pol
  "DOPO ",//Ita
  "AFTER "//Jpn
};

const char *str_turnoff3[] =
{
  "MINUTES",//eng
  "MINUTES",//fin
  "MINUTES",//franc
  "MINUTOS",//Port
  "MINUTES",//Rus
  "MINUTOS",//Esp
  "MINUTER",//Swed
  "MINUTEN",//Deu
  "MINUTACH",//Ceska
  "MINUTES",//Dan
  "MINUTES",//Est
  "MINUTES",//Latv
  "MINUTES",//Liet
  "MINUTES",//Nor
  "MINUTES",//Pol
  "MINUTI",//Ita
  "MINUTES"//Jpn
};

const char *str_closing[] =
{
  "CLOSING",//eng
  "CLOSING",//fin
  "ETEINDRE",//franc
  "FECHANDO",//Port
  "CLOSING",//Rus
  "CERRANDO",//Esp
  "ST�NGER AV",//Swed
  "ABSCHALTEN",//Deu
  "ZAVIRAM",//Ceska
  "CLOSING",//Dan
  "CLOSING",//Est
  "CLOSING",//Latv
  "CLOSING",//Liet
  "CLOSING",//Nor
  "CLOSING",//Pol
  "CHIUDENDO",//Ita
  "CLOSING"//Jpn
};

//settings.c
const char *str_set_Deg[] =
{
  "DEG ",//eng
  "DEG ",//fin
  "DEG ",//franc
  "GRAU",//Port
  "DEG ",//Rus
  "DEG ",//Esp
  "DEG ",//Swed
  "GRAD",//Deu
  "DEG ",//Ceska
  "DEG ",//Dan
  "DEG ",//Est
  "DEG ",//Latv
  "DEG ",//Liet
  "DEG ",//Nor
  "DEG ",//Pol
  "DEG ",//Ita
  "DEG "//Jpn
};

const char *str_set_Grad[] =
{
  "GRAD",//eng
  "GRAD",//fin
  "GRAD",//franc
  "GRAD",//Port
  "GRAD",//Rus
  "GRAD",//Esp
  "GRAD",//Swed
  "GON ",//Deu
  "GRAD",//Ceska
  "GRAD",//Dan
  "GRAD",//Est
  "GRAD",//Latv
  "GRAD",//Liet
  "GRAD",//Nor
  "GRAD",//Pol
  "GRAD",//Ita
  "GRAD"//Jpn
};

const char *str_set_Percent[] =
{
  "%%   ",//eng
  "%%   ",//fin
  "%%   ",//franc
  "%%   ",//Port
  "%%   ",//Rus
  "%%   ",//Esp
  "%%   ",//Swed
  "%%   ",//Deu
  "%%   ",//Ceska
  "%%   ",//Dan
  "%%   ",//Est
  "%%   ",//Latv
  "%%   ",//Liet
  "%%   ",//Nor
  "%%   ",//Pol
  "%%   ",//Ita
  "%%   "//Jpn
};

const char *str_Yes[] =
{
  "YES",//eng
  "YES",//fin
  "OUI",//franc
  "SIM",//Port
  "YES",//Rus
  "SI ",//Esp
  " JA",//Swed
  " JA ",//Deu
  "ANO",//Ceska
  "YES",//Dan
  "YES",//Est
  "YES",//Latv
  "YES",//Liet
  "YES",//Nor
  "YES",//Pol
  "SI ",//Ita
  "YES"//Jpn
};

const char *str_No[] =
{
  "NO ",//eng
  "NO ",//fin
  "NON",//franc
  "NAO",//Port
  "NO ",//Rus
  "NO ",//Esp
  "NEJ",//Swed
  "NEIN",//Deu
  "NE ",//Ceska
  "NO ",//Dan
  "NO ",//Est
  "NO ",//Latv
  "NO ",//Liet
  "NO ",//Nor
  "NO ",//Pol
  "NO ",//Ita
  "NO "//Jpn
};

//laser.c
const char *str_No_Laser[] =
{
  "NO LASER",//eng
  "NO LASER",//fin
  "PAS LASER",//franc
  "SEM LASER",//Port
  "NO LASER",//Rus
  "NO LASER",//Esp
  "INGEN LASER",//Swed
  "KEIN LASER",//Deu
  "ZADNA LASER",//Ceska
  "NO LASER",//Dan
  "NO LASER",//Est
  "NO LASER",//Latv
  "NO LASER",//Liet
  "NO LASER",//Nor
  "NO LASER",//Pol
  "NESSUN LASER",//Ita
  "NO LASER"//Jpn
};

const char *hgt_str_Laser[] =
{
  "LASER...",//eng
  "LASER...",//fin
  "LASER...",//franc
  "LASER...",//Port
  "LASER...",//Rus
  "LASER...",//Esp
  "LASER...",//Swed
  "LASER...",//Deu
  "LASER...",//Ceska
  "LASER...",//Dan
  "LASER...",//Est
  "LASER...",//Latv
  "LASER...",//Liet
  "LASER...",//Nor
  "LASER...",//Pol
  "LASER...",//Ita
  "LASER..."//Jpn
};

const char *s200_strAim1[] =
{
  "AIM AND",//eng
  "AIM AND",//fin
  "VISER,",//franc
  "MIRE E",//Port
  "AIM AND",//Rus
  "APUNTAR Y",//Esp
  "SIKTA, TRYCK",//Swed
  "ZIELE,DR�CKE",//Deu
  "ZAMIRTE NA",//Ceska
  "AIM AND",//Dan
  "AIM AND",//Est
  "AIM AND",//Latv
  "AIM AND",//Liet
  "AIM AND",//Nor
  "AIM AND",//Pol
  "PUNTARE E",//Ita
  "AIM AND"//Jpn
};

const char *s200_strAim2[] =
{
  "PRESS ON TO",//eng
  "PRESS ON TO",//fin
  "[ON] pr. DEM",//franc
  "CLIQUE NO ON",//Port
  "PRESS ON TO",//Rus
  "PRESIONAR ON",//Esp
  "[ON] F�R",//Swed
  "ON F�R DIE",//Deu
  "CIL A",//Ceska
  "PRESS ON TO",//Dan
  "PRESS ON TO",//Est
  "PRESS ON TO",//Latv
  "PRESS ON TO",//Liet
  "PRESS ON TO",//Nor
  "PRESS ON TO",//Pol
  "PREMERE ON",//Ita
  "PRESS ON TO"//Jpn
};

const char *s200_strAim3[] =
{
  "FIRE LASER",//eng
  "FIRE LASER",//fin
  "LASER",//franc
  "PARA MEDIR",//Port
  "FIRE LASER",//Rus
  "PARA LASER",//Esp
  "LASERM�TNING",//Swed
  "LASERMESSUNG",//Deu
  "STLACTE [ON]",//Ceska
  "FIRE LASER",//Dan
  "FIRE LASER",//Est
  "FIRE LASER",//Latv
  "FIRE LASER",//Liet
  "FIRE LASER",//Nor
  "FIRE LASER",//Pol
  "PER LASER",//Ita
  "FIRE LASER"//Jpn
};

const char *s200_strAim4[] =
{
  "OR PRESS DME",//eng
  "OR PRESS DME",//fin
  "OU sur [DME]",//franc
  "OU PRESSIONE",//Port
  "OR PRESS DME",//Rus
  "O PRES. DME",//Esp
  "ELLER [DME]",//Swed
  "ODER DME F�R",//Deu
  "NEBO [DME]",//Ceska
  "OR PRESS DME",//Dan
  "OR PRESS DME",//Est
  "OR PRESS DME",//Latv
  "OR PRESS DME",//Liet
  "OR PRESS DME",//Nor
  "OR PRESS DME",//Pol
  "O PREM. DME",//Ita
  "OR PRESS DME"//Jpn
};

const char *s200_strAim5[] =
{
  "TO FIRE DME",//eng
  "TO FIRE DME",//fin
  "MES. DME",//franc
  "DME PARA DME",//Port
  "TO FIRE DME",//Rus
  "DISPARA DME",//Esp
  "DMEM�TNING",//Swed
  "DME MESSUNG",//Deu
  "",//Ceska
  "TO FIRE DME",//Dan
  "TO FIRE DME",//Est
  "TO FIRE DME",//Latv
  "TO FIRE DME",//Liet
  "TO FIRE DME",//Nor
  "TO FIRE DME",//Pol
  "PER DME",//Ita
  "TO FIRE DME"//Jpn
};

const char *s200_strAim6[] =
{
  "FIRE DME",//eng
  "FIRE DME",//fin
  "MES. DME",//franc
  "PARA DME",//Port
  "FIRE DME",//Rus
  "DISPARA DME",//Esp
  "DMEM�TNING",//Swed
  "DME MESSUNG",//Deu
  "STISKNI DME",//Ceska
  "FIRE DME",//Dan
  "FIRE DME",//Est
  "FIRE DME",//Latv
  "FIRE DME",//Liet
  "FIRE DME",//Nor
  "FIRE DME",//Pol
  "PER DME",//Ita
  "FIRE DME"//Jpn
};


const char *s200_strNoVal1[] =
{
  "NO VALUE",//eng
  "NO VALUE",//fin
  "NON MESURE",//franc
  "SEM VALOR",//Port
  "NO VALUE",//Rus
  "SIN VALOR",//Esp
  "INGET V�RDE",//Swed
  "KEINE",//Deu
  "NEZMERENO",//Ceska
  "NO VALUE",//Dan
  "NO VALUE",//Est
  "NO VALUE",//Latv
  "NO VALUE",//Liet
  "NO VALUE",//Nor
  "NO VALUE",//Pol
  "NESSUN",//Ita
  "NO VALUE"//Jpn
};

const char *s200_strNoVal2[] =
{
  "",//eng
  "",//fin
  "",//franc
  "",//Port
  "",//Rus
  "",//Esp
  "",//Swed
  "MESSUNG",//Deu
  "",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "VALORE",//Ita
  ""//Jpn
};

const char *s200_strShotAgain1[] =
{
  "SHOT AGAIN",//eng
  "SHOT AGAIN",//fin
  "REMESURER",//franc
  "TENTE DENOVO",//Port
  "SHOT AGAIN",//Rus
  "DISPARAR",//Esp
  "SKJUT IGEN",//Swed
  "NOCHMALS",//Deu
  "MERIT ZNOVA",//Ceska
  "SHOT AGAIN",//Dan
  "SHOT AGAIN",//Est
  "SHOT AGAIN",//Latv
  "SHOT AGAIN",//Liet
  "SHOT AGAIN",//Nor
  "SHOT AGAIN",//Pol
  "SPARARE",//Ita
  "SHOT AGAIN"//Jpn
};

const char *s200_strShotAgain2[] =
{
  "",//eng
  "",//fin
  "",//franc
  "",//Port
  "",//Rus
  "OTRA VEZ",//Esp
  "",//Swed
  "MESSEN",//Deu
  "",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "DI NUOVO",//Ita
  ""//Jpn
};

const char *str_oled_LaserFirst1[] =
{
  "FIR",//eng
  "FIR",//fin
  "PRE",//franc
  "PRI",//Port
  "FIR",//Rus
  "PRI",//Esp
  "F�R",//Swed
  "ERS",//Deu
  "BLI",//Ceska
  "FIR",//Dan
  "FIR",//Est
  "FIR",//Latv
  "FIR",//Liet
  "FIR",//Nor
  "FIR",//Pol
  "PRI",//Ita
  "FIR"//Jpn
};

const char *str_oled_LaserFirst2[] =
{
  "ST",//eng
  "ST",//fin
  "MI",//franc
  "MEI",//Port
  "ST",//Rus
  "ME",//Esp
  "STA",//Swed
  "TE",//Deu
  "ZKY",//Ceska
  "ST",//Dan
  "ST",//Est
  "ST",//Latv
  "ST",//Liet
  "ST",//Nor
  "ST",//Pol
  "MO",//Ita
  "ST"//Jpn
};

const char *str_oled_LaserFirst3[] =
{
  "",//eng
  "",//fin
  "ERS",//franc
  "RO",//Port
  "",//Rus
  "RO",//Esp
  "",//Swed
  "",//Deu
  "",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "",//Ita
  ""//Jpn
};

const char *str_oled_LaserStrong1[] =
{
  "STR",//eng
  "STR",//fin
  "PLU",//franc
  "FOR",//Port
  "STR",//Rus
  "FU",//Esp
  "STA",//Swed
  "ST�",//Deu
  "SIL",//Ceska
  "STR",//Dan
  "STR",//Est
  "STR",//Latv
  "STR",//Liet
  "STR",//Nor
  "STR",//Pol
  "FO",//Ita
  "STR"//Jpn
};

const char *str_oled_LaserStrong2[] =
{
  "ONG",//eng
  "ONG",//fin
  "S F",//franc
  "TE",//Port
  "ONG",//Rus
  "ER",//Esp
  "RKA",//Swed
  "RKS",//Deu
  "NY",//Ceska
  "ONG",//Dan
  "ONG",//Est
  "ONG",//Latv
  "ONG",//Liet
  "ONG",//Nor
  "ONG",//Pol
  "RT",//Ita
  "ONG"//Jpn
};

const char *str_oled_LaserStrong3[] =
{
  "",//eng
  "",//fin
  "ORT",//franc
  "",//Port
  "",//Rus
  "TE",//Esp
  "STE",//Swed
  "TE",//Deu
  "",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "E",//Ita
  ""//Jpn
};

const char *str_oled_LaserLast1[] =
{
  "LA",//eng
  "LA",//fin
  "DER",//franc
  "UL",//Port
  "LA",//Rus
  "UL",//Esp
  "SIS",//Swed
  "LET",//Deu
  "DAL",//Ceska
  "LA",//Dan
  "LA",//Est
  "LA",//Latv
  "LA",//Liet
  "LA",//Nor
  "LA",//Pol
  "UL",//Ita
  "LA"//Jpn
};

const char *str_oled_LaserLast2[] =
{
  "ST",//eng
  "ST",//fin
  "NI",//franc
  "TI",//Port
  "ST",//Rus
  "TI",//Esp
  "TA",//Swed
  "ZTE",//Deu
  "EKY",//Ceska
  "ST",//Dan
  "ST",//Est
  "ST",//Latv
  "ST",//Liet
  "ST",//Nor
  "ST",//Pol
  "TI",//Ita
  "ST"//Jpn
};

const char *str_oled_LaserLast3[] =
{
  "",//eng
  "",//fin
  "ER",//franc
  "MO",//Port
  "",//Rus
  "MO",//Esp
  "",//Swed
  "",//Deu
  "",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "MO",//Ita
  ""//Jpn
};

const char *str_Deleted[] =
{
  "DELETED",//eng
  "DELETED",//fin
  "EFFACER",//franc
  "APAGADOS",//Port
  "DELETED",//Rus
  "ELIMINADO",//Esp
  "RADERAD",//Swed
  "GEL�SCHT",//Deu
  "SMAZANO",//Ceska
  "DELETED",//Dan
  "DELETED",//Est
  "DELETED",//Latv
  "DELETED",//Liet
  "DELETED",//Nor
  "DELETED",//Pol
  "CANCELLATI",//Ita
  "DELETED"//Jpn
};

const char *str_oled_Deleted[] =
{
  "DEL",//eng
  "DEL",//fin
  "EFF",//franc
  "APA",//Port
  "DEL",//Rus
  "ELI",//Esp
  "RAD",//Swed
  "L�",//Deu
  "SM",//Ceska
  "DEL",//Dan
  "DEL",//Est
  "DEL",//Latv
  "DEL",//Liet
  "DEL",//Nor
  "DEL",//Pol
  "CAN",//Ita
  "DEL"//Jpn
};

const char *str_oled_Deleted2[] =
{
  "",//eng
  "",//fin
  "",//franc
  "GAR",//Port
  "",//Rus
  "MIN",//Esp
  "ERA",//Swed
  "SCH",//Deu
  "AZ",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "CEL",//Ita
  ""//Jpn
};

const char *str_Compass[] =
{
  "COMPASS",//eng
  "COMPASS",//fin
  "BOUSSOLE",//franc
  "BUSSOLA",//Port
  "COMPASS",//Rus
  "BRUJULA",//Esp
  "KOMPASS",//Swed
  "KOMPASS",//Deu
  "KOMPAS",//Ceska
  "COMPASS",//Dan
  "COMPASS",//Est
  "COMPASS",//Latv
  "COMPASS",//Liet
  "COMPASS",//Nor
  "COMPASS",//Pol
  "BUSSOLA",//Ita
  "COMPASS"//Jpn
};

//CalcDangerous.c
const char *str_menu_MinDist[] =
{
  "MIN. DIST.",//eng
  "MIN. DIST.",//fin
  "MIN. DIST.",//franc
  "DIST. MIN.",//Port
  "MIN. DIST.",//Rus
  "DIST. MIN.",//Esp
  "MIN. DIST.",//Swed
  "MIN. DIST.",//Deu
  "MIN. VZDAL",//Ceska
  "MIN. DIST.",//Dan
  "MIN. DIST.",//Est
  "MIN. DIST.",//Latv
  "MIN. DIST.",//Liet
  "MIN. DIST.",//Nor
  "MIN. DIST.",//Pol
  "DIST. MIN.",//Ita
  "MIN. DIST."//Jpn
};

const char *str_cd_ErrTrp1[] =
{
  "ERROR",//eng
  "ERROR",//fin
  "ERROR",//franc
  "ERRO",//Port
  "ERROR",//Rus
  "ERROR",//Esp
  "ERROR",//Swed
  "ERROR",//Deu
  "ERROR",//Ceska
  "ERROR",//Dan
  "ERROR",//Est
  "ERROR",//Latv
  "ERROR",//Liet
  "ERROR",//Nor
  "ERROR",//Pol
  "ERRORE",//Ita
  "ERROR"//Jpn
};

const char *str_cd_ErrTrp2[] =
{
  "AIM AT TRP",//eng
  "AIM AT TRP",//fin
  "AIM AT TRP",//franc
  "AIM AT TRP",//Port
  "AIM AT TRP",//Rus
  "AIM AT TRP",//Esp
  "SIKTA P� TRP",//Swed
  "AIM AT TRP",//Deu
  "AIM AT TRP",//Ceska
  "AIM AT TRP",//Dan
  "AIM AT TRP",//Est
  "AIM AT TRP",//Latv
  "AIM AT TRP",//Liet
  "AIM AT TRP",//Nor
  "AIM AT TRP",//Pol
  "AIM AT TRP",//Ita
  "AIM AT TRP"//Jpn
};

const char *str_cd_ErrTrp3[] =
{
  "FIRST",//eng
  "FIRST",//fin
  "FIRST",//franc
  "FIRST",//Port
  "FIRST",//Rus
  "FIRST",//Esp
  "F�RST",//Swed
  "FIRST",//Deu
  "FIRST",//Ceska
  "FIRST",//Dan
  "FIRST",//Est
  "FIRST",//Latv
  "FIRST",//Liet
  "FIRST",//Nor
  "FIRST",//Pol
  "FIRST",//Ita
  "FIRST"//Jpn
};

const char *str_cd_NotOk[] =
{
  "NOT OK",//eng
  "NOT OK",//fin
  "PAS OK",//franc
  "NAO OK",//Port
  "NOT OK",//Rus
  "NO OK",//Esp
  "EJ OK",//Swed
  "NICHT OK",//Deu
  "NENI OK",//Ceska
  "NOT OK",//Dan
  "NOT OK",//Est
  "NOT OK",//Latv
  "NOT OK",//Liet
  "NOT OK",//Nor
  "NOT OK",//Pol
  "NON OK",//Ita
  "NOT OK"//Jpn
};

const char *str_cd_Ok[] =
{
  "OK",//eng
  "OK",//fin
  "OK",//franc
  "OK",//Port
  "OK",//Rus
  "OK",//Esp
  "OK",//Swed
  "OK",//Deu
  "OK",//Ceska
  "OK",//Dan
  "OK",//Est
  "OK",//Latv
  "OK",//Liet
  "OK",//Nor
  "OK",//Pol
  "OK",//Ita
  "OK"//Jpn
};

const char *str_cd_Diff[] =
{
  "DIFF:",//eng
  "DIFF:",//fin
  "DIFF:",//franc
  "DIF:",//Port
  "DIFF:",//Rus
  "DIF:",//Esp
  "DIFF:",//Swed
  "DIFF:",//Deu
  "DIF:",//Ceska
  "DIFF:",//Dan
  "DIFF:",//Est
  "DIFF:",//Latv
  "DIFF:",//Liet
  "DIFF:",//Nor
  "DIFF:",//Pol
  "DIF:",//Ita
  "DIFF:"//Jpn
};

  const char *str_cd_Diff2[] =
{
  "DIFF:",//eng
  "DIFF:",//fin
  "DIFF:",//franc
  "DIF:",//Port
  "DIFF:",//Rus
  "DIF:",//Esp
  "DIFF:",//Swed
  "DIF:",//Deu
  "DIFF:",//Ceska
  "DIFF:",//Dan
  "DIFF:",//Est
  "DIFF:",//Latv
  "DIFF:",//Liet
  "DIFF:",//Nor
  "DIFF:",//Pol
  "DIF:",//Ita
  "DIFF:"//Jpn
};

const char *str_cd_Dist[] =
{
  "DIST: ",//eng
  "DIST: ",//fin
  "DIST: ",//franc
  "DIST:",//Port
  "DIST: ",//Rus
  "DIST: ",//Esp
  "DIST: ",//Swed
  "DIST: ",//Deu
  "VZDAL:",//Ceska
  "DIST: ",//Dan
  "DIST: ",//Est
  "DIST: ",//Latv
  "DIST: ",//Liet
  "DIST: ",//Nor
  "DIST: ",//Pol
  "DIST: ",//Ita
  "DIST: "//Jpn
};

const char *str_cd_Pitch[] =
{
  "PITCH:",//eng
  "PITCH:",//fin
  "PITCH:",//franc
  "ANG.:",//Port
  "PITCH:",//Rus
  "ANG.: ",//Esp
  "VINKEL:",//Swed
  "WINKEL:",//Deu
  "SKLON:",//Ceska
  "PITCH:",//Dan
  "PITCH:",//Est
  "PITCH:",//Latv
  "PITCH:",//Liet
  "PITCH:",//Nor
  "PITCH:",//Pol
  "ANG.: ",//Ita
  "PITCH:"//Jpn
};

const char *str_unitM[] =
{
  "M",//eng
  "M",//fin
  "M",//franc
  "M",//Port
  "M",//Rus
  "M",//Esp
  "M",//Swed
  "M",//Deu
  "M",//Ceska
  "M",//Dan
  "M",//Est
  "M",//Latv
  "M",//Liet
  "M",//Nor
  "M",//Pol
  "M",//Ita
  "M"//Jpn
};

const char *str_unitFT[] =
{
  "FT",//eng
  "FT",//fin
  "FT",//franc
  "FT",//Port
  "FT",//Rus
  "FT",//Esp
  "FT",//Swed
  "FT",//Deu
  "FT",//Ceska
  "FT",//Dan
  "FT",//Est
  "FT",//Latv
  "FT",//Liet
  "FT",//Nor
  "FT",//Pol
  "FT",//Ita
  "FT"//Jpn
};

const char *str_manual_object_hgt1[] =
{
"MANUAL",//eng
"MANUAL",//fin
"HAUTEUR",//franc
"ALTURA",//Port
"MANUAL",//Rus
"ALTURA",//Esp
"MANUELL",//Swed
"MANUELLE",//Deu
"MANUALNI",//Ceska
"MANUAL",//Dan
"MANUAL",//Est
"MANUAL",//Latv
"MANUAL",//Liet
"MANUAL",//Nor
"MANUAL",//Pol
"ALTEZZA",//Ita
"MANUAL"//Jpn
};

const char *str_manual_object_hgt2[] =
{
"HEIGHT",//eng
"HEIGHT",//fin
"MANUELLE",//franc
"MANUAL",//Port
"HEIGHT",//Rus
"MANUAL",//Esp
"H�JD",//Swed
"H�HE",//Deu
"VYSKA",//Ceska
"HEIGHT",//Dan
"HEIGHT",//Est
"HEIGHT",//Latv
"HEIGHT",//Liet
"HEIGHT",//Nor
"HEIGHT",//Pol
"MANUALE",//Ita
"HEIGHT"//Jpn
};

//datamem.c
const char *str_error_filehandle[] =
{
  "FILE HANDLE",//eng
  "FILE HANDLE",//fin
  "FILE HANDLE",//franc
  "FILE HANDLE",//Port
  "FILE HANDLE",//Rus
  "FILE HANDLE",//Esp
  "FILE HANDLE",//Swed
  "FILE HANDLE",//Deu
  "SOUBOR",//Ceska
  "FILE HANDLE",//Dan
  "FILE HANDLE",//Est
  "FILE HANDLE",//Latv
  "FILE HANDLE",//Liet
  "FILE HANDLE",//Nor
  "FILE HANDLE",//Pol
  "FILE HANDLE",//Ita
  "FILE HANDLE"//Jpn
};

const char *str_oled_Err[] =
{
  "ERR",//eng
  "ERR",//fin
  "ERR",//franc
  "ERR",//Port
  "ERR",//Rus
  "ERR",//Esp
  "ERR",//Swed
  "ERR",//Deu
  "CHY",//Ceska
  "ERR",//Dan
  "ERR",//Est
  "ERR",//Latv
  "ERR",//Liet
  "ERR",//Nor
  "ERR",//Pol
  "ERR",//Ita
  "ERR"//Jpn
};

const char *str_oled_Err2[] =
{
  "",//eng
  "",//fin
  "",//franc
  "",//Port
  "",//Rus
  "",//Esp
  "",//Swed
  "",//Deu
  "BA",//Ceska
  "",//Dan
  "",//Est
  "",//Latv
  "",//Liet
  "",//Nor
  "",//Pol
  "",//Ita
  ""//Jpn
};

char *str_msg_TryAgainQ[] =
{
  "TRY AGAIN?\n[ON]",//eng
  "TRY AGAIN?\n[ON]",//fin
  "REESSAYER?\n[ON]",//franc
  "TENTAR\nNOVAMENTE?\n[ON]",//Port
  "TRY AGAIN?\n[ON]",//Rus
  "REINTENTAR?\n[ON]",//Esp
  "PROVA IGEN?\n[ON]",//Swed
  "NEUER VER-\nSUCH [ON]",//Deu
  "NOVY POKUS\n[ON]",//Ceska
  "TRY AGAIN?\n[ON]",//Dan
  "TRY AGAIN?\n[ON]",//Est
  "TRY AGAIN?\n[ON]",//Latv
  "TRY AGAIN?\n[ON]",//Liet
  "TRY AGAIN?\n[ON]",//Nor
  "TRY AGAIN?\n[ON]",//Pol
  "RITENTARE?\n[ON]",//Ita
  "TRY AGAIN?\n[ON]"//Jpn
};

const char *str_oled_Gps[] =
{
  "GPS",//eng
  "GPS",//fin
  "GPS",//franc
  "GPS",//Port
  "GPS",//Rus
  "GPS",//Esp
  "GPS",//Swed
  "GPS",//Deu
  "GPS",//Ceska
  "GPS",//Dan
  "GPS",//Est
  "GPS",//Latv
  "GPS",//Liet
  "GPS",//Nor
  "GPS",//Pol
  "GPS",//Ita
  "GPS"//Jpn
};


//map3d.c
const char *str_menu_MapGps[] =
{
  "MAP GPS",//eng
  "MAP GPS",//fin
  "CARTE GPS",//franc
  "MAP GPS",//Port
  "MAP GPS",//Rus
  "MAPEO GPS",//Esp
  "MAP GPS",//Swed
  "MAP GPS",//Deu
  "MAP GPS",//Ceska
  "MAP GPS",//Dan
  "MAP GPS",//Est
  "MAP GPS",//Latv
  "MAP GPS",//Liet
  "MAP GPS",//Nor
  "MAP GPS",//Pol
  "MAPPA GPS",//Ita
  "MAP GPS"//Jpn
};

const char *str_menu_GpsArea[] =
{
  "GPS AREA",//eng
  "GPS AREA",//fin
  "SURF. GPS",//franc
  "AREA GPS",//Port
  "GPS AREA",//Rus
  "AREA GPS",//Esp
  "GPS AREA",//Swed
  "GPS FL�CHE",//Deu
  "GPS PLOCHA",//Ceska
  "GPS AREA",//Dan
  "GPS AREA",//Est
  "GPS AREA",//Latv
  "GPS AREA",//Liet
  "GPS AREA",//Nor
  "GPS AREA",//Pol
  "AREA GPS",//Ita
  "GPS AREA"//Jpn
};

const char *str_menu_Method[] =
{
  "METHOD",//eng
  "METHOD",//fin
  "METHODE",//franc
  "METODO",//Port
  "METHOD",//Rus
  "METODO",//Esp
  "METOD",//Swed
  "METHODE",//Deu
  "METODA",//Ceska
  "METHOD",//Dan
  "METHOD",//Est
  "METHOD",//Latv
  "METHOD",//Liet
  "METHOD",//Nor
  "METHOD",//Pol
  "METODO",//Ita
  "METHOD"//Jpn
};

const char *str_Continue[] =
{
  "CONTINUE",//eng
  "CONTINUE",//fin
  "CONTINUE",//franc
  "CONTINUE",//Port
  "CONTINUE",//Rus
  "CONTINUAR",//Esp
  "FORTS�TT",//Swed
  "WEITER",//Deu
  "POKRACUJ",//Ceska
  "CONTINUE",//Dan
  "CONTINUE",//Est
  "CONTINUE",//Latv
  "CONTINUE",//Liet
  "CONTINUE",//Nor
  "CONTINUE",//Pol
  "CONTINUARE",//Ita
  "CONTINUE"//Jpn
};

const char *str_With[] =
{
  "WITH",//eng
  "WITH",//fin
  "AVEC",//franc
  "COM",//Port
  "WITH",//Rus
  "CON",//Esp
  "MED",//Swed
  "MIT",//Deu
  "S",//Ceska
  "WITH",//Dan
  "WITH",//Est
  "WITH",//Latv
  "WITH",//Liet
  "WITH",//Nor
  "WITH",//Pol
  "CON",//Ita
  "WITH"//Jpn
};

char *str_msg_FileOpen[] =
{
  "FILE OPEN",//eng
  "FILE OPEN",//fin
  "OUVERT\nFICHIER",//franc
  "ABRIR\nARQUIVO",//Port
  "FILE OPEN",//Rus
  "ARCHIVO\nABIERTO",//Esp
  "FIL �PPEN",//Swed
  "DATEI\n�FFNEN",//Deu
  "OTEV.SOUBOR",//Ceska
  "FILE OPEN",//Dan
  "FILE OPEN",//Est
  "FILE OPEN",//Latv
  "FILE OPEN",//Liet
  "FILE OPEN",//Nor
  "FILE OPEN",//Pol
  "FILE\nAPERTO",//Ita
  "FILE OPEN"//Jpn
};

const char *str_oled_Base1[] =
{
  "BA",//eng
  "BA",//fin
  "BA",//franc
  "BA",//Port
  "BA",//Rus
  "BA",//Esp
  "BA",//Swed
  "BAS",//Deu
  "BA",//Ceska
  "BA",//Dan
  "BA",//Est
  "BA",//Latv
  "BA",//Liet
  "BA",//Nor
  "BA",//Pol
  "BA",//Ita
  "BA"//Jpn
};

const char *str_oled_Base2[] =
{
  "SE",//eng
  "SE",//fin
  "SE",//franc
  "SE",//Port
  "SE",//Rus
  "SE",//Esp
  "S",//Swed
  "IS",//Deu
  "ZE",//Ceska
  "SE",//Dan
  "SE",//Est
  "SE",//Latv
  "SE",//Liet
  "SE",//Nor
  "SE",//Pol
  "SE",//Ita
  "SE"//Jpn
};

const char *str_Dme[] =
{
  "DME",//eng
  "DME",//fin
  "DME",//franc
  "DME",//Port
  "DME",//Rus
  "DME",//Esp
  "DME",//Swed
  "DME",//Deu
  "DME",//Ceska
  "DME",//Dan
  "DME",//Est
  "DME",//Latv
  "DME",//Liet
  "DME",//Nor
  "DME",//Pol
  "DME",//Ita
  "DME"//Jpn
};

const char *str_Laser[] =
{
  "LASER",//eng
  "LASER",//fin
  "LASER",//franc
  "LASER",//Port
  "LASER",//Rus
  "LASER",//Esp
  "LASER",//Swed
  "LASER",//Deu
  "LASER",//Ceska
  "LASER",//Dan
  "LASER",//Est
  "LASER",//Latv
  "LASER",//Liet
  "LASER",//Nor
  "LASER",//Pol
  "LASER",//Ita
  "LASER"//Jpn
};

const char *str_Baseline[] =
{
  "BASELINE",//eng
  "BASELINE",//fin
  "LN. DE BASE",//franc
  "LINHA BASE",//Port
  "BASELINE",//Rus
  "BASE",//Esp
  "BASPUNKT",//Swed
  "GRUNDLINIE",//Deu
  "LINIE ZEM",//Ceska
  "BASELINE",//Dan
  "BASELINE",//Est
  "BASELINE",//Latv
  "BASELINE",//Liet
  "BASELINE",//Nor
  "BASELINE",//Pol
  "LINEA BASE",//Ita
  "BASELINE"//Jpn
};

const char *str_OnTarget[] =
{
  "ON TARGET",//eng
  "ON TARGET",//fin
  "VERS CIBLE",//franc
  "ON ALVO",//Port
  "ON TARGET",//Rus
  "OBJETIVO",//Esp
  "P� M�LET",//Swed
  "ERFASSE ZIEL",//Deu
  "NA CIL   ",//Ceska
  "ON TARGET",//Dan
  "ON TARGET",//Est
  "ON TARGET",//Latv
  "ON TARGET",//Liet
  "ON TARGET",//Nor
  "ON TARGET",//Pol
  "OBIETTIVO",//Ita
  "ON TARGET"//Jpn
};

const char *str_oled_Target1[] =
{
  "TAR",//eng
  "TAR",//fin
  "CIB",//franc
  "AL",//Port
  "TAR",//Rus
  "OBJ",//Esp
  "M�L",//Swed
  "ZI",//Deu
  "CIL",//Ceska
  "TAR",//Dan
  "TAR",//Est
  "TAR",//Latv
  "TAR",//Liet
  "TAR",//Nor
  "TAR",//Pol
  "OBT",//Ita
  "TAR"//Jpn
};

const char *str_oled_Target2[] =
{
  "GET",//eng
  "GET",//fin
  "LE",//franc
  "VO",//Port
  "GET",//Rus
  "TVO",//Esp
  "ET",//Swed
  "EL",//Deu
  "",//Ceska
  "GET",//Dan
  "GET",//Est
  "GET",//Latv
  "GET",//Liet
  "GET",//Nor
  "GET",//Pol
  "TVO",//Ita
  "GET"//Jpn
};

const char *str_StoreTargetQ[] =        //max 12 tecken
{
  "STORE TARGET",//eng
  "STORE TARGET",//fin
  "ENREGISTRER?",//franc
  "SALVA MEDIDA",//Port
  "STORE TARGET",//Rus
  "GUARDAR OBJ?",//Esp
  "SPARA M�LET",//Swed
  "SPEICHERN?",//Deu
  "ULOZIT CIL?",//Ceska
  "STORE TARGET",//Dan
  "STORE TARGET",//Est
  "STORE TARGET",//Latv
  "STORE TARGET",//Liet
  "STORE TARGET",//Nor
  "STORE TARGET",//Pol
  "SALVARE OB.?",//Ita
  "STORE TARGET"//Jpn
};

const char *str_Enter[] =
{
  "[ON]",//eng
  "[ON]",//fin
  "[ON]",//franc
  "[ON]",//Port
  "[ON]",//Rus
  "[ON]",//Esp
  "[ON]",//Swed
  "[ON]",//Deu
  "[ON]",//Ceska
  "[ON]",//Dan
  "[ON]",//Est
  "[ON]",//Latv
  "[ON]",//Liet
  "[ON]",//Nor
  "[ON]",//Pol
  "[ON]",//Ita
  "[ON]"//Jpn
};

const char *str_oled_StoreQ1[] =
{
  "ST",//eng
  "ST",//fin
  "MEM",//franc
  "SAL",//Port
  "ST",//Rus
  "GU",//Esp
  "SP",//Swed
  "SPE",//Deu
  "ULO",//Ceska
  "ST",//Dan
  "ST",//Est
  "ST",//Latv
  "ST",//Liet
  "ST",//Nor
  "ST",//Pol
  "SAL",//Ita
  "ST"//Jpn
};

const char *str_oled_StoreQ2[] =
{
  "OR",//eng
  "OR",//fin
  "?",//franc
  "VAR",//Port
  "OR",//Rus
  "ARD",//Esp
  "AR",//Swed
  "ICH",//Deu
  "ZIT",//Ceska
  "OR",//Dan
  "OR",//Est
  "OR",//Latv
  "OR",//Liet
  "OR",//Nor
  "OR",//Pol
  "VA?",//Ita
  "OR"//Jpn
};

const char *str_oled_StoreQ3[] =
{
  "E?",//eng
  "E?",//fin
  "",//franc
  "?",//Port
  "E?",//Rus
  "AR?",//Esp
  "A?",//Swed
  "ER?",//Deu
  "?",//Ceska
  "E?",//Dan
  "E?",//Est
  "E?",//Latv
  "E?",//Liet
  "E?",//Nor
  "E?",//Pol
  "",//Ita
  "E?"//Jpn
};

const char *str_menu_UseLaser[] =
{
  "USE LASER",//eng
  "USE LASER",//fin
  "UTL LASER",//franc
  "USAR LASER",//Port
  "USE LASER",//Rus
  "USAR LASER",//Esp
  "ANV. LASER",//Swed
  "NUTZE LASER",//Deu
  "UZIJ LASER",//Ceska
  "USE LASER",//Dan
  "USE LASER",//Est
  "USE LASER",//Latv
  "USE LASER",//Liet
  "USE LASER",//Nor
  "USE LASER",//Pol
  "USARE LASER",//Ita
  "USE LASER"//Jpn
};

const char *str_menu_UseGps[] =
{
  "USE GPS",//eng
  "USE GPS",//fin
  "UTL GPS",//franc
  "USAR GPS",//Port
  "USE GPS",//Rus
  "USAR GPS",//Esp
  "ANV. GPS",//Swed
  "NUTZE GPS",//Deu
  "UZIJ GPS",//Ceska
  "USE GPS",//Dan
  "USE GPS",//Est
  "USE GPS",//Latv
  "USE GPS",//Liet
  "USE GPS",//Nor
  "USE GPS",//Pol
  "USARE GPS",//Ita
  "USE GPS"//Jpn
};

const char *str_menu_UseDme[] =
{
  "USE DME",//eng
  "USE DME",//fin
  "UTL DME",//franc
  "USAR DME",//Port
  "USE DME",//Rus
  "USAR DME",//Esp
  "ANV. DME",//Swed
  "NUTZE DME",//Deu
  "UZIJ DME",//Ceska
  "USE DME",//Dan
  "USE DME",//Est
  "USE DME",//Latv
  "USE DME",//Liet
  "USE DME",//Nor
  "USE DME",//Pol
  "USARE DME",//Ita
  "USE DME"//Jpn
};

const char *str_menu_Finish[] =
{
  "FINISH",//eng
  "FINISH",//fin
  "FINI",//franc
  "FIM",//Port
  "FINISH",//Rus
  "TERMINAR",//Esp
  "AVSLUTA",//Swed
  "FERTIG",//Deu
  "HOTOVO",//Ceska
  "FINISH",//Dan
  "FINISH",//Est
  "FINISH",//Latv
  "FINISH",//Liet
  "FINISH",//Nor
  "FINISH",//Pol
  "FINIRE",//Ita
  "FINISH"//Jpn
};

const char *str_menu_NewRef[] =
{
  "NEW REF.",//eng
  "NEW REF.",//fin
  "NOUV. REF.",//franc
  "NOVA REF.",//Port
  "NEW REF.",//Rus
  "NUEVA REF.",//Esp
  "NY REF.",//Swed
  "NEUE REF.",//Deu
  "NOVY REF.",//Ceska
  "NEW REF.",//Dan
  "NEW REF.",//Est
  "NEW REF.",//Latv
  "NEW REF.",//Liet
  "NEW REF.",//Nor
  "NEW REF.",//Pol
  "NUOVA RIF.",//Ita
  "NEW REF."//Jpn
};

const char *str_menu_NextTarget[] =
{
  "NEXT TARGET",//eng
  "NEXT TARGET",//fin
  "CIBLE.SUIV.",//franc
  "PROX. ALVO",//Port
  "NEXT TARGET",//Rus
  "PROXIMO OBJ",//Esp
  "N�STA M�L",//Swed
  "N�CHST ZIEL",//Deu
  "PRISTI CIL",//Ceska
  "NEXT TARGET",//Dan
  "NEXT TARGET",//Est
  "NEXT TARGET",//Latv
  "NEXT TARGET",//Liet
  "NEXT TARGET",//Nor
  "NEXT TARGET",//Pol
  "PROSSIMO OB",//Ita
  "NEXT TARGET"//Jpn
};

const char *str_menu_Return[] =
{
  "RETURN",//eng
  "RETURN",//fin
  "RETOUR",//franc
  "RETORNAR",//Port
  "RETURN",//Rus
  "VOLVER",//Esp
  "RETURN",//Swed
  "ZUR�CK",//Deu
  "ZPATKY",//Ceska
  "RETURN",//Dan
  "RETURN",//Est
  "RETURN",//Latv
  "RETURN",//Liet
  "RETURN",//Nor
  "RETURN",//Pol
  "TORNARE",//Ita
  "RETURN"//Jpn
};

const char *str_oled_New[] =
{
  "NEW",//eng
  "NEW",//fin
  "NV.",//franc
  "NEW",//Port        //l�t st�, finns ingen bra f�rkortning f�r nova
  "NEW",//Rus
  "NEW",//Esp        //l�t st�, finns ingen bra f�rkortning f�r nueva
  "NY",//Swed
  "NEU",//Deu
  "NOV",//Ceska
  "NEW",//Dan
  "NEW",//Est
  "NEW",//Latv
  "NEW",//Liet
  "NEW",//Nor
  "NEW",//Pol
  "NEW",//Ita        //l�t st�, finns ingen bra f�rkortning f�r nuova
  "NEW"//Jpn
};

const char *str_oled_Ref[] =
{
  "REF",//eng
  "REF",//fin
  "REF",//franc
  "REF",//Port
  "REF",//Rus
  "REF",//Esp
  "REF",//Swed
  "REF",//Deu
  "REF",//Ceska
  "REF",//Dan
  "REF",//Est
  "REF",//Latv
  "REF",//Liet
  "REF",//Nor
  "REF",//Pol
  "RIF",//Ita
  "REF"//Jpn
};

const char *str_oled_Move1[] =
{
  "MO",//eng
  "MO",//fin
  "ALL",//franc
  "VAP",//Port
  "MO",//Rus
  "MO",//Esp
  "G�",//Swed
  "GE",//Deu
  "JDI",//Ceska
  "MO",//Dan
  "MO",//Est
  "MO",//Latv
  "MO",//Liet
  "MO",//Nor
  "MO",//Pol
  "MOV",//Ita
  "MO"//Jpn
};

const char *str_oled_Move2[] =
{
  "VE",//eng
  "VE",//fin
  "ER",//franc
  "ARA",//Port
  "VE",//Rus
  "VER",//Esp
  "",//Swed
  "HE",//Deu
  "",//Ceska
  "VE",//Dan
  "VE",//Est
  "VE",//Latv
  "VE",//Liet
  "VE",//Nor
  "VE",//Pol
  "ERE",//Ita
  "VE"//Jpn
};

const char *str_MoveTo[] =
{
  "MOVE TO",//eng
  "MOVE TO",//fin
  "ALLERS VERS",//franc
  "VA PARA",//Port
  "MOVE TO",//Rus
  "MOVER A",//Esp
  "G� TILL",//Swed
  "GEHE ZU",//Deu
  "JDI NA",//Ceska
  "MOVE TO",//Dan
  "MOVE TO",//Est
  "MOVE TO",//Latv
  "MOVE TO",//Liet
  "MOVE TO",//Nor
  "MOVE TO",//Pol
  "PASSARE A",//Ita
  "MOVE TO"//Jpn
};

const char *str_NewRef[] =
{
  "NEW REF.!",//eng
  "NEW REF.!",//fin
  "NOUV. REF.!",//franc
  "PROX. REF.!",//Port
  "NEW REF.!",//Rus
  "NUEVA REF.!",//Esp
  "NY REF.!",//Swed
  "NEUER REF.!",//Deu
  "NOVY REF.!",//Ceska
  "NEW REF.!",//Dan
  "NEW REF.!",//Est
  "NEW REF.!",//Latv
  "NEW REF.!",//Liet
  "NEW REF.!",//Nor
  "NEW REF.!",//Pol
  "NUOVI RIF.!",//Ita
  "NEW REF.!"//Jpn
};

const char *str_ThenEnter[] =
{
  "THEN [ON]",//eng
  "THEN [ON]",//fin
  "[ON] pr.",//franc
  "PRESSIONE ON",//Port
  "THEN [ON]",//Rus
  "LUEGO [ON]",//Esp
  "STARTA SEDAN",//Swed
  "STARTE MIT",//Deu
  "POTOM SPUST",//Ceska
  "THEN [ON]",//Dan
  "THEN [ON]",//Est
  "THEN [ON]",//Latv
  "THEN [ON]",//Liet
  "THEN [ON]",//Nor
  "THEN [ON]",//Pol
  "PREMERE [ON]",//Ita
  "THEN [ON]"//Jpn
};

const char *str_ToStartGps[] =
{
  "TO START GPS",//eng
  "TO START GPS",//fin
  "DEMARER GPS",//franc
  "INICIAR GPS",//Port
  "TO START GPS",//Rus
  "INICIAR GPS",//Esp
  "GPS MED [ON]",//Swed
  "[ON] DAS GPS",//Deu
  "GPS [ON]",//Ceska
  "TO START GPS",//Dan
  "TO START GPS",//Est
  "TO START GPS",//Latv
  "TO START GPS",//Liet
  "TO START GPS",//Nor
  "TO START GPS",//Pol
  "PER GPS",//Ita
  "TO START GPS"//Jpn
};

const char *str_menu_NewTarget[] =
{
  "NEW TARGET",//eng
  "NEW TARGET",//fin
  "NOUV. CIBLE",//franc
  "NOVO ALVO",//Port
  "NEW TARGET",//Rus
  "NUEVO OBJ.",//Esp
  "NYTT M�L",//Swed
  "NEUES ZIEL",//Deu
  "NOVY CIL",//Ceska
  "NEW TARGET",//Dan
  "NEW TARGET",//Est
  "NEW TARGET",//Latv
  "NEW TARGET",//Liet
  "NEW TARGET",//Nor
  "NEW TARGET",//Pol
  "NUOVA OB.",//Ita
  "NEW TARGET"//Jpn
};

const char *str_StoreQ[] =
{
  "STORE?",//eng
  "STORE?",//fin
  "ENREGISTRER?",//franc
  "SALVAR?",//Port
  "STORE?",//Rus
  "GUARDAR?",//Esp
  "SPARA?",//Swed
  "SPEICHERN?",//Deu
  "ULOZIT?",//Ceska
  "STORE?",//Dan
  "STORE?",//Est
  "STORE?",//Latv
  "STORE?",//Liet
  "STORE?",//Nor
  "STORE?",//Pol
  "SALVARE?",//Ita
  "STORE?"//Jpn
};

const char *str_PressEnter[] =
{
  "PRESS [ON]",//eng
  "PRESS [ON]",//fin
  "[ON]",//franc
  "PRESSIONE ON",//Port
  "PRESS [ON]",//Rus
  "PRESIONAR ON",//Esp
  "TRYCK [ON]",//Swed
  "BEST�TIGE",//Deu
  "POTVRDIT ON",//Ceska
  "PRESS [ON]",//Dan
  "PRESS [ON]",//Est
  "PRESS [ON]",//Latv
  "PRESS [ON]",//Liet
  "PRESS [ON]",//Nor
  "PRESS [ON]",//Pol
  "PREMERE [ON]",//Ita
  "PRESS [ON]"//Jpn
};

const char *str_Target[] =
{
  "TARGET!",//eng
  "TARGET!",//fin
  "CIBLE!",//franc
  "ALVO!",//Port
  "TARGET!",//Rus
  "OBJETIVO!",//Esp
  "M�LET",//Swed
  "ZIEL!",//Deu
  "CIL!",//Ceska
  "TARGET!",//Dan
  "TARGET!",//Est
  "TARGET!",//Latv
  "TARGET!",//Liet
  "TARGET!",//Nor
  "TARGET!",//Pol
  "OBIETTIVO!",//Ita
  "TARGET!"//Jpn
};

const char *str_Seq[] =
{
  "SEQ",//eng
  "SEQ",//fin
  "SEQ",//franc
  "SEQ",//Port
  "SEQ",//Rus
  "SEC",//Esp
  "SEK",//Swed
  "SEQ",//Deu
  "SEK",//Ceska
  "SEQ",//Dan
  "SEQ",//Est
  "SEQ",//Latv
  "SEQ",//Liet
  "SEQ",//Nor
  "SEQ",//Pol
  "SEQ",//Ita
  "SEQ"//Jpn
};

const char *str_Sum[] =
{
  "SUM",//eng
  "SUM",//fin
  "SOM",//franc
  "RESUMO",//Port
  "SUM",//Rus
  "SUM",//Esp
  "SUM",//Swed
  "SUM",//Deu
  "SUM",//Ceska
  "SUM",//Dan
  "SUM",//Est
  "SUM",//Latv
  "SUM",//Liet
  "SUM",//Nor
  "SUM",//Pol
  "SUM",//Ita
  "SUM"//Jpn
};

const char *str_menu_LastTarget[] =
{
  "LAST TARGET",//eng
  "LAST TARGET",//fin
  "DERN. CIBLE",//franc
  "ULT. ALVO",//Port
  "LAST TARGET",//Rus
  "ULTIMO OBJ.",//Esp
  "SISTA M�LET",//Swed
  "LETZTE ZIEL",//Deu
  "POSLED. CIL",//Ceska
  "LAST TARGET",//Dan
  "LAST TARGET",//Est
  "LAST TARGET",//Latv
  "LAST TARGET",//Liet
  "LAST TARGET",//Nor
  "LAST TARGET",//Pol
  "ULTIMO OB.",//Ita
  "LAST TARGET"//Jpn
};

const char *str_menu_Reference[] =
{
  "REFERENCE",//eng
  "REFERENCE",//fin
  "REFERENCE",//franc
  "REFERENCIA",//Port
  "REFERENCE",//Rus
  "REFERENCIA",//Esp
  "REFERENS",//Swed
  "REFERENZ",//Deu
  "REFERENCE",//Ceska
  "REFERENCE",//Dan
  "REFERENCE",//Est
  "REFERENCE",//Latv
  "REFERENCE",//Liet
  "REFERENCE",//Nor
  "REFERENCE",//Pol
  "RIFERIMENTO",//Ita
  "REFERENCE"//Jpn
};

const char *str_oled_Q[] =
{
  "?",//eng
  "?",//fin
  "?",//franc
  "?",//Port
  "?",//Rus
  "?",//Esp
  "?",//Swed
  "?",//Deu
  "?",//Ceska
  "?",//Dan
  "?",//Est
  "?",//Latv
  "?",//Liet
  "?",//Nor
  "?",//Pol
  "?",//Ita
  "?"//Jpn
};

const char *str_Length[] =
{
  "LENGTH",//eng
  "LENGTH",//fin
  "LONGUEUR",//franc
  "COMPRIMENTO",//Port
  "LENGTH",//Rus
  "LONGITUD",//Esp
  "L�NGD",//Swed
  "L�NGE",//Deu
  "DELKA",//Ceska
  "LENGTH",//Dan
  "LENGTH",//Est
  "LENGTH",//Latv
  "LENGTH",//Liet
  "LENGTH",//Nor
  "LENGTH",//Pol
  "LUNGHEZZA",//Ita
  "LENGTH"//Jpn
};

const char *str_MagnDecl[] =
{
  "MAGNET.DECL.",//eng
  "MAGNET.DECL.",//fin
  "DECL.MAGNET.",//franc
  "DECL.MAGNET.",//Port
  "MAGNET.DECL.",//Rus
  "DECL.MAGNET.",//Esp
  "MAGNET.DEKL.",//Swed
  "MAGNET.DEKL.",//Deu
  "MAGNET.DEKL.",//Ceska
  "MAGNET.DECL.",//Dan
  "MAGNET.DECL.",//Est
  "MAGNET.DECL.",//Latv
  "MAGNET.DECL.",//Liet
  "MAGNET.DECL.",//Nor
  "MAGNET.DECL.",//Pol
  "DECL.MAGNET.",//Ita
  "MAGNET.DECL."//Jpn
};

const char *str_TargetHeight[] =
{
  "TARGET HGT",//eng
  "TARGET HGT",//fin
  "HAUT CIBLE",//franc
  "ALTURA ALVO",//Port
  "TARGET HGT",//Rus
  "ALTURA OBJ.",//Esp
  "M�LH�JD",//Swed
  "ZIEL H�HE",//Deu
  "VYSKA CIL",//Ceska
  "TARGET HGT",//Dan
  "TARGET HGT",//Est
  "TARGET HGT",//Latv
  "TARGET HGT",//Liet
  "TARGET HGT",//Nor
  "TARGET HGT",//Pol
  "ALTEZZA OB.",//Ita
  "TARGET HGT"//Jpn
};

const char *str_menu_Height_XY[] =
{
  "HEIGHT",//eng
  "HEIGHT",//fin
  "HAUT",//franc
  "ALTURA",//Port
  "HEIGHT",//Rus
  "ALTURA",//Esp
  "H�JD",//Swed
  "H�HE",//Deu
  "VYSKA",//Ceska
  "HEIGHT",//Dan
  "HEIGHT",//Est
  "HEIGHT",//Latv
  "HEIGHT",//Liet
  "HEIGHT",//Nor
  "HEIGHT",//Pol
  "ALTEZZA",//Ita
  "HEIGHT"//Jpn
};

const char *str_menu_Offset[] =
{
  "OFFSET",//eng
  "OFFSET",//fin
  "OBSTACLE",//franc
  "SEM VISADA",//Port
  "OFFSET",//Rus
  "RUTA ALT.",//Esp
  "OFFSET",//Swed
  "HINDERNIS",//Deu
  "ODCHYLKA",//Ceska
  "OFFSET",//Dan
  "OFFSET",//Est
  "OFFSET",//Latv
  "OFFSET",//Liet
  "OFFSET",//Nor
  "OFFSET",//Pol
  "ALTRA ROTTA",//Ita
  "OFFSET"//Jpn
};

const char *str_menu_Laser[] =
{
  "LASER",//eng
  "LASER",//fin
  "LASER",//franc
  "LASER",//Port
  "LASER",//Rus
  "LASER",//Esp
  "LASER",//Swed
  "LASER",//Deu
  "LASER",//Ceska
  "LASER",//Dan
  "LASER",//Est
  "LASER",//Latv
  "LASER",//Liet
  "LASER",//Nor
  "LASER",//Pol
  "LASER",//Ita
  "LASER"//Jpn
};

const char *str_menu_Dme[] =
{
  "DME",//eng
  "DME",//fin
  "DME",//franc
  "DME",//Port
  "DME",//Rus
  "DME",//Esp
  "DME",//Swed
  "DME",//Deu
  "DME",//Ceska
  "DME",//Dan
  "DME",//Est
  "DME",//Latv
  "DME",//Liet
  "DME",//Nor
  "DME",//Pol
  "DME",//Ita
  "DME"//Jpn
};

const char *str_menu_Target[] =
{
  "TARGET",//eng
  "TARGET",//fin
  "CIBLE",//franc
  "ALVO",//Port
  "TARGET",//Rus
  "OBJETIVO",//Esp
  "M�LET",//Swed
  "ZIEL",//Deu
  "CIL",//Ceska
  "TARGET",//Dan
  "TARGET",//Est
  "TARGET",//Latv
  "TARGET",//Liet
  "TARGET",//Nor
  "TARGET",//Pol
  "OBIETTIVO",//Ita
  "TARGET"//Jpn
};

const char *str_Area[] =
{
  "AREA",//eng
  "AREA",//fin
  "SURFACE",//franc
  "AREA",//Port
  "AREA",//Rus
  "AREA",//Esp
  "AREA",//Swed
  "FL�CHE",//Deu
  "PLOCHA",//Ceska
  "AREA",//Dan
  "AREA",//Est
  "AREA",//Latv
  "AREA",//Liet
  "AREA",//Nor
  "AREA",//Pol
  "AREA",//Ita
  "AREA"//Jpn
};

const char *str_Autosave1[] =
{
  "AUTOSAVE",//eng
  "AUTOSAVE",//fin
  "SAUVAUTO",//franc
  "SALVAR",//Port
  "VALOR AUTO-",//Rus
  "AUTOSAVE",//Esp
  "AUTOSPARA",//Swed
  "ZIELE AUTOM.",//Deu
  "AUTOMATICKY",//Ceska
  "AUTOSAVE",//Dan
  "AUTOSAVE",//Est
  "AUTOSAVE",//Latv
  "AUTOSAVE",//Liet
  "AUTOSAVE",//Nor
  "AUTOSAVE",//Pol
  "VALORI AUTO-",//Ita
  "AUTOSAVE"//Jpn
};

const char *str_Autosave2[] =
{
  "VALUES?",//eng
  "VALUES?",//fin
  "VAL?",//franc
  "AUTOMATIC.?",//Port
  "VALUES?",//Rus
  "GUARDADO?",//Esp
  "V�RDEN?",//Swed
  "SPEICHERN?",//Deu
  "ULOZIT?",//Ceska
  "VALUES?",//Dan
  "VALUES?",//Est
  "VALUES?",//Latv
  "VALUES?",//Liet
  "VALUES?",//Nor
  "VALUES?",//Pol
  "SALVATI?",//Ita
  "VALUES?"//Jpn
};


char *str_msg_Log[] =
{
  "LOG",//eng
  "LOG",//fin
  "GPS LOG",//franc
  "LOG",//Port
  "LOG",//Rus
  "REGISTRAR",//Esp
  "LOG",//Swed
  "LOG",//Deu
  "LOG",//Ceska
  "LOG",//Dan
  "LOG",//Est
  "LOG",//Latv
  "LOG",//Liet
  "LOG",//Nor
  "LOG",//Pol
  "REGISTRARE",//Ita
  "LOG"//Jpn
};

char *str_msg_StartEnter[] =
{
  "START\n[ON]",//eng
  "START\n[ON]",//fin
  "DEMAR\n[ON]",//franc
  "INICIAR\n[ON]",//Port
  "START\n[ON]",//Rus
  "START\n[ON]",//Esp
  "STARTA\n[ON]",//Swed
  "START\n[ON]",//Deu
  "START\n[ON]",//Ceska
  "START\n[ON]",//Dan
  "START\n[ON]",//Est
  "START\n[ON]",//Latv
  "START\n[ON]",//Liet
  "START\n[ON]",//Nor
  "START\n[ON]",//Pol
  "START\n[ON]",//Ita
  "START\n[ON]"//Jpn
};

char *str_msg_SaveKmlData[] =
{
  "SAVE KML\nDATA",//eng
  "SAVE KML\nDATA",//fin
  "ENR. KML\nDATA",//franc
  "SAVA\nARQUIVO\nKML",//Port
  "SAVE KML\nDATA",//Rus
  "SALVAR\nDATOS KML",//Esp
  "SPARA KML\nDATA",//Swed
  "KML.DATEI\nSPEICHERN",//Deu
  "ULOZ KML\nDATA",//Ceska
  "SAVE KML\nDATA",//Dan
  "SAVE KML\nDATA",//Est
  "SAVE KML\nDATA",//Latv
  "SAVE KML\nDATA",//Liet
  "SAVE KML\nDATA",//Nor
  "SAVE KML\nDATA",//Pol
  "SALVA\nDATI KML",//Ita
  "SAVE KML\nDATA"//Jpn
};

char *str_msg_SaveCsvData[] =
{
  "SAVE CSV\nDATA",//eng
  "SAVE CSV\nDATA",//fin
  "ENR. CSV\nDATA",//franc
  "SAVA\nARQUIVO\nCSV",//Port
  "SAVE CSV\nDATA",//Rus
  "SALVAR\nDATOS CSV",//Esp
  "SPARA CSV\nDATA",//Swed
  "CSV.DATEI\nSPEICHERN",//Deu
  "ULOZ CSV\nDATA",//Ceska
  "SAVE CSV\nDATA",//Dan
  "SAVE CSV\nDATA",//Est
  "SAVE CSV\nDATA",//Latv
  "SAVE CSV\nDATA",//Liet
  "SAVE CSV\nDATA",//Nor
  "SAVE CSV\nDATA",//Pol
  "SALVA\nDATI CSV",//Ita
  "SAVE CSV\nDATA"//Jpn
};

char *str_msg_SaveJsonData[] =
{
  "SAVE JSON\nDATA",//eng
  "SAVE JSON\nDATA",//fin
  "ENR. JSON\nDATA",//franc
  "SAVA\nARQUIVO\nJSON",//Port
  "SAVE JSON\nDATA",//Rus
  "SALVAR\nDATOS JSON",//Esp
  "SPARA JSON\nDATA",//Swed
  "JSON.DATEI\nSPEICHERN",//Deu
  "ULOZ JSON\nDATA",//Ceska
  "SAVE JSON\nDATA",//Dan
  "SAVE JSON\nDATA",//Est
  "SAVE JSON\nDATA",//Latv
  "SAVE JSON\nDATA",//Liet
  "SAVE JSON\nDATA",//Nor
  "SAVE JSON\nDATA",//Pol
  "SALVA\nDATI JSON",//Ita
  "SAVE JSON\nDATA"//Jpn
};

char *str_msg_NoGps[] =
{
  "NO GPS",//eng
  "NO GPS",//fin
  "PAS GPS",//franc
  "SEM GPS",//Port
  "NO GPS",//Rus
  "NO GPS",//Esp
  "INGEN GPS",//Swed
  "KEIN GPS",//Deu
  "ZADNA GPS",//Ceska
  "NO GPS",//Dan
  "NO GPS",//Est
  "NO GPS",//Latv
  "NO GPS",//Liet
  "NO GPS",//Nor
  "NO GPS",//Pol
  "NESSUN GPS",//Ita
  "NO GPS"//Jpn
};

const char *str_delete_all[] =
{
"DELETE ALL",//eng
"DELETE ALL",//fin
"DELETE ALL",//franc
"DELETE ALL",//Port
"DELETE ALL",//Rus
"DELETE ALL",//Esp
"RADERA ALLA",//Swed
"ALLES L�SCH",//Deu
"DELETE ALL",//Ceska
"DELETE ALL",//Dan
"DELETE ALL",//Est
"DELETE ALL",//Latv
"DELETE ALL",//Liet
"DELETE ALL",//Nor
"DELETE ALL",//Pol
"DELETE ALL",//Ita
"DELETE ALL"//Jpn
};

const char *str_msg_SureQ[] =
{
"SURE?",//eng
"SURE?",//fin
"SURE?",//franc
"SURE?",//Port
"SURE?",//Rus
"SURE?",//Esp
"S�KERT?",//Swed
"SICHER?",//Deu
"SURE?",//Ceska
"SURE?",//Dan
"SURE?",//Est
"SURE?",//Latv
"SURE?",//Liet
"SURE?",//Nor
"SURE?",//Pol
"SURE?",//Ita
"SURE?"//Jpn
};

const char *str_msg_SureQQ[] =
{
"SURE??",//eng
"SURE??",//fin
"SURE??",//franc
"SURE??",//Port
"SURE??",//Rus
"SURE??",//Esp
"S�KERT??",//Swed
"SICHER??",//Deu
"SURE??",//Ceska
"SURE??",//Dan
"SURE??",//Est
"SURE??",//Latv
"SURE??",//Liet
"SURE??",//Nor
"SURE??",//Pol
"SURE??",//Ita
"SURE??"//Jpn
};


//VLG2-2 Diameterm�tning
const char *str_Dia_Diameter[] =
{
"DIAMETER",//eng
  "HALKAISIJA",//fi
  "DIAMETRE",//Fra
  "DIAMETRO",//Por
  "DIAMETER",//Rus
  "DIAMETRO",//Spa
  "DIAMETER",//Sve
  "DURCHMESSER",//Deu
  "PRUMER",//tcech
  "DIAMETER",//dan
  "L�BIM��T",//est
  "DIAMETERS",//let
  "SKERSMENS",//lit
  "DIAMETER",//nor
  "SREDNICA",//Pol
  "DIAMETRO",//Ita
  "DIAMETER",//Jpn
};

const char *str_Dia_Height[] =
{
  "HEIGHT",//eng
  "HEIGHT",//fin
  "HAUT",//franc
  "ALTURA",//Port
  "HEIGHT",//Rus
  "ALTURA",//Esp
  "H�JD",//Swed
  "H�HE",//Deu
  "VYSKA",//Ceska
  "HEIGHT",//Dan
  "HEIGHT",//Est
  "HEIGHT",//Latv
  "HEIGHT",//Liet
  "HEIGHT",//Nor
  "HEIGHT",//Pol
  "ALTEZZA",//Ita
  "HEIGHT"//Jpn
};

const char *str_Fixed_Dia[] =
{
"FIXED DIAM. ",//eng
  "VAK. HALK.  ",//fi
  "DIAM. FIXE. ",//Fra
  "DIAM. FIXO  ",//Por
  "FIXED DIAM. ",//Rus
  "DIAM. FIJO  ",//Spa
  "FAST DIAM.  ",//Sve
  "FESTER DUR. ",//Deu
  "SABIT CAP   ",//tcech
  "FAST DIAM.  ",//dan
  "FIKSE. L�BI ",//est
  "FIKSETS DIA.",//let
  "FIKS. SKERS.",//lit
  "FAST DIAM.  ",//nor
  "STALA SRED. ",//Pol
  "DIAM. FISSO ",//Ita
  "FIXED DIAM. ",//Jpn
};

const char *str_oled_Dia[] =
{
"DIA",//eng
"DIA",//fin
"DIA",//franc
"DIA",//Port
"DIA",//Rus
"DIA",//Esp
"DIA",//Swed
"DIA",//Deu
"DIA",//Ceska
"DIA",//Dan
"DIA",//Est
"DIA",//Latv
"DIA",//Liet
"DIA",//Nor
"DIA",//Pol
"DIA",//Ita
"DIA"//Jpn
};

const char *str_oled_H[] =
{
"H",//eng
"H",//fin
"H",//franc
"A",//Port
"H",//Rus
"A",//Esp
"H",//Swed
"H",//Deu
"V",//Ceska
"H",//Dan
"H",//Est
"H",//Latv
"H",//Liet
"H",//Nor
"H",//Pol
"A",//Ita
"H"//Jpn
};


const char *str_Dia_Bar1[] =
{
"ADJUST THE",//eng
"S��D� N�YTT.",//fi
"AJUSTEZ LA",//Fra
"AJUSTE A",//Por
"ADJUST THE",//Rus
"AJUSTE LA",//Spa
"JUSTERA",//Sve
"STELLEN SIE",//Deu
"UPRAVTE",//tcech
"JUSTER",//dan
"REGULEERIGE",//est
"PIELAGOJIET",//let
"REGULIUOKITE",//lit
"JUSTER",//nor
"DOSTOSUJ",//Pol
"REGOLARE IL",//Ita
"ADJUST THE",//Jpn
};
const char *str_Dia_Bar2[] =
{
"SIGHT WITH",//eng
"DME/SEND",//fi
"VUE AVEC LES",//Fra
"VISTA COM OS",//Por
"SIGHT WITH",//Rus
"MIRA CON LOS",//Spa
"SIKTET MED",//Sve
"DAS VISIER MIT",//Deu
"ZAMERENI",//tcech
"SIKTET MED",//dan
"SIHTIKKU",//est
"TEMEKLI AR",//let
"TEIKKLA SU",//lit
"SIKTET MED",//nor
"WIDOK ZA",//Pol
"MIRINO CON",//Ita
"SIGHT WITH",//Jpn
};
const char *str_Dia_Bar3[] =
{
"DME/SEND",//eng
"PAINIKKEILLA",//fi
"DME/SEND",//Fra
"BOTOES DME/SEND",//Por
"DME/SEND",//Rus
"DME/SEND",//Spa
"DME/SEND",//Sve
"DME/SEND EIN",//Deu
"TLA.DME/SEND",//tcech
"MED DME/SEND",//dan
"DME/SEND",//est
"DME/SEND",//let
"DME/SEND",//lit
"DME/SEND",//nor
"POM.DME/SEND",//Pol
"DME/SEND",//Ita
"DME/SEND",//Jpn
};

//ut�kat till 10 tecken
const char *str_sel_func_names_vl[][12+1] =     //VLG2-14 lagt till Reversed Compass, texter    //VLG2-15 lagt till Baf
{
  {"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//eng
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//fin
{"HAUT 1PL","HAUT 3P","HAUT DME","3D VECTEUR","BOUSSOLE","BOUSS. INV","ANGLE","CARTE OBJ","CARTE RT.","ARB.RISQUE","DELTA HAUT","BAF",""},//franc
{"ALTURA 1PL","ALTURA 3P","ALTURA DME","VETOR 3D","BUSSOLA","BUSS. INV.","ANGULO","LASER MAP","MAP.TRILHA","RISCO EM LT","ALT.FLECHA","BAF",""},//Port
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Rus
{"ALTURA 1PL","ALTURA 3P","ALTURA DME","VECTOR 3D","BRUJULA","BRUJ. INV.","ANGULO","MAPEO OBJ","MAPEO RUTA","LINE CLEAR","ALT.DELTA","BAF",""},//Esp
{"H�JD 1PL","H�JD 3P","H�JD DME","3D VEKTOR","KOMPASS","KOMP. REV.","VINKEL","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA H�JD","BAF",""},//Swed
{"H�HE 1PL","H�HE 3P","H�HE DME","3D VEKTOR","KOMPASS","KOMP. INV.","WINKEL","OBJ. KART","ROUTE KART","RISIKOBAUM","DELTA H�HE","BAF",""},//Deu
{"VYSKA 1PL","VYSKA 3P","VYSKA DME","3D VEKTOR","KOMPAS","KOMP. OBR.","SKLON","MAP CIL","MAP TRASA","RISK.STROM","PRUVES","BAF",""},//Ceska
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Dan
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Est
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Latv
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Liet
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Nor
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Pol
{"ALT. 1PL","ALT. 3P","ALT. DME","VETTORE 3D","BUSSOLA","BUSS. INV.","ANGOLO","MAP OGGETT","MAP. ROTTA","LINE CLEAR","ALT. DELTA","BAF",""},//Ita
{"HEIGHT 1PL","HEIGHT 3P","HEIGHT DME","3D VECTOR","COMPASS","COMP. REV","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""}//Jpn
};

const char *str_sel_func_names_l[][11+1] =
{
  {"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//eng
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//fin
{"HAUT 1PL","HAUT 3P","3D VECTEUR","BOUSSOLE","BOUSS. INV","ANGLE","CARTE OBJ","CARTE RT.","ARB.RISQUE","DELTA HAUT","BAF",""},//franc
{"ALTURA 1PL","ALTURA 3P","VETOR 3D","BUSSOLA","BUSS. INV.","ANGULO","LASER MAP","MAP.TRILHA","RISCO EM LT","ALT.FLECHA","BAF",""},//Port
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Rus
{"ALTURA 1PL","ALTURA 3P","VECTOR 3D","BRUJULA","BRUJ. INV.","ANGULO","MAPEO OBJ","MAPEO RUTA","LINE CLEAR","ALT.DELTA","BAF",""},//Esp
{"H�JD 1PL","H�JD 3P","3D VEKTOR","KOMPASS","KOMP. REV.","VINKEL","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA H�JD","BAF",""},//Swed
{"H�HE 1PL","H�HE 3P","3D VEKTOR","KOMPASS","KOMP. INV.","WINKEL","OBJ. KART","ROUTE KART","RISIKOBAUM","DELTA H�HE","BAF",""},//Deu
{"VYSKA 1PL","VYSKA 3P","3D VEKTOR","KOMPAS","KOMP. OBR.","SKLON","MAP CIL","MAP TRASA","RISK.STROM","PRUVES","BAF",""},//Ceska
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Dan
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Est
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Latv
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Liet
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Nor
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV.","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""},//Pol
{"ALT. 1PL","ALT. 3P","VETTORE 3D","BUSSOLA","BUSS. INV.","ANGOLO","MAP OGGETT","MAP. ROTTA","LINE CLEAR","ALT. DELTA","BAF",""},//Ita
{"HEIGHT 1PL","HEIGHT 3P","3D VECTOR","COMPASS","COMP. REV","ANGLE","MAP TARGET","MAP TRAIL","LINE CLEAR","DELTA HGT","BAF",""}//Jpn
};

const char *str_menu_SelectFuncs[] =
{
  "MENUS",//eng
  "MENUS",//fin
  "MENUS",//franc       //Menus
  "MENUS",//Port
  "MENUS",//Rus
  "MENUS",//Esp         //Men�s
  "MENYER",//Swed
  "MEN�S",//Deu
  "MENUS",//Ceska
  "MENUS",//Dan
  "MENUS",//Est
  "MENUS",//Latv
  "MENUS",//Liet
  "MENUS",//Nor
  "MENUS",//Pol
  "MENUS",//Ita
  "MENUS"//Jpn
};

//VLG2-15 Baf
const char *str_menu_BAF[] =    //TODO: �vers�tt nya texter
{
"BAF       ",//eng
"BAF       ",//fin
"BAF       ",//franc
"BAF       ",//Port
"BAF       ",//Rus
"BAF       ",//Esp
"BAF       ",//Swed
"BAF       ",//Deu
"BAF       ",//Ceska
"BAF       ",//Dan
"BAF       ",//Est
"BAF       ",//Latv
"BAF       ",//Liet
"BAF       ",//Nor
"BAF       ",//Pol
"BAF       ",//Ita
"BAF       "//Jpn
};

char *str_msg_BAF[] =
{
"BAF",//eng
"BAF",//fin
"BAF",//franc
"BAF",//Port
"BAF",//Rus
"BAF",//Esp
"BAF",//Swed
"BAF",//Deu
"BAF",//Ceska
"BAF",//Dan
"BAF",//Est
"BAF",//Latv
"BAF",//Liet
"BAF",//Nor
"BAF",//Pol
"BAF",//Ita
"BAF"//Jpn
};

const char *str_Factor[] =
{
"FACTOR",//eng
"FACTOR",//fin
"FACTOR",//franc
"FACTOR",//Port
"FACTOR",//Rus
"FACTOR",//Esp
"FACTOR",//Swed
"FACTOR",//Deu
"FACTOR",//Ceska
"FACTOR",//Dan
"FACTOR",//Est
"FACTOR",//Latv
"FACTOR",//Liet
"FACTOR",//Nor
"FACTOR",//Pol
"FACTOR",//Ita
"FACTOR"//Jpn
};

const char *str_TotNum[] =
{
"TOT NUM",//eng
"TOT NUM",//fin
"TOT NUM",//franc
"TOT NUM",//Port
"TOT NUM",//Rus
"TOT NUM",//Esp
"TOT NUM",//Swed
"TOT NUM",//Deu
"TOT NUM",//Ceska
"TOT NUM",//Dan
"TOT NUM",//Est
"TOT NUM",//Latv
"TOT NUM",//Liet
"TOT NUM",//Nor
"TOT NUM",//Pol
"TOT NUM",//Ita
"TOT NUM"//Jpn
};

const char *str_TotBA[] =
{
"TOT BA",//eng
"TOT BA",//fin
"TOT BA",//franc
"TOT BA",//Port
"TOT BA",//Rus
"TOT BA",//Esp
"TOT BA",//Swed
"TOT BA",//Deu
"TOT BA",//Ceska
"TOT BA",//Dan
"TOT BA",//Est
"TOT BA",//Latv
"TOT BA",//Liet
"TOT BA",//Nor
"TOT BA",//Pol
"TOT BA",//Ita
"TOT BA"//Jpn
};

char *str_msg_MaxBAF[] =
{
"MAX 25",//eng
"MAX 25",//fin
"MAX 25",//franc
"MAX 25",//Port
"MAX 25",//Rus
"MAX 25",//Esp
"MAX 25",//Swed
"MAX 25",//Deu
"MAX 25",//Ceska
"MAX 25",//Dan
"MAX 25",//Est
"MAX 25",//Latv
"MAX 25",//Liet
"MAX 25",//Nor
"MAX 25",//Pol
"MAX 25",//Ita
"MAX 25"//Jpn
};

char *str_msg_MaxBAFft[] =
{
"MAX 109",//eng
"MAX 109",//fin
"MAX 109",//franc
"MAX 109",//Port
"MAX 109",//Rus
"MAX 109",//Esp
"MAX 109",//Swed
"MAX 109",//Deu
"MAX 109",//Ceska
"MAX 109",//Dan
"MAX 109",//Est
"MAX 109",//Latv
"MAX 109",//Liet
"MAX 109",//Nor
"MAX 109",//Pol
"MAX 109",//Ita
"MAX 109"//Jpn
};

char *str_msg_MinBAF[] =
{
"SET BAF",//eng
"SET BAF",//fin
"SET BAF",//franc
"SET BAF",//Port
"SET BAF",//Rus
"SET BAF",//Esp
"SET BAF",//Swed
"SET BAF",//Deu
"SET BAF",//Ceska
"SET BAF",//Dan
"SET BAF",//Est
"SET BAF",//Latv
"SET BAF",//Liet
"SET BAF",//Nor
"SET BAF",//Pol
"SET BAF",//Ita
"SET BAF"//Jpn
};

const char *str_SightType[] =
{
"SIGHT TYPE",//eng
"SIGHT TYPE",//fin
"TYPE DE VUE",//franc
"TIPO VISAO",//Port
"SIGHT TYPE",//Rus
"TIPO  VISTA",//Esp
"TYP AV SIKTE",//Swed
"ANSICHTSTYP",//Deu
"TYP POHLEDU",//Ceska
"UDSIGTSTYPE",//Dan
"SIGHT TYPE",//Est
"SIGHT TYPE",//Latv
"SIGHT TYPE",//Liet
"SIGHT TYPE",//Nor
"SIGHT TYPE",//Pol
"TIPO   VISTA",//Ita
"SIGHT TYPE"//Jpn
};

//VLG2-16 Obex
const char *str_menu_Bluetooth[] =
{
"BLUETOOTH",//eng
"BLUETOOTH",//fin
"BLUETOOTH",//franc
"BLUETOOTH",//Port
"BLUETOOTH",//Rus
"BLUETOOTH",//Esp
"BLUETOOTH",//Swed
"BLUETOOTH",//Deu
"BLUETOOTH",//Ceska
"BLUETOOTH",//Dan
"BLUETOOTH",//Est
"BLUETOOTH",//Latv
"BLUETOOTH",//Liet
"BLUETOOTH",//Nor
"BLUETOOTH",//Pol
"BLUETOOTH",//Ita
"BLUETOOTH"//Jpn
};

char *str_msg_UpdateBios[] =
{
"UPDATE\nBIOS",//eng
"UPDATE\nBIOS",//fin
"UPDATE\nBIOS",//franc
"UPDATE\nBIOS",//Port
"UPDATE\nBIOS",//Rus
"UPDATE\nBIOS",//Esp
"UPDATE\nBIOS",//Swed
"UPDATE\nBIOS",//Deu
"UPDATE\nBIOS",//Ceska
"UPDATE\nBIOS",//Dan
"UPDATE\nBIOS",//Est
"UPDATE\nBIOS",//Latv
"UPDATE\nBIOS",//Liet
"UPDATE\nBIOS",//Nor
"UPDATE\nBIOS",//Pol
"UPDATE\nBIOS",//Ita
"UPDATE\nBIOS"//Jpn
};

/*
const char *str_[] =
{
"",//eng
"",//fin
"",//franc
"",//Port
"",//Rus
"",//Esp
"",//Swed
"",//Deu
"",//Ceska
"",//Dan
"",//Est
"",//Latv
"",//Liet
"",//Nor
"",//Pol
"",//Ita
""//Jpn
};
*/


