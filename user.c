#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include"user.h"


#include"geo.h"
/*********************************************************************************************************/
/*TEXT ROUTINS*/

/* DosToWinText   */
/* JN 051115 V1.0 */
char *DosToWinText(char *str)
{
  char ch,*p=str;
  while(ch=*str)
  {
   if(ch=='�')
     *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   str++;
  }
 return  p;
}

/* WinToDosText   */
/* JN 051115 V1.0 */
char *WinToDosText(char *str)
{
  char ch,*p=str;
  while(ch=*str)
  {
   if(ch=='�')
     *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   else if(ch=='�')
    *str='�';
   str++;
  }
 return  p;
}

/*********************************************************************************************************/
/* SERIAL ROUTINS */
#include<stdio.h>
#include<string.h>
//#include<intrinsic.h>
//#include <yfuns.h>
#include "stdarg.h"

/*ComPrintf() */
/* Formatted string to comport */
/* JN 051121                    */
int ComPrintf(const char *fmt, ...)
{
 int i;
 char s[256];
 va_list ap;
 va_start(ap,fmt);
 i=vsprintf(s,fmt,ap);
 SerialTextOut(s);
 va_end(ap);
 return i;
}


/*********************************************************************************************************/
//Fileroutins

// int ReadOrCreateIniFile(char *FileName,unsigned char *InitData,int SizeOfInitData)
// BIOS Vers>=V1.08
// Set  inifile or read if it exist
// Set InitData to default before calling this function
// JN 051121
// Returns:
// Num of bytes read/write or:
// READ_ERROR -2
// WRITE_ERROR -3

int ReadOrCreateIniFile(char *FileName,void  *InitData,int SizeOfInitData)
{
 int handle=-1,res=READ_ERROR;
  if((handle=open(FileName,_LLIO_RDWR))<0)
   {
    if((handle=create(FileName,_LLIO_RDWR))<0)
      res=WRITE_ERROR;
     else
    if((res=write(handle,(unsigned char *)InitData,SizeOfInitData))<SizeOfInitData)
     res=WRITE_ERROR;
   }
  else
  {
   if(lseek(handle,0,SEEK_SET)>=0)
    res=read(handle,(unsigned char *)InitData,SizeOfInitData);
  }

 close(handle);
 return res;
}

//*******************************************************************
// int WriteIniFile(char *FileName,void  *InitData,int SizeOfInitData)
// BIOS Vers>=V1.08
// Set inifile
// Set InitData to default before calling this function
// JN 051121
// Returns:
// Num of bytes read/write or:
// READ_ERROR -2
// WRITE_ERROR -3
int WriteIniFile(char *FileName,void  *InitData,int SizeOfInitData)
{
 int handle=-1,res=READ_ERROR;
 if((handle=open(FileName,_LLIO_RDWR))<0)
   {
    if((handle=create(FileName,_LLIO_RDWR))<0)
     res=WRITE_ERROR;
    else if((res=write(handle,(unsigned char *)InitData,SizeOfInitData))<SizeOfInitData)
     res=WRITE_ERROR;
   }
  else
  {
   if(lseek(handle,0,SEEK_SET)>=0)
    if((res=write(handle,(unsigned char *)InitData,SizeOfInitData))<SizeOfInitData)
     res=WRITE_ERROR;
  }
 close(handle);
 return res;
}

//*******************************************************************
// int DeleteExplorerFile(char* filenameext,char* DelStr)
// BIOS Vers>=V1.08
// Delete one or all files, from Explorer, with extension filenameext
// MK 051121
//*******************************************************************
int DeleteExplorerFile(char* filenameext,char* DelStr)
{
  char filename[FILENAMESIZE+1];
  strcpy(filename,"*");
  strcat(filename,filenameext);

  Cls();
  if(Explorer(filename)==FILEOPEN)
    if(YesNo(0,DelStr)==1)
      if(YesNo(0,DelStr)==1)
        if(DeleteFile(filename)==FILEDELETE)
        {
          return 1;
        }

  Cls();
  return 0;
}




/*********************************************************************************************************/
/* Hardware routins */
// BIOS Vers>=V1.08
int freset()
{
 return SysReset();
}

/***************************************************************************************************************/
/* Input routins                                                                                               */
/***************************************************************************************************************/


/********************************************************************/
/* GetInt(int x,int y,int num,int pos,int decimal)                  */
/* String input, return int or -1                                   */
/* Input format  mm.cc  (string input with cursor)                  */
/* Jn 051129                                                        */
/********************************************************************/
int GetInt(int x,int y,int num,int pos,int decimal)
{
 int i,ch,integer=pos-decimal-1,num_ten;
 char tintstr[10],tdecimalstr[10],i_or_d;
 struct disp_enter_string_type tint={0,0,0,0,0,"","0123456789",0};
 struct disp_enter_string_type tdecimal={0,0,0,0,0,"","0123456789",0};

 tint.x = x;
 tint.y = y;
 tint.maxpos = integer;
 tint.str = tintstr;

 tdecimal.x = x+integer+1;
 tdecimal.y = y;
 tdecimal.maxpos = decimal;
 tdecimal.str = tdecimalstr;


 if(decimal<=0)
 {
  struct disp_enter_string_type tintnodecimal={0,0,0,0,0,"","0123456789",0};
  tintnodecimal.x = x;
  tintnodecimal.y = y;
  tintnodecimal.maxpos = pos;
  tintnodecimal.str = tintstr;

  sprintf(tintstr,"%0*.*d",pos,pos,num);
  do
  ch=DispEnterString(&tintnodecimal);
  while(ch!=KEY_EXIT&&ch!=KEY_ENTER);
  if(ch==KEY_EXIT)
   return -1;
  return atoi(tintstr);
 };
 for(num_ten=1,i=0;i<decimal;i++)
  num_ten*=10;

 sprintf(tintstr,"%0*.*d",integer,integer,num/num_ten);
 sprintf(tdecimalstr,"%0*.*d",decimal,decimal,num%num_ten);
 xprintf(x,y,0,10,"%s.%s",tintstr,tdecimalstr);
 i_or_d=1;
 do
{
 if(i_or_d)
  ch=DispEnterString(&tint);
 else
  ch=DispEnterString(&tdecimal);
 switch(ch)
 {
  case KEY_LEFT:
  case KEY_RIGHT:
  i_or_d^=1;
  break;
  case KEY_EXIT:
  return -1;
 }
}
 while(ch!=KEY_ENTER);
 return atoi(tintstr)*num_ten+atoi(tdecimalstr);

}


/********************************************************************/
/* int GetRadie(int x,int y,int radie)                              */
/* return radiex100 (cm) or -1                                      */
/* Input format  mm.cc                                              */
/* Jn 051129                                                        */
/********************************************************************/
int GetRadius(int x,int y,int radie)
{
 return GetInt(x,y,radie,5,2);
}

#define MAXCHAR 12
void format(char *s,int num,int numdigits,int numdecimal,char PadCh)
{
  char ts[MAXCHAR+1];
  int i,n,neg;
  if(num<0)
  {
    neg=1;
    num=-num;
  }
  else
    neg=0;

  if(numdigits>MAXCHAR)
  {
    s[0]=0;
    return;
  }

  sprintf(ts,"%d",num);
  n=strlen(ts);
  if(n>=MAXCHAR)
  {
    s[0]=0;
    return;
  }

  for(i=1;i<=numdigits;i++)
  {
    if(i<=n)
      s[numdigits-i]=ts[n-i];
    else
      s[numdigits-i]=PadCh;

  }

  if(numdecimal)
  {
    for(i=0;i<numdigits-numdecimal-1;i++)
    {
      s[i]=s[i+1];
    }
    if(s[i-1]==PadCh)
      s[i-1]='0';

      s[i++]='.';//anv ej men...
    for(;i<numdigits;i++)
      if(!isdigit(s[i]))
        s[i]='0';
  }
  s[numdigits]=0;

  if(neg)
      s[0]='-';
}

/**********************************
*
*
***********************************/
void putxy(int x,int y,char *str)
{
  struct disp_text_type p={x,y,0,strlen(str),str};
  TextOut(&p);
}

/**********************************
*
*
***********************************/
void putxy2(int x,int y,char *str)
{
  struct disp_text_type p={x,y,0,strlen(str),str};
  TextOut2(&p);
}

/**********************************
*
*
***********************************/
void putxy3(int x,int y,int type,char *str)
{
  struct disp_text_type p={x,y,type,strlen(str),str};
  TextOut(&p);
}

/**********************************
*
*
***********************************/
void putint(int tx,int ty,int num,int numdigits,int numdecimal)
{
  char s[MAXCHAR+1];
  format(s,num,numdigits,numdecimal,' ');
  putxy(tx,ty,s);
}

/**********************************
*
*
***********************************/
void putintzero(int tx,int ty,int num,int numdigits,int numdecimal)
{
  char s[MAXCHAR+1];
  format(s,num,numdigits,numdecimal,'0');
  putxy(tx,ty,s);
}

/**********************************
*
*
***********************************/
void putint2(int tx,int ty,int num,int numdigits,int numdecimal)
{
  char s[MAXCHAR+1];
  format(s,num,numdigits,numdecimal,' ');
  putxy2(tx,ty,s);
}

/**********************************
*
*
***********************************/
int xprintf(int tx,int ty,int type,int len,const char *fmt, ...)
{

 int i;
 char s[256];

 va_list ap;
 va_start(ap,fmt);
 i=vsprintf(s,fmt,ap);
 if(len<256)
  s[len]=0;
putxy3( tx, ty, type, s);

 va_end(ap);
 return i;

}

/**********************************
*
*
***********************************/
int scankey()
{
  return GetButtons();
}

/**********************************
*
*
***********************************/
int scankeych()
{
  return GetButtons()+'0';
}

/**********************************
*
*
***********************************/
int getchar()
{
  return GetSystemKey();
}

/**********************************
*
*
***********************************/
int WhileEnterPressed()
{
  while(GetButtons()&(KEY_ENTER-'0'))
    Idle();
  return 0;
}

/**********************************
*
*
***********************************/
int SelectString(int tx,int ty,const char *str[],int *act,char n)
{
  int ch,i=*act;
  do
  {
    xprintf(tx,ty,DISP_REVERSE,strlen(str[i]),str[i]);

    ch=GetSystemKey();
    xprintf(tx,ty,0,strlen(str[i]),str[i]);
    switch(ch)
    {
    case KEY_LEFT:
      if(--i<0){
        i=n-1;
      }
      break;
    case KEY_RIGHT:
      if(++i>=n){
        i=0;
      }
      break;
    case 0:
    case KEY_EXIT:
      return KEY_EXIT;
    case KEY_ENTER:
      *act=i;
      return KEY_ENTER;
    default:
      break;
    }
  }while(1);
}


/**********************************
*
*
***********************************/
void My_Bell(int ms)
{
  Bell(ms);
  Wait(ms);
}

/**********************************
*
*
***********************************/
void My_bell2(int ms, int freq)
{
  bell(ms,freq);
  wait(ms);
}

/**********************************
*
*
***********************************/
/**********************************
Change number, 0-9
***********************************/
int My_InputNum_(int tx,int ty,int num)
{
  int ch;

  do
  {
    if(num<0){
      num=9;
    }
    else if(num>9){
      num=0;
    }

    putint(tx,ty,num,1,0);
    PutCursor(tx,ty);
    Cursor(1);
    ch=GetSystemKey();
    Cursor(0);
    putint(tx,ty,num,1,0);

    switch(ch)
    {
    case KEY_RIGHT:
      num+=1;
      break;
    case KEY_LEFT:
      num-=1;
      break;
    case 0:
    case KEY_EXIT:
      return -1;
    case KEY_ENTER:
      return num;
    default:
      break;
    }
  }while(1);
}

/**********************************
Input number, 1 decimal
***********************************/
int My_InputNum(int tx,int ty,int num,int digits)
{
 int i,temp,sum,t10;

 t10=1;
 for(i=0; i<(digits-1); i++){
  t10*=10;
 }
 sum=0;
 putint(tx,ty,num,digits+1,1);//1 decimal
 for(i=0;i<digits;i++)
 {
  if((temp=My_InputNum_(tx+i+(i==digits-1),ty,num/t10))<0){
   return -1;
  }
  num=num-(num/t10*t10);
  sum+=(temp*t10);
  t10/=10;
 }
 return sum;
}

/**********************************
*
* VLG2-15 skriver ut 0 ist�llet f�r mellanslag
***********************************/
int My_InputNumZero(int tx,int ty,int num,int digits)
{
 int i,temp,sum,t10;

 t10=1;
 for(i=0; i<(digits-1); i++){
  t10*=10;
 }
 sum=0;
 putintzero(tx,ty,num,digits+1,1);//1 decimal
 for(i=0;i<digits;i++)
 {
  if((temp=My_InputNum_(tx+i+(i==digits-1),ty,num/t10))<0){
   return -1;
  }
  num=num-(num/t10*t10);
  sum+=(temp*t10);
  t10/=10;
 }
 return sum;
}


/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/


