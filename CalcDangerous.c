#include "MyFuncs.h"

extern const char unit_strM[];
extern const char unit_strFT[];

static int cdelta, cdist,hcdist;

int DangTreeFastManHgt(void);      //VLG-30 Line Clear med manuell inmatning av ledningsh�jden

/**********************************
*
*
***********************************/
int DangTreeMenu()
{
  static int res=0;
  int tres;
  const char *p[]={str_menu_LineClear_,str_menu_MinDist_,str_menu_Exit_,""};
  struct disp_meny_type tmeny={0,0,3,0,11,str_menu_LineClear_,p};

  //VLG-30 anv�nder CalcMenu1 f�r att komma ih�g valet i Line Clear-menyn, ej Exit
  res = ee->CalcMenu1;

  if(res > 1)
    res=0;

  tmeny.act_item=res;
tres=res;

  do
  {
  cls();
   res=Disp_Menu(&tmeny);
   switch(res)
   {
   case 0:
     DangTreeFast();
     break;
   case 1:
      DangTree3P();
      break;
   default:
     if(res<2 && tres!=res)     //Spara ej om Exit
     {
       ee->CalcMenu1=res;
      SetGeoSettings();//save ee
     }
    return 1;
   }
  }while(1);
}

/**********************************
Calculate angle
***********************************/
int CalcBeta(int ialpha,int isd/*dm*/,int trpH /*dm*/)
{// When use trpH
 float b,talpha,tbeta,sd;
 sd=isd;
 talpha=ialpha/18000.0*MA_PI;
 b=sd*sin(talpha)-trpH;
 tbeta=atan(b/sd/cos(talpha))*18000.0/MA_PI;
 if(tbeta<0){
  return (int)(tbeta-.5);
 }
 return (int)(tbeta+.5);
}

/***************************************
                    x hline
    -__             |
   |   --__         |
 |   |     --__     | c
_______        --_  |
   |-----------__-O | href
   |              | |
   |___---- b     | |

****************************************/

/**********************************
#3947 Calculate safety length Fast method
***********************************/
int safetylen_Fast(int ialpha,int ibeta,int isd/*dm alt ftx10*/)
{
 float b,c,talpha,tbeta,res,sd,tdelta;
 sd=isd;
 talpha=ialpha/18000.0*MA_PI;
 tbeta=ibeta/18000.0*MA_PI;
 b=cos(talpha)*sd/cos(tbeta);

 tdelta = cdelta/18000.0*MA_PI;
 c = cdist*1.0;

 res=sqrt(b*b+c*c-2.0*b*c*cos(tdelta-tbeta));
 return (int)(res+.5);
}

void MenuRedrawManHgt()
{
  cls();
  putxy(0,0,(char*)str_manual_object_hgt1_);
  putxy(0,1,(char*)str_manual_object_hgt2_);
}

/**********************************
// VLG-30 Fast Dangerous Tree, Manual Line Height
***********************************/
int DangTreeFastManHgt(void)    //Om trycker Exit p� 1pl-m�tningen s� f�r d� ange fash�jden manuellt, exit igen f�r att avbryta
{
  int mh;

  irset();
  cdelta=0;
  cdist=0;
  hcdist=0;

  mh=ee->man_hgt;        //senaste v�rdet finns lagrat i EE
  MenuRedrawManHgt();
  if(Metric())
  {
    if((mh=My_InputNum(12-5,2,mh,3))<0){
      return 0;
    }
  }
  else{
    if((mh=My_InputNum(12-5,2,mh,4))<0){
      return 0;
    }
  }
  if(mh!=ee->man_hgt)
  {
    ee->man_hgt=mh;
    SetGeoSettings();//save ee
  }

  cdelta=9000;
  cdist=mh - ee->EErefH;  //ledningsh�jden - �gonh�jden
  hcdist=0;

  return 1;
}


/**********************************
#3947 Fast Dangerous Tree
***********************************/
int DangTreeFast(void)
{
  int res;

  irset();
  cdelta=0;
  cdist=0;
  hcdist=0;

  if(fhgt1pl_lineclear() == 0){
    if(DangTreeFastManHgt() == 0)       //VLG-30 manuell inmatning av fash�jden
      return 1;
  }
  else
  {
  cdelta=sd.alpha;
  cdist=sd.distance;
  hcdist=sd.hdistance;
  }

  do
  {
    data_set();
    sd.measmethod=ONE_P_LASER_METHOD;  //default laser
    sd.treemethod=ONE_P_LASER_OR_DME_METHOD_LINECLEAR;

    checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

    if(fhgt3pl_() == 0){   //3pl m�tning , vertex p� DME vid laser
      return 1;
    }

    res=ShowResultLimitsFast3pl();
  }
  while(res);

  return 1;
}

/**********************************
#3947 Show Result
***********************************/
int ShowResultLimitsFast3pl(void)
{
  int th,hd,ch;
  sd.height=abs(sd.height);

  if(sd.measmethod==TRANSPONDER_METHOD)//dme
  {
    //calc beta angle
    if(sd.alpha<sd.beta)
    {
      cls();
      putxy(0,0,(char*)str_cd_ErrTrp1_);
      putxy(0,1,(char*)str_cd_ErrTrp2_);
      putxy(0,2,(char*)str_cd_ErrTrp3_);
      Sight_Oled(0);
      PrintStrOled(0,42,str_oled_Err_);
      PrintStrOled(0,42,str_oled_Err2_);
      My_Bell(400);
      getchar();
      return 1;
    }

    sd.beta=CalcBeta(sd.gamma,sd.distance,ee->EEtrpHeigth);
    th=safetylen_Fast(sd.gamma,sd.beta,sd.distance);
  }
  else
  {
    if(sd.alpha<sd.beta){//normal=top last
      sd.beta=sd.alpha;//bottom last...reverse order
    }
    th=safetylen_Fast(sd.gamma,sd.beta,sd.distance);
  }
  hd=sd.hdistance;
  if(hcdist > 0){
    hd -= hcdist;
  }
  cls();
  putxy(0,0,(char*)hgt_str_H_);
  putxy(0,1,(char*)hgt_str_HD_);
  putint(12-6,0,sd.height,6,1);
  putint(12-6,1,hd,6,1);

  My_Bell(200);
  if(th<sd.height){

    xprintf(0,2,0,12,"%s%s",str_cd_Diff2_, str_cd_NotOk_);
    My_Bell(400);
  }
  else{
    xprintf(0,2,0,12,"%s %s",str_cd_Diff_,str_cd_Ok_);
  }

  putint(0,3,th-sd.height,6,1);
  if(Metric()){
    putxy(7,3,(char*)str_unitM_);
  }
  else{
    putxy(6,3,(char*)str_unitFT_);
  }

  copy_to_ir_str(1,hd);//horisontellt avst�nd fas-tr�d
  copy_to_ir_str(2,th-sd.height);//s�kerhetsdifferensen
  copy_to_ir_str(0,sd.height);//tr�dh�jden    //kopierar denna sist s� att det blir r�tt h�jd i nmea

  My_getc();
  sd.treemethod=ONE_P_LASER_METHOD_LINECLEAR_FINAL;//final res
  sd.SaveOk=1;
  sd.sek=1;
  if((ch=dme_key())==KEY_ENTER||ch==KEY_RIGHT){
    return 1;
  }
  return 0;
}

/**********************************

***********************************/
//VLG-4 alternativ line clear funktion
// skjut p� 2 st�llen p� ledning, skjut sedan p� tr�det
// r�knar ut kortaste avst�ndet mellan t�nkt linje mellan 2 punkterna p� linan och tr�det
int DangTree3P()
{
  force_compass=1;      //tvinga anv�ndande av kompass
  data_set();//reset data storage output
  f3d_3p_data();
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}

/**********************************

***********************************/
int f3d_3p_data()
{
  float dAZ,dDeg;
  double dX,dY,dH,dHD,dSD;
  struct xyz_type xyz[4];  //3 inm�tta + ber�knade punkten p� linjen

  //f�rsta punkten p� linan, punkt A
  Sight_Oled(0);
  PrintStrOled(0,32,"P 1");
  while(f3d1pl())
  {
    int starth;
    memset(&xyz,0,sizeof(xyz));

    xyz[0].distance=sd.distance;//dm alt ftx10
    xyz[0].pitch=(int)rint(sd.gPitch*FRADTODEG*100.0);//degsx100
    xyz[0].yaw=(int)rint(sd.gYaw*100.0);//to degs  degsx100
    xyz[0].Cartesian=ConvertToCartesian(xyz[0].distance/10.0,xyz[0].pitch/100.0,xyz[0].yaw/100.0);


    //andra punkten p� linan, punkt B
    Sight_Oled(0);
  PrintStrOled(0,32,"P 2");
  bell(200,3000);
  if(f3d1pl())
    {
      xyz[1].distance=sd.distance;//dm alt ftx10
      xyz[1].pitch=(int)rint(sd.gPitch*FRADTODEG*100.0);//degsx100
      xyz[1].yaw=(int)rint(sd.gYaw*100.0);//to degs  degsx100
      xyz[1].Cartesian=ConvertToCartesian(xyz[1].distance/10.0,xyz[1].pitch/100.0,xyz[1].yaw/100.0);

    //tr�det, punkt C
      Sight_Oled(0);
  PrintStrOled(0,32,"P 3");
  bell(200,3000);
  sd.sek=2;     //s�tt om sd.sek s� att den inte sparar i minnet n�r trycker p� SEND
  if(f3d1pl())
    {
      starth=sd.height;//save for later if target is stored

      xyz[2].distance=sd.distance;//dm alt ftx10
      xyz[2].pitch=(int)rint(sd.gPitch*FRADTODEG*100.0);//degsx100
      xyz[2].yaw=(int)rint(sd.gYaw*100.0);//to degs  degsx100
      xyz[2].Cartesian=ConvertToCartesian(xyz[2].distance/10.0,xyz[2].pitch/100.0,xyz[2].yaw/100.0);

    {
      //     O C
      //     |
      //     |
      //     |
      // O---X-----O
      // A   P     B

      //linjen mellan A och B
      //(x1,y1,z1) + (x2-x1,y2-y1,z2-z1)t = (x1,y1,z1) + (a,b,c)t
      //riktningsvektorn U = (a,b,c)

      //vektorn mellan C och punkten P p� linjen
      //x3 - (x1 + a*t),y3 - (y1 + b*t),z3 - (z1 + c*t)

      //Vektorn CP �r vinkelr�t mot riktningsvektorn U
      //U*CP=0, hitta punkten P

      double a,b,c,ap,bp,cp,t;

      a = xyz[1].Cartesian.x - xyz[0].Cartesian.x;      //x2-x1
      b = xyz[1].Cartesian.y - xyz[0].Cartesian.y;      //y2-y1
      c = xyz[1].Cartesian.z - xyz[0].Cartesian.z;      //z2-z1
      ap = xyz[2].Cartesian.x - xyz[0].Cartesian.x;      //x3-x1
      bp = xyz[2].Cartesian.y - xyz[0].Cartesian.y;      //y3-y1
      cp = xyz[2].Cartesian.z - xyz[0].Cartesian.z;      //z3-z1

      //a*ap + b*bp + c*cp = (a*a + b*b + c*c)*t, l�s ekvationen f�r att f� ut t

      t = (a*ap + b*bp + c*cp)/(a*a + b*b + c*c);

      //s�tt in v�rdet p� t i ekvationen f�r linjen genom A-B f�r att f� ut punkten P
      xyz[3].Cartesian.x = xyz[0].Cartesian.x + (xyz[1].Cartesian.x - xyz[0].Cartesian.x)*t;    //x1 + (x2-x1)*t
      xyz[3].Cartesian.y = xyz[0].Cartesian.y + (xyz[1].Cartesian.y - xyz[0].Cartesian.y)*t;    //y1 + (y2-y1)*t
      xyz[3].Cartesian.z = xyz[0].Cartesian.z + (xyz[1].Cartesian.z - xyz[0].Cartesian.z)*t;    //z1 + (z2-z1)*t

      //str�ckan D (3-4)
      dX=(xyz[3].Cartesian.x-xyz[2].Cartesian.x);
      dY=(xyz[3].Cartesian.y-xyz[2].Cartesian.y);
      dH=(xyz[3].Cartesian.z-xyz[2].Cartesian.z);
      dSD=sqrt(dX*dX+dY*dY+dH*dH);
      dHD=sqrt(dX*dX+dY*dY);
      dDeg=asin(dH/dSD)*FRADTODEG;//vinkeln 90deg = upp

      {
        static float pfPhiDeg, pfTheDeg,pfRhoDeg,pfChiDeg;
        float fR[3][3];
        float fBc[3]={dX,-dY,0.0};
        f3DOFMagnetometerMatrixNED(fR,fBc);
        fNEDAnglesDegFromRotationMatrix(fR,&pfPhiDeg, &pfTheDeg, &dAZ,&pfRhoDeg, &pfChiDeg);
      }
    }

    Bell(300);
      data_set();//reset data storage output
      sd.treemethod=ONE_P_3D_METHOD;
      sd.measmethod=ONE_P_LASER_METHOD;
      sd.add_to_Hgt=ee->EErefH;
      sd.height=(int)rint(dH*10.0);             //VLG-4 h�jdv�rdet blir negativt om linan ligger l�gre �n grenen
      sd.distance=(int)rint(dSD*10.0);
      sd.hdistance=(int)rint(dHD*10.0);
      sd.gPitch=dDeg/FRADTODEG;
      sd.gamma=(int)rint(dDeg*100.0);
      sd.gYaw=dAZ;
      sd.yaw=(int)rint(dAZ*100.0);
      sd.SaveOk=true;
      sd.prev_distance=xyz[2].distance;//store last point for gps location
      sd.prev_gamma=xyz[2].pitch;//store last point
      sd.prev_yaw=xyz[2].yaw;//store last point
      sd.prev_hgt=starth;//start hgt
      sd.sek=3;//final sek

      copy_to_ir_str(0,sd.height);//update blue struct in case of transmission, ignore ir

      if(ee->EEangletype==1)
        dDeg=ConvToGrad((int)rint(dDeg*100.0))/100.0;
      else if(ee->EEangletype==2)
        dDeg=ConvToProc((int)rint(dDeg*100.0))/10.0;

      //resultatvisning i displayen, f�rslag fr�n Andre p� Eloforte i Brasilien
      cls();
      xprintf(0,0,0,12,"      *     ");
      xprintf(0,1,0,12,"      |     ");
      xprintf(0,2,0,12,"*-----|----*");
      xprintf(0,3,1,6,str_cd_Dist_);
      xprintf(6,3,0,6,"%5.1f",dSD);
      xprintf(0,4,1,6,str_cd_Pitch_);
      xprintf(6,4,0,6,"%5.1f",dDeg);

      cls_Oled();
      PrintStrOled(0,22,str_oled_D_);//try use 2decimals
      PrintIntOled(0,32,(int)rint(dSD*100.0));
    }
    else
      break;
    }
    else
      break;
    if(dme_key()!=KEY_ENTER)
      break;
    data_set();//reset data storage output
    Sight_Oled(0);
  PrintStrOled(0,32,"P 1");
  }
    return 1;
  }

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/


