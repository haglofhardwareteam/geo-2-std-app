#include <time.h>
#include "MyFuncs.h"

#define DATAn_ 1024 //512

struct GPSType GpsData;

/**********************************
*
*
***********************************/
void CloseFile(int *handle)
{
  close(*handle);
  *handle=-1;
}

/**********************************
*
*
***********************************/
void SendBlueStr(char *p)
{
  SetComPort(SERIE0);   //BLUE CLASSIC eller BLE UART
  SerialTextOut(p);
}

/**********************************
*
*
***********************************/
void SendBlueLEStr(char *p)
{
  SetComPort(SERIE4); //BLUE BLE, service uuid = "9e000000-f685-4ea5-b58a-85287cb04965" characteristic uuid = "9e010000-f685-4ea5-b58a-85287cb04965"
  SerialTextOut(p);
}

/**********************************
*
*
***********************************/
void SendDataBT()
{
  char *p;
  int flag=GetStatus();

  if(flag&STATUS_BLUE_CONNECTED)
  {
    //finns inget s�tt att kolla om uppkopplad mot classic eller BLE, skicka data p� b�gge s�tten

    //if(BlueConnected())
    {
      if((p=malloc(DATAn_))!=NULL)
      {
        sprintf(p,"$PHGF,HVV,%.1f,%s,%.1f,D,%.1f,D,%.1f,%s,%.1f,%s,%.1f,%s,",sdBlue.hdistance/10.0,Metric()?"M":"F",sdBlue.gYaw,sdBlue.gPitch*180.0/PI,sdBlue.distance/10.0,Metric()?"M":"F",sdBlue.height/10.0,Metric()?"M":"F",sd.diameter/10.0,Metric()?"CM":"IN");  //VLG2-12 Lagt till diameterv�rdet i NMEA-str�ngen, cm eller inch, med 1 decimal
        add_nmea_crc(p,DATAn_);
        SendBlueStr(p);
        free(p);
      }
    }

    //Update gatt database if Bluetooth >= V1.1.184
    //if(BlueLEConnected())
    {
      if((p=malloc(DATAn_))!=NULL)
      {
        int16 hdist,sdist,hgt,pitch,yaw;
        hdist=sdBlue.hdistance;
        sdist=sdBlue.distance;
        hgt=sdBlue.height;
        if(!Metric())
        {
          hdist=(int)rint(hdist/3.281);
          sdist=(int)rint(sdist/3.281);
          hgt=(int)rint(hgt/3.281);
        }
        if(hdist>9999)
          hdist=9999;
        if(sdist>9999)
          sdist=9999;
        if(hgt>9999)
          hgt=9999;
        if(hgt<-999)
          hgt=-999;

        pitch=(int16)rint(sdBlue.gPitch*1800.0/PI);
        yaw=(int16)rint(sdBlue.gYaw*10.0);
        sprintf(p,"%4d%4d%4d%4d%4d",hdist,sdist,hgt,pitch,yaw);
        SendBlueLEStr(p);
        free(p);
      }
    }
  }
}


/**********************************
*
*
***********************************/
int GetIdData()
{
  int ch;
  char str[IDn+1];
  struct disp_enter_string_type tt={12-IDn,1,0,IDn,0,"","0123456789",0};

  if(sd.PreDefId)//done in advance
    return 1;

  sprintf(str,"%0*d",IDn,sd.id);
  tt.str=str;
  cls();
  clr_buff();
  Sight_Oled(0);
  PrintStrOled(0,42,str_ID_);
  putxy(0,0,(char*)str_Store_);
  putxy(0,1,(char*)str_ID_);
  do
    ch=DispEnterString(&tt);
  while(ch==KEY_RIGHT);
  if(ch==KEY_EXIT)
    return -1;
  sd.id=atoi(tt.str);
  sd.PreDefId=1;
  return sd.id;
}


#define DOC_END "</Document>\r\n</kml>\r\n"
#define POLYDOC_END "</coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark></Document></kml>\r\n"
/**********************************
*
*
***********************************/
int StoreDataKml(int *handle,struct GPSType *gp)
{
  int res;
  char *p;
  if(sd.pgps->lon==0.0&&sd.pgps->lat==0.0)
    return 0; //No use to store Kml data if no gps
  if(!sd.SaveOk)
    return 1;
  if((p=malloc(DATAn_))!=NULL)
  {
    p[0]=0;
    res=1;
    switch(sd.measmethod)
    {
      char *typetxt;
    case ONE_P_LASER_METHOD:
    case TREE_P_METHOD:
  case TREE_P_METHOD_TRAIL:     //VLG-2 TRAILXY
    case ANGLE_METHOD:
      case ANGLE_METHOD_TRAIL:  //VLG-7
    case TRANSPONDER_METHOD:
    case DME_LASER_METHOD:
    case AREAMETHOD:
    case GPSMETHOD:
    case LENMETHOD:
    case GPSSINGLEMETHOD:

      if(sd.measmethod==ONE_P_LASER_METHOD)
      {
       if(sd.treemethod==ONE_P_3D_METHOD)
        typetxt="3D";
       else if(sd.treemethod==ONE_P_LASER_METHOD_RELATIVE||sd.treemethod==ONE_P_LASER_METHOD_RELATIVE_XY)       //VLG-2 TRAILXY
         break;//typetxt="1P_REL"; help function, no store
       else if(sd.treemethod==COMPASS_METHOD)
       {
         if(rev_compass)
       typetxt="COMPASS REV";   //VLG2-14 byt metodnamn om reverse compass
       else
         typetxt="COMPASS";
       }
       else if(sd.treemethod==ONE_P_LASER_METHOD_COMPASS || sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)//MAP TRAIL    //VLG-2 MAP TRAILXY
       typetxt="TRAIL";
       else if(sd.treemethod==ONE_P_LASER_METHOD_REPEAT||sd.treemethod==ONE_P_LASER_METHOD_GPS) //MAP TARGET        //VLG-2 MAP GPS
       {
         if(sd.type_target_point==POINT_LASER_BASE || sd.type_target_point==POINT_DME_BASE)      //LGEO3DPILE-6        //VLG-2 spara typen i sd-struct och titta p� den ist�llet
              typetxt="BASE";
            else
            typetxt="TARGET";
        }
       else
        typetxt="1P";
      }
      else if(sd.measmethod==TREE_P_METHOD||sd.measmethod==TREE_P_METHOD_TRAIL) //VLG-2 TRAILXY
      typetxt="3P";
     else if(sd.measmethod==ANGLE_METHOD || sd.measmethod==ANGLE_METHOD_TRAIL)  //VLG-7
       typetxt="ANGLE";
     else if(sd.measmethod==TRANSPONDER_METHOD)
       typetxt="DME";
     else if(sd.measmethod==DME_LASER_METHOD)
       typetxt="DMELASER";
     else if(sd.measmethod==AREAMETHOD)
       typetxt="AREA";
     else if(sd.measmethod==GPSMETHOD||sd.measmethod==GPSSINGLEMETHOD)
       typetxt="GPSPOS";
     else
       if(sd.measmethod==LENMETHOD)
         typetxt="LEN";

     if(sd.measmethod==GPSMETHOD)
     {
       if(lseek(*handle,-strlen(POLYDOC_END),SEEK_END)>0)
       {
           sprintf(p,"%.7f,%.7f,%.1f\r\n"POLYDOC_END,gp->lonf,gp->latf,gp->hgt);
           if(WriteDataOnDisk(handle,p)<=0)
             res=-1;
       }
     }
     else
       if(lseek(*handle,-strlen(DOC_END),SEEK_END)>0)
       {
         if(sd.type_target_point==POINT_LASER_BASE || sd.type_target_point==POINT_DME_BASE)  //LGEO3DPILE-6        //VLG-2 spara typen i sd-struct och titta p� den ist�llet
           sprintf(p,"<Placemark>\r\n<styleUrl>#VStyle3</styleUrl>\r\n");
         else
           sprintf(p,"<Placemark>\r\n<styleUrl>#VStyle</styleUrl>\r\n");

         if(WriteDataOnDisk(handle,p)>0)
         {
           if(sd.diameter>0)
           {
             //VLG2-2 Skriver ut Diameterv�rdet om den finns, kml
             if(Metric())
               sprintf(p,"<name>P%d,%d</name><description>\r\n<table><tr><td><b>Method:</b></td><td>%s</td></tr><tr><td><b>Latitude:</b></td><td>%.7f</td></tr><tr><td><b>Longitude:</b></td><td>%.7f</td></tr><tr><td><b>Altitude:</b></td><td>%.0fm</td></tr><tr><td><b>Z:</b></td><td>%.1fm</td></tr><tr><td><b>Height:</b></td><td>%.1f%s</td></tr><tr><td><b>H.Distance:</b></td><td>%.1f%s</td></tr><tr><td><b>Bearing:</b></td><td>%.1f</td></tr><tr><td><b>Pitch:</b></td><td>%.1f</td></tr><tr><td><b>S.Distance:</b></td><td>%.1f%s</td></tr><tr><td><b>Diameter:</b></td><td>%.1f%s</td></tr></table>\r\n</description>\r\n",sd.id,sd.sek,typetxt,gp->latf,gp->lonf,gp->hgt+sd.targetz,sd.targetz,sd.height/10.0,"m",sd.hdistance/10.0,"m",sd.yaw/100.0,sd.gamma/100.0,sd.distance/10.0,"m",sd.diameter/10.0,"cm");
             else
              sprintf(p,"<name>P%d,%d</name><description>\r\n<table><tr><td><b>Method:</b></td><td>%s</td></tr><tr><td><b>Latitude:</b></td><td>%.7f</td></tr><tr><td><b>Longitude:</b></td><td>%.7f</td></tr><tr><td><b>Altitude:</b></td><td>%.0fm</td></tr><tr><td><b>Z:</b></td><td>%.1fm</td></tr><tr><td><b>Height:</b></td><td>%.1f%s</td></tr><tr><td><b>H.Distance:</b></td><td>%.1f%s</td></tr><tr><td><b>Bearing:</b></td><td>%.1f</td></tr><tr><td><b>Pitch:</b></td><td>%.1f</td></tr><tr><td><b>S.Distance:</b></td><td>%.1f%s</td></tr><tr><td><b>Diameter:</b></td><td>%.1f%s</td></tr></table>\r\n</description>\r\n",sd.id,sd.sek,typetxt,gp->latf,gp->lonf,gp->hgt+sd.targetz,sd.targetz,sd.height/10.0,"ft",sd.hdistance/10.0,"ft",sd.yaw/100.0,sd.gamma/100.0,sd.distance/10.0,"ft",sd.diameter/10.0,"\"");
           }
           else
             sprintf(p,"<name>P%d,%d</name><description>\r\n<table><tr><td><b>Method:</b></td><td>%s</td></tr><tr><td><b>Latitude:</b></td><td>%.7f</td></tr><tr><td><b>Longitude:</b></td><td>%.7f</td></tr><tr><td><b>Altitude:</b></td><td>%.0fm</td></tr><tr><td><b>Z:</b></td><td>%.1fm</td></tr><tr><td><b>Height:</b></td><td>%.1f%s</td></tr><tr><td><b>H.Distance:</b></td><td>%.1f%s</td></tr><tr><td><b>Bearing:</b></td><td>%.1f</td></tr><tr><td><b>Pitch:</b></td><td>%.1f</td></tr></table>\r\n</description>\r\n",sd.id,sd.sek,typetxt,gp->latf,gp->lonf,gp->hgt+sd.targetz,sd.targetz/*,Metric()?"m":"ft"*/,sd.height/10.0,Metric()?"m":"ft",sd.hdistance/10.0,Metric()?"m":"ft",sd.yaw/100.0,sd.gamma/100.0);
           if(WriteDataOnDisk(handle,p)>0)
             if(WriteDataOnDisk(handle,"<Point>\r\n<extrude>1</extrude>\r\n<altitudeMode>relativeToGround</altitudeMode>\r\n<coordinates>")>0)
             {
               sprintf(p,"%.7f,%.7f,%.1f</coordinates>\r\n</Point>\r\n</Placemark>\r\n",gp->lonf,gp->latf,sd.targetz);
               if(WriteDataOnDisk(handle,p)<=0)
                 res|=-1;
               sprintf(p,"<Placemark>\r\n<styleUrl>#VStyle2</styleUrl><LineString>\r\n<altitudeMode>clampToGround</altitudeMode>\r\n<coordinates>%.7f,%.7f,%.1f,%.7f,%.7f,%.1f</coordinates>\r\n</LineString>\r\n</Placemark>"DOC_END,sd.pgps->lonf,sd.pgps->latf,sd.startz,gp->lonf,gp->latf,sd.targetz);
               if(WriteDataOnDisk(handle,p)<=0)
                 res|=-1;
             }
         }
       }
      break;
    default:
      break;
    }
    free(p);
  }
  else
    res=0;//malloc failed
return res;
}

//VLG2-10 lagt till GeoJsonutskrift
#define JSONDOC_END "]}\r\n"
#define JSONPOLYDOC_END "]]}}]}\r\n"
/**********************************
*
*
***********************************/
int StoreDataJson(int *handle,struct GPSType *gp)
{
  int res;
  int bIsFirst=0;
  char c;
  char *p;
  if(sd.pgps->lon==0.0&&sd.pgps->lat==0.0)
    return 0; //No use to store Kml data if no gps
  if(!sd.SaveOk)
    return 1;
  if((p=malloc(DATAn_))!=NULL)
  {
    p[0]=0;
    res=1;
    switch(sd.measmethod)
    {
      char *typetxt;
    case ONE_P_LASER_METHOD:
    case TREE_P_METHOD:
  case TREE_P_METHOD_TRAIL:     //VLG-2 TRAILXY
    case ANGLE_METHOD:
      case ANGLE_METHOD_TRAIL:  //VLG-7
    case TRANSPONDER_METHOD:
    case DME_LASER_METHOD:
    case AREAMETHOD:
    case GPSMETHOD:
    case LENMETHOD:
    case GPSSINGLEMETHOD:

      if(sd.measmethod==ONE_P_LASER_METHOD)
      {
       if(sd.treemethod==ONE_P_3D_METHOD)
        typetxt="3D";
       else if(sd.treemethod==ONE_P_LASER_METHOD_RELATIVE||sd.treemethod==ONE_P_LASER_METHOD_RELATIVE_XY)       //VLG-2 TRAILXY
         break;//typetxt="1P_REL"; help function, no store
       else if(sd.treemethod==COMPASS_METHOD)
       {
         if(rev_compass)
       typetxt="COMPASS REV";   //VLG2-14 byt metodnamn om reverse compass
       else
         typetxt="COMPASS";
       }
       else if(sd.treemethod==ONE_P_LASER_METHOD_COMPASS || sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)//MAP TRAIL    //VLG-2 MAP TRAILXY
       typetxt="TRAIL";
       else if(sd.treemethod==ONE_P_LASER_METHOD_REPEAT||sd.treemethod==ONE_P_LASER_METHOD_GPS) //MAP TARGET        //VLG-2 MAP GPS
       {
         if(sd.type_target_point==POINT_LASER_BASE || sd.type_target_point==POINT_DME_BASE)      //LGEO3DPILE-6        //VLG-2 spara typen i sd-struct och titta p� den ist�llet
              typetxt="BASE";
            else
            typetxt="TARGET";
        }
       else
        typetxt="1P";
      }
      else if(sd.measmethod==TREE_P_METHOD||sd.measmethod==TREE_P_METHOD_TRAIL) //VLG-2 TRAILXY
      typetxt="3P";
     else if(sd.measmethod==ANGLE_METHOD || sd.measmethod==ANGLE_METHOD_TRAIL)  //VLG-7
       typetxt="ANGLE";
     else if(sd.measmethod==TRANSPONDER_METHOD)
       typetxt="DME";
     else if(sd.measmethod==DME_LASER_METHOD)
       typetxt="DMELASER";
     else if(sd.measmethod==AREAMETHOD)
       typetxt="AREA";
     else if(sd.measmethod==GPSMETHOD||sd.measmethod==GPSSINGLEMETHOD)
       typetxt="GPSPOS";
     else
       if(sd.measmethod==LENMETHOD)
         typetxt="LEN";

     if(sd.measmethod==GPSMETHOD)
     {
       if(lseek(*handle,-strlen(JSONPOLYDOC_END)-1,SEEK_END)>0)
       {
         if(read(*handle,(unsigned char*)&c,1)==1)
         {
           //m�ste kolla om finns sparade punkter, f�r inte vara avslutande kommatecken

           if(c=='[')
             bIsFirst=1;

           if(lseek(*handle,-strlen(JSONPOLYDOC_END),SEEK_END)>0)
           {
             sprintf(p,"%s[%.7f,%.7f]"JSONPOLYDOC_END,bIsFirst==1? " ":", ",gp->lonf,gp->latf);
             if(WriteDataOnDisk(handle,p)<=0)
               res=-1;
           }
         }
       }
     }
     else
       if(lseek(*handle,-strlen(JSONDOC_END)-1,SEEK_END)>0)
       {
         if(read(*handle,(unsigned char*)&c,1)==1)
         {
           //m�ste kolla om finns sparade punkter, f�r inte vara avslutande kommatecken

           if(c=='[')
             bIsFirst=1;

           if(lseek(*handle,-strlen(JSONDOC_END),SEEK_END)>0)
           {
             sprintf(p,"%s{ \"type\": \"Feature\", \"properties\": { \"Name\": \"P%d,%d\", \"Method:\": \"%s\",  \"Unit:\":\"%s\",\"H.Distance:\": %.01f, \"S.Distance:\": %.01f, \"Height:\": %.01f, \"Bearing:\": %.01f,\"Pitch:\": %.01f, \"Altitude:\": %.01f, \"Z:\": %.01f, \"Latitude:\": %.07f, \"Longitude:\": %.07f, \"Diameter:\": %.01f },\"geometry\": { \"type\": \"Point\", \"coordinates\": [ %.07f, %.07f ] } },",bIsFirst==1? " ":", ",sd.id,sd.sek,typetxt,Metric()? "Metric":"Feet",sd.hdistance/10.0,sd.distance/10.0,sd.height/10.0,sd.yaw/100.0,sd.gamma/100.0,gp->hgt+sd.targetz,sd.targetz,gp->latf,gp->lonf,sd.diameter/10.0,gp->lonf,gp->latf);

             if(WriteDataOnDisk(handle,p)<=0)
               res|=-1;
             else
             {
               sprintf(p,"{ \"type\": \"Feature\", \"geometry\": { \"type\": \"LineString\", \"coordinates\": [ [ %.07f, %.07f ], [ %.07f, %.07f ] ] } }"JSONDOC_END, sd.pgps->lonf,sd.pgps->latf,gp->lonf,gp->latf);
               if(WriteDataOnDisk(handle,p)<=0)
                 res|=-1;
             }
           }
         }
       }
      break;
    default:
      break;
    }
    free(p);
  }
  else
    res=0;//malloc failed
return res;
}

/**********************************
*
*
***********************************/
struct GPSType *My_fGpsGetPos(int num, int time)
{
  GetGPSData(&GpsData,num,time);
  return &GpsData;
}

/*1=ok, 0=not saved -1 error */
/**********************************
*
*
***********************************/
int ReadGps()
{
  int res=0;

if(sd.PreDefGps&&sd.pgps->lon!=0.0&&sd.pgps->lat!=0.0)
 return 1;
  do
    {
      int ch;
      Cls();
      cls_Oled();
  PrintStrOled(0,42,str_oled_Gps_);

    if(sd.treemethod==ONE_P_LASER_METHOD_GPS)//VLG-2 MAP GPS
  {
    if(sd.sek<=1)
      sd.pgps=My_fGpsGetPos(500,0);
    else
      sd.pgps=My_fGpsGetPos(2,0);  //autosparar efter 2 positioner
  }
  else if(ee->useGps)
       sd.pgps=My_fGpsGetPos(500,0);
    else
      break;
    if(sd.pgps->ch==KEY_EXIT)
      break;
    if(sd.pgps->lon!=0.0&&sd.pgps->lat!=0.0)
    {
      res=1;
      break;
    }

     if((ch=Message(str_msg_NoGps_,str_msg_TryAgainQ_))==KEY_EXIT)
       break;
     if(ch!=KEY_ENTER)
       break;
    }
    while(1);
  return res;
}

// dec to wgs84x100
/**********************************
*
*
***********************************/
int GpsDecToGrad(struct GPSType *GpsData)
{
  double fractpart, intpart;
  if(GpsData->latf>=0.0)
    GpsData->lat_dir[0]='N';
  else
    GpsData->lat_dir[0]='S';
  if(GpsData->lonf>=0.0)
    GpsData->lon_dir[0]='E';
  else
    GpsData->lon_dir[0]='W';

  fractpart = modf(GpsData->latf,&intpart);//63.17827167->63;0.17827167
  GpsData->lat=intpart*100.0+fractpart*60.0;//6300+0.17827167*60->6310.6963
  fractpart = modf(GpsData->lonf,&intpart);//63.17827167->63;0.17827167
  GpsData->lon=intpart*100.0+fractpart*60.0;//6300+0.17827167*60->6310.6963
  return 1;
}

// wgs84x100 to dec
/**********************************
*
*
***********************************/
int GpsGradToDec(struct GPSType *GpsData)
{
  double fractpart, intpart;
  fractpart = modf(GpsData->lat/100.0, &intpart);//6310.69563->63;10.69563
  GpsData->latf=intpart+(100.0*fractpart)/60.0;//min->degr
  fractpart = modf(GpsData->lon/100.0, &intpart);//6310.69563->63;10.69563
  GpsData->lonf=intpart+(100.0*fractpart)/60.0;//min->degr

  if(GpsData->lat_dir[0]!='N')
  {
    GpsData->latf=-fabs(GpsData->latf);

  }
  if(GpsData->lon_dir[0]!='E')
  {

    GpsData->lonf=-fabs(GpsData->lonf);
  }
  return 1;
}

/**********************************
*
*
***********************************/
int AddData(int type)
{
  struct xyz_type xyz;//cartesian
  static struct GPSType targetgps;//target data
  int yaw;

  int thandlekml=-1,thandlecsv=-1,res=0,thandlejson=-1;
  if(sd.SaveOk||type==STORESETTINGS)
    ;
  else
    return 0;

  if(type==STOREDATA)
  {
    if(sd.treemethod==ONE_P_LASER_METHOD_LINECLEAR||sd.treemethod==ONE_P_LASER_OR_DME_METHOD_LINECLEAR||sd.measmethod==TREE_P_METHOD_FIRSTLASERSHOT||sd.treemethod==DELTA_METHOD_LASERSHOT)
    {
      sd.id=sd.id-1;// keep last id, (simulate store inc id)
      return 1;//sim store/accept
    }
    if(sd.sek<=1&&sd.treemethod!=ONE_P_3D_METHOD||sd.treemethod==ONE_P_3D_METHOD&&sd.sek==3)
    {
      if((GetIdData())<0)
        return 0;
      ReadGps();//if activated
    }
    else if(sd.sek>1 && sd.treemethod==ONE_P_LASER_METHOD_GPS)//VLG-2 MAP GPS
    {
      ReadGps();
      sd.id=sd.id-1;//use last id when seq>1;
    }
    else
    {
      sd.id=sd.id-1;//use last id when seq>1;
    }
  }

  if(!auto_save_target)        //VLG-2 �ndrat visning vid autospar
  {
    cls();
    xprintf(0,0,0,4,"... ");//storing...
    Sight_Oled(0);
    PrintStrOled(0,42,"...");
  }

   if(type==STOREDATA)
  {
    memset(&targetgps,0,sizeof(targetgps));

    if(sd.treemethod==ONE_P_3D_METHOD)//special, r�kna om ref punkt till f�rsta m�let vid 3d m�tning
    {
      sd.startz=sd.prev_hgt/10.0;
      if(sd.prev_distance>=0&&sd.pgps!=NULL&&sd.pgps->lon!=0.0&&sd.pgps->lat!=0.0)//recalc gps pos to first target
    {
      //xyz.Cartesian=ConvertToCartesian(sd.prev_distance/10.0,sd.prev_gamma/100.0,sd.prev_yaw/100.0);//special case 3d vektor starts from another point
      struct xyz_type c;//cartesian
      c.Cartesian=ConvertToCartesian(sd.prev_distance/10.0,sd.prev_gamma/100.0,sd.prev_yaw/100.0);//special case 3d vektor starts from another point
      if(!Metric())//coordinates in metric
      {
        c.Cartesian.x=c.Cartesian.x/3.28084;//to m
        c.Cartesian.y=c.Cartesian.y/3.28084;//to m
      }
      sd.pgps->x+=c.Cartesian.x;//adj lat long target
      sd.pgps->y+=c.Cartesian.y;//
      Gps_ConvertCoord(UTMXY_TO_WGS84LATLONG,sd.pgps);      //UTMxytoWGS84LatLong(sd.pgps);//adj gps pos to first target
      GpsGradToDec(sd.pgps);//to decimal degree
    }
    }

    yaw = sd.yaw;
    if(rev_compass)     //VLG2-14 r�kna tillbaka och skicka in rikta kompassv�rdet
    {
      //r�kna om v�rdet +180 mod 360
        float dAZ = sd.yaw/100.0 + 180.0;
        if(dAZ >= 360.0)
          dAZ -= 360.0;
        yaw = ((int)rint(dAZ*100.0));
    }
    xyz.Cartesian=ConvertToCartesian(sd.distance/10.0,sd.gamma/100.0,/*sd.*/yaw/100.0);//target vektor

    if(!Metric())//coordinates in metric
        {
          xyz.Cartesian.x=xyz.Cartesian.x/3.28084;//to m
          xyz.Cartesian.y=xyz.Cartesian.y/3.28084;//to m
          xyz.Cartesian.z=xyz.Cartesian.z/3.28084;//to m (not used)
        }

    if(sd.measmethod!=TREE_P_METHOD_TRAIL && sd.measmethod!=ANGLE_METHOD_TRAIL)      //VLG-2, VLG-7 TRAILXY, ska inte uppdatera koordinaterna i det h�r fallet utan ta de som finns f�r att det ska bli samma tr�d som target
    {
    sd.targetx=sd.startx+xyz.Cartesian.x;//lokala koord
    sd.targety=sd.starty+xyz.Cartesian.y;
    }

    if(sd.measmethod==ONE_P_LASER_METHOD)
    {
      if(!Metric())//coordinates in metric
       sd.targetz=sd.startz+sd.height/10.0/3.28084;//to m
      else
       sd.targetz=sd.startz+sd.height/10.0;//m tot hgt
    }
    else if(sd.measmethod!=ANGLE_METHOD_TRAIL)  //VLG-7 TRAILXY, ska inte uppdatera targetz i det h�r fallet
      { //3p measuring tree start can be in any level
      int th;
      th=calc_hgt_3(sd.distance,sd.gamma,0,sd.beta,0);//treebase to eye
      sd.targetz=(sd.height+th)/10.0;//m tot hgt
      sd.targetz+=(sd.add_to_Hgt/10.0);//add eye hgt

      if(!Metric())//coordinates in metric
       sd.targetz=sd.targetz/3.28084;//to m tot hgt

      sd.targetz+=sd.startz;//add ev. rel hgt
      }
    else
    {
      ;
    }

    if(sd.pgps!=NULL)
    {
      targetgps=*sd.pgps;//
      if(sd.pgps->lon!=0.0&&sd.pgps->lat!=0.0 && sd.measmethod!=TREE_P_METHOD_TRAIL && sd.measmethod!=ANGLE_METHOD_TRAIL)  //VLG-2, VLG-7 TRAILXY, ska inte uppdatera koordinaterna i det h�r fallet utan ta de som finns f�r att det ska bli samma tr�d som target
      {
        targetgps.x+=sd.targetx;//adj lat long target
        targetgps.y+=sd.targety;//

        sd.targetx=targetgps.x;//store new target
        sd.targety=targetgps.y;//

        Gps_ConvertCoord(UTMXY_TO_WGS84LATLONG,&targetgps);      //UTMxytoWGS84LatLong(&targetgps);//To LatLong
        GpsGradToDec(&targetgps);//to decimal degree
      }
    }
    else//no gps
    {
      sd.pgps=&targetgps;//avoid a null pointer just in case
    }

    if(sd.treemethod==ONE_P_3D_METHOD&&sd.sek<3)// no storing until a final 3d
       return 1;

    if(sd.treemethod==ONE_P_LASER_METHOD_REPEAT && (sd.type_target_point==POINT_LASER_BASE || sd.type_target_point==POINT_DME_BASE))//add area,len  //VLG-2 ber�knar endast area p� baspunkterna
    {
      if(sd.sek==1 || (sd.sek > 1 && sd.xf==0.0 && sd.yf==0.0))//area       //VLG-2 Ifall f�rsta punkten inte var baspunkt (av n�gon anledning)
    {
     sd.xf=sd.x0=sd.targetx;    //save first point
     sd.yf=sd.y0=sd.targety;
    }
      else
      {
        sd.x1=sd.targetx;
        sd.y1=sd.targety;
        sd.len+=sqrt((sd.x0-sd.x1)*(sd.x0-sd.x1)+(sd.y0-sd.y1)*(sd.y0-sd.y1));
        sd.area+=sd.x0*sd.y1-sd.y0*sd.x1;
        sd.x0=sd.x1;
        sd.y0=sd.y1;
      }
    }
    else if(sd.treemethod==ONE_P_LASER_METHOD_COMPASS || sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)//len     //VLG-2 MAP TRAILXY
    {
      sd.len+=sd.distance/10.0;
    }
  }

  if(sd.handlekml<0)//pre created?
  {
    if((thandlekml=OpenOrCreateKmlFile(DATAKMLFILENAME))<0)
      return -1;
  }
  else
    thandlekml=sd.handlekml;


  if(sd.handlecsv<0)//pre created?
  {
    if((thandlecsv=OpenOrCreateCsvFile(type,DATACSVFILENAME))<0)
    {
      CloseFile(&thandlekml);
      return -1;
    }
  }
  else
    thandlecsv=sd.handlecsv;

  if(sd.handlejson<0)//pre created?     //VLG2-10 lagt till GeoJsonutskrift
  {
    if((thandlejson=OpenOrCreateJsonFile(type,DATAJSONFILENAME))<0)
    {
      CloseFile(&thandlecsv);
      CloseFile(&thandlekml);
      return -1;
    }
  }
  else
    thandlejson=sd.handlejson;

  if(thandlekml!=0&&thandlecsv!=1&&thandlejson!=2)//sys error forget to close a file?
  {
    cls();
    xprintf(0,0,0,12,str_msg_Error_);//storing...
    xprintf(0,1,0,12,str_error_filehandle_);//storing...
    Sight_Oled(0);
  PrintStrOled(0,42,str_oled_Err_);
  PrintStrOled(0,52,str_oled_Err2_);
  GetSystemKey();
      return 0;
  }

  res=0;

  //add the first ref point to csv v1.9 when storing first target point
   if(sd.sek<=1&&type==STOREDATA&&sd.treemethod==ONE_P_LASER_METHOD_REPEAT)
   {
     struct storedatatype tsd=sd;
     struct GPSType ttargetgps=targetgps;
     sd.sek=0;//sek=0 first ref pt
     sd.distance=0;
     sd.alpha=0;
     sd.distance=0;
     sd.hdistance=0;
     sd.height=0;
     sd.gamma=0.0;
     sd.yaw=0.0;
     sd.targetx=0.0;
     sd.targety=0.0;
     sd.targetz=0.0;
     if(sd.pgps!=NULL)
     {
       ttargetgps.lonf=sd.pgps->lonf;
       ttargetgps.latf=sd.pgps->latf;
       sd.targetx=sd.pgps->x;
       sd.targety=sd.pgps->y;
     }
     sd.treemethod=ONE_P_LASER_METHOD_RELATIVE;//sim rel target first pos current place
     res|=StoreDataCsv(type,&thandlecsv,&ttargetgps);//store 1st ref pos //V1.9
     sd=tsd;//restore target pt
   }
   if(type==STOREDATA)
   {
      res|=StoreDataKml(&thandlekml,&targetgps);//store new settings or data

      //VLG-2 om baspunkt s� spara ner till tempor�r fil
      StoreDataAreaPoint(&targetgps,sd.targetz,sd.type_target_point,sd.treemethod);

      //VLG2-10 lagt till GeoJsonutskrift
      res|=StoreDataJson(&thandlejson,&targetgps);//store new settings or data
   }
    res|=StoreDataCsv(type,&thandlecsv,&targetgps);//store new settings or data

   if(sd.handlekml<0)
    CloseFile(&thandlekml);
   if(sd.handlecsv<0)
    CloseFile(&thandlecsv);
   if(sd.handlejson<0)
    CloseFile(&thandlejson);

   clr_buff();    //del any keys
    return res;//num of bytes or fail
}

/**********************************
*
*
***********************************/
int MemStore()
{
  int res;
  int ttim,ttim2;

  if(ee->EEsaveEEmem)
  {
    ttim=GetTimer();
    SaveDispContenz();
    res=AddData(STOREDATA);
    RestoreDispContenz();
    if(res<0)
      Message(str_msg_Error_,str_msg_MemFullQ_);
    if(res>0)
    {
      xprintf(0,0,DISP_REVERSE,12,"%-12s",str_DataSaved_);
      if(auto_save_target)
      {
        //v�nta lite f�r att f� en j�mnare f�rdr�jning
        ttim2=abs(ttim-GetTimer());
                  if(ttim2 < 200)
                    Wait(200-ttim2);
      }
      bell(200,3000);
      sd.sek++;
      sd.id=(sd.id+1)%100000;//auto inc id
      sd.stored=1;
      sd.SaveOk=false;//store once max
    }
    if(!auto_save_target)       //VLG-2 rensa inte siktet om autosave
      Sight_Oled(0);//clr
  }

  return 1;
}

/**********************************
*
*
***********************************/
int WriteDataOnDisk(int *handle,char *str)
{
  return write(*handle,(unsigned char *)str,strlen(str));
}

/**********************************
*
*
***********************************/
static int getfield(char *ptr,int n)
{

  char *tp,*tpp;

  if(ptr[0]=='$')
  {
     tp=tpp=ptr;
  for(int i=0;i<n&&tp!=NULL;i++)
     if((tp=strstr(tpp+1,";"))!=NULL)
       tpp=tp;

     if(tpp!=NULL)
       return atoi(tpp+1);
  }
 return -1;
}

/**********************************
*
*
***********************************/
int getlastdataset()
{
  int thandle=-1,size,n=0;
  memset(&sd,0,sizeof(sd));
 if((thandle=open(DATACSVFILENAME,_LLIO_WRONLY))<0)
   return 0;
 size=filesize(thandle);
 if(size>DATAn_)
   size=DATAn_;
 if(size)
 if(lseek(thandle,-size,SEEK_END)>=0)
 {
   char *p,*tp,*tpp;
   if((p=malloc(size+1))!=NULL)
   {
     p[size]=0;
     read(thandle,(unsigned char *)p,size);
     tpp=p;
     while((tp=strstr(tpp+1,"$"))!=NULL)
       tpp=tp;
     if(tpp!=NULL)
       if((n=getfield(tpp,6))>=0)
         sd.id=n;
     else
       n=sd.id=0;

     free(p);
   }

 }
  CloseFile(&thandle);
 return n;
}

/* added v2.2, update gps time from sys timer when no new gps info is available */
/**********************************
*
*
***********************************/
int ReadSysUtcTime()
{
  time_t t;
  struct tm *T;
  time(&t);
  if(t!=-1)
  {
    T=localtime(&t);
    if(T->tm_year>=99)//1900 based 2017=117, changed to y99 since some gps don't have date
      t=T->tm_hour*10000+T->tm_min*100+T->tm_sec;//to utc hhmmss
    else
      t=0;////bad time?
  }
  else
    t=0;//no time
  return t;
}

/**********************************
*
*
***********************************/
int StoreDataCsv(int type,int *handle,struct GPSType *gp)
{
  int res=0;
  char *p;
  if((p=malloc(DATAn_))!=NULL)
  {
  switch(type)
  {
  case STORESETTINGS:
    sprintf(p,"#;;SET;%d;%d;%d;;%c;%.1f;%.1f;%.1f;%.1f;;;;;;;;;;;;;;;;;;\r\n"   //VLG2-2 ut�kat med DIA, csv
            ,product
            ,ver
            ,GetSerialNr()
              ,Metric()?'M':'F'
                ,ee->EEtrpHeigth/10.0
                  ,ee->EErefH/10.0
                    ,ee->EEpivot/10.0
                      ,ee->decl/10.0
                      );
       if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
    break;
  case STOREDATA:
    p[0]=0;
    if(sd.SaveOk)//store once max
    {
      char tlat[64+1],tlon[64+1],tns[1+1],tew[1+1],utc[6+1],hdop[6+1],date[6+1],alt[6+1],zon[3+1];
    zon[0]=alt[0]=date[0]=hdop[0]=utc[0]=tlat[0]=tlon[0]=tns[0]=tew[0]='\0';

      if(sd.pgps!=NULL)
      {

       if(sd.pgps->lon!=0.0&&sd.pgps->lat!=0.0)
       {
         gp->time=ReadSysUtcTime();//V2.2 read sys time and convert to utc, systime is updated via gps UTC field
         sprintf(tlat,"%.7f",gp->latf);
         sprintf(tlon,"%.7f",gp->lonf);
         sprintf(tns,"%c",gp->lat_dir[0]);
         sprintf(tew,"%c",gp->lon_dir[0]);
         sprintf(utc,"%d",gp->time);
         sprintf(zon,"%d%c",gp->zon,gp->zon_letter);
         if(gp->hdop<1000.0)
           sprintf(hdop,"%.1f",gp->hdop);
         if(fabs(gp->hgt)<=1000000.0)//altitude
           sprintf(alt,"%.1f",gp->hgt+sd.targetz);//1 dec V1.9
         if(gp->date<1000000)
           sprintf(date,"%06d",gp->date);
       }
    }

    switch(sd.measmethod)
    {
      char *typetxt;
      char recordtype;

    case ONE_P_LASER_METHOD:
    case TRANSPONDER_METHOD:
    case DME_LASER_METHOD:

      if(sd.treemethod==ONE_P_3D_METHOD)
        typetxt="3D";
      else if(sd.treemethod==ONE_P_LASER_METHOD_RELATIVE || sd.treemethod==ONE_P_LASER_METHOD_RELATIVE_XY)     //VLG-2 TRAILXY
        typetxt="1P_REL"; //V1.9
      else if(sd.treemethod==COMPASS_METHOD)
       {
         if(rev_compass)
       typetxt="COMPASS REV";   //VLG2-14 byt metodnamn om reverse compass
       else
         typetxt="COMPASS";
       }
      else if(sd.treemethod==ONE_P_LASER_METHOD_COMPASS || sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)       //VLG-2 MAP TRAILXY
        typetxt="TRAIL";
      else if(sd.treemethod==ONE_P_LASER_METHOD_REPEAT||sd.treemethod==ONE_P_LASER_METHOD_GPS)        //VLG-2 MAP GPS
      {
        if(sd.type_target_point/*point_basepoint*/==POINT_LASER_BASE || sd.type_target_point/*point_basepoint*/==POINT_DME_BASE)      //LGEO3DPILE-6        //VLG-2 spara typen i sd-struct och titta p� den ist�llet
          typetxt="BASE";
        else
          typetxt="TARGET";
      }
      else
        typetxt="1P";


      if(sd.measmethod==TRANSPONDER_METHOD)
        typetxt="DME";
      else if(sd.measmethod==DME_LASER_METHOD)
        typetxt="DMELASER";
      else if(sd.measmethod==COMPASS_METHOD)
       {
         if(rev_compass)
       typetxt="COMPASS REV";   //VLG2-14 byt metodnamn om reverse compass
       else
         typetxt="COMPASS";
       }


      //"MARK;STATUS;TYPE;PROD;VER;SNR;ID;UNIT;TRPH;REFH;P.OFF;DECL;LAT;N/S;LON;E/W;ALTITUDE;HDOP;DATE;UTC;SEQ;AREA;VOL;SD;HD;H;DIA;PITCH;AZ;X(m);Y(m);Z(m);UTM ZONE;      //VLG2-2 ut�kat med DIA
      if(sd.treemethod==ONE_P_LASER_METHOD_RELATIVE || sd.treemethod==ONE_P_LASER_METHOD_RELATIVE_XY)    //VLG-2
        recordtype='&';//avoid mixing this ref point with target point V1.9->
      else
        recordtype='$';//target symbol

      if(sd.diameter>0) //VLG2-2 lagt till diameter i egen kolumn, efter h�jden i csv-filen
      {
        //VLG2-2 Skriver ut Diameterv�rdet om den finns
        if(Metric())
          sprintf(p,"%c;1;%s;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%s\r\n",recordtype,typetxt,sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.distance/10.0,sd.hdistance/10.0,sd.height/10.0,sd.diameter/10.0,sd.gamma/100.0,sd.yaw/100.0,sd.targetx,sd.targety,sd.targetz,zon);
        else
        sprintf(p,"%c;1;%s;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%s\r\n",recordtype,typetxt,sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.distance/10.0,sd.hdistance/10.0,sd.height/10.0,sd.diameter/10.0,sd.gamma/100.0,sd.yaw/100.0,sd.targetx,sd.targety,sd.targetz,zon);
      }
      else
        sprintf(p,"%c;1;%s;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;%.1f;%.1f;%.1f;;%.1f;%.1f;%.1f;%.1f;%.1f;%s;\r\n",recordtype,typetxt,sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.distance/10.0,sd.hdistance/10.0,sd.height/10.0,sd.gamma/100.0,sd.yaw/100.0,sd.targetx,sd.targety,sd.targetz,zon);
      if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
      break;
    case TREE_P_METHOD:
    case TREE_P_METHOD_TRAIL:   //VLG-3 TRAILXY
      if(sd.diameter>0) //VLG2-2 lagt till diameter i egen kolumn i csv-filen
      {
        //VLG2-2 Skriver ut Diameterv�rdet om den finns. DIA-kolumnen, ligger efter h�jden
        if(Metric())
          sprintf(p,"$;1;3P;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%s\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.distance/10.0,sd.hdistance/10.0,sd.height/10.0,sd.diameter/10.0,sd.gamma/100.0,sd.yaw/100.0,sd.targetx,sd.targety,sd.targetz,zon);
        else
          sprintf(p,"$;1;3P;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%s\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.distance/10.0,sd.hdistance/10.0,sd.height/10.0,sd.diameter/10.0,sd.gamma/100.0,sd.yaw/100.0,sd.targetx,sd.targety,sd.targetz,zon);
      }
      else
      sprintf(p,"$;1;3P;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;%.1f;%.1f;%.1f;;%.1f;%.1f;%.1f;%.1f;%.1f;%s;\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.distance/10.0,sd.hdistance/10.0,sd.height/10.0,sd.gamma/100.0,sd.yaw/100.0,sd.targetx,sd.targety,sd.targetz,zon);
      if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
      break;
    case ANGLE_METHOD:
      sprintf(p,"$;1;ANGLE;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;;;;;%.1f;;;;;;\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.gPitch*180.0/PI);
      if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
      break;
case ANGLE_METHOD_TRAIL:  //VLG-7
      sprintf(p,"$;1;ANGLE;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;;;;;%.1f;%.1f;;;;;\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.gPitch*180.0/PI,sd.yaw/100.0);      //skriv �ven ut b�ringen sd.yaw
      if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
      break;
    case ONE_P_3D_METHOD:

      break;
    case AREAMETHOD:
      sprintf(p,"$;1;AREA;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;%.0f;;;;;;;;;;;;\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.area);
      if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
      break;
    case GPSMETHOD:
    case GPSSINGLEMETHOD:
      sprintf(p,"$;1;GPSPOS;;;;%d;;;;;;%s;%s;%s;%s;%s;%s;%s;%s;%d;;;;;;;;;;%.1f;%.1f;%.1f;%s\r\n",sd.id,tlat,tns,tlon,tew,alt,hdop,date,utc,sd.sek,sd.pgps->x,sd.pgps->y,sd.pgps->hgt,zon);
      if((res=WriteDataOnDisk(handle,p))<=0)
        res=-1;
      break;
    default:
      break;
    }
    }
  }

    free(p);

  }
  else
    res=0;//malloc failed
return res;
}

//VLG-2 kml polygon, spara punkter till tempor�r fil
/**********************************
*
*
***********************************/
int StoreDataAreaPoint(struct GPSType *gp, double targetz, int type, int method)
{
  struct kmlareapointdatatype kap;
  memset(&kap,0x00,sizeof(kap));
  kap.dLat = gp->latf;
  kap.dLon = gp->lonf;
  kap.dAlt = gp->hgt + targetz;

  if(method!=ONE_P_LASER_METHOD_REPEAT)  //spara inte referenspunkter m.m.
    return 0;

  if(type!=POINT_LASER_BASE && type!=POINT_DME_BASE)  //spara endast baspunkter
    return 0;

  if(gHandle >= 0 && lseek(gHandle,0,SEEK_END)>=0)
  {
    //spara data
    if(write(gHandle,(unsigned char *)&kap,sizeof(kap))!=sizeof(kap))
    {
      //gick inte att spara
      CloseFile(&gHandle);
      return -1;
    }
    return 1;
  }
  return -1;
}

//VLG-2 skapa tempor�r fil
/**********************************
*
*
***********************************/
int CreateAreaPointFile()
{
  int tHandle=-1;

  if((tHandle=open(TEMPAREAPOINTFN,_LLIO_RDWR))>=0)
  {
    //fil finns, radera, skapa ny
    CloseFile(&tHandle);
    DeleteFile(TEMPAREAPOINTFN);
  }
  if((tHandle = create(TEMPAREAPOINTFN,_LLIO_RDWR))<0)
    return -1;

  return tHandle;
}

//VLG-2 skriv polygonpunkterna till kml-filen
/**********************************
*
*
***********************************/
int WriteKmlPolygon(double area, double len, int *kmlhandle)
{
  char *p;
  struct kmlareapointdatatype kap,kaps;
  int first;

  //kolla storleken p� fil om n�got finns att skriva, annars retur
  if(gHandle < 0 || filesize(gHandle)<=0)
    return 1;

  if((p=malloc(DATAn_))!=NULL)
  {
    p[0]=0;
    if(lseek(gHandle,0,SEEK_SET)>=0)
    {
      if(lseek(*kmlhandle,-strlen(DOC_END),SEEK_END)>0)
      {
        sprintf(p,"<Style id='VStyle4'>\r\n<LineStyle>\r\n<color>ffff00ff</color>\r\n<width>2</width>\r\n</LineStyle><PolyStyle><fill>0</fill></PolyStyle>\r\n</Style><Placemark>\r\n<name>GEO Polygon</name><description><table><tr><td><b>Area:</b></td><td>%.0f</td></tr><tr><td><b>Length:</b></td><td>%.0f</td></tr></table></description>\r\n<styleUrl>#VStyle4</styleUrl><Polygon>\r\n<altitudeMode>clampToGround</altitudeMode>\r\n<outerBoundaryIs>\r\n<LinearRing>\r\n<coordinates>\r\n",area,len);
        if(WriteDataOnDisk(kmlhandle,p)>0)
        {
          first=1;
          while(read(gHandle,(unsigned char*)&kap,sizeof(kap))==sizeof(kap))
          {
            if(first)
            {
              memcpy(&kaps,&kap,sizeof(kap));
              first=0;
            }
            sprintf(p,"%.7f,%.7f,%.1f\r\n",kap.dLon,kap.dLat,kap.dAlt);
            if(WriteDataOnDisk(kmlhandle,p)<0)
              break;
          }

          if(first==0)
          {
            //skriv startpunkten sist ocks� f�r att knyta ihop polygonet
            sprintf(p,"%.7f,%.7f,%.1f\r\n",kaps.dLon,kaps.dLat,kaps.dAlt);
            WriteDataOnDisk(kmlhandle,p);
          }
        }

        WriteDataOnDisk(kmlhandle,"</coordinates>\r\n</LinearRing>\r\n</outerBoundaryIs>\r\n</Polygon>\r\n</Placemark>\r\n"DOC_END);
      }
    }
    free(p);
  }
  return 1;
}

/**********************************
*
*
***********************************/
int OpenOrCreateKmlFile(char *filekmlname)
{
  int thandlekml=-1;
  if((thandlekml=open(filekmlname,_LLIO_WRONLY))<0)
    if((thandlekml=create(filekmlname,_LLIO_WRONLY))<0)
      return -1;
    else
      if(WriteDataOnDisk(&thandlekml,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<kml xmlns='http://earth.google.com/kml/2.0'>\r\n<Document>\r\n<Style id='VStyle'>\r\n<LineStyle>\r\n<color>ffff0000</color>\r\n<width>2</width>\r\n</LineStyle>\r\n</Style><Style id='VStyle2'>\r\n<LineStyle>\r\n<color>ff0000ff</color>\r\n<width>2</width>\r\n</LineStyle>\r\n</Style>\r\n<Style id='VStyle3'>\r\n<LineStyle>\r\n<color>ffff0000</color>\r\n<width>2</width>\r\n</LineStyle>\r\n<IconStyle>\r\n<Icon><href>http://maps.google.com/mapfiles/kml/pushpin/grn-pushpin.png</href></Icon>\r\n</IconStyle>\r\n</Style>\r\n"DOC_END)<=0)        //LGEO3DPILE-6 gr�n ikonf�rg f�r baspunkter
        {
        CloseFile(&thandlekml);
        return -1;
      }
    return thandlekml;
}


//<Placemark>\r\n<styleUrl>#VStyle</styleUrl><Polygon><altitudeMode>relativeToGround</altitudeMode><outerBoundaryIs><LinearRing><coordinates>
/**********************************
*
*
***********************************/
int OpenOrCreatePolygonKmlFile(char *filekmlname)
{
  int thandlekml=-1;
  if((thandlekml=open(filekmlname,_LLIO_WRONLY))<0)
    if((thandlekml=create(filekmlname,_LLIO_WRONLY))<0)
      return -1;
    else
      if(WriteDataOnDisk(&thandlekml,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<kml xmlns='http://earth.google.com/kml/2.0'>\r\n<Document>\r\n<Style id='VStyle'>\r\n<LineStyle>\r\n<color>ff0000ff</color>\r\n<width>2</width>\r\n</LineStyle><PolyStyle><fill>0</fill></PolyStyle>\r\n</Style><Placemark>\r\n<name>GEO Polygon</name><styleUrl>#VStyle</styleUrl><Polygon><altitudeMode>clampToGround</altitudeMode><outerBoundaryIs><LinearRing><coordinates>\r\n"POLYDOC_END)<=0)       //VLG-2 tagit bort extra > vid <altitudeMode>, stod <altitudeMode>>
      {
        CloseFile(&thandlekml);
        return -1;
      }
    return thandlekml;
}

/**********************************
*
*
***********************************/
int OpenOrCreateCsvFile(int type,char *filecsvname)
{
      int thandlecsv=-1;

if((thandlecsv=open(filecsvname,_LLIO_WRONLY))<0)
    if((thandlecsv=create(filecsvname,_LLIO_WRONLY))<0)
      return -1;
    else
      if(WriteDataOnDisk(&thandlecsv,"MARK;STATUS;TYPE;PROD;VER;SNR;ID;UNIT;TRPH;REFH;P.OFF;DECL;LAT;N/S;LON;E/W;ALTITUDE;HDOP;DATE;UTC;SEQ;AREA;VOL;SD;HD;H;DIA;PITCH;AZ;X(m);Y(m);Z(m);UTM ZONE;\r\n")<=0)        //VLG2-2: ut�kat med DIAMETER-kolumn, efter h�jd
      {
        CloseFile(&thandlecsv);
        return -1;
      }
    else if(type==STOREDATA||type==FORCESTORESETTINGS)
        StoreDataCsv(STORESETTINGS,&thandlecsv,NULL);//just created file, store cur settings
    return thandlecsv;
}

/**********************************
*
*
***********************************/
int OpenOrCreateJsonFile(int type,char *filejsonname)
{
  int thandlejson=-1;
  char str[256];
  if((thandlejson=open(filejsonname,_LLIO_WRONLY))<0)
    if((thandlejson=create(filejsonname,_LLIO_WRONLY))<0)
      return -1;
    else
    {
      sprintf(str,"{\"type\": \"FeatureCollection\",\"name\": \"%s\",\"features\": [%s",filejsonname,JSONDOC_END);
      if(WriteDataOnDisk(&thandlejson,str)<=0)
      {
        CloseFile(&thandlejson);
        return -1;
      }
    }
    return thandlejson;
}

/**********************************
*
*
***********************************/
int OpenOrCreatePolygonJsonFile(int type,char *filejsonname)
{
  int thandlejson=-1;
  char str[256];
  if((thandlejson=open(filejsonname,_LLIO_WRONLY))<0)
    if((thandlejson=create(filejsonname,_LLIO_WRONLY))<0)
      return -1;
    else
    {
      sprintf(str,"{\"type\": \"FeatureCollection\",\"name\": \"%s\",\"features\": [{\"type\": \"Feature\",\"properties\": {\"Name\" : \"GEO Polygon\" },\"geometry\": { \"type\": \"Polygon\", \"coordinates\":  [[%s",filejsonname,JSONPOLYDOC_END);
      if(WriteDataOnDisk(&thandlejson,str)<=0)
      {
        CloseFile(&thandlejson);
        return -1;
      }
    }
    return thandlejson;
}

/**********************************
*
*
***********************************/
int fMemoryMenu()
{
  const char *p[]={
    str_menu_SendFiles_,
    str_menu_EnableMem_,
    str_menu_ClearMem_,
    str_menu_Bluetooth_,    //VLG2-16 Obex
    str_menu_Exit_,
    ""};

  static int n=0;
  struct disp_meny_type tmeny={0,0,3,0,11,str_menu_Mem_,p};
  if(n>3)
    n=0;
  tmeny.act_item=n;
  cls();
  switch(n=Disp_Menu(&tmeny))
  {
  case 0: fmemsendfileBlueLE();
    break;
  case 1: fEnableMem();
    break;
  case 2: fmemdel();
    break;
  case 3:
    fObex();
    break;
  default:
    break;
  };
  return 1;
}

/**********************************
*
* VLG2-16 Obex
***********************************/
int fObex()
{
  //Kr�ver minst Bios v3.2
  if(GetVerNr() < 32)
  {
    Message(str_msg_Error_,str_msg_UpdateBios_);
    return 0;
  }

  GetObexData("");      //DATA-mappen

  return 1;
}

/**********************************
*
*
***********************************/
int fmemsendfileBlueLE()
{
  const char *pBox[] = {"CSV","KML", "GEOJSON", ""};
  int nRes[3],res=1;

  nRes[0]=g_Sysdata.nSendFileType[0];    //csv
  nRes[1]=g_Sysdata.nSendFileType[1];    //kml
 nRes[2]=g_Sysdata.nSendFileType[2];    //geojson  //VLG2-10 lagt till json

  struct disp_menybox_type tBox = {0,0,3,0,10,str_menu_Select_,pBox,nRes};

  Cls();
  cls_Oled();

  //val av filtyp
  if(CheckBox(&tBox)==KEY_ENTER)
  {
    if(nRes[0]!=0 || nRes[1]!=0 || nRes[2]!=0)        //om inget valt s� finns ingen anledning att skicka
    {
      //skicka filtyper enligt valet
      if(nRes[0] != 0)  //CSV
      {
        res=SendObexFile("*.CSV",nRes[1]==0);       //skicka eof om kml ej skickas
      }

      if(res && nRes[1] != 0)  //KML
      {
        SendObexFile("*.KML",1);        //skicka eof, f�r Z-kommando och avsluta bt
      }

      if(res && nRes[2] != 0)  //GEOJSON
      {
        SendObexFile("*.GEOJSON",1);        //skicka eof, f�r Z-kommando och avsluta bt
      }
    }

    //kolla om n�got �ndrats
    if(nRes[0] != g_Sysdata.nSendFileType[0] || nRes[1] != g_Sysdata.nSendFileType[1] || nRes[2] != g_Sysdata.nSendFileType[2])
    {
      g_Sysdata.nSendFileType[0] =  nRes[0];    //csv
      g_Sysdata.nSendFileType[1] =  nRes[1];    //kml
      g_Sysdata.nSendFileType[2] =  nRes[2];    //geojson

      SaveSysdata();        //save
    }
  }
  return 1;
}

/**********************************
*
*
***********************************/
int fEnableMem()
{
  int tnum=ee->EEsaveEEmem;

  //save data in memory
  cls();
  xprintf(0,0,0,12,str_SaveDataIn_);
  if((tnum=My_YesNoExit(0,2,(char*)str_Memory_,tnum))<0)
    return 1;

  if(tnum!=ee->EEsaveEEmem)
  {
    ee->EEsaveEEmem=tnum;
    Bell(200);
      SetGeoSettings();//save ee
  }
  return 0;
}

/**********************************
*
*
***********************************/
int fmemdel()
{
  int res;
  struct FileInfoType F;
  char str_msg[36+1];

  cls();
  sprintf(str_msg,"%-12.12s",str_delete_all_);
  if((res=YesNo(0,str_msg))<=0||res==2)
    return 1;

  cls();
  sprintf(str_msg,"%s",str_msg_SureQ_);
  if((res = YesNo(false,str_msg))==1)
  {
    cls();
    putxy(0,0,"...");
    while(FindFirst(".CSV",&F)>=0)
      if(DeleteFile(F.name)<0)
        break;
    while(FindFirst(".KML",&F)>=0)
      if(DeleteFile(F.name)<0)
        break;
    data_set();
    sd.id=1;
    AddData(STORESETTINGS);//store setting in datafile
    clr_buff();
    Bell(200);
  }
  return 0;
}

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/
