#include "MyFuncs.h"

//Standardprogrammet f�r Geo2, produktkod 1000
//Anv�nd programmet b�de f�r VL och L, bios s�ger typen av instrument STATUS_USE_DME

/*Historik
V3.2 F�rsta version, utvecklad fr�n standardprogrammet Geo v3.2

V3.3
- Visar nu kompassriktningen i HEIGHT DME funktionen, som det g�rs i HEIGHT 3P, VLG-26
- �ndrat i Line Clear s� kan ange manuell inmatning av ledningsh�jden, VLG-30
- Skjuter kontinuerligt med lasern, VLG-28 (Bios)
- Visar vinkelv�rdet i OLED under m�tning, som i kompassm�tningen, VLGF-29

V3.4
- Lagt till Diameterm�tning i h�jdfunktionerna 1PL, 3P och 2P DME och val i Settings (bios), VLG2-2
- Valbart vilka funktioner som ska visas i huvudmenyn, val i Settings, VLG2-5
- �ndrat 3D Vektorm�tningen, om scannar avst�ndet s� beh�ver inte trycka SEND f�r att g� vidare,VLG2-8

V3.5
- Lagt till diameterv�rdet i NMEA-str�ngen i cm eller inch med 1 decimal

V3.6
- �ndra rad 2 fr�n produktnummer till produktnamn i displayen, STD
- Bytt namn p� Geo2 standardapp till STD
- Aktivera �ven Height DME som default VL Geo2 Std

V3.7
- Reversed compass, VLG2-14

*/

int force_compass;      //flagga f�r att tvinga anv�ndande av kompassen //VLG2-2 Kompassen kommer alltid anv�ndas, STATUS_USE_COMPAS returnerar alltid 1. Tagit bort ikonen fr�n huvudmenyn. Kontrollerna f�r vara kvar om �ndras i framtiden
int point_basepoint;     //LGEO3DPILE-6
int auto_save_target;    //VLG-2 flagga f�r att autospara v�rden i map target
int no_save_angle;       //VLG-2 flagga f�r att inte kunna spara i measure_angle_() p� Send, f�r map target
int UseLaser;
int main_lang;
int use_dme;     //flagga fr�n bios om det �r VL eller L instrument, anv�nd ist�llet f�r USE_DME direktivet i koden
int measured_continously;     //VLG2-8 flagga f�r att visa om skjutit kontinuerligt med lasern under m�tning
int rev_compass;        //VLG2-14 flagga om reversed compass ska anv�ndas

struct storedatatype sd;
struct storedatatype autosd;     //VLG-2 f�r autospara h�jd
struct SendBlueType sdBlue;//V2.0 use binary struct to send saved data
struct sysdatatype g_Sysdata;

int gHandle;            //VLG-2 globalt file handle

int checkrollflag;      //VLG2-2 Diameterm�tning, anv�nds som argument till ToggleOledTxtAndSightGetSystemKey f�r att styra om den ska reagera p� tiltning av GEOn
struct meas_dia_type dia_res;   //VLG2-2 Diameterm�tning, resultatet fr�n diameterm�tningen

/**********************************
*
*
***********************************/
void MainMenuRedraw()
{
  cls();
  My_Disp_Draw_Icon(&logo);
  My_SetFont(68); //SetFont(GetFont6x8());      //Om anv�nder annan font �n 6x8 eller 8x13 s� uppdatera My_SetFont-funktionen f�r att st�dja dessa
  if(use_dme)
  {
    xprintf(16-6,0,0,8,"VLGEO2");
  }
  else
  {
    xprintf(16-6,0,0,8,"L GEO2");
  }
  //xprintf(16-5,1,0,12,"P%04d",product); //VLG2-13 �ndra rad 2 fr�n produktnummer till produktnamn i displayen, STD
  xprintf(16-5,1,0,12,"STD");
  xprintf(16-5,2,0,12,"V%d.%d",ver/10,ver%10);  //VLG2-13 flyttat ett steg �t v�nster
  putint(16-5,3,GetSerialNr(),5,0);
  My_SetFont(813); //SetFont(GetFont8x13());
}


/**********************************
*
*
***********************************/
void main()
{
  int flag=0,ch,lang;

  InitApp();    //Init Application

  while(1)
  {
    ch = GetChar(DISP);
  mainloop:
    Cls();
    if(ch==KEY_LEFT)//DME KEY
      fDme();
    else
      do
      {
        //Om �ndrar antalet s� uppdatera �ven NUM_OF_VL_FUNCTIONS i MyFuncs.h      //OBS! Kan �ven p�verka val av funktioner i huvudmenyn
        const struct MenuItemType MainMenuItemVL[]=     //VLGEO instrument
        {
          {str_menu_Height1PL_,&ico_1pl,fhgt1pl},
          {str_menu_Height3P_,&ico_3pl,fhgt3pl},
          {str_menu_HeightDme_,&ico_2p,fhgt2pdme},
          {str_menu_3D_,&ico_3d,f3d},
          {str_menu_Compass_,&ico_comp,fmeasure_compass},
          {str_menu_CompassRev_,&ico_comp_rev,fmeasure_compass_rev},       //VLG2-14 lagt till Reversed Compass funktion
          {str_menu_Angle_,&ico_none,measure_angle},
          {str_menu_MapTarget_,&ico_map,fmeasure_map},
          {str_menu_MapTrail_,&ico_trail,fmeasure_trailxy_},
          {str_menu_LineClear_,&ico_dang,DangTreeMenu},
          {str_menu_DeltaHeight_,&ico_delta,fhgtdelta},
          {str_menu_BAF_,&ico_baf,fmeasure_baf},        //VLG2-15 lagt till Baf-funktion
          {str_menu_Mem_,&ico_memory,fMemoryMenu},
          {str_menu_Settings_,&ico_none,fSettings},
          {str_menu_Usb_,&ico_none,fUsb}
        };

        const struct MenuType MainMenuVL=
        {
          MainMenuRedraw,
          MainMenuItemVL,
        };

        //Om �ndrar antalet s� uppdatera �ven NUM_OF_L_FUNCTIONS i MyFuncs.h      //OBS! Kan �ven p�verka val av funktioner i huvudmenyn
        const struct MenuItemType MainMenuItemL[]=      //LGEO instrument
        {
          {str_menu_Height1PL_,&ico_1pl,fhgt1pl},
          {str_menu_Height3P_,&ico_3pl,fhgt3pl},
          {str_menu_3D_,&ico_3d,f3d},
          {str_menu_Compass_,&ico_comp,fmeasure_compass},
          {str_menu_CompassRev_,&ico_comp_rev,fmeasure_compass_rev},       //VLG2-14 lagt till Reversed Compass funktion
          {str_menu_Angle_,&ico_none,measure_angle},
          {str_menu_MapTarget_,&ico_map,fmeasure_map},
          {str_menu_MapTrail_,&ico_trail,fmeasure_trailxy_},
          {str_menu_LineClear_,&ico_dang,DangTreeMenu},
          {str_menu_DeltaHeight_,&ico_delta,fhgtdelta},
          {str_menu_BAF_,&ico_baf,fmeasure_baf},        //VLG2-15 lagt till Baf-funktion
          {str_menu_Mem_,&ico_memory,fMemoryMenu},
          {str_menu_Settings_,&ico_none,fSettings},
          {str_menu_Usb_,&ico_none,fUsb}
        };

        const struct MenuType MainMenuL=
        {
          MainMenuRedraw,
          MainMenuItemL,
        };

        force_compass=0;    //flagga f�r att tvinga anv�ndande av kompassen
        auto_save_target=0;
        no_save_angle=0;
        rev_compass=0;

        checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

        lang=main_lang;
        if(use_dme)
          ExecMeny(&MainMenuVL,sizeof(MainMenuItemVL)/sizeof(MainMenuItemVL[0]),&ee->lastmenu,1);
        else
          ExecMeny(&MainMenuL,sizeof(MainMenuItemL)/sizeof(MainMenuItemL[0]),&ee->lastmenu,1);

        if(lang != main_lang)
          goto mainloop;        //Bytt spr�k, l�s om textstr�ngarna

        flag= GetStatus();
      }
    while(flag&STATUS_USB_5V);

    flag= GetStatus();  //l�s status ifall kommer direkt fr�n fDme-funktionen
    if(/*system_is_debugger_present()||*/flag&STATUS_USE_BLUETOOTH||flag&STATUS_USE_GPS)        //st�ng inte ner om debugger k�rs
    {
      int i=10;//10==20min
      while(i--)
      {
        cls();
        putxy(0,0,(char *)str_turnoff1_);
        putxy(0,1,(char *)str_turnoff2_);
        putint(0,2,i*TIMETOSLEEP/60,2,0);//min
        putxy(3,2,(char *)str_turnoff3_);
        if((ch=GetSystemKey())==KEY_EXIT)
          break;
        if(ch>0)
          goto mainloop;
      }
    }
    cls();
    putxy(0,0,(char *)str_closing_);
    ExitBluetooth();  //BlueDisconnect();//disconnect if ev, connected
    while(1)
      Power(0);       //Power off
  }
}


/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

