#include "MyFuncs.h"

//TODO: inst�llning i Settings om ska anv�nda diameterm�tning vid h�jdm�tning? �ven ikon d� i huvudmenyn?

//TODO: init, exit diametervariabler till ee

int checkrollflag;      //anv�nds som argument till ToggleOledTxtAndSightGetSystemKey f�r att styra om den ska reagera p� tiltning av GEOn

static int dia_tx, dia_ty;
static int dia_linel;
static int dia_offset_r;
static int dia_offset_l;
static double dia_factor;
static int dia_line_style;
static int dia_prew_move;


/**********************************
*
*
***********************************/
int ConvToInch(int mm)
{
  long temp;
  temp=(long)mm*3937L;
  return rounddiv(temp,10000);
}


/**********************************
*
*
***********************************/
void init_dia_meas()
{
  dia_tx = 48;
  dia_ty = 12;
  dia_linel = 8;

  //TODO: f�r test
  dia_offset_l = 5;
  dia_offset_r = 5;
  dia_factor = 2.14;           //Anv�nd samma faktor f�r feet mode, r�knar i cm och mm och g�r om resultatet till feet och inch i display och utskrift
  dia_line_style = 0;

  /* TODO: h�mta inst�llningarna fr�n minnet
  dia_offset_l = ee->offset_left;
  dia_offset_r = ee->offset_right;
  dia_factor = ee->DiaFactor / 100.0;           //Anv�nd samma faktor f�r feet mode, r�knar i cm och mm och g�r om resultatet till feet och inch i display och utskrift
  dia_line_style = ee->line_style;
*/
  if(dia_offset_l < dia_offset_r)
    dia_prew_move = RP;
  else
    dia_prew_move=LP;

  checkrollflag=LASERROLLED_NONE;
}

/**********************************
*
*
***********************************/
void exit_dia_meas()
{
  checkrollflag=LASERROLLED_NONE;      //s� att inte reagerar om enheten vinklas �t sidan

  //TODO: spara endast om n�got �ndrats
  /*
  if(ee->offset_left != dia_offset_l || ee->offset_right != dia_offset_r || ee->line_style != dia_line_style)
  {
  ee->offset_left = dia_offset_l;
  ee->offset_right = dia_offset_r;
  ee->line_style = dia_line_style;
  SetGeoSettings();//save ee
  }*/
}


/**********************************
*
*
***********************************/
void DrawLineOled(int tx, int ty, int max, int line_style)
{
  if(line_style != LINE_BAR)    //Anv�nder just nu endast Fulla streck eller linje
  {
    for(int i=0; i<max; i++)
    {
      if(line_style==LINE_FILLED)   //fulla streck
      {
        disp_put_pix_Oled_CharMode(tx, ty+i);
      }
      else if(line_style==LINE_SECOND)   //varannan prick
      {
        if(i%2==0)
          disp_put_pix_Oled_CharMode(tx, ty+i);
      }
      else if(line_style==LINE_242)      //2 prick - 4 tomt - 2 prick
      {
        if(i==0 || i==1 || i==max-2 || i==max-1)
          disp_put_pix_Oled_CharMode(tx, ty+i);
      }
      else if(line_style==LINE_161)      //prick - 6 tomt - prick
      {
        if(i==0 || i==max-1)
          disp_put_pix_Oled_CharMode(tx, ty+i);
      }
    }
  }
  else  //en linje
  {
    if(max < 0)
    {
      for(int i=max; i<=0; i++)
        disp_put_pix_Oled_CharMode(tx+i, ty);
    }
    else if(max > 0)
    {
      for(int i=0; i<=max; i++)
        disp_put_pix_Oled_CharMode(tx+i, ty);
    }
    else
    {
      disp_put_pix_Oled_CharMode(tx, ty);
    }
  }
}

/**********************************
*
*
***********************************/
void ClrLineOled(int tx, int ty, int max, int line_style)
{
  if(line_style != LINE_BAR)
  {
    for(int i=0; i<max; i++)
    {
      if(line_style==LINE_FILLED)   //fulla streck
      {
        disp_clr_pix_Oled_CharMode(tx, ty+i);
      }
      else if(line_style==LINE_SECOND)   //varannan prick
      {
        if(i%2==0)
          disp_clr_pix_Oled_CharMode(tx, ty+i);
      }
      else if(line_style==LINE_242)      //2 prick - 4 tomt - 2 prick
      {
        if(i==0 || i==1 || i==max-2 || i==max-1)
          disp_clr_pix_Oled_CharMode(tx, ty+i);
      }
      else if(line_style==LINE_161)      //prick - 6 tomt - prick
      {
        if(i==0 || i==max-1)
          disp_clr_pix_Oled_CharMode(tx, ty+i);
      }
    }
  }
  else  //en linje
  {
    if(max < 0)
    {
      for(int i=max; i<=0; i++)
        disp_clr_pix_Oled_CharMode(tx+i, ty);
    }
    else if(max > 0)
    {
      for(int i=0; i<=max; i++)
        disp_clr_pix_Oled_CharMode(tx+i, ty);
    }
    else
    {
      disp_clr_pix_Oled_CharMode(tx, ty);
    }
  }
}


/**********************************
*
*
***********************************/
int meas_dia(int return_on_enter)
{
  int ch=0;
  int dia=0;
  int redraw;
  int toffset_r, toffset_l;

  toffset_r = dia_offset_r;
  toffset_l = dia_offset_l;

  sd.measmethod=ONE_P_LASER_METHOD;     //st�ller om metoden f�r sparningen

  clr_buff();

  Sight_Oled(0);
      Bell(200);

      //diameterm�tning
    cls();
    putxy(0,0, (char*)str_Dia_Dist_);
    if(Metric())
    {
    putint(5,0,sd.distance_cm,6,2);   //visa avst�ndet i cm
    }
    else
    {
      putint(5,0,ConvToFeet(sd.distance_cm),6,1);      //skriver ut distance i feet, 1 decimal
    }

    putxy(0,2, (char*)str_Dia_Bar1_);
  putxy(0,3, (char*)str_Dia_Bar2_);
  putxy(0,4, (char*)str_Dia_Bar3_);

  //justera strecken DME+SEND, l�ngt tryck SEND f�r att byta typ p� strecket. ON f�r att m�ta avst�ndet och r�kna ut diametern
    redraw=0;

    cls_Oled();
    if(dia_line_style != LINE_BAR)
    {
      DrawLineOled(dia_tx-dia_offset_l-1,dia_ty,dia_linel,dia_line_style);       //skriv ut nya linjerna
      DrawLineOled(dia_tx+dia_offset_r,dia_ty,dia_linel,dia_line_style);
    }
    else
    {
      DrawLineOled(dia_tx-1,16,-dia_offset_l,dia_line_style);       //skriv ut nya linjerna
      DrawLineOled(dia_tx,16,dia_offset_r,dia_line_style);
    }
    disp_redraw_Oled();
SetOledTimer(30); //s� att Oled visar strecken l�ngre

    do
    {
      if(redraw)
      {
        //uppdatera en linje per tryck, dubbelt s� m�nga steg

        if(dia_line_style != LINE_BAR)
        {
          ClrLineOled(dia_tx-toffset_l-1,dia_ty,dia_linel,dia_line_style);       //rensa gamla linjen
          DrawLineOled(dia_tx-dia_offset_l-1,dia_ty,dia_linel,dia_line_style);       //skriv ut nya

          ClrLineOled(dia_tx+toffset_r,dia_ty,dia_linel,dia_line_style);
          DrawLineOled(dia_tx+dia_offset_r,dia_ty,dia_linel,dia_line_style);
        }
        else
        {
          ClrLineOled(dia_tx-1,16,-toffset_l,dia_line_style);       //rensa gamla linjen
          DrawLineOled(dia_tx-1,16,-dia_offset_l,dia_line_style);       //skriv ut nya

          ClrLineOled(dia_tx,16,toffset_r,dia_line_style);
          DrawLineOled(dia_tx,16,dia_offset_r,dia_line_style);
        }

        disp_redraw_Oled();
        redraw=0;

      SetOledTimer(30); //s� att Oled visar strecken l�ngre
      }

      if(return_on_enter)
        checkrollflag=LASERROLLED_NONE;
      else
        checkrollflag=LASERROLLED_UP;
      ch=CheckRollGetSystemKey(checkrollflag);

      switch(ch)
      {
      case LASERROLLED_UP:
        //�terg� till avst�ndsm�tningen
        Bell(200);
        checkrollflag=LASERROLLED_NONE;
        return LASERROLLED_UP;
      case KEY_LEFT:
        redraw=1;       //rita om

        SetTimer(500);
        while(GetTimer() && scankey()&KEY_LEFT_BIN);    //om l�ngt tryck f�r att rita ut strecken utan att flytta dessa

        if(!GetTimer() && scankey()&KEY_LEFT_BIN) //kolla om fortfarande h�ller inne send efter 1sek
        {
          wait(100); //v�nta lite

          if(scankey()&KEY_LEFT_BIN)  //h�ller fortfarande inne send
          {
            Bell(100);

            while(scankey()&KEY_LEFT_BIN);      //v�nta tills sl�ppt upp knappen
            wait(100); //v�nta lite

            cls_Oled();  //rensa old och rita om strecken
            clr_buff();
            break;
          }
        }

        if(dia_prew_move==RM || dia_prew_move==LP)
        {
          toffset_l = dia_offset_l;
          if(--dia_offset_l<0)
          {
            Bell(10);
            dia_offset_l=0;
          }
          dia_prew_move=LM;
        }
        else
        {
          toffset_r = dia_offset_r;
          if(--dia_offset_r<0)
          {
            Bell(10);
            dia_offset_r=0;
          }
          dia_prew_move=RM;
        }

        break;
      case KEY_RIGHT:
        redraw=1;       //rita om

        SetTimer(500);
        while(GetTimer() && scankey()&KEY_RIGHT_BIN);    //om l�ngt tryck s� byt typ p� strecket

        if(!GetTimer() && scankey()&KEY_RIGHT_BIN) //kolla om fortfarande h�ller inne send efter 1sek
        {
          wait(100); //v�nta lite

          if(scankey()&KEY_RIGHT_BIN)  //h�ller fortfarande inne send
          {
            Bell(100);

            if(++dia_line_style > MAX_LINE_STYLE-1)
              dia_line_style=0;

            while(scankey()&KEY_RIGHT_BIN);      //v�nta tills sl�ppt upp knappen
            wait(100); //v�nta lite

            cls_Oled();  //rensa old och rita om strecken
            clr_buff();
            break;
          }
        }

        if(dia_prew_move==RP || dia_prew_move==LM)
        {
          toffset_l = dia_offset_l;
          if(++dia_offset_l>MAX_LINE_OFFSET)
          {
            Bell(10);
            dia_offset_l = MAX_LINE_OFFSET;
          }
          dia_prew_move=LP;
        }
        else
        {
          toffset_r = dia_offset_r;
          if(++dia_offset_r>MAX_LINE_OFFSET)
          {
            Bell(10);
            dia_offset_r = MAX_LINE_OFFSET;
          }
          dia_prew_move=RP;
        }
        break;
      case KEY_ENTER:
        break;
      case KEY_EXIT:
        goto end_dia;  //Key Exit
      default:
        break;
      }
    }while(ch!=KEY_ENTER);

    Wait(20);   //V�nta lite, tappar annars pipet nedan d� knapptryckspipet fortfarande k�rs
    Bell(200);
Sight_Oled(0);

    WhileEnterPressed();   //v�rde f�tts, v�nta tille enter sl�ppts

    if(return_on_enter)
    {
      ch= KEY_ENTER;
      goto end_dia;
    }

    //r�kna ut diametern
    if(sd.distance_cm > 0)      //division med distance_cm i en ber�kning nedan //slope-avst�ndet dit diametern m�ts
    {
      int calcdia,dia_cm;

    //r�kna ut dia och visa i displayen och oled
      //2cm/offset p� 10m avst�nd
      if(dia_line_style != LINE_BAR)
        dia = (int)(dia_factor*(dia_offset_l + dia_offset_r)*sd.distance_cm*10.0/1000.0+0.5);       //mm
      else
        dia = (int)(dia_factor*(dia_offset_l + dia_offset_r+2)*sd.distance_cm*10.0/1000.0+0.5);        //om LINE_BAR s� m�ste �ka med 2 f�r att kompensera att offset = 0 �nd� ger 2 prickar i mitten

      cls();

      //Fig 5A Patentet, D=S(1+S/2L), f�renklad formel
      //L = dist, S = synlig del av tr�det, D=verklig dia
      //calcdia = (int)(dia*(1.0+dia/(2.0*sd.distance_cm*10.0))+0.5); //mm      //f�renklad formel

      //D = (S^2 + S*SQRT(S^2 + 4*L^2)) / 2L
      calcdia = (int)((dia*dia + dia*sqrt(dia*dia +4.0*sd.distance_cm*10.0*sd.distance_cm*10.0))/(2.0*sd.distance_cm*10.0) + 0.5);  //mm

      putxy(0,0, (char*)str_Dia_Dist_);
      //putxy(0,1, str_Dia_SDia); //Surface dia
      putxy(0,2, (char*)str_Dia_Dia_); //Calculated dia

      if(Metric())
      {
        putint(5,0,sd.distance_cm,6,2);      //skriver ut distance i cm, 2 decimaler
        //putint(5,1,dia,6,1);      //mm
        putint(5,2,calcdia,6,1);   //mm
      }
      else
      {
        //Om feet s� konvertera dia till inch och dist till feet
      putint(5,0,ConvToFeet(sd.distance_cm),6,1);      //skriver ut distance i feet, 1 decimal
        //putint(5,1,ConvToInch(dia),6,1);      //inch, 1 decimal
        putint(5,2,ConvToInch(calcdia),6,1);   //inch, 1 decimal
      }

      cls_Oled();
      PrintStrOled(0,22,str_oled_Dist_);
      PrintStrOled(0,42,str_oled_Dia_);

      if(Metric())
      {
      dia_cm = roundi(calcdia);        //cm
      PrintIntOled(0,32,sd.distance_cm);//try use 2decimals
      putint2_Oled(0,52,dia_cm,3,0);
      }
      else
      {
        //Om feet s� konvertera dia till inch och dist till feet
        dia_cm = ConvToInch(calcdia);
        dia_cm = roundi(dia_cm);
      PrintIntOled(0,32,ConvToFeet(sd.distance_cm)*10);//feet, try use 2decimals
        putint2_Oled(0,52,dia_cm,3,0); //inch
      }

      SetOledTimer(30);

      sd.treemethod = ONE_P_LASER_METHOD_DIAMETER; //f�r sparningen
      sd.distance1 = dia;       //mm

      sd.distance2 = calcdia;   //Skriver endast ut distance2 i csv och kml, ej distance1

      if(sd.SaveOk==0)
        sd.SaveOk=1;    //s� kan spara nytt v�rde �ven om inte m�tt om avst�ndet (blir d� samma avst�nd som f�reg�ende m�tning)

      do
      {
        if((ch=dme_key())==KEY_EXIT)
        {
          goto end_dia;
        }
        else if(ch==KEY_RIGHT && sd.stored)    //om sparat s� hoppa �ver till h�jdm�tningen igen
        {
          ch=KEY_ENTER;
          goto end_dia;
        }
      }while(ch!=KEY_ENTER);
    }
    else
    {
      //inget avst�nd, kan inte r�kna ut n�gon diameter
      Bell(300);
      cls();
      xprintf(0,0,0,12,"%s %dcm",str_Dia_Dist,sd.distance_cm);
      xprintf(0,2,0,12,str_calc_err1_);
     xprintf(0,3,0,12,str_calc_err2_);

     cls_Oled();
     PrintStrOled(0,22,str_oled_Err_);

      GetSystemKey();
      ch=0;
    }

    clr_buff();

    end_dia:
      checkrollflag=LASERROLLED_NONE;
    return ch;
}


/**********************************
*
*
***********************************/
int distance_to_cm(int dist)
{
  if(Metric())
  {
    return dist;        //�r redan i cm
  }
  else
  {
    //g�r om fr�n ftx10 till cm
    int temp;
  temp=dist*3048;
  return rounddiv(temp,1000);
  }
}

/**********************************
*
* Slope distance
***********************************/
int Sdist(int tdistance)
{
 return rounddiv(1L*tdistance*5000,icos(sd.alpha));
}

/**********************************
*
*
***********************************/
void PrintHeightOled()
{
   PrintIntOled(0,62,sd.height*10);
}



/**********************************
*
*
***********************************/


/**********************************
*
*
***********************************/
