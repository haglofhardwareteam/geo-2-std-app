/* xtime.h internal header */
/* Copyright 2009-2010 IAR Systems AB. */
#ifndef _XTIME
#define _XTIME

#ifndef _SYSTEM_BUILD
#pragma system_include
#endif

#include "xtinfo.h"
_C_STD_BEGIN

                /* macros */
#define WDAY    1       /* to get day of week right */

/* Bias between 1900 (struct tm) and 1970 time_t. */
#define _TBIAS_DAYS (70 * 365L + 17)
#define _TBIAS  (_TBIAS_DAYS * 86400LU)

                /* type definitions */
typedef struct
{       /* rule for daylight savings time */
  unsigned char wday, hour, day, mon, year;
} Dstrule;

                /* internal declarations */
_C_LIB_DECL
__ATTRIBUTES const char * _Gentime(const struct tm *, const _Tinfo *, char, 
                                  char,
                                  int *, char *);
__ATTRIBUTES Dstrule * _Getdst(const char *);
__ATTRIBUTES const char * _Gettime(const char *, int, int *);
__ATTRIBUTES int __iar_Isdst32(const struct tm *);
__ATTRIBUTES struct tm * __iar_Ttotm32(struct tm *, __time32_t, 
                                       int);
__ATTRIBUTES int __iar_Daysto32(int, int);
#if _DLIB_TIME_ALLOW_64
  __ATTRIBUTES int __iar_Isdst64(const struct tm *);
  __ATTRIBUTES struct tm * __iar_Ttotm64(struct tm *, __time64_t, 
                                         int);
  __ATTRIBUTES int __iar_Daysto64(int, int);
#endif /* _DLIB_TIME_ALLOW_64 */
__ATTRIBUTES long _Tzoff(void);

_END_C_LIB_DECL
_C_STD_END
#endif /* _XTIME */

/*
 * Copyright (c) 1992-2009 by P.J. Plauger.  ALL RIGHTS RESERVED.
 * Consult your license regarding permissions and restrictions.
V5.04:0576 */
