//#include "geo.h"

#ifndef DATAMEM_
#define DATAMEM_
#define DATAMAXSTRn 128
extern char datastr[DATAMAXSTRn+1];       //minst 45+1 orginal 9*5+1
#define IDn 5

enum {STORESETTINGS,STOREDATA,FORCESTORESETTINGS};

#define DATAKMLFILENAME "DATA.KML"
#define DATACSVFILENAME "DATA.CSV"
#define DATAJSONFILENAME "DATA.GEOJSON"

void CloseFile(int *handle);
void SendBlueStr(char *p);
void SendBlueLEStr(char *p);
void SendDataBT();
int GetIdData();
int StoreDataKml(int *handle,struct GPSType *gp);
int StoreDataJson(int *handle,struct GPSType *gp);  //VLG2-10 lagt till GeoJsonutskrift
struct GPSType *My_fGpsGetPos(int num, int time);
int ReadGps();
int GpsDecToGrad(struct GPSType *GpsData);
int GpsGradToDec(struct GPSType *GpsData);
int AddData(int type);
int MemStore();
int WriteDataOnDisk(int *handle,char *str);
int getlastdataset();
int ReadSysUtcTime();
int StoreDataCsv(int type,int *handle,struct GPSType *gp);
int StoreDataAreaPoint(struct GPSType *gp, double targetz, int type, int method);        //VLG-2
int CreateAreaPointFile();
int WriteKmlPolygon(double area, double len, int *kmlhandle);
int OpenOrCreateKmlFile(char *filekmlname);
int OpenOrCreatePolygonKmlFile(char *filekmlname);
int OpenOrCreateCsvFile(int type,char *filecsvname);
int OpenOrCreateJsonFile(int type,char *filejsonname);  //VLG2-10 lagt till GeoJsonutskrift
int OpenOrCreatePolygonJsonFile(int type,char *filejsonname);

int fMemoryMenu();
int fmemsendfileBlueLE();
int fEnableMem();
int fmemdel();

int fObex();    //VLG2-16 Obex
#endif
