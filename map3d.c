#include "MyFuncs.h"

/**********************************
*
*
***********************************/
int GetSystemKeyNoTimeLimit()
{
  int ch;
  while(1)  //no time limit
  {
    if((ch=GetSystemKey())==KEY_EXIT||ch==KEY_ENTER)
      break;
    My_bell2(100,1000);//attention
    //wait(100);
    bell(100,3000);//attention
    Sight_Oled(0);//turn of oled, save life hours
  }
  cls_Oled();
  return ch;
}

/**********************************
*
*
***********************************/
int GetSystemKeyNoTimeLimitAllKeys()    //LGEO3DPILE-6
{
  int ch;
  while(1)  //no time limit
  {
    if((ch=GetSystemKey())==KEY_EXIT||ch==KEY_ENTER||ch==KEY_LEFT||ch==KEY_RIGHT)
      break;
    My_bell2(100,1000);//attention
    //wait(100);
    bell(100,3000);//attention
    Sight_Oled(0);//turn of oled, save life hours
  }
  cls_Oled();
  return ch;
}

/**********************************
*
*
***********************************/
int fmeasure_map()
{
  int res;
  const char *p[]={str_menu_MapTarget_,str_menu_MapGps_,str_menu_GpsArea_,str_menu_Exit_,""};
  struct disp_meny_type tmeny={0,0,3,0,11,str_menu_Method_,p};

  do
  {
    cls();
    res=Disp_Menu(&tmeny);
    switch(res)
    {
    case 0:
      if(use_dme)
        fmeasure_map_laserVL();
      else
        fmeasure_map_laserL();
      break;
    case 1:
      fmeasure_map_gps();
      break;
    case 2:
      GpsLog();
      break;
    default:
      return 1;
    }
  }while(1);
}


/**********************************
*
*
***********************************/
int fmeasure_map_laserVL()      //DME + Laser
{
  int ch,flag,thandle=-1,tSaveEEmem,old_point,auto_s;
  char tname[64],str_msg[36+1];
  int res;
  int point_basepoint;     //LGEO3DPILE-6

  // float FirstGpsAltitude=0.0;//in case gps is used as new reference ponts, use to calc local z
  tSaveEEmem=ee->EEsaveEEmem;
  //cont.on last file if exist?
  cls();
  xprintf(0,0,0,12,"...");
  clr_buff();
  data_set();//reset data storage output
  do
  {
    char tname[64];
    sprintf(tname,"MAP%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sd.id++;//next id may be available
    }
  }while(thandle>=0);
  cls();
  clr_buff();
  do
  {
    //char tname[64];
    //int res;
    data_set();//reset data storage output
    if((GetIdData())<0)
      return 1;
    sprintf(tname,"MAP%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);      //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sprintf(tname,"MAP%-5d",sd.id);
      Bell(200);
      cls();
      sprintf(str_msg,"%-12.12s%-12.12s%s",str_Continue_,str_With_,tname);
      if((res=YesNo(1,str_msg))<0||res==2)
        return 1;
      if(res)//cont with latest id
        break;
      sd.id++;//next id may be available
    }
  }while(thandle>=0);

  {
    char mapfilekmlname[64];//if individ file
    char mapfilecsvname[64];//if individ file
    char mapfilejsonname[64];//if individ file
    sprintf(mapfilekmlname,"MAP%d.KML",sd.id);//create filename
    sprintf(mapfilecsvname,"MAP%d.CSV",sd.id);//create filename
   sprintf(mapfilejsonname,"MAP%d.GEOJSON",sd.id);//create filename //VLG2-10 lagt till GeoJsonutskrift
    if((sd.handlekml=OpenOrCreateKmlFile(mapfilekmlname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      return -1;
    }
    if((sd.handlecsv=OpenOrCreateCsvFile(FORCESTORESETTINGS,mapfilecsvname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlekml);
      return -1;
    }
    if((sd.handlejson=OpenOrCreateJsonFile(FORCESTORESETTINGS,mapfilejsonname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlecsv);
      CloseFile(&sd.handlekml);
      return -1;
    }

    //VLG-2 Kml Area, skapa tempor�r fil f�r att spara punkterna f�r polygonet, global file handle
    gHandle = CreateAreaPointFile();
  }

  //VLG-2 sl� p� autosparning
  auto_save_target=ee->AutoSaveTarget;   //h�mta val fr�n minnet
  sprintf(tname,"MAP%-5d",sd.id);
  Bell(200);
  cls();
  sprintf(str_msg,"%-12.12s%-12.12s",str_Autosave1_,str_Autosave2_);
  if((res=YesNo(auto_save_target,str_msg))<0||res==2)
  {
    CloseFile(&sd.handlekml);
    CloseFile(&sd.handlecsv);
    CloseFile(&sd.handlejson);
    CloseFile(&gHandle);
    auto_save_target=0;   //VLG-2 sl� av autosparning
    return 1;
  }
  auto_save_target=res;
  if(auto_save_target!=ee->AutoSaveTarget)
  {
    ee->AutoSaveTarget = auto_save_target;
    SetGeoSettings();//save ee
  }

  ee->EEsaveEEmem = 1; //force mem saving
  old_point=point_basepoint=POINT_LASER_BASE;     //LGEO3DPILE-6
  no_save_angle=1;      //VLG-2 sl� av sparning vid vinkelm�tning
  memset(&autosd,0x00,sizeof(autosd));
  auto_s = auto_save_target;
  force_compass=1;      //tvinga anv�ndande av kompass
  do
  {

  TargetLoop:
    point_basepoint = old_point;
    auto_save_target = auto_s;
    do
    {
      do
      {
        cls();
        Sight_Oled(0);
        Bell(100);

        if(point_basepoint==POINT_LASER_BASE || point_basepoint==POINT_DME_BASE)
        {
          PrintStrOled(0,42,str_oled_Base1_);
          PrintStrOled(0,52,str_oled_Base2_);
          if(point_basepoint==POINT_DME_BASE)
            xprintf(0,0,0,12,str_Dme_);
          else
            xprintf(0,0,0,12,str_Laser_);
          xprintf(0,1,1,12,str_Baseline_);
        }
        else
        {
          PrintStrOled(0,42,str_oled_Target1_);
          PrintStrOled(0,52,str_oled_Target2_);
          xprintf(0,0,0,12,str_Laser_);
          xprintf(0,1,1,12,str_OnTarget_);
        }
        ch=GetSystemKeyNoTimeLimitAllKeys();
        switch(ch)
        {
        case KEY_LEFT:
          if(++point_basepoint > POINT_DME_BASE)
            point_basepoint = POINT_LASER_TARGET;
          break;
        case KEY_RIGHT:
          if(--point_basepoint < POINT_LASER_TARGET)
            point_basepoint = POINT_DME_BASE;
          break;
        case KEY_EXIT:
          goto check_save_and_menu;
        }
      }while(ch!=KEY_ENTER);

      sd.type_target_point = point_basepoint;       //s�tt typen i sd-struct, point_basepoint vid m�tning
      while(fhgt_laser_dme_repeat()/*fhgt1pl_repeat()*/||scankeych()!=KEY_EXIT);//exit only when press exit

      //s�tter om basepoint-flaggan s� att anv�ndaren slipper �ndra metod

      if(point_basepoint==POINT_LASER_BASE || point_basepoint==POINT_DME_BASE)
      {
        old_point = point_basepoint;      //spara om laser eller dme anv�nds f�r basen
        point_basepoint=POINT_LASER_TARGET;
      }
      else if(point_basepoint==POINT_LASER_TARGET)
      {
        //point_basepoint=old_point;
        break;    //LGEO3DPILE-6 hoppa ut till ref-menyn ist�llet, ska inte kunna st�lla om till basepoint igen om valt on target
      }
    }while(1);   //kan nu v�lja mellan baspunkt och punkt p� h�gen, ESC x 2 f�r att ta ny referenspunkt

    //store the first alt as a reference if needed later
    /*
    if(sd.pgps==NULL||sd.pgps->lon==0.0&&sd.pgps->lat==0.0||FirstGpsAltitude>0.0)
    ;
     else
    FirstGpsAltitude=sd.pgps->hgt;//use as a starting reference in case gps is used again, just to calc the local zheight.
    */

  check_save_and_menu:
    if(auto_save_target)
    {
      if(autosd.SaveOk)      //finns n�got att spara?
      {
        sd = autosd;
        ir_str_ram();
        memset(&autosd,0x00,sizeof(autosd));
      }
      sd.PreDefGps=0;
    }
    else if(sd.SaveOk&&!sd.stored)//avsluta och gl�mt lagra sista?
    {
      cls();
      Bell(100);
      xprintf(0,0,0,12,str_StoreTargetQ_);
      xprintf(0,1,0,12,str_Enter_);
      PrintStrOled(0,42,str_oled_StoreQ1_);
      PrintStrOled(0,52,str_oled_StoreQ2_);
      PrintStrOled(0,62,str_oled_StoreQ3_);
      if(GetSystemKeyNoTimeLimit()==KEY_ENTER)
        ir_str_ram();//save....
    }

    auto_s = auto_save_target;
    auto_save_target=0;   //VLG-2 sl� av autospar i ref-l�ge

    flag=0;//assume no new ref point
    do
    {
    RefLoop:
      do
      {
        const char *p[]={str_menu_UseLaser_,str_menu_UseGps_,str_menu_UseDme_,"",str_menu_Finish_,""};
        struct disp_meny_type tmeny={0,0,3,0,11,str_menu_NewRef_,p};
        static int lastmenu=0;
        tmeny.act_item=lastmenu;

        if(flag)//have a new ref
        {
          tmeny.act_item=3;
          p[3]=str_menu_NextTarget_;//to new target
        }
        else
          p[3]=str_menu_Return_;//back to target

        cls();
        Sight_Oled(0);
        wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
        Bell(100);
        PrintStrOled(0,42,str_oled_New_);
        PrintStrOled(0,52,str_oled_Ref_);
        ch=Disp_Menu(&tmeny);
        if(tmeny.act_item<=2)    //remember option 0-2
          lastmenu=tmeny.act_item;

        if(ch==3)//return to TARGET
          goto TargetLoop;//new ref, assume want to measure more
        if(ch==4)//last menu Exit
        {
          goto EndLoop;
        }

        if(ch==0 || ch==2)//meny 0 eller 2
        {
          while((fhgt_laser_dme_relative(ch)/*fhgt1pl_relative()*/||scankeych()!=KEY_EXIT)&&!sd.stored);

          if(flag==0&&!sd.stored)
            goto EndLoop;
          else if(flag&&!sd.stored)
            goto TargetLoop;//new ref, assume want to measure more
          else
            flag=1;//at least one ref point
          cls();
          Sight_Oled(0);
          Bell(100);
          PrintStrOled(0,42,str_oled_Move1_);
          PrintStrOled(0,52,str_oled_Move2_);
          xprintf(0,0,0,12,str_MoveTo_);
          xprintf(0,1,0,12,str_NewRef_);
          GetSystemKeyNoTimeLimit();
        }
        else
        {
          if(ch==1)//GPS
          {
            cls();
            Sight_Oled(0);
            Bell(100);
            PrintStrOled(0,42,str_oled_Move1_);
            PrintStrOled(0,52,str_oled_Move2_);
            xprintf(0,0,0,12,str_MoveTo_);
            xprintf(0,1,0,12,str_NewRef_);
            xprintf(0,2,0,12,str_ThenEnter_);
            xprintf(0,3,0,12,str_ToStartGps_);
            ch=GetSystemKeyNoTimeLimit();
            if(ch==KEY_EXIT)
            {
              if(!flag)
                goto EndLoop;
              else
                goto TargetLoop;//new ref, assume want to measure more
            }
            if(ReadGps())
            {
              My_Bell(200);
              sd.startx=0.0;//relative x
              sd.starty=0.0;//relative y
              //              sd.startz=sd.targetz;//relative z    keep last z
              //sd.startz=sd.pgps->hgt-FirstGpsAltitude;//adj z using gps altitude ignore hgt from gps, use last z
              sd.targetz=0.0;//reset target z in case gps is used again
              goto TargetLoop;
            }
            goto RefLoop;//no gps, try alt. method
          }
        }
      }
      while(!sd.stored);

      if(sd.pgps==NULL||sd.pgps->lon==0.0&&sd.pgps->lat==0.0)
      {
        sd.startx=sd.targetx;//new start lokal pos
        sd.starty=sd.targety;
        sd.startz=sd.targetz;
      }
      else
      {
        struct GPSType tgps=*sd.pgps;
        tgps.x=sd.targetx;//save temp new pos Utm x
        tgps.y=sd.targety;//save temp new pos Utm y
        Gps_ConvertCoord(UTMXY_TO_WGS84LATLONG,&tgps);//To LatLong
        GpsGradToDec(&tgps);//to decimal degree
        sd.pgps->lonf=tgps.lonf;//new pos, update lonf
        sd.pgps->latf=tgps.latf;//new pos, update latf
        sd.pgps->lon=tgps.lon;//new pos, update lon
        sd.pgps->lat=tgps.lat;//new pos, update lat

        sd.startx=sd.targetx-sd.pgps->x;//relative x
        sd.starty=sd.targety-sd.pgps->y;//relative y
        sd.startz=sd.targetz;//relative z
      }

      My_Bell(200);
    }
    while(1);
  EndLoop:
    {
      const char *p[]={str_menu_NewTarget_,str_menu_Finish_,""};
      struct disp_meny_type tmeny={0,0,2,1,11,"",p};
      cls();
      Sight_Oled(0);
      wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
      Bell(100);
      ch=Disp_Menu(&tmeny);
      if(ch==1)
        break;
      flag=1;
    }
  }
  while(flag);

  ee->EEsaveEEmem=tSaveEEmem;
  sd.measmethod=AREAMETHOD;
  sd.SaveOk=1;
  sd.len+=sqrt((sd.x0-sd.xf)*(sd.x0-sd.xf)+(sd.y0-sd.yf)*(sd.y0-sd.yf)); //close polygon
  sd.area+=sd.x0*sd.yf-sd.y0*sd.xf;    //close polygon
  sd.area/=2.0;
  if(sd.area<0.0)      //area = abs(area)
    sd.area*=-1.0;

  point_basepoint=POINT_LASER_TARGET;     //LGEO3DPILE-6
  auto_save_target=0;   //VLG-2 sl� av autosparning
  no_save_angle=0;      //VLG-2 sl� p� sparning vid vinkelm�tning

  PrintAreaLen();

  clr_buff();
  GetSystemKeyNoTimeLimit();

  //VLG-2 skapa polygonet i kml-filen
  WriteKmlPolygon(sd.area, sd.len, &sd.handlekml);

  //st�ng filerna
  CloseFile(&sd.handlekml);
  CloseFile(&sd.handlecsv);
   CloseFile(&sd.handlejson);
  CloseFile(&gHandle);

  //VLG-2 radera tempfilen
  DeleteFile(TEMPAREAPOINTFN);

  data_set();
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}

int fmeasure_map_laserL()       //Enbart Laser
{
  int ch,flag,thandle=-1,tSaveEEmem,old_point,auto_s;
  char tname[64],str_msg[36+1];
  int res;
  int point_basepoint;     //LGEO3DPILE-6

  // float FirstGpsAltitude=0.0;//in case gps is used as new reference ponts, use to calc local z
  tSaveEEmem=ee->EEsaveEEmem;
  //cont.on last file if exist?
  cls();
  xprintf(0,0,0,12,"...");
  clr_buff();
  data_set();//reset data storage output
  do
  {
    char tname[64];
    sprintf(tname,"MAP%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sd.id++;//next id may be available
    }
  }while(thandle>=0);
  cls();
  clr_buff();
  do
  {
    data_set();//reset data storage output
    if((GetIdData())<0)
      return 1;
    sprintf(tname,"MAP%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sprintf(tname,"MAP%-5d",sd.id);
      Bell(200);
      cls();
      sprintf(str_msg,"%-12.12s%-12.12s%s",str_Continue_,str_With_,tname);
      if((res=YesNo(1,str_msg))<0||res==2)
        return 1;
      if(res)//cont with latest id
        break;
      sd.id++;//next id may be available
    }
  }while(thandle>=0);

  {
    char mapfilekmlname[64];//if individ file
    char mapfilecsvname[64];//if individ file
    char mapfilejsonname[64];//if individ file
    sprintf(mapfilekmlname,"MAP%d.KML",sd.id);//create filename
    sprintf(mapfilecsvname,"MAP%d.CSV",sd.id);//create filename
   sprintf(mapfilejsonname,"MAP%d.GEOJSON",sd.id);//create filename //VLG2-10 lagt till GeoJsonutskrift
    if((sd.handlekml=OpenOrCreateKmlFile(mapfilekmlname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      return -1;
    }
    if((sd.handlecsv=OpenOrCreateCsvFile(FORCESTORESETTINGS,mapfilecsvname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlekml);
      return -1;
    }
    if((sd.handlejson=OpenOrCreateJsonFile(FORCESTORESETTINGS,mapfilejsonname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlecsv);
      CloseFile(&sd.handlekml);
      return -1;
    }

    //VLG-2 Kml Area, skapa tempor�r fil f�r att spara punkterna f�r polygonet, global file handle
    gHandle = CreateAreaPointFile();
  }

  //VLG-2 sl� p� autosparning
  auto_save_target=ee->AutoSaveTarget;   //h�mta val fr�n minnet
  sprintf(tname,"MAP%-5d",sd.id);
  Bell(200);
  cls();
  sprintf(str_msg,"%-12.12s%-12.12s",str_Autosave1_,str_Autosave2_);
  if((res=YesNo(auto_save_target,str_msg))<0||res==2)
  {
    CloseFile(&sd.handlekml);
    CloseFile(&sd.handlecsv);
    CloseFile(&sd.handlejson);
    CloseFile(&gHandle);
    return 1;
  }
  auto_save_target=res;
  if(auto_save_target!=ee->AutoSaveTarget)
  {
    ee->AutoSaveTarget = auto_save_target;
    SetGeoSettings();//save ee
  }

  ee->EEsaveEEmem=1; //force mem saving
  old_point=point_basepoint=POINT_LASER_BASE;     //LGEO3DPILE-6
  no_save_angle=1;      //VLG-2 sl� av sparning vid vinkelm�tning
  memset(&autosd,0x00,sizeof(autosd));
  auto_s = auto_save_target;
  force_compass=1;      //tvinga anv�ndande av kompass
  do
  {

  TargetLoop:
    point_basepoint = old_point;
    auto_save_target = auto_s;
    do
    {
      do
      {
        cls();
        Sight_Oled(0);
        Bell(100);

        if(point_basepoint==POINT_LASER_BASE)
        {
          PrintStrOled(0,42,str_oled_Base1_);
          PrintStrOled(0,52,str_oled_Base2_);
          xprintf(0,0,0,12,str_Laser_);
          xprintf(0,1,1,12,str_Baseline_);
        }
        else
        {
          PrintStrOled(0,42,str_oled_Target1_);
          PrintStrOled(0,52,str_oled_Target2_);
          xprintf(0,0,0,12,str_Laser_);
          xprintf(0,1,1,12,str_OnTarget_);
        }
        ch=GetSystemKeyNoTimeLimitAllKeys();
        switch(ch)
        {
        case KEY_LEFT:
          if(++point_basepoint > POINT_LASER_BASE)
            point_basepoint = POINT_LASER_TARGET;
          break;
        case KEY_RIGHT:
          if(--point_basepoint < POINT_LASER_TARGET)
            point_basepoint = POINT_LASER_BASE;
          break;
        case KEY_EXIT:
          goto check_save_and_menu;
        }
      }while(ch!=KEY_ENTER);

      sd.type_target_point = point_basepoint;       //s�tt typen i sd-struct, point_basepoint vid m�tning
      while(fhgt1pl_repeat()||scankeych()!=KEY_EXIT);//exit only when press exit

      //s�tter om basepoint-flaggan s� att anv�ndaren slipper �ndra metod

      if(point_basepoint==POINT_LASER_BASE)
      {
        old_point = point_basepoint;      //spara om laser anv�nds f�r basen
        point_basepoint=POINT_LASER_TARGET;
      }
      else if(point_basepoint==POINT_LASER_TARGET)
      {
        //point_basepoint=old_point;
        break;    //LGEO3DPILE-6 hoppa ut till ref-menyn ist�llet, ska inte kunna st�lla om till basepoint igen om valt on target
      }
    }while(1);   //kan nu v�lja mellan baspunkt och punkt p� h�gen, ESC x 2 f�r att ta ny referenspunkt

    //store the first alt as a reference if needed later
    /*
    if(sd.pgps==NULL||sd.pgps->lon==0.0&&sd.pgps->lat==0.0||FirstGpsAltitude>0.0)
    ;
     else
    FirstGpsAltitude=sd.pgps->hgt;//use as a starting reference in case gps is used again, just to calc the local zheight.
    */

  check_save_and_menu:
    if(auto_save_target)
    {
      if(autosd.SaveOk)      //finns n�got att spara?
      {
        sd = autosd;
        ir_str_ram();
        memset(&autosd,0x00,sizeof(autosd));
      }
      sd.PreDefGps=0;
    }
    else if(sd.SaveOk&&!sd.stored)//avsluta och gl�mt lagra sista?
    {
      cls();
      Bell(100);
      xprintf(0,0,0,12,str_StoreTargetQ_);
      xprintf(0,1,0,12,str_Enter_);
      PrintStrOled(0,42,str_oled_StoreQ1_);
      PrintStrOled(0,52,str_oled_StoreQ2_);
      PrintStrOled(0,62,str_oled_StoreQ3_);
      if(GetSystemKeyNoTimeLimit()==KEY_ENTER)
        ir_str_ram();//save....
    }

    auto_s = auto_save_target;
    auto_save_target=0;   //VLG-2 sl� av autospar i ref-l�ge

    flag=0;//assume no new ref point
    do
    {
    RefLoop:
      do
      {
        const char *p[]={str_menu_UseLaser_,str_menu_UseGps_,"",str_menu_Finish_,""};
        struct disp_meny_type tmeny={0,0,3,0,11,str_menu_NewRef_,p};
        static int lastmenu=0;
        tmeny.act_item=lastmenu;

        if(flag)//have a new ref
        {
          tmeny.act_item=2;
          p[2]=str_menu_NextTarget_;//to new target
        }
        else
          p[2]=str_menu_Return_;//back to target

        cls();
        Sight_Oled(0);
        Bell(100);
        PrintStrOled(0,42,str_oled_New_);
        PrintStrOled(0,52,str_oled_Ref_);
        ch=Disp_Menu(&tmeny);
        if(tmeny.act_item<=1)    //remember option 0-1
          lastmenu=tmeny.act_item;

        if(ch==2)//return to TARGET
          goto TargetLoop;//new ref, assume want to measure more
        if(ch==3)//last menu Exit
        {
          goto EndLoop;
        }

        if(ch==0)//meny 0
        {
          while((fhgt1pl_relative()||scankeych()!=KEY_EXIT)&&!sd.stored);

          if(flag==0&&!sd.stored)
            goto EndLoop;
          else if(flag&&!sd.stored)
            goto TargetLoop;//new ref, assume want to measure more
          else
            flag=1;//at least one ref point
          cls();
          Sight_Oled(0);
          Bell(100);
          PrintStrOled(0,42,str_oled_Move1_);
          PrintStrOled(0,52,str_oled_Move2_);
          xprintf(0,0,0,12,str_MoveTo_);
          xprintf(0,1,0,12,str_NewRef_);
          GetSystemKeyNoTimeLimit();
        }
        else
        {
          if(ch==1)//GPS
          {
            cls();
            Sight_Oled(0);
            Bell(100);
            PrintStrOled(0,42,str_oled_Move1_);
            PrintStrOled(0,52,str_oled_Move2_);
            xprintf(0,0,0,12,str_MoveTo_);
            xprintf(0,1,0,12,str_NewRef_);
            xprintf(0,2,0,12,str_ThenEnter_);
            xprintf(0,3,0,12,str_ToStartGps_);
            ch=GetSystemKeyNoTimeLimit();
            if(ch==KEY_EXIT)
            {
              if(!flag)
                goto EndLoop;
              else
                goto TargetLoop;//new ref, assume want to measure more
            }
            if(ReadGps())
            {
              My_Bell(200);
              sd.startx=0.0;//relative x
              sd.starty=0.0;//relative y
              //              sd.startz=sd.targetz;//relative z    keep last z
              //sd.startz=sd.pgps->hgt-FirstGpsAltitude;//adj z using gps altitude ignore hgt from gps, use last z
              sd.targetz=0.0;//reset target z in case gps is used again
              goto TargetLoop;
            }
            goto RefLoop;//no gps, try alt. method
          }
        }
      }
      while(!sd.stored);

      if(sd.pgps==NULL||sd.pgps->lon==0.0&&sd.pgps->lat==0.0)
      {
        sd.startx=sd.targetx;//new start lokal pos
        sd.starty=sd.targety;
        sd.startz=sd.targetz;
      }
      else
      {
        struct GPSType tgps=*sd.pgps;
        tgps.x=sd.targetx;//save temp new pos Utm x
        tgps.y=sd.targety;//save temp new pos Utm y
        Gps_ConvertCoord(UTMXY_TO_WGS84LATLONG,&tgps);//To LatLong
        GpsGradToDec(&tgps);//to decimal degree
        sd.pgps->lonf=tgps.lonf;//new pos, update lonf
        sd.pgps->latf=tgps.latf;//new pos, update latf
        sd.pgps->lon=tgps.lon;//new pos, update lon
        sd.pgps->lat=tgps.lat;//new pos, update lat

        sd.startx=sd.targetx-sd.pgps->x;//relative x
        sd.starty=sd.targety-sd.pgps->y;//relative y
        sd.startz=sd.targetz;//relative z
      }

      My_Bell(200);
    }
    while(1);
  EndLoop:
    {
      const char *p[]={str_menu_NewTarget_,str_menu_Finish_,""};
      struct disp_meny_type tmeny={0,0,2,1,11,"",p};
      cls();
      Sight_Oled(0);
      wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
      Bell(100);
      ch=Disp_Menu(&tmeny);
      if(ch==1)
        break;
      flag=1;
    }
  }
  while(flag);

  ee->EEsaveEEmem=tSaveEEmem;
  sd.measmethod=AREAMETHOD;
  sd.SaveOk=1;
  sd.len+=sqrt((sd.x0-sd.xf)*(sd.x0-sd.xf)+(sd.y0-sd.yf)*(sd.y0-sd.yf)); //close polygon
  sd.area+=sd.x0*sd.yf-sd.y0*sd.xf;    //close polygon
  sd.area/=2.0;
  if(sd.area<0.0)      //area = abs(area)
    sd.area*=-1.0;

  point_basepoint=POINT_LASER_TARGET;     //LGEO3DPILE-6
  auto_save_target=0;   //VLG-2 sl� av autosparning
  no_save_angle=0;      //VLG-2 sl� p� sparning vid vinkelm�tning

  PrintAreaLen();

  clr_buff();
  GetSystemKeyNoTimeLimit();

  //VLG-2 skapa polygonet i kml-filen
  WriteKmlPolygon(sd.area, sd.len, &sd.handlekml);

  //st�ng filerna
  CloseFile(&sd.handlekml);
  CloseFile(&sd.handlecsv);
  CloseFile(&sd.handlejson);
    CloseFile(&gHandle);

  //VLG-2 radera tempfilen
  DeleteFile(TEMPAREAPOINTFN);

  data_set();
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}

//VLG-2 MAP GPS
/**********************************
*
*
***********************************/
int fmeasure_map_gps()
{
  int ch,flag,thandle=-1,tSaveEEmem;
  char str_msg[36+1];

  tSaveEEmem=ee->EEsaveEEmem;
  //cont.on last file if exist?
  cls();
  xprintf(0,0,0,12,"...");
  clr_buff();
  data_set();//reset data storage output
  do
  {
    char tname[64];
    sprintf(tname,"MAPGPS%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sd.id++;//next id may be available
    }
  }while(thandle>=0);
  cls();
  clr_buff();
  do
  {
    char tname[64];
    int res;
    data_set();//reset data storage output
    if((GetIdData())<0)
      return 1;
    sprintf(tname,"MAPGPS%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sprintf(tname,"MAPGPS%-5d",sd.id);
      Bell(200);
      cls();
      sprintf(str_msg,"%-12.12s%-12.12s%s",str_Continue_,str_With_,tname);
      if((res=YesNo(1,str_msg))<0||res==2)
        return 1;
      if(res)//cont with latest id
        break;
      sd.id++;//next id may be available
    }
  }while(thandle>=0);

  {
    char mapfilekmlname[64];//if individ file
    char mapfilecsvname[64];//if individ file
    char mapfilejsonname[64];//if individ file
    sprintf(mapfilekmlname,"MAPGPS%d.KML",sd.id);//create filename
    sprintf(mapfilecsvname,"MAPGPS%d.CSV",sd.id);//create filename
   sprintf(mapfilejsonname,"MAPGPS%d.GEOJSON",sd.id);//create filename //VLG2-10 lagt till GeoJsonutskrift
    if((sd.handlekml=OpenOrCreateKmlFile(mapfilekmlname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      return -1;
    }
    if((sd.handlecsv=OpenOrCreateCsvFile(FORCESTORESETTINGS,mapfilecsvname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlekml);
      return -1;
    }
    if((sd.handlejson=OpenOrCreateJsonFile(FORCESTORESETTINGS,mapfilejsonname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlecsv);
      CloseFile(&sd.handlekml);
      return -1;
    }
  }

  ee->EEsaveEEmem=1; //force mem saving
  force_compass=1;      //tvinga anv�ndande av kompass
  do
  {
    cls();
    Sight_Oled(0);
    Bell(100);

    PrintStrOled(0,42,str_oled_Target1_);
    PrintStrOled(0,52,str_oled_Target2_);
    xprintf(0,0,0,12,str_Laser_);
    xprintf(0,1,1,12,str_OnTarget_);

    ch=GetSystemKeyNoTimeLimitAllKeys();
    if(ch== KEY_EXIT)
      goto check_save_and_menu_gps;

    while(fhgt1pl_gps()||scankeych()!=KEY_EXIT);//exit only when press exit

  check_save_and_menu_gps:

    if(sd.SaveOk&&!sd.stored)//avsluta och gl�mt lagra sista?
    {
      cls();
      Bell(100);
      xprintf(0,0,0,12,str_StoreTargetQ_);
      xprintf(0,1,0,12,str_Enter_);
      PrintStrOled(0,42,str_oled_StoreQ1_);
      PrintStrOled(0,52,str_oled_StoreQ2_);
      PrintStrOled(0,62,str_oled_StoreQ3_);
      if(GetSystemKeyNoTimeLimit()==KEY_ENTER)
        ir_str_ram();//save....
    }

    flag=0;//assume no new ref point

    {
      const char *p[]={str_menu_NewTarget_,str_menu_Finish_,""};     //ny textstr�ng, italienska
      struct disp_meny_type tmeny={0,0,2,1,11,"",p};
      cls();
      Sight_Oled(0);
      Bell(100);
      ch=Disp_Menu(&tmeny);
      if(ch==1)
        break;
      flag=1;
    }
  }
  while(flag);

  ee->EEsaveEEmem=tSaveEEmem;
  CloseFile(&sd.handlekml);
  CloseFile(&sd.handlecsv);
  CloseFile(&sd.handlejson);
  sd.measmethod=AREAMETHOD;
  sd.SaveOk=1;

  clr_buff();
  data_set();
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}


//VLG-2 bytt namn p� funktionen
/**********************************
*
*
***********************************/
int fmeasure_trailxy_()
{
  int ch,thandle=-1,tSaveEEmem,tEEtrpHgt;
  float FirstGpsAltitude=0.0;//in case gps is used as new reference ponts, use to calc local z
  char str_msg[36+1];

  tSaveEEmem=ee->EEsaveEEmem;
  tEEtrpHgt = ee->EEtrpHeigth;

  //cont.on last file if exist?
  cls();
  xprintf(0,0,0,12,"...");
  clr_buff();
  data_set();
  do
  {
    char tname[64];
    sprintf(tname,"TRAIL%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sd.id++;//next id may be available
    }
  }while(thandle>=0);
  cls();
  clr_buff();
  do
  {
    char tname[64];
    int res;
    data_set();//reset data storage output
    if((GetIdData())<0)
      return 1;
    sprintf(tname,"TRAIL%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sprintf(tname,"TRAIL%-5d",sd.id);
      Bell(200);
      cls();
      sprintf(str_msg,"%-12.12s%-12.12s%s",str_Continue_,str_With_,tname);
      if((res=YesNo(1,str_msg))<0||res==2)
        return 1;
      if(res)//cont with latest id
      {
        break;
      }
      sd.id++;//next id may be available
    }
  }while(thandle>=0);

  {
    char mapfilekmlname[64];//if individ file
    char mapfilecsvname[64];//if individ file
    char mapfilejsonname[64];//if individ file
    sprintf(mapfilekmlname,"TRAIL%d.KML",sd.id);//create filename
    sprintf(mapfilecsvname,"TRAIL%d.CSV",sd.id);//create filename
   sprintf(mapfilejsonname,"TRAIL%d.GEOJSON",sd.id);//create filename //VLG2-10 lagt till GeoJsonutskrift
    if((sd.handlekml=OpenOrCreateKmlFile(mapfilekmlname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      return -1;
    }
    if((sd.handlecsv=OpenOrCreateCsvFile(FORCESTORESETTINGS,mapfilecsvname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlekml);
      return -1;
    }
    if((sd.handlejson=OpenOrCreateJsonFile(FORCESTORESETTINGS,mapfilejsonname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlecsv);
      CloseFile(&sd.handlekml);
      return -1;
    }
  }

  ee->EEsaveEEmem=1; //force mem saving
  force_compass=1;      //tvinga anv�ndande av kompass

  //St�ll in den magnetiska deklinationen, viktigt f�r m�tningen att den �r korrekt inst�lld
  {
    int isign,tnum,tdecl,tTargetpHgt;
    const char *sign[]={"+","-"};
    cls();
    Bell(100);
    putxy(0,0,(char*)str_MagnDecl_);
    tdecl=ee->decl;
    if(ee->decl<0)
      isign=1;
    else
      isign=0;

    xprintf(12-8,1,0,5,"%02d.%d>",abs(ee->decl/10),abs(ee->decl%10));

    if(SelectString(12-8-1,1,sign,&isign,2)==KEY_EXIT)
    {
      goto EndLoopCoordXY;
    }

    if((tnum=My_InputNum(12-8,1,abs(ee->decl),3))<0)
    {
      goto EndLoopCoordXY;
    }

    ee->decl=tnum*(-2*isign+1);//

    //Target height (Ziel H�he), st�ll in m�lh�jden s� att kan anv�nda t.ex. pinne med reflektor att skjuta p� ist�llet f�r p� marken
    Bell(100);
    putxy(0,2,(char*)str_TargetHeight_);
    tTargetpHgt = ee->EEtargetHeight;

    if((tnum=My_InputNum(12-8,3,ee->EEtargetHeight,2))<0)
    {
      goto EndLoopCoordXY;
    }

    ee->EEtargetHeight = tnum;

    //spara EE om �ndrat deklinationen eller Target Height
    if(tdecl != ee->decl || tTargetpHgt != ee->EEtargetHeight)
    {
      ee->EEsaveEEmem=tSaveEEmem;      //�terst�ll ee-save tillf�lligt till ursprungsinst�llningen eftersom sparar ee-datat
      SetGeoSettings();//save ee
      ee->EEsaveEEmem=1; //force mem saving
    }

    ee->EEtrpHeigth = ee->EEtargetHeight; //s�tt Transponder Height till Target Height f�r h�jdfunktionerna och ber�kningarnas skull, �terst�ll vid avslut
  }

  //sl� p� gps om den inte �r aktiverad
  if(!ee->useGps)
    ee->useGps = 1;

    //VLG-2 tagit bort slut-startpunkt efter feedback fr�n Urs och kundtest i Alperna

    do
    {
    TargetLoopXY:
      cls();
      Bell(100);
      Sight_Oled(0);
      PrintStrOled(0,42,str_oled_Target1_);
      PrintStrOled(0,52,str_oled_Target2_);

      do
      {
        int chl;
        if(use_dme)
          chl=UseLaserOrDme();
        else
          chl=UseLaserOnly();
        if(chl==KEY_EXIT)
          goto EndLoopXY;
        while((ch=ReadCompAndLaserOrDme(chl,ONE_P_LASER_METHOD_COMPASS_XY,0))!=KEY_RIGHT && ch != KEY_EXIT && ch!=0);//laser or dme, fixed target      //VLG-2 visar b�ring till slutpunkten   //Om Enter s� visar b�ringen igen
      }
      while(!sd.stored);
      if(sd.sek==2)//store the first alt as a reference if needed later
        if(sd.pgps==NULL||sd.pgps->lon==0.0&&sd.pgps->lat==0.0)
          ;
        else
          FirstGpsAltitude=sd.pgps->hgt;//use as a starting reference in case gps is used again, just to calc the local zheight.

        if(sd.SaveOk&&!sd.stored)//avsluta och gl�mt lagra sista?
        {
          cls();
          wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
          Bell(100);
          xprintf(0,0,0,12,str_StoreQ_);
          xprintf(0,1,0,12,str_PressEnter_);
          PrintStrOled(0,42,str_oled_StoreQ1_);
          PrintStrOled(0,52,str_oled_StoreQ2_);
          PrintStrOled(0,62,str_oled_StoreQ3_);
          if((ch=GetSystemKeyNoTimeLimit())==KEY_ENTER)
            ir_str_ram();//save....
        }

        //if(ch==0)//Ask if Exit
        //  goto EndLoop;
        if(!sd.stored)
          goto EndLoopXY;

        wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
        Bell(100);
        cls();
        Sight_Oled(0);
        PrintStrOled(0,42,str_oled_Move1_);
        PrintStrOled(0,52,str_oled_Move2_);
        xprintf(0,0,0,12,str_MoveTo_);
        xprintf(0,1,0,12,str_Target_);
        //VLG-2, slutpunkt seq 0, startpunkt seq 1
        xprintf(0,3,0,12,"%s %d",str_Seq_,sd.sek-1/*2*/);
        if(Metric())
        {
          xprintf(0,4,0,12,"%s %.0f m",str_Sum_,sd.len);
        }
        else
        {
          xprintf(0,4,0,12,"%s %.0f ft",str_Sum_,sd.len);
        }
        ch=GetSystemKeyNoTimeLimit();

        if(ch==KEY_EXIT)
          goto EndLoopXY;

        do
        {
        RefLoopXY:
          //new laser rew
          if(sd.pgps==NULL||sd.pgps->lon==0.0&&sd.pgps->lat==0.0)
          {
            sd.startx=sd.targetx;//new start lokal pos
            sd.starty=sd.targety;
            sd.startz=sd.targetz;
          }
          else
          {
            struct GPSType tgps=*sd.pgps;
            tgps.x=sd.targetx;//save temp new pos Utm x
            tgps.y=sd.targety;//save temp new pos Utm y
            Gps_ConvertCoord(UTMXY_TO_WGS84LATLONG,&tgps);//To LatLong
            GpsGradToDec(&tgps);//to decimal degree
            sd.pgps->lonf=tgps.lonf;//new pos, update lonf
            sd.pgps->latf=tgps.latf;//new pos, update latf
            sd.pgps->lon=tgps.lon;//new pos, update lon
            sd.pgps->lat=tgps.lat;//new pos, update lat

            sd.startx=sd.targetx-sd.pgps->x;//relative x
            sd.starty=sd.targety-sd.pgps->y;//relative y
            sd.startz=sd.targetz;//relative z
          }

          do
          {
            const char *p[]={str_menu_LastTarget_,str_menu_Height_XY_,str_menu_Offset_,str_menu_Finish_,""};       //VLG-7 lagt in funktion f�r att m�ta lutningen p� berg och dal, endast f�r FOMEA-versionen
            struct disp_meny_type tmeny={0,0,3,0,11,str_menu_Reference_,p};

            cls();
            Sight_Oled(0);
            wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
            Bell(100);

            PrintStrOled(0,42,str_oled_Ref_);
            PrintStrOled(0,52,str_oled_Q_);
            ch=Disp_Menu(&tmeny);

            if(ch==0)//target is the ref
            {
              goto TargetLoopXY;
            }
            else if(ch==1)//VLG-2 TRAILXY
            {
              struct storedatatype tsd=sd;//save info from the trail

              Sight_Oled(0);
              PrintStrOled(0,42,str_oled_Hgt_);

              sd.PreDefGps=1;
              sd.measmethod=TREE_P_METHOD_TRAIL;
              sd.stored=0;
              sd.SaveOk=0;
              fhgt3pl_();   //VLG-2 laserm�tning eller DME        //visningen av tempor�r h�jd f�r stutzh�he (3P laser) blir fel d� den ox� kompenserar f�r zielh�he
              //VLG-2 skulle kunna l�gga in koll om sd.stored h�r ifall blir problem med att anv�ndarna gl�mmer att trycka Send f�r att spara

              tsd.sek=sd.sek;       //uppdatera sek-numret
              sd=tsd;               //l�s tillbaka data
            }
            /*else if(ch==3)//meny 3 VLG-7 M�t in vinkel upp och ner p� bergssidan, endast f�r FOMEA-versionen
            {
            struct storedatatype tsd=sd;//save info from the trail
            int ret;

            sd.measmethod=ANGLE_METHOD_TRAIL;
            sd.treemethod=ANGLE_METHOD;
            sd.stored=0;
            sd.SaveOk=0;

            sd.distance=0;        //Nollar dessa s� att inte altitud-v�rdet �ndras i csv-, kml-filerna f�r vinkelm�tningen
            sd.hdistance=0;
            sd.height=0;
            //sd.targetz=0;

            do
            {
            ret=fmeasure_angle(SHOW_ALPHA_HANGN_BERG);  //berg      //loopa tills sparat eller tryck esc, kunna skjuta om vinkeln, g� vidare p� SEND
          }while(!sd.stored && ret!=0);

            if(ret)
            {
            sd.stored=0;
            sd.SaveOk=0;
            do
            {
            ret=fmeasure_angle(SHOW_ALPHA_HANGN_DAL);   //dal
          }while(!sd.stored && ret!=0);
          }

            tsd.sek=sd.sek;       //uppdatera sek-numret
            sd=tsd;               //l�s tillbaka data
          }*/
            else if(ch==3)//meny 3 Exit
            {
              goto EndLoopXY;
            }
            else //VLG-2 TRAILXY ta ref-punkt utanf�r sp�ret
            {
              int ch1;
              {
                const char *pVL[]={str_menu_UseLaser_,str_menu_UseGps_,str_menu_UseDme_,str_menu_Exit_,""};
                struct disp_meny_type tmenyVL={0,0,3,0,11,str_menu_Offset_,pVL};

                const char *pL[]={str_menu_UseLaser_,str_menu_UseGps_,str_menu_Exit_,""};
                struct disp_meny_type tmenyL={0,0,3,0,11,str_menu_Offset_,pL};

                cls();
                Sight_Oled(0);
                wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
                Bell(100);

                PrintStrOled(0,42,str_oled_Ref_);
                PrintStrOled(0,52,str_oled_Q_);
                if(use_dme)
                  ch1=Disp_Menu(&tmenyVL);
                else
                  ch1=Disp_Menu(&tmenyL);
              }

              if(use_dme)
              {
                if(ch1==3)//meny 4 Exit
                  goto RefLoopXY;
              }
              else
              {
                if(ch1==2)//meny 3 Exit
                  goto RefLoopXY;
              }

              if(ch1==0||ch1==2)//meny 1 laser or dme
              {
                if(ch1==2)//DME       //F�r LGEO s� tas ch1==2 hand om ovan
                  ReadCompAndLaserOrDme(USEDME,ONE_P_LASER_METHOD_RELATIVE_XY,0);//Dme, relative target      //VLG-2 MAP TRAIL COORD lagt till argument
                else
                  //while((fhgt1pl_relative()||scankeych()!=KEY_EXIT)&&!sd.stored);
                  while((ReadCompAndLaserOrDme(USELASER,ONE_P_LASER_METHOD_RELATIVE_XY,0)||scankeych()!=KEY_EXIT)&&!sd.stored);      //VLG-2 MAP TRAIL COORD lagt till argument
                if(!sd.stored)
                  goto RefLoopXY;
                cls();
                Sight_Oled(0);
                wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
                Bell(100);
                PrintStrOled(0,42,str_oled_Move1_);
                PrintStrOled(0,52,str_oled_Move2_);
                xprintf(0,0,0,12,str_MoveTo_);
                xprintf(0,1,0,12,str_NewRef_);
                GetSystemKeyNoTimeLimit();
              }
              else
                if(ch1==1)//GPS
                {
                  cls();
                  Sight_Oled(0);
                  Bell(100);
                  PrintStrOled(0,42,str_oled_Move1_);
                  PrintStrOled(0,52,str_oled_Move2_);
                  xprintf(0,0,0,12,str_MoveTo_);
                  xprintf(0,1,0,12,str_NewRef_);
                  xprintf(0,2,0,12,str_ThenEnter_);
                  xprintf(0,3,0,12,str_ToStartGps_);
                  ch=GetSystemKeyNoTimeLimit();
                  if(ch==KEY_EXIT)
                    goto RefLoopXY;

                  if(ReadGps())
                  {
                    My_Bell(100);
                    sd.startx=0.0;//relative x
                    sd.starty=0.0;//relative y
                    //sd.startz=sd.targetz;//relative z    keep last z
                    sd.startz=sd.pgps->hgt-FirstGpsAltitude;//adj z using gps altitude
                    sd.targetz=0.0;//reset target z in case gps is used again
                    goto TargetLoopXY;
                  }
                  goto RefLoopXY;//no gps, try alt. method
                }
            }
          }
          while(!sd.stored);
        }
        while(1);

    EndLoopXY:
      {
        const char *p[]={str_menu_NextTarget_,str_menu_Finish_,str_menu_NewRef_,""};      //VLG-2 TRAILXY kan nu komma tillbaka till ref-loop, ifall var lite snabb och t�nkte m�ta h�jd men tog last target
        struct disp_meny_type tmeny={0,0,2,1,11,"",p};
        cls();
        Sight_Oled(0);
        wait(200);      //v�nta lite s� att s�ker p� att f�reg�ende pip hunnit avslutas
        Bell(100);
        ch=Disp_Menu(&tmeny);
        if(ch==1)
          break;
        else if(ch==2)
          goto RefLoopXY;
      }
    }
    while(1);

EndLoopCoordXY:   //VLG-2 MAP TRAIL COORD
  ee->EEsaveEEmem=tSaveEEmem;
  ee->EEtrpHeigth = tEEtrpHgt;   //�terst�ll transponder height
  CloseFile(&sd.handlekml);
  CloseFile(&sd.handlecsv);
  CloseFile(&sd.handlejson);
  sd.measmethod=LENMETHOD;
  sd.SaveOk=1;

  cls();
  xprintf(0,2,0,12,str_Length_);
  if(Metric())
  {
    xprintf(0,3,0,12,"%.0f m",sd.len);
  }
  else
  {
    xprintf(0,3,0,12,"%.0f ft",sd.len);
  }
  clr_buff();
  GetSystemKeyNoTimeLimit();
  data_set();
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}

/**********************************
*
*
***********************************/
int UseLaserOrDme()
{
  static int n=0;
  const char *p[]={str_menu_Laser_,str_menu_Dme_,str_menu_Exit_,""};
  struct disp_meny_type tmeny={0,0,2,0,11,str_menu_Target_,p};
  if(n<0||n>1)
    n=0;
  cls();
  tmeny.act_item=n;
  n=Disp_Menu(&tmeny);
  if(n>=sizeof(p)/sizeof(p[0])-2)
    return KEY_EXIT;
  return n;
}

/**********************************
*
*
***********************************/
int UseLaserOnly()
{
  static int n=0;
  const char *p[]={str_menu_Laser_,str_menu_Exit_,""};
  struct disp_meny_type tmeny={0,0,2,0,11,str_menu_Target_,p};
  if(n<0||n>0)
    n=0;
  cls();
  tmeny.act_item=n;
  n=Disp_Menu(&tmeny);
  if(n>=sizeof(p)/sizeof(p[0])-2)
    return KEY_EXIT;
  return n;
}

/**********************************
*
*
***********************************/
void PrintAreaLen()
{
  cls();
  xprintf(0,0,0,12,str_Area_);
  xprintf(0,2,0,12,str_Length_);
  if(Metric())
  {
    if(sd.area<5000.0)//<.5ha
      xprintf(0,1,0,12,"%.0f m2",sd.area);
    else
      xprintf(0,1,0,12,"%.1f ha",sd.area/10000.0);

    xprintf(0,3,0,12,"%.0f m",sd.len);
  }
  else
  {
    sd.area*=10.764;//m->ft2
    sd.len*=3.28;//m->ft
    if(sd.area<20000.0)//<.5ac
      xprintf(0,1,0,12,"%.0f ft2",sd.area);
    else
      xprintf(0,1,0,12,"%.1f acre",sd.area/43560.2);//to ac

    xprintf(0,3,0,12,"%.0f ft",sd.len);
  }
}

/**********************************
*
*
***********************************/
int GpsLog()
{
  int thandle=-1,cnt_pos=0;
  double prevx=0.0,prevy=0.0,firstlonf,firstlatf,firstz;
  char str_msg[36+1];
  cls();
  xprintf(0,0,0,12,"...");
  clr_buff();
  data_set();//reset data storage output

  do
  {
    char tname[64];
    sprintf(tname,"GPS%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sd.id++;//next id may be available
    }
  }while(thandle>=0);
  cls();
  clr_buff();
  do
  {
    char tname[64];
    int res;
    data_set();//reset data storage output
    if((GetIdData())<0)
      return 1;
    sprintf(tname,"GPS%d.KML",sd.id);
    if((thandle=open(tname,_LLIO_WRONLY))>=0)
    {
      close(thandle);   //Kan inte k�ra CloseFile eftersom thandle d� s�tts till -1 och kollen nedan inte fungerar
      sprintf(tname,"GPS%-5d",sd.id);
      Bell(200);
      cls();
      sprintf(str_msg,"%-12.12s%-12.12s%s",str_Continue_,str_With_,tname);
      if((res=YesNo(1,str_msg))<0||res==2)
        return 1;
      if(res)//cont with latest id
        break;
      sd.id++;//next id may be available
    }
  }while(thandle>=0);

  {
    char mapfilekmlname[64];//if individ file
    char mapfilecsvname[64];//if individ file
    char mapfilejsonname[64];//if individ file
    sprintf(mapfilekmlname,"GPS%d.KML",sd.id);//create filename
    sprintf(mapfilecsvname,"GPS%d.CSV",sd.id);//create filename
    sprintf(mapfilejsonname,"GPS%d.GEOJSON",sd.id);//create filename         //VLG2-10 lagt till GeoJsonutskrift
    if((sd.handlekml=OpenOrCreatePolygonKmlFile(mapfilekmlname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      return -1;
    }
    if((sd.handlecsv=OpenOrCreateCsvFile(FORCESTORESETTINGS,mapfilecsvname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlekml);
      return -1;
    }
    if((sd.handlejson=OpenOrCreatePolygonJsonFile(FORCESTORESETTINGS,mapfilejsonname))<0)
    {
      Message(str_msg_Error_,str_msg_FileOpen_);
      CloseFile(&sd.handlecsv);
      CloseFile(&sd.handlekml);
      return -1;
    }
  }
  if(Message(str_msg_Log_,str_msg_StartEnter_)!=KEY_EXIT)
  {
    cls();
    do
    {
      if(cnt_pos==0)
        //sd.pgps=fGpsGetPosNoClrSbuff(3,0);//averaging 5 pos first p
        sd.pgps=My_fGpsGetPos(3,0);
      else
        //sd.pgps=fGpsGetPosNoClrSbuff(2,0);//averaging 2 pos
        sd.pgps=My_fGpsGetPos(1,0);       //ej medelv�rde, r�cker med en l�sning
      if(sd.pgps->ch==KEY_EXIT)
        break;
      if(sd.pgps->lon!=0.0&&sd.pgps->lat!=0.0)
      {
        double dist=sqrt((sd.pgps->x-prevx)*(sd.pgps->x-prevx)+(sd.pgps->y-prevy)*(sd.pgps->y-prevy));
        if(dist>=5.0||sd.pgps->ch==KEY_ENTER)
        {
          prevx=sd.pgps->x;
          prevy=sd.pgps->y;

          bell(200,2000);
          sd.SaveOk=1;
          sd.measmethod=GPSMETHOD;
          if(StoreDataKml(&sd.handlekml,sd.pgps)<=0)//store new settings or data
            if(Message(str_msg_Error_,str_msg_SaveKmlData_)==KEY_EXIT)
              break;
          if(StoreDataCsv(STOREDATA,&sd.handlecsv,sd.pgps)<=0)//store new settings or data
            if(Message(str_msg_Error_,str_msg_SaveCsvData_)==KEY_EXIT)
              break;
          if(StoreDataJson(&sd.handlejson,sd.pgps)<=0)//store new settings or data
            if(Message(str_msg_Error_,str_msg_SaveJsonData_)==KEY_EXIT)
              break;
          cnt_pos++;
          sd.sek++;
          if(cnt_pos==1)//area
          {
            sd.xf=sd.x0=prevx;    //save first point
            sd.yf=sd.y0=prevy;

            firstlonf=sd.pgps->lonf;//used also to close the polygon kml
            firstlatf=sd.pgps->latf;
            firstz=sd.pgps->hgt;
          }
          else
          {
            sd.x1=prevx;
            sd.y1=prevy;
            sd.len+=sqrt((sd.x0-sd.x1)*(sd.x0-sd.x1)+(sd.y0-sd.y1)*(sd.y0-sd.y1));
            sd.area+=sd.x0*sd.y1-sd.y0*sd.x1;
            sd.x0=sd.x1;
            sd.y0=sd.y1;
          }
        }
      }
      else
        if(Message(str_msg_NoGps_,str_msg_TryAgainQ_)==KEY_EXIT)
          break;

    }
    while(sd.pgps->ch!=KEY_EXIT);
  }
  if(cnt_pos>1)
  {
    sd.pgps->lonf=firstlonf;
    sd.pgps->latf=firstlatf;
    sd.pgps->hgt=firstz;
    GpsDecToGrad(sd.pgps);//to lat/lon
    sd.SaveOk=1;
    sd.measmethod=GPSMETHOD;
    if(StoreDataKml(&sd.handlekml,sd.pgps)<=0)//store new settings or data
      Message(str_msg_Error_,str_msg_SaveKmlData_);
    if(StoreDataJson(&sd.handlejson,sd.pgps)<=0)//store new settings or data
      Message(str_msg_Error_,str_msg_SaveJsonData_);
  }
  CloseFile(&sd.handlekml);
  CloseFile(&sd.handlecsv);
  CloseFile(&sd.handlejson);

  sd.len+=sqrt((sd.x0-sd.xf)*(sd.x0-sd.xf)+(sd.y0-sd.yf)*(sd.y0-sd.yf)); //close polygon
  sd.area+=sd.x0*sd.yf-sd.y0*sd.xf;    //close polygon
  sd.area/=2.0;
  if(sd.area<0.0)      //area = abs(area)
    sd.area*=-1.0;
  PrintAreaLen();
  clr_buff();
  GetSystemKeyNoTimeLimit();
  data_set();
  return 1;
}
