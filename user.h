#ifndef USER_
#define USER_

int DeleteExplorerFile(char* filenameext,char* DelStr);
int GetInt(int x,int y,int num,int pos,int decimal);
int GetRadius(int x,int y,int radius);
void format(char *s,int num,int numdigits,int numdecimal,char PadCh);
void putxy(int x,int y,char *str);
void putxy2(int x,int y,char *str);
void putxy3(int x,int y,int type,char *str);
void putint(int tx,int ty,int num,int numdigits,int numdecimal);
void putintzero(int tx,int ty,int num,int numdigits,int numdecimal);
void putint2(int tx,int ty,int num,int numdigits,int numdecimal);
int scankey();  //user.c
int scankeych();  //user.c
int SelectString(int tx,int ty,const char *str[],int *act,char n);
void My_Bell(int ms);
void My_bell2(int ms, int freq);
int My_InputNum(int tx,int ty,int num,int digits);
int My_InputNumZero(int tx,int ty,int num,int digits);
#endif

