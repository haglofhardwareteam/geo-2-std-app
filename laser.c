#include"MyFuncs.h"

#define MAXBUFFROWS     5    //antal rader
#define MAXBUFFCHARS    12
static char DispBuff[MAXBUFFROWS][MAXBUFFCHARS+1]; //12tecken per rad
static int act_counter;

/**********************************
*
*
***********************************/
int fS200Id(void)
{
  char *p;
  int res;
  cls();

  if(!Laser(LASERCMD_ON,0))
  {
    cls();
    putxy(0, 0,(char*)str_No_Laser_);
    return GetSystemKey();
  }

  res=Laser(LASERCMD_ID,0);

  putint(0,3,res,5,0);
  p=(char *)Laser(LASERCMD_LASTSTATUS,0);
  putxy(0,0,p);
  GetSystemKey();
  return res;
}

/**********************************
*
*
***********************************/
int s200ReadDist()
{
  return Laser(LASERCMD_DIST,0);
}

/**********************************
*
*
***********************************/
int s200CmdGoStart()
{
  return Laser(LASERCMD_AUTO_DIST,1);
}

/**********************************
*
*
***********************************/
int s200CmdGoStop()
{
  return Laser(LASERCMD_AUTO_DIST,0);
}



/**********************************
*
*  VLG-28 Skjuter med lasern tills sl�pper ON, tar sist m�tta v�rdet (bios)
***********************************/
int s200ReadDistCont()
{
  int res,loop,dist,tres = 0, key_released;
  res=0;
  measured_continously=0;            //VLG2-8 flagga f�r att visa om skjutit kontinuerligt med lasern under m�tning
  if((res=s200CmdGoStart())>0)        //GO,0 f�r kontinuerlig m�tning
  {
    do
    {
      if((res = s200ReadDist())>0)
      {
        dist = res;

        if(Metric())
          dist=roundi(dist);

        if(dist > MIN_LASER_DIST)  //min 5dm
        {
          Sight_Oled(0);
          PrintStrOled(0,22,(char *)hgt_str_SD_);//try use 2decimals
          PrintIntOled(0,32,dist*10);//try use 2decimals

          cls();
          putxy(0,0, (char *)hgt_str_SD_);
          putint(12-6,0,dist,6,1);
        }
        else
        {
          Sight_Oled(0);
          PrintStrOled(0,42,"---");

          cls();
          putxy(0,0, (char *)s200_strNoVal1_);
          putxy(0,1, (char *)s200_strNoVal2_);
          putxy(0,2, (char *)s200_strShotAgain1_);
          putxy(0,3, (char *)s200_strShotAgain2_);
        }
      }
      else //laser err
      {
        if(res==0)//no dist code
        {
          res = 0;
          dist = 0;
          goto wait_loop_start;
        }
        else
        {
          clr_buff();
          res = 0;
          goto stop_meas_cont;
        }
      }

      //testa om ON fortfarande intryckt, annars avbryt
    wait_loop_start:
      loop=0;
      key_released = 0;
      do
      {
        if(scankey()&KEY_ENTER_BIN)
        {
          wait(50);
        }
        else
        {
          key_released = 1;      //sl�ppt knappen
          break;
        }
      }while(++loop<7/*5*/);  //v�nta max 350ms

      if(key_released==0 && scankey()&KEY_ENTER_BIN)        //ON fortfarande nedtryckt
      {
        if(dist > MIN_LASER_DIST)  //min 5dm
        {
          //spara sista v�rdet och anv�nd det ist�llet f�r nya n�r ON-knappen sl�pps
          tres = res;

          measured_continously=1;            //VLG2-8 flagga f�r att visa om skjutit kontinuerligt med lasern under m�tning
        }

        Sight_Oled(1);
        loop=0;
        do
        {
          if(scankey()&KEY_ENTER_BIN)
          {
            wait(50);
          }
          else
          {
            break;      //sl�ppt knappen
          }
        }while(++loop<2/*0*/);  //v�nta max 100ms
      }
      else
        break;      //sl�ppt knappen

    }while(1);      //ON fortfarande intryckt

    if(tres > 0)
    {
      //spara sista v�rdet och anv�nd det ist�llet f�r nya n�r ON-knappen sl�pps
      res = tres;
    }
  }
  else
    goto stop_meas_cont_no_auto_off;    //Beh�ver inte skicka stop kommando om go,0 fallerar

stop_meas_cont:
  //skicka stopp kommando
  s200CmdGoStop();

stop_meas_cont_no_auto_off:
  SetOledTimer(9);    //ca 9s, set timer annars kan v�rdena i oled f�rsvinna p� en g�ng, beroende hur l�nge du h�llit inne ON-knappen

  return res;
}

/**********************************
*
* Sl�r av str�mmen till lasern
***********************************/
int s200PowerOffReady()
{
  return Laser(LASERCMD_OFF,0);
}

/**********************************
*
* V�ntar p� READY meddelande fr�n laser
***********************************/
int s200PowerOnReady()
{
  return Laser(LASERCMD_ON,0);
}

/**********************************
*
* V�ntar inte p� n�got READY meddelande, g�r snabbare
***********************************/
int s200PowerOnNoReady()
{
  return Laser(LASERCMD_POWER,1);
}

/**********************************
*
* Sl�r av str�mmen till lasern
***********************************/
int s200PowerOffNoReady()
{
  return Laser(LASERCMD_POWER,0);
}


/**********************************
*
*
***********************************/
int s200GetMode()
{
  return Laser(LASERCMD_GET_MODE,0);
}

/**********************************
*
*
***********************************/
int s200SetMode(int mode)
{
  return Laser(LASERCMD_SET_MODE,mode);
}

/**********************************
*
*
***********************************/
int s200GetLaserSettings()
{
  return Laser(LASERCMD_READ_SETTINGS,0);
}

/**********************************
*
*
***********************************/
int s200SetLaserSettings()
{
  return Laser(LASERCMD_STORE_SETTINGS ,0);
}

/**********************************
*
*
***********************************/
void PrintAngleOled()
{
  int tangle=sd.alpha;
  if(ee->EEangletype==1)
    tangle=ConvToGrad(tangle);
  else if(ee->EEangletype==2)
    tangle=ConvToProc(tangle)*10;
  PrintIntOled(0,62,tangle);
  disp_put_pix_Oled_CharMode(62-1,3*6); //font6x8.x);
  disp_redraw_Oled();
}

/**********************************
*
* VLGF-29 vill se vinkelv�rdet i OLED under m�tning, som i kompassm�tningen
***********************************/
void PrintAngleOled2(int tangle)
{
  if(ee->EEangletype==1)
    tangle=ConvToGrad(tangle);
  else if(ee->EEangletype==2)
    tangle=ConvToProc(tangle)*10;
  PrintIntOled(0,62,tangle);
  disp_put_pix_Oled_CharMode(62-1,3*6); //font6x8.x);
  disp_redraw_Oled();
}

/**********************************
Redraw only angle data
***********************************/
void redrawOnlyAngle(int hgt)
{
  cls();
  /* endast f�r FOMEA-versionen
  if(hgt==SHOW_ALPHA_HANGN_BERG) //VLG-7 visa mer text
  {
  xprintf(0,0,0,12,str_Hang_Berg_);
}
  else if(hgt==SHOW_ALPHA_HANGN_DAL)
  {
  xprintf(0,0,0,12,str_Hang_Dal_);
}*/

  putxy(0,1, (char*)hgt_str_Deg_);
  putxy(0,2, (char *)hgt_str_Grad_);
  putxy(0,3, (char *)hgt_str_Percent_);
}

/**********************************
Redraw
***********************************/
void redrawAngle(void)
{
  const char *sDeg[] = {str_set_Deg_,str_set_Grad_,str_set_Percent_};
  int flag=GetStatus();

  cls();
  putxy(0,0, (char *)hgt_str_SD_);
  if(sd.distance){
    putint(12-6,0,sd.distance,6,1);
  }
  putxy(0,1, (char *)hgt_str_HD_);
  putxy(0,2, (char *)hgt_str_H_);
  xprintf(0,3,0,8, sDeg[ee->EEangletype]);
  if(force_compass || (ee->useGps && ee->EEsaveEEmem) || flag&STATUS_USE_COMPAS)   //VLG-19 visa Azimuth
  {
    putxy(0,4,(char *)hgt_str_AZ_);
    if(ee->AzimuthType==1u) //VLG-2 Azimuth
    {
      putxy(11,4,"'");
    }
  }
}

/**********************************
Redraw
***********************************/
void redrawAngleResult(void)
{
  int tHdist;
  int flag=GetStatus();
  sd.height=calc_hgt(sd.distance,-sd.alpha,0,ADD_REF_H);//1p measurment
  if(sd.treemethod == ONE_P_LASER_METHOD_COMPASS_XY || sd.treemethod == ONE_P_LASER_METHOD_RELATIVE_XY)
    sd.height -= ee->EEtrpHeigth;     //VLG-2 kompenserar f�r Target Height      //visningen av tempor�r h�jd f�r stutzh�he (3P laser) blir fel d� den ox� kompenserar f�r zielh�he
  copy_to_ir_str(0,sd.height);
  tHdist=Hdist(sd.distance);
  copy_to_ir_str(3,tHdist);
  putint(12-6,1,tHdist,6,1);
  putint(12-6,2,sd.height,6,1);
  redrawAngleNum(sd.alpha,3);

  cls_Oled();
  PrintStrOled(0,22,str_oled_D_);
  PrintIntOled(0,32,sd.distance*10);
  if(sd.treemethod==COMPASS_METHOD||sd.treemethod==ONE_P_LASER_METHOD_COMPASS||sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)    //VLG-2 MAP TRAILXY
  {
    if(force_compass || (ee->useGps && ee->EEsaveEEmem) || flag&STATUS_USE_COMPAS)   //VLG-19 visa Azimuth
    {

      PrintStrOled(0,42,(char *)hgt_str_AZ_);
      if(ee->AzimuthType==1u) //VLG-2 Azimuth
      {
        putint2_Oled(0,52,(int)rint(fGradToGon(sd.gYaw)),3,0);
        putxy2_Oled(18,52,"'");
      }
      else
        putint2_Oled(0,52,(int)rint(sd.gYaw),3,0);
    }
    PrintAngleOled();
  }
  else
  {
    PrintIntOled(0,42,sd.hdistance*10);//try use 2decimals
    PrintStrOled(0,52,str_oled_Hgt_);
    PrintIntOled(0,62,sd.height*10);
  }
}

/**********************************
Redraw Height
***********************************/
void redrawHgt(int show_compass)
{
  int talpha;
  int flag=GetStatus();
  const char *sDeg[] = {str_set_Deg_,str_set_Grad_,str_set_Percent_};

  cls();
  putxy(0,0, (char *)hgt_str_SD_);putint(12-6,0,sd.distance,6,1);
  putxy(0,1, (char *)hgt_str_HD_);putint(12-6,1,sd.hdistance,6,1); //anv hdistance ist�llet, fel vinkel anv annars i vissa funktioner
  xprintf(0,2,0,8,sDeg[ee->EEangletype]);

  if(ee->EEangletype==0){
    talpha=roundi(sd.alpha);
  }
  else if(ee->EEangletype==1){
    talpha=roundi(ConvToGrad(sd.alpha));
  }
  else{
    talpha=ConvToProc(sd.alpha);
  }
  putint(12-6,2,talpha,6,1);
  putxy(0,3, (char *)hgt_str_H1_);

  if(show_compass)       //VLG-26 lagt till argument f�r visning av kompassriktningen i HEIGHT DME funktionen
  {
    if(force_compass || (ee->useGps && ee->EEsaveEEmem) || flag&STATUS_USE_COMPAS)   //VLG-19 visa Azimuth
    {
      putxy(0,4,(char*)hgt_str_AZ_);
      if(ee->AzimuthType==1u)
      {
        putxy(11,4,"'");
      }

      if(ee->AzimuthType==1u)
      {
        putint(12-7,4,(int)rint(fGradToGon(sd.gYaw)*10.0),6,1);
      }
      else
        putint(12-6,4,(int)rint(sd.gYaw*10.0),6,1);
    }
  }
}

/**********************************
Redraw
***********************************/
void redrawAngleNum(int talpha,int ty)
{
  int tmpalpha;
  int flag=GetStatus();

  if(ee->EEangletype==0){
    tmpalpha=roundi(talpha);
  }
  else if(ee->EEangletype==1){
    tmpalpha=roundi(ConvToGrad(talpha));
  }
  else{
    tmpalpha=ConvToProc(talpha);
  }

  putint(12-6,ty,tmpalpha,6,1);
  if(force_compass || (ee->useGps && ee->EEsaveEEmem) || flag&STATUS_USE_COMPAS)   //VLG-19 visa Azimuth
  {

    if(ee->AzimuthType==1u) //VLG-2 Azimuth
    {
      putint(12-7,ty+1,(int)rint(fGradToGon(sd.gYaw)*10.0),6,1);
    }
    else
      putint(12-6,ty+1,(int)rint(sd.gYaw*10.0),6,1);
  }

}

/**********************************
Redraw angle
***********************************/
void redrawOnlyAngleNum(int tangle)
{
  putint(12-6,1,roundi(tangle),6,1);
  putint(12-6,2,roundi(ConvToGrad(tangle)),6,1);
  putint(12-6,3,ConvToProc(tangle),6,1);

}

/**********************************
Redraw
***********************************/
void redrawHgtNum(int counter,int angle,int AddRefH)
{
  int tnum;
  char str[MAXBUFFCHARS+1];

  sd.height=calc_hgt_3(sd.distance,sd.gamma,sd.beta,angle,AddRefH);
  tnum=counter+1;

  if(main_lang==ITALIA || main_lang==PORTUGUESE || main_lang==ESPANOL)
  {
    if(tnum<10)
      sprintf(str,"%s%d%6d.%d",hgt_str_H_,tnum,sd.height/10,abs(sd.height%10));
    else
      sprintf(str,"%s%2d%5d.%d",hgt_str_H_,tnum,sd.height/10,abs(sd.height%10));
  }
  else
  {
    if(tnum<10)
      sprintf(str,"%s%d%8d.%d",hgt_str_H_,tnum,sd.height/10,abs(sd.height%10));
    else
      sprintf(str,"%s%2d%7d.%d",hgt_str_H_,tnum,sd.height/10,abs(sd.height%10));
  }

  ScrollDispBuff(counter,str);
}

/**********************************
Redraw
***********************************/
void Redraw(int hgt)
{
  if(hgt==SHOW_ALPHA || hgt==SHOW_ALPHA_HANGN_BERG || hgt==SHOW_ALPHA_HANGN_DAL){       //VLG-7 �ndrat lite f�r att anv. i fmeasure_trailxy_
    redrawOnlyAngle(hgt);
  }
  else if(hgt==SHOW_ALPHA_AND_H){
    redrawAngle();
  }
  else{//SHOW_H
    redrawHgt(0);
  }
}

/**********************************
Redraw
***********************************/
void RedrawNum(int counter,int hgt,int angle,int AddRefH)
{
  if(hgt==SHOW_ALPHA){
    redrawOnlyAngleNum(angle);
  }
  else if(hgt==SHOW_ALPHA_AND_H){
    redrawAngleNum(angle,3);//angle,row y
  }
  else{
    redrawHgtNum(counter,angle,AddRefH);
  }
}

/**********************************
*
*
***********************************/
void InitDispBuff()
{
  memset(&DispBuff, 0x00,sizeof(DispBuff));
  act_counter = -1;
}

/**********************************
*
*
***********************************/
void ScrollDispBuff(int counter, char *str)
{
  if(counter<MAXBUFFROWS)
  {
    strncpy(DispBuff[counter],str,MAXBUFFCHARS);      //uppdatera p� aktuell rad i bufferten
  }
  else        //fr�n 6:e h�jden
  {
    if(act_counter != counter)        //ny h�jd
    {
      //flytta upp alla rader och fyll p� p� sista
      for(int i=0; i<MAXBUFFROWS-1; i++)
      {
        strncpy(DispBuff[i],DispBuff[i+1],MAXBUFFCHARS);
      }
    }
    strncpy(DispBuff[MAXBUFFROWS-1],str,MAXBUFFCHARS);        //ny data till sista raden i bufferten
  }
  PrintDispBuff(counter);

}

/**********************************
*
*
***********************************/
void PrintDispBuff(int counter)
{
  int startrow=0;

  if(counter==0 || counter==1)
    startrow=3;
  else if(counter==2)
    startrow=2;
  else if(counter==3)
    startrow=1;
  else
    startrow=0;

  if(act_counter != counter)    //ny h�jd
  {
    act_counter = counter;     //om ny counter s� skriv ut alla rader i displayen

    for(int i=0; i<MAXBUFFROWS; i++)
    {
      if(DispBuff[i][0] != '\0')
        xprintf(0,startrow++,0,12,"%12.12s",DispBuff[i]);
      else
        break;
    }
  }
  else  //fortfarande p� samma h�jd
  {
    //uppdatera endast aktuella raden
    if(counter<MAXBUFFROWS)
      xprintf(0,startrow+counter,0,12,"%12.12s",DispBuff[counter]);
    else        //fr�n 6:e h�jden
      xprintf(0,4,0,12,"%12.12s",DispBuff[MAXBUFFROWS-1]);//alltid sista raden
  }

}

/**********************************
fKeyLeft
***********************************/
int fKeyLeft(void)
{
  int i,ch;
  wait(100);
  for(i=0;i<4;i++)
  {
    if((ch=My_getc()) == KEY_EXIT || ch == KEY_RIGHT){
      return -1;
    }
    wait(50);
  }
  if(scankey()&KEY_RIGHT_BIN){
    return -1;//tryckte Exit
  }

  return 0;
}

/**********************************
fKeyRight
***********************************/
int fKeyRight(void)
{
  int i,ch;
  wait(100);
  for(i=0;i<4;i++)
  {
    if((ch=My_getc()) == KEY_EXIT || ch == KEY_LEFT){
      return -1;
    }
    wait(50);
  }
  if(scankey()&KEY_LEFT_BIN){
    return -1;//tryckte Exit
  }

  if(!no_save_angle)    //VLG-2 kan nu sl� av sparning vid vinkelm�tning, f�r map target
    ir_str_ram();

  return 0;

}

/**********************************
fKeyEnter
***********************************/
int fKeyEnter(void)
{
  return 0;
}

/**********************************
fKeyExit
***********************************/
int fKeyExit(void)
{
  return -1;
}

/**********************************
fKeyLeftEnter
***********************************/
int fKeyLeftEnter(void)
{
  return 0;
}

/**********************************
fKeyRightEnter
***********************************/
int fKeyRightEnter(void)
{
  sd.alpha=0;
  sd.hdistance=sd.distance;
  My_Bell(400);
  return -2;
}



/**********************************
Measure angle
***********************************/
int measure_angle(void)
{
  data_set();//reset data storage output
  sd.use_ir=1;
  irset();//

  sd.measmethod=sd.treemethod=ANGLE_METHOD;
  return fmeasure_angle(SHOW_ALPHA);
}

/**********************************
*
*
***********************************/
int fmeasure_angle(int hgt)     //VLG-7 delat upp int measure_angle(void)
{
  int ret;

  UseLaser=0;

  if((ret=measure_angle_(0,hgt,NO_ADD_REF_H)))
  {
    PrintAngleOled();
    Bell(200);

    sd.yaw=((int)rint(sd.gYaw*100.0));//for ev. storing
    sd.gamma=((int)rint(sd.gPitch*18000.0/PI));//for ev. storing

    WhileEnterPressed(); //v�nta tills enter sl�ppts
    clr_buff(); //rensar tangentbordsbufferten, annars ligger Entertryck kvar sedan vinkelm�tningen

    if(dme_key()==KEY_LEFT && hgt==SHOW_ALPHA){    //VLG-7 inte kunna aktivera dme-dist
      dmef(DME_WAIT_FOR_KEY,sd.alpha);        //cm alt ftx10, visa horisontella avst�ndet
    }
  }

  if(hgt==SHOW_ALPHA)
    return 1;
  else
    return ret;        //VLG-7 f�r att kunna ta hand om esc
}


/**********************************
Measure angle filter sample version
***********************************/
int measure_angle_(int counter,int hgt,int AddRefH)
{//                                               001       010       011          100         101          110      111
  int KeyReleased,retv,angle,ch,(*g[7])(void)={fKeyEnter,fKeyLeft,fKeyLeftEnter,fKeyRight,fKeyRightEnter,fKeyExit,fKeyExit};
  struct sensordatatype sensor;
  int i=0;

  retv=0;
  if(hgt==SHOW_H)
  {
    ;
  }
  else
  {
    Redraw(hgt);
    if(hgt == SHOW_ALPHA_HANGN_BERG || hgt == SHOW_ALPHA_HANGN_DAL)      //VLG-7 st�ll om flaggan s� att det inte p�verkar n�got mer nedan, ska bara visa vinkel
      hgt = SHOW_ALPHA;
  }

  Sight_Oled(1);
  KeyReleased=0;
  SetTimer(60000);

  ResetMagCalCount();//reset filters
  sensor.counter=0;       //nollar f�r s�kerhets skull

  while(GetTimer()>0)
  {
    if(force_compass || (ee->useGps && ee->EEsaveEEmem) || GetStatus()&STATUS_USE_COMPAS)       //VLG2-2 Kompassen kommer alltid anv�ndas, STATUS_USE_COMPAS returnerar alltid 1. Tagit bort ikonen fr�n huvudmenyn. Kontrollerna f�r vara kvar om �ndras i framtiden
      sensor.usesensor=1;       //anv�nd kompassen
    else
      sensor.usesensor=0;
    angle=read_angle(&sensor);

    //VLG2-2 Diameterm�tning, om diameter och checkroll-flagg satt s� kolla om enheten lutats �t sidan eller tillbaka upp
    if((ch=GetChar(DISP))== LASERROLLED_UP||ch==LASERROLLED_RIGHT)
    {
      if(ee->useDiameter && checkrollflag!=LASERROLLED_NONE)
      {
        return ch;
      }
    }//Ta hand om Exit, l�tt att det missas annars
    else if(ch==KEY_EXIT)
      return 0;

    if(((scankey()&KEY_ENTER_BIN)==0 && !UseLaser) || (sd.distance<=0 && hgt!=SHOW_ALPHA))
    {
      //VLGF-29 vill se vinkelv�rdet i OLED under m�tning, som i kompassm�tningen
      if(sd.measmethod == ANGLE_METHOD && hgt==SHOW_ALPHA || (sd.measmethod==TREE_P_METHOD || sd.measmethod == TRANSPONDER_METHOD) && sd.treemethod==NORMALMETHOD && hgt==SHOW_H)  //VLG2-2 skriver ut h�jdv�rdet i oled, 3P-m�tningen, 2P DME-m�tning
      {
        if(i++==20)
        {
          Sight_Oled(0);

          if((sd.measmethod==TREE_P_METHOD || sd.measmethod == TRANSPONDER_METHOD) && sd.treemethod==NORMALMETHOD && hgt==SHOW_H)
            PrintHeightOled();  //VLG2-2 skriver ut h�jdv�rdet i oled, 3P-m�tningen, 2P DME-m�tning
          else
            PrintAngleOled2(angle);
        }
        else
          if(i==33/*30*/) //Anv�nder samma v�rden som i ReadCompAndLaserOrDme
            Sight_Oled(1);
          else
            if(i==46/*40*/)
            {
              Sight_Oled(0);
              i=0;
            }
      }
      else
      {
        BlinkSight(200,600);//added to save oled
      }
      KeyReleased=1;
      RedrawNum(counter,hgt,angle,AddRefH);
    }
    else
    {
      //Sight_Oled(1);//force on
      if(KeyReleased)
      {
        Sight_Oled(1);//force on
        wait(300);
        ResetMagCalCount();//reset filters
        sensor.counter=0;       //OBS! Nolla countern s� att den tvingas l�sa in nya vinkelv�rden
      }
      KeyReleased=0;

      if(sensor.counter>=sensor.maxcounter)
      {
        Sight_Oled(0);
    	SetTimer(60000);
        //RedrawNum(counter,hgt,angle,AddRefH);//calc and display height/angle
        sd.alpha=angle;
        sd.gPitch=sensor.gPitch;
        sd.gYaw=sensor.gYaw;        //f�r nu �ven kompassv�rdet fr�n read_angle-funktionen
      if(rev_compass)   //VLG2-14 lagt till reversed compass
      {
        //r�kna om v�rdet +180 mod 360
        float dAZ = sd.gYaw + 180.0;
        if(dAZ >= 360.0)
          dAZ -= 360.0;
        sd.gYaw = dAZ;
      }

        sd.gRoll=sensor.gRoll;
        sd.SaveOk=1;//JN
        RedrawNum(counter,hgt,angle,AddRefH);//calc and display height/angle    //anv�nder sd.gYaw
        if(hgt==SHOW_H)
        {
          if(counter%3==0)
          {
            irset();//restart ir after 3hgts
            copy_to_ir_str(3,sd.hdistance/*Hdist(distance)*/); //blir fel hdistance annars som skickas efter 3:e v�rdet
            copy_to_ir_str(4,roundi(ConvToGrad(sd.alpha)));
          }
          copy_to_ir_str(counter%3,sd.height);
          PrintStrOled(0,52,str_oled_Hgt_);
          PrintIntOled(0,62,sd.height*10);
        }
        else
        {
          if(counter == 0){  //fel hdistance anv�nds annars, styr med counter==0 och hgt (!=SHOW_H)
            {
              sd.hdistance = Hdist(sd.distance);   //ber�kna och spara horisontellt avst�nd
              sd.hdistance_cm = Hdist(sd.distance_cm);    //VLG2-2 Diameterm�tning, ber�kna horisontellt avst�nd �ven i cm
            }
          }
          copy_to_ir_str(3,sd.hdistance);
          copy_to_ir_str(4,roundi(ConvToGrad(sd.alpha)));
        }
        retv=1;
        break;
      }
    }

    if((ch=My_getc())>0)
    {
      ch=ch-'0';//to bin Enter=1...
      //SetTimer(60000);
      if(ch<sizeof(g)/sizeof(g[0]))
        if((retv=(*g[ch-1])())<0)
        {
          if(retv==-2){//force angle=0 by user...not in VL....
            retv=1;
          }
          else{
            retv=0;
          }
          break;
        }
      SetTimer(60000);  //timern kan nollas i n�gon av knapptryckningarna i g[]
    }//if((ch=getc())>0)
  }//while(GetTimer0()>0u)

  return retv;
}

/**********************************
*
*
***********************************/
void redrawAimLaserText(void)
{
  cls();
  putxy(0,0, (char *)s200_strAim1_);
  putxy(0,1, (char *)s200_strAim2_);
  putxy(0,2, (char *)s200_strAim3_);
}

/**********************************
One Laser Shot
***********************************/
int LaserShot(int show_aim_text, int copy_dist, int copy_ir, int meas_ang, int scan_mode)
{
  int ch;
  int Lmode=-1;
  int t;
  int UseCompass = GetStatus()&STATUS_USE_COMPAS;

  measured_continously=0;            //VLG2-8 flagga f�r att visa om skjutit kontinuerligt med lasern under m�tning

  if(show_aim_text)
  {
    redrawAimLaserText();
    if(show_aim_text==2){
      putxy(0,3, (char *)s200_strAim4_);
      putxy(0,4, (char *)s200_strAim5_);
    }
  }

  do
  {
    if(1)  //Blink text/sight
    {
      ch=ToggleOledTxtAndSightGetSystemKey(checkrollflag);
      if(checkrollflag!=LASERROLLED_NONE && (ch==LASERROLLED_UP || ch==LASERROLLED_RIGHT))      //VLG2-2 Om diameterm�tning s� reagerar nu om vinklar enheten �t sidan
        return ch;
      Sight_Oled(1);
    }
    else
    {
      Sight_Oled(1);
      ch=GetSystemKey();
    }
    sd.OledDisplayDataValid=0;
    switch(ch)
    {
    case KEY_ENTER:
      if(scan_mode == 0)
      {
        wait(300);
        if(scankey() == KEY_ENTER_BIN)
        {
          //l�ngt entertryck, returnera
          return -2;
        }
      }

      do
      {
        Sight_Oled(1);

        ch = KEY_ENTER;    //f�r att det ska bli bra om scannar och hittar ett m�l, vid avslut ur loopen
        cls();
        putxy(0,0, (char *)hgt_str_Laser_);

        //scanning, m�t bara om avst�nd, inte vinkel (tar tid)
        //om inget v�rde f�s och Enter h�lls nedtryckt, m�t om, avbryt n�r giltigt v�rde f�s, obs v�nta minst 10ms mellan m�tningarna, annars h�nger sig lasern

        if((t = s200ReadDistCont()/*s200ReadDist()*/) > 0)     //VLG-28 Skjuter med lasern tills sl�pper ON, tar sist m�tta v�rdet (bios)
        {
          sd.OledDisplayDataValid=1;//used to blink sight/result in oled
          sd.distance = t;

          sd.distance_cm = distance_to_cm(t);   //VLG2-2 Diameterm�tning, spara alltid sd.distance_cm i cm, t i cm eller ftx10

          if(!meas_ang)
          {
            PrintStrOled(0,42,hgt_str_SD_);//try use 2decimals
            PrintIntOled(0,52,sd.distance);//try use 2decimals
          }
          if(Metric())
            sd.distance=roundi(sd.distance);

          if(sd.distance <= MIN_LASER_DIST/*5*/){   //inget avst�nd eller mindre �n 5 dm V1.9
            cls();
            putxy(0,0, (char *)s200_strNoVal1_);
            putxy(0,1, (char *)s200_strNoVal2_);
            putxy(0,2, (char *)s200_strShotAgain1_);
            putxy(0,3, (char *)s200_strShotAgain2_);
            Sight_Oled(0);//blinka till med korset n�r meddelande tagits emot fr�n lasern
            PrintStrOled(0,42,"---");
            My_Bell(500);
            //wait(500);
            ch = 0;
          }
          else
          {
            sd.SaveOk=1;//jn
            if(copy_dist==1){
              sd.distance1=sd.distance;
            }
            else if(copy_dist==2){
              sd.distance2=sd.distance;
            }
            else if(copy_dist==3){  //fLaser()
              sd.distance += ee->EErefH;
              sd.add_to_Hgt=ee->EErefH;//save info about ref
              sd.height=sd.distance;
            }

            if(copy_ir){
              copy_to_ir_str(3,sd.distance);
            }
            if(meas_ang)
            {
              if(force_compass || (ee->useGps && ee->EEsaveEEmem) || UseCompass)   //VLG-19 sl� endast av lasern om kompassen ska anv�ndas
                s200PowerOffNoReady();
              Sight_Oled(0);//blinka till med korset n�r meddelande tagits emot fr�n lasern
              wait(20);
              Sight_Oled(1);

              measure_angle_(0,SHOW_ALPHA_AND_H,NO_ADD_REF_H);
              if(force_compass || (ee->useGps && ee->EEsaveEEmem) || UseCompass)   //VLG-19 sl� endast p� lasern om kompassen ska anv�ndas
                s200PowerOnNoReady();   //VLG-2 flyttar uppstart av lasern hit s� att det g�r att spara v�rdet direkt efter pipet

              redrawAngleResult();
              Bell(200);
              clr_buff();

              //VLG-2 om autospar s� spara och returnera. Eg spara f�reg�ende v�rde.
              if(auto_save_target)
              {
                if(autosd.SaveOk)      //finns n�got att spara?
                {
                  struct storedatatype tsd;

                  //dessa s�tts f�rst i fhgt1pl_ - funktionen
                  sd.gamma=sd.alpha;//angle 1
                  sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw

                  tsd = sd;
                  sd = autosd;  //spara f�reg�ende punkt

                  ir_str_ram(); //det ber�knas och s�tts massa saker i sd-structen vid sparningen

                  tsd.x0=sd.x0; //dessa s�tts vid sparningen
                  tsd.y0=sd.y0;
                  tsd.xf=sd.xf;
                  tsd.yf=sd.yf;
                  tsd.area = sd.area;
                  tsd.len = sd.len;

                  tsd.sek = sd.sek;     //uppdatera n�sta sekvensnummer
                  tsd.id = sd.id;       //tappar �ven id-numret
                  autosd = tsd; //l�gg nya punkten i k� f�r att sparas
                  sd = tsd;       //n�dv�ndigt??
                }
                else
                {
                  if(sd.sek<=1)  //p� f�rsta punkten ska gps-pos tas
                  {
                    wait(200);    //v�ntar lite innan g�r �ver till GPS
                    SaveOledDispContenz();      //spara displayerna f�r att kunna visa data efter gps-inl�sningen
                    SaveDispContenz();
                    if(ReadGps()>0)
                      sd.PreDefGps=1;//avoid gps when storing, have allready

                    RestoreOledDispContenz();
                    RestoreDispContenz();
                  }

                  //dessa s�tts f�rst i fhgt1pl_ - funktionen
                  sd.gamma=sd.alpha;//angle 1
                  sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw

                  autosd = sd;  //kommer hit efter varje ny ref-punkt

                  //p� f�rsta punkten s� fejka i displayen och pip att den sparar
                  xprintf(0,0,DISP_REVERSE,12,"%-12s",(char *)str_DataSaved_);
                  wait(200);    //v�ntar lite
                  bell(200,3000);
                }

                return 1;
              }
            }//meas ang
            break;  //ska hoppa ut h�r om h�ller Enter intryckt i scanl�ge eftersom f�tt giltigt v�rde
          }//have dist
        }
        else
        {
          Sight_Oled(0);
          redrawAimLaserText();
          ch=0;
          wait(10);   //fick inget v�rde, prova att ta nytt om enter nedtryckt, v�nta lite annars h�nger lasern sig
        }
      }while(scankey()==KEY_ENTER_BIN);   //ta nytt v�rde ifall enter nedtryckt
      break;
    case KEY_RIGHT:
      Sight_Oled(0);    //VLG-2 blir konstig prick i displayen d� raderat och trycker p� SEND igen, p.g.a. tv� anrop till Sight_Oled(1) efter varandra
      if(scankey()&KEY_LEFT_BIN){

        return -1;//tryckte Exit
      }

      if(auto_save_target)      //VLG-2 autosparar v�rden, f�r map target
      {
        //l�ngre SEND-tryck f�r att radera, annars l�tt att sista v�rdet raderas n�r man trycker esc och SEND lite f�re DME
        SetTimer(200);
        while(GetTimer())
        {
          Idle();
          if(scankey()&KEY_LEFT_BIN){

            return -1;//tryckte Exit
          }
        }

        if(!(scankey()&KEY_RIGHT_BIN)){ //se att SEND fortfarande �r intryckt
          break;
        }

        if(autosd.SaveOk)      //finns n�got att spara
        {
          memset(&autosd,0x00,sizeof(autosd));
          Sight_Oled(0);
          PrintStrOled(0,42,str_oled_Deleted_);
          PrintStrOled(0,52,str_oled_Deleted2_);
          xprintf(0,0,DISP_REVERSE,12,"%-12s",str_Deleted_);
          My_bell2(300,3000);
          //wait(300);
          bell(300,3000);
        }
      }
      else if(sd.SaveOk)//something to send?
      {
        Sight_Oled(0);
        ir_str_ram();
        return 1;
      }

      break;

    case KEY_LEFT:
      if(scankey()&KEY_RIGHT_BIN){

        return -1;//tryckte Exit
      }
      if(use_dme && ((sd.measmethod==ONE_P_LASER_METHOD && sd.treemethod == ONE_P_LASER_OR_DME_METHOD_LINECLEAR)||sd.measmethod==TRANSPONDER_METHOD && sd.treemethod == ONE_P_LASER_OR_DME_METHOD_LINECLEAR || sd.measmethod==TWO_P_LASER_METHOD || sd.measmethod==TREE_P_METHOD_TRAIL))        //VLG-2 MAP TRAILXY kunna anv�nda DME f�r h�jdm�tningen ocks�
      {    //#4237 2PL m�t avst�ndet till rot med ultraljud ist�llet //#3947 kunna m�ta avst�ndet med ultraljud, farliga tr�dfunktionen
        wait(300);
        if(scankey()&KEY_LEFT_BIN)  //om h�llt inne dme... annars byt lasermode
          return -3;
      }

      if(Lmode<0)
      {
        Lmode=s200GetMode();    //0=first, 1=strongest, 2=last
      }
      else
        Lmode++;
      if(Lmode<0)
        Lmode=0;//just in case of error
      Lmode%=3;//0,1,2
      {
        char *s1[]={(char*)str_oled_LaserFirst1_,(char*)str_oled_LaserStrong1_,(char*)str_oled_LaserLast1_};
        char *s2[]={(char*)str_oled_LaserFirst2_,(char*)str_oled_LaserStrong2_,(char*)str_oled_LaserLast2_};
        char *s3[]={(char*)str_oled_LaserFirst3_,(char*)str_oled_LaserStrong3_,(char*)str_oled_LaserLast3_};

        s200SetMode(Lmode);

        Bell(100);
        Sight_Oled(0);
        PrintStrOled(0,42,s1[Lmode]);
        PrintStrOled(0,52,s2[Lmode]);
        PrintStrOled(0,62,s3[Lmode]);
        sd.OledDisplayDataValid=1;//used to blink sight/result in oled
      }
      break;
    case KEY_EXIT:
      return -1;
    default:   //timeout
      if(sd.treemethod==NORMALMETHOD)
        return -1;
      else//keep laser alive when doing complex method's
      {
        Sight_Oled(0);//save oled light
        sd.OledDisplayDataValid=1;
        break;
      }
    }
  }while(ch!=KEY_ENTER);

  return sd.distance;
}

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/
