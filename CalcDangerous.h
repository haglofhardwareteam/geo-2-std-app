#ifndef __CALCDANGEROUS_h
#define __CALCDANGEROUS_h

int DangTreeMenu();

int CalcBeta(int ialpha,int isd/*dm*/,int trpH /*dm*/);
int safetylen_Fast(int ialpha,int ibeta,int isd/*dm alt ftx10*/);
int DangTreeFast(void);
int ShowResultLimitsFast3pl(void);
int ShowResultLimitsFast(void);

int DangTree3P();
int f3d_3p_data();
#endif
