#include "MyFuncs.h"

const char cCLRROW[] = "            ";

/**********************************
*
*
***********************************/
int fDme()
{
  //Swi 70 int dmef(MeasureAgainWhenKey,alphax100),  retur slope dist:  <0 error, >=0 cm alt ftx10, alphax100=vinkel f�r Hdist visning.
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan
  dmef(DME_WAIT_FOR_KEY,0);   //om L-instrument s� blir endast laser

  return 1;
}

/**********************************
*
*
***********************************/
int fhgt1pl(void)
{
  int res;
  data_set();//reset data storage output
  sd.use_ir=1;
  irset();

  //VLG2-2 Diameterm�tning, initiera m�tningen
  if(ee->useDiameter)
    checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning
  else
    checkrollflag=LASERROLLED_NONE;

  res = fhgt1pl_();

  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

  return res;
}

/**********************************
*
*
***********************************/
int fhgt1pl_lineclear(void)
{
  int res;
  data_set();//reset data storage output
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan
  sd.treemethod=ONE_P_LASER_METHOD_LINECLEAR;
  res=fhgt1pl_();
  return res;
}

/**********************************
*
*
***********************************/
int fhgt1pl_relative()
{
  int res;
  sd.treemethod=ONE_P_LASER_METHOD_RELATIVE;
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

  res=fhgt1pl_();

  return res;
}

/**********************************
*
*
***********************************/
int fhgt1pl_repeat(void)
{
  int res;
  sd.treemethod=ONE_P_LASER_METHOD_REPEAT;
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

  res=fhgt1pl_();

  return res;
}

/**********************************
*
*
***********************************/
int fhgt1pl_gps(void)
{
  int res;
  sd.treemethod=ONE_P_LASER_METHOD_GPS;
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan
  res=fhgt1pl_();
  return res;
}

/**********************************
*
*
***********************************/
int fhgt1pl_compass(void)
{
  int res;
  sd.treemethod=ONE_P_LASER_METHOD_COMPASS;
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan
  res=fhgt1pl_();
  return res;
}

/**********************************
*
*
***********************************/
int fhgt1pl_(void)
{
  int ch=0,res;
  UseLaser=1;
  sd.stored=0;//zero, data_set() is sometimes not used
  sd.SaveOk=0;//zero
  sd.OledDisplayDataValid=0;//zero

  sd.measmethod=ONE_P_LASER_METHOD;

  if(s200PowerOnReady()<=0)
  {
    ch=0;
    goto end_1p;
  }

  //t�nd sikte
  //Enter f�r att ta avst�ndet
  //Escape avbryter till huvudmenyn
  redrawAimLaserText();

  do
  {
    if((res=LaserShot(0,0,1,1,SCAN_MODE)) >= 0)   //VLG2-2 Anpasst f�r diameterm�tning
    {
      sd.gamma=sd.alpha;//angle 1
      sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw
      sd.beta=sd.alpha;
      WhileEnterPressed();   //v�rde f�tts, v�nta tills enter sl�ppts

      if(sd.treemethod==ONE_P_3D_METHOD||sd.treemethod==ONE_P_LASER_METHOD_RELATIVE||sd.treemethod==ONE_P_LASER_METHOD_REPEAT||sd.treemethod==ONE_P_LASER_METHOD_COMPASS||sd.treemethod==ONE_P_LASER_METHOD_LINECLEAR||sd.treemethod==ONE_P_LASER_METHOD_GPS||sd.treemethod==ONE_P_LASER_METHOD_RELATIVE_XY)//retur om sparad vid 3d och map. //VLG-2 MAP GPS, TRAIL XY
      {
        if(sd.stored || (sd.treemethod==ONE_P_3D_METHOD && measured_continously))//sparat?  //VLG2-8 eller om 3D Vektorm�tning och scannat avst�ndet
        {
          ch=1;//next 3d
          goto end_1p;
        }
      }
      else if(sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)       //VLG-2 MAP TRAILXY annan hantering av knapptryck
      {
        if((ch=ToggleOledTxtAndSightGetSystemKey(LASERROLLED_NONE)) == KEY_RIGHT)
        {
          Sight_Oled(0);
          if(scankey()&KEY_LEFT_BIN){

            ch=KEY_EXIT;//tryckte Exit
            goto end_1p;
          }

          if(sd.SaveOk)//something to send?
          {
            ir_str_ram();
          }
          else
            ch=0;
        }
        else if(ch==KEY_LEFT)     //�ndra laser-filter
        {
          ChStoreDisInt(KEY_LEFT);
          continue;
        }
        else if(ch==KEY_EXIT)
        {
          ch=KEY_EXIT;
        }
        goto end_1p;
      }
    }
    else if(res==LASERROLLED_RIGHT)    //VLG2-2 m�t diametern, 1PL-m�tningen
    {
      if(sd.distance>0)        //m�ste ta ett avst�nd f�rst innan kan hoppa in i diameterm�tning
      {
        if((ch=My_Meas_Dia(sd.hdistance_cm,ee->EEdiameter,ADD_REF_H))==KEY_EXIT)
        {
          res=0;
          goto end_1p;
        }

        redrawAngle();
        redrawAngleResult();      //OBS! r�knar om sd.height

        //om sparats s� skriv ut i stora displayen
        if(sd.stored)
        {
          xprintf(0,0,DISP_REVERSE,12,"%-12s",str_DataSaved_);
          sd.stored=0;
        }

      }

      res=1;
      checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning
    }
    else
    {
      res=0;
      goto end_1p;
    }
  }while(res>=0);

end_1p:
  Sight_Oled(0);
  s200PowerOffReady(); //in case laser is on
  UseLaser=0;
  if(sd.treemethod==ONE_P_LASER_METHOD_COMPASS_XY)      //VLG-2 MAP TRAILXY
  {
    return ch;
  }
  if(sd.treemethod!=NORMALMETHOD)//new Dangerous
  {
    return -ch;
  }
  return 1;
}

/**********************************
*
*
***********************************/
int fhgt1pl_compass_return(void)        //VLG-2 1pl-m�tning, returnerar direkt med tangenttryck
{
  int ch=0;
  UseLaser=1;
  sd.stored=0;//zero, data_set() is sometimes not used
  sd.SaveOk=0;//zero
  sd.OledDisplayDataValid=0;//zero

  sd.measmethod=ONE_P_LASER_METHOD;
checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

  if(s200PowerOnReady()<=0)
  {
    ch=0;
    goto end_1p_cr;
  }

  //t�nd sikte
  //Enter f�r att ta avst�ndet
  //Escape avbryter till huvudmenyn
  redrawAimLaserText();

  while(LaserShot(0,0,1,1,SCAN_MODE) >= 0)
  {
    sd.gamma=sd.alpha;//angle 1
    sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw
    sd.beta=sd.alpha;
    WhileEnterPressed();   //v�rde f�tts, v�nta tille enter sl�ppts

    if((ch=ToggleOledTxtAndSightGetSystemKey(LASERROLLED_NONE)) == KEY_RIGHT)
    {
      Sight_Oled(0);
      if(scankey()&KEY_LEFT_BIN){

        ch=0;//tryckte Exit
        goto end_1p_cr;
      }

      if(sd.SaveOk)//something to send?
      {
        ir_str_ram();
      }
      else
        ch=0;
    }
    else if(ch==KEY_LEFT)     //�ndra laser-filter
    {
      ChStoreDisInt(KEY_LEFT);
      continue;
    }
    else if(ch==KEY_EXIT)
    {
      ch=0;
    }
    goto end_1p_cr;
  }

end_1p_cr:
  Sight_Oled(0);
  s200PowerOffReady(); //in case laser is on
  UseLaser=0;

  return ch;
}

/**********************************
3 points, laser
***********************************/
int fhgt3pl(void)
{
  int res;
  data_set();//reset data storage output
  sd.use_ir=1;
  sd.measmethod=TREE_P_METHOD_FIRSTLASERSHOT;//start with laser

  res = fhgt3pl_();

  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

  return res;
}

/**********************************
*
* tree method allready defined by calling function
***********************************/
int fhgt3pl_(void)
{
  int ch, counter,res=1,tmp,first,scanmode,show;
  int AddTrpH=NO_ADD_REF_H;
  int useTrailxyDme=0;
  UseLaser=1;
  counter=0;
  sd.distance=0;
  sd.hdistance=0;
  irset();

  InitDispBuff();       //init scroll buffert

  checkrollflag=LASERROLLED_NONE;       //bara f�r att vara s�ker p� att inte reagerar p� vridning tills flaggan s�tts

  if(s200PowerOnReady()<=0)
  {
    res=0;
    goto end_3p;
  }

  //t�nd sikte
  //Enter f�r att ta avst�ndet
  //Escape avbryter till huvudmenyn

  //slinga s� att s� l�nge kort tryck s� m�t om avst�ndet, l�ngre tryck hoppa �ver till vinkelm�tningen (om avst�nd tagits)
  if(use_dme)
  {
    if(sd.measmethod==TREE_P_METHOD_TRAIL){
      first=2;
      useTrailxyDme=1;
    }
    else if(sd.treemethod!=NORMALMETHOD){
      first=2;    //visa lite mer uppstartstext om farliga tr�d metoden
    }
    else{
      first=1;
    }
  }
  else
  {
    first=1;
  }
  scanmode=1;
  do
  {
    if((tmp=LaserShot(first,0,1,1,scanmode)) >= 0)
    {
      sd.gamma=sd.alpha;   //angle 1
      sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw

      WhileEnterPressed();    //v�nta tills enter sl�ppt
      scanmode=0; //hoppa �ver till vinkelm�tning p� l�ngt entertryck
      if(sd.stored)
      {
        tmp = -2;//if stored, next angle.
      }
    }
    else if(tmp == -2)
    {
      My_getc();   //l�ngt enter, b�rja m�t vinkeln
    }
    else if(tmp == -3)  //kunna m�ta avst�ndet med ultraljud, farliga tr�dfunktionen
    {
      int ret;
      s200PowerOffNoReady(); //turn off laser
      if((ret=dmef(DME_NO_WAIT_FOR_KEY,0))>0)    //cm alt ftx10
      {
        AddTrpH=ADD_REF_H;
        sd.distance = ret;      //cm alt ftx10

        sd.distance_cm = distance_to_cm(ret);   //VLG2-2 Diameterm�tning, spara alltid sd.distance_cm i cm

        if(Metric())
          sd.distance=roundi(sd.distance);    //dm
        sd.measmethod = TRANSPONDER_METHOD;
        Sight_Oled(0);//blinka till med korset n�r meddelande tagits emot fr�n dme
        wait(20);
        Sight_Oled(1);
        measure_angle_(0,SHOW_ALPHA_AND_H,AddTrpH);
        redrawAngleResult();
        My_Bell(200);
        //wait(200);
        while(GetButtons()&(KEY_LEFT-'0'))
          Idle();

        sd.gamma=sd.alpha;//angle 1
        sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw
        sd.beta=sd.alpha;
      }
      else
      {
        Sight_Oled(0);//VLG-2 inget avst�nd, f�r dubbelprick om inte k�r Sight_Oled(0)
        PrintStrOled(0,42,"---");

        s200PowerOnNoReady();
        AddTrpH=NO_ADD_REF_H;
        tmp=0;
      }
    }
    else
    {
      res=0;
      goto end_3p;
    }
    first=0;    //visa inte aim text

  }while(tmp>=0);

  s200PowerOffReady(); //turn off laser

  //G� sedan �ver till vinkelm�tning automatiskt, Escape f�r att b�rja om
  //Enter m�t vinkel
  //Enter m�t andra vinkeln

  //�ndra inte measmethod om dme och farliga tr�d
  if(sd.measmethod == TRANSPONDER_METHOD){
    counter = 0;
    show = SHOW_H;
    redrawHgt(0);
  }
  else{
    if(sd.measmethod!=TREE_P_METHOD_TRAIL)      //VLG-2 TRAILXY, s�tt inte om m�tmetoden
      sd.measmethod=TREE_P_METHOD;//3=3p Laser
    counter = 1;
    show = SHOW_ALPHA_AND_H;
    AddTrpH = NO_ADD_REF_H;
  }

  if(sd.measmethod!=TREE_P_METHOD_TRAIL && !useTrailxyDme)        //VLG-2 TRAILXY uppdaterar sek-numret
    sd.sek=1;//set sek to 1 in case storing was used to finish laser

  UseLaser=0;
  if(measure_angle_(counter,show,AddTrpH)) //�ndrat counter till 1, hdist och alpha ska tas fr�n LaserShot-m�tningen
  {
    //Om farliga tr�dmetoden och anv�nt dme s� ska gamma=beta=angle 1, alpha=angle 2
    if(sd.measmethod != TRANSPONDER_METHOD){
      sd.beta=sd.alpha;//angle 2
    }
    Bell(200);
    WhileEnterPressed();
    My_getc();

    //Om farliga tr�dmetoden och anv�nt dme s� returnera h�r
    if(sd.treemethod!=NORMALMETHOD && sd.measmethod == TRANSPONDER_METHOD)//new Dangerous
    {
      if(useTrailxyDme)
      {
        //VLG-2 MAP TRAILXY, m�tt med DME, spara v�rdet
        sd.measmethod=TREE_P_METHOD_TRAIL;
        if((ch=dme_key()) == KEY_EXIT || ch==0)
        {
          res=0;
        }
        else{
          res=1;
        }
      }
      else
      {
        if((ch=getchar()) == KEY_EXIT || ch==0)
        {
          res=0;
        }
        else{
          res=1;
        }
      }
      goto end_3p;
    }

    counter=0;
    redrawHgt(0);

    //VLG2-2 Diameterm�tning, om  diameter s� s�tt checkroll-flagg h�r s� att kan hoppa �ver till diameteruts�kningen i vinkelm�tningen
    if(sd.treemethod==NORMALMETHOD && ee->useDiameter)
    {
      checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning i vinkelm�tningen
    }
    else
    {
      checkrollflag=LASERROLLED_NONE;
    }

meas_angle_3p:
  while((res=measure_angle_(counter,SHOW_H,NO_ADD_REF_H)))
    {
      Bell(200);
      WhileEnterPressed();
      My_getc();

      if(sd.treemethod!=NORMALMETHOD)//new Dangerous
      {
        if((ch=dme_key()) == KEY_EXIT || ch==0)
        {
          res=0;
        }
        else{
          res=1;
        }

        goto end_3p;
      }
      else//JN
      {
        if(ee->useDiameter)
        {
          if(res == LASERROLLED_RIGHT)
          goto meas_dia_3p;         //Om  diameter s� vill hoppa in i diameterm�tningen direkt
          else if(res == LASERROLLED_UP)
            goto meas_angle_3p; //�terg� till vinkelm�tningen
        }

//      wait_for_key:
        //VLG2-2 Diameterm�tning, reagera endast om tippar �t h�ger
        if(ee->useDiameter)
          checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning
        else
          checkrollflag=LASERROLLED_NONE;
        if((ch=ToggleOledTxtAndSightGetSystemKey(checkrollflag))==KEY_EXIT)//JN
          goto end_3p;
        else if(ch==KEY_RIGHT)
          ir_str_ram();
        else if(ch==LASERROLLED_RIGHT)    //VLG2-2 m�t diametern, 3P-m�tningen
        {
        meas_dia_3p:
          ch = My_Meas_Dia(sd.hdistance_cm,ee->EEdiameter,NO_ADD_REF_H);

          Redraw(SHOW_H);
          RedrawNum(counter,SHOW_H,sd.alpha,NO_ADD_REF_H);

          //om sparats s� skriv ut i stora displayen
          if(sd.stored)
          {
            xprintf(0,0,DISP_REVERSE,12,"%-12s",str_DataSaved_);
            sd.stored=0;
          }

          if(ch==KEY_EXIT)
            goto end_3p;
          else if(ch==LASERROLLED_UP)
            goto meas_angle_3p; //wait_for_key;
          else
            ch=0;
        }
        if(ee->useDiameter)
          checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning i vinkelm�tningen
        else
          checkrollflag=LASERROLLED_NONE;       //s� att inte reagerar p� tiltning l�ngre
      }

      counter++;        //gjort om scrollningen av h�jder, g�rs nu i redrawHgtNum()

    }//while(measure_angle_(counter,SHOW_H,NO_ADD_REF_H))
  } //if(measure_angle_(counter,SHOW_ALPHA_BOTTOM_OR_TOP,NO_ADD_REF_H))

end_3p:
  Sight_Oled(0);        //VLG-2 sl�cker �ven OLED
  s200PowerOffReady(); //in case laser is on
  UseLaser=0;
  if(sd.treemethod!=NORMALMETHOD){//new Dangerous
    return -res;
  }
  return 1;
}

/**********************************
Redraw height
***********************************/
void HgtRedraw(void)
{
  cls();
  putxy(0,0, (char*)hgt_str_MDist_);
  putint(12-6,0,ee->EEman_dist,6,1);
  putxy(0,2, (char*)hgt_str_MDistKey_);
  if(use_dme)
    putxy(0,3, (char*)hgt_str_DmeKey_);
}

/**********************************
*
*
***********************************/
int fhgt2pdme(void)
{
  int ch,counter,AddTrpH,res=0,ret;

  irset();
  data_set();//reset data storage output
  sd.use_ir=1;
  My_getc();
  AddTrpH=NO_ADD_REF_H;
  sd.distance=0;
  sd.hdistance=0;
  UseLaser=0;

  checkrollflag=LASERROLLED_NONE;       //bara f�r att vara s�ker p� att inte reagerar p� vridning tills flaggan s�tts

  InitDispBuff();       //init scroll buffert

  do
  {
    Sight_Oled(1);
    HgtRedraw();
    ch=getchar();
    switch(ch)
    {
    case KEY_ENTER: //dme
      UseLaser=1; //s� att den tar vinkeln automatiskt vid entertryck

      if((ret=dmef(DME_NO_WAIT_FOR_KEY,0))>0)        //cm alt ftx10
      {
        sd.distance=ret;      //cm alt ftx10

        sd.distance_cm = distance_to_cm(ret);   //VLG2-2 Diameterm�tning, spara alltid sd.distance_cm i cm

        if(Metric())
          sd.distance=roundi(sd.distance);    //dm
        sd.measmethod = TRANSPONDER_METHOD;
        AddTrpH = ADD_REF_H;
        Sight_Oled(0);
        wait(20);
        Sight_Oled(1);
      }
      else
      {
        ch = 0;
        UseLaser=0;
      }
      break;
    case KEY_RIGHT: //man dist
      sd.measmethod = TRANSPONDER_METHOD;
      AddTrpH = ADD_REF_H;
      sd.distance = ee->EEman_dist;
      sd.distance_cm = distance_to_cm(Metric()? ee->EEman_dist*10 : ee->EEman_dist);       //VLG2-2 Diameterm�tning, manuellt avst�nd i dm eller ftx10
      break;
    case KEY_EXIT:
      res=0;
      goto end_2pdme;
    default:
      break;
    }
  }while((ch!=KEY_ENTER) && (ch!=KEY_LEFT) && (ch!=KEY_RIGHT));

  counter=0;
  copy_to_ir_str(3,sd.distance);
  if(measure_angle_(0,SHOW_ALPHA_AND_H,AddTrpH))//alfa
  {
    Bell(200);

    if(UseLaser)
    {
      redrawAngleResult();
    }
    WhileEnterPressed();   //v�nta tills enter sl�ppt

    sd.gamma=sd.beta=sd.alpha;//gamma==alpha vid 2P
    sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw
    redrawHgt(1);       //VLG-26 lagt till argument f�r visning av kompassriktningen
    UseLaser = 0;

    //VLG2-2 Diameterm�tning, om diameter s� s�tt checkroll-flagg h�r s� att kan hoppa �ver till diameteruts�kningen i vinkelm�tningen
    if(ee->useDiameter)
    {
      checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning i vinkelm�tningen
    }
    else
    {
      checkrollflag=LASERROLLED_NONE;
    }
  meas_angle_2pdme:
    while((res=measure_angle_(counter,SHOW_H,AddTrpH)))
    {
      Bell(200);

      WhileEnterPressed();
      My_getc();

      if(sd.treemethod!=NORMALMETHOD)//new Dangerous
      {
        if((ch=getchar()) == KEY_EXIT || ch==0)
        {
          res=0;
        }
        else{
          res=1;
        }

        goto end_2pdme;
      }
      else
      {
        if(ee->useDiameter)
        {
          if(res == LASERROLLED_RIGHT)
          goto meas_dia_2pdme;         //Om  diameter s� vill hoppa in i diameterm�tningen direkt
          else if(res == LASERROLLED_UP)
            goto meas_angle_2pdme; //�terg� till vinkelm�tningen
        }

      //wait_for_key:
        //VLG2-2 Diameterm�tning, reagera endast om tippar �t h�ger
        if(ee->useDiameter)
          checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning
        else
          checkrollflag=LASERROLLED_NONE;
        if((ch=ToggleOledTxtAndSightGetSystemKey(checkrollflag))==KEY_EXIT)
          goto end_2pdme;
        else if(ch==KEY_RIGHT)
          ir_str_ram();
        else if(ch==LASERROLLED_RIGHT)    //VLG2-2 m�t diametern, 3P-m�tningen
        {
        meas_dia_2pdme:
          ch = My_Meas_Dia(sd.hdistance_cm,ee->EEdiameter,AddTrpH);

          Redraw(SHOW_H);
          RedrawNum(counter,SHOW_H,sd.alpha,AddTrpH);

          //om sparats s� skriv ut i stora displayen
          if(sd.stored)
          {
            xprintf(0,0,DISP_REVERSE,12,"%-12s",str_DataSaved_);
            sd.stored=0;
          }

          if(ch==KEY_EXIT)
            goto end_2pdme;
          else if(ch==LASERROLLED_UP)
            goto meas_angle_2pdme;      //wait_for_key;
          else
            ch=0;
        }
        if(ee->useDiameter)
          checkrollflag=LASERROLLED_RIGHT;       //s� att reagerar p� tiltning i vinkelm�tningen
        else
          checkrollflag=LASERROLLED_NONE;       //s� att inte reagerar p� tiltning l�ngre
      }
      counter++;    //gjort om scrollningen av h�jder, g�rs nu i redrawHgtNum()

    }//while(measure_angle_(counter,SHOW_H,AddTrpH))
  }//if(measure_angle_(0,SHOW_ALPHA_AND_H,AddTrpH))

end_2pdme:
  Sight_Oled(0);        //VLG-2 sl�cker �ven OLED
  UseLaser = 0;
  s200PowerOffReady();    //just in case

  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan

  if(sd.treemethod!=NORMALMETHOD)//new Dangerous
  {
    return -res;
  }
  return 1;
}

/**********************************
*
*
***********************************/
int f3d1pl(void)
{
  int res,tseq=sd.sek;
  data_set();//reset data storage output
  irset();
  sd.sek=tseq;
  sd.treemethod=ONE_P_3D_METHOD;
  checkrollflag=LASERROLLED_NONE;  //VLG2-2 Diameterm�tning, s� att inte reagerar d� enheten tiltas �t sidan
  res = fhgt1pl_();
  return res;
}

/**********************************
*
*
***********************************/
struct xyz_type *f3d_data()
{
  float dSD,dHD,dH,dAZ,dDeg,dX,dY;
  struct xyz_type xyz[2];
  static struct xyz_type res;
  const char *sDeg[] = {str_set_Deg_,str_set_Grad_,str_set_Percent_};

  memset(&res,0,sizeof(res));
  res.metric=Metric();
  while(f3d1pl())        //VLG2-8 f�renkla 3d-m�tningsrutinen, om scannat avst�ndet s� ska inte beh�va trycka SEND-knappen, utan d� �r det det avst�ndet som g�ller och programmet g�r vidare direkt
  {
    int starth=sd.height;//save for later if target is stored
    memset(&xyz,0,sizeof(xyz));

    xyz[0].distance=sd.distance;//dm alt ftx10
    xyz[0].pitch=(int)rint(sd.gPitch*FRADTODEG*100.0);//degsx100
    xyz[0].yaw=(int)rint(sd.gYaw*100.0);//to degs  degsx100
    xyz[0].Cartesian=ConvertToCartesian(xyz[0].distance/10.0,xyz[0].pitch/100.0,xyz[0].yaw/100.0);

    if(f3d1pl())
    {
      xyz[1].distance=sd.distance;//dm alt ftx10
      xyz[1].pitch=(int)rint(sd.gPitch*FRADTODEG*100.0);//degsx100
      xyz[1].yaw=(int)rint(sd.gYaw*100.0);//to degs  degsx100
      xyz[1].Cartesian=ConvertToCartesian(xyz[1].distance/10.0,xyz[1].pitch/100.0,xyz[1].yaw/100.0);

      dX=(xyz[1].Cartesian.x-xyz[0].Cartesian.x);
      dY=(xyz[1].Cartesian.y-xyz[0].Cartesian.y);
      dH=(xyz[1].Cartesian.z-xyz[0].Cartesian.z);
      dSD=sqrt(dX*dX+dY*dY+dH*dH);
      dHD=sqrt(dX*dX+dY*dY);
      dDeg=asin(dH/dSD)*FRADTODEG;//vinkeln 90deg = upp
      {
        static float pfPhiDeg, pfTheDeg,pfRhoDeg,pfChiDeg;
        float fR[3][3];
        float fBc[3]={dX,-dY,0.0};
        f3DOFMagnetometerMatrixNED(fR,fBc);
        fNEDAnglesDegFromRotationMatrix(fR,&pfPhiDeg, &pfTheDeg, &dAZ,&pfRhoDeg, &pfChiDeg);
      }

      Bell(200);
      data_set();//reset data storage output
      sd.treemethod=ONE_P_3D_METHOD;
      sd.measmethod=ONE_P_LASER_METHOD;
      sd.add_to_Hgt=ee->EErefH;
      sd.height=(int)rint(dH*10.0);
      sd.distance=(int)rint(dSD*10.0);
      sd.hdistance=(int)rint(dHD*10.0);
      sd.gPitch=dDeg/FRADTODEG;
      sd.gamma=(int)rint(dDeg*100.0);
      sd.gYaw=dAZ;
      sd.yaw=(int)rint(dAZ*100.0);
      sd.SaveOk=true;
      sd.prev_distance=xyz[0].distance;//store last point for gps location
      sd.prev_gamma=xyz[0].pitch;//store last point
      sd.prev_yaw=xyz[0].yaw;//store last point
      sd.prev_hgt=starth;//start hgt

      sd.sek=3;//final sek

      copy_to_ir_str(0,sd.height);//update blue struct in case of transmission, ignore ir

      if(ee->EEangletype==1)
        dDeg=ConvToGrad((int)rint(dDeg*100.0))/100.0;
      else if(ee->EEangletype==2)
        dDeg=ConvToProc((int)rint(dDeg*100.0))/10.0;

      cls();
      xprintf(0,0,DISP_REVERSE,3, hgt_str_SD_);
      xprintf(0,1,DISP_REVERSE,3, hgt_str_HD_);
      xprintf(0,2,DISP_REVERSE,3, hgt_str_H_);
      xprintf(0,3,DISP_REVERSE,4,sDeg[ee->EEangletype]);
      xprintf(0,4,DISP_REVERSE,3, hgt_str_AZ_);
      xprintf(12-8,0,0,8,"%8.1f",dSD);
      xprintf(12-8,1,0,8,"%8.1f",dHD);
      xprintf(12-8,2,0,8,"%8.1f",dH);
      xprintf(12-8,3,0,8,"%8.1f",dDeg);
      if(ee->AzimuthType==1u) //VLG-2 Azimuth
      {
        xprintf(12-8,4,0,8,"%7.1f'",fGradToGon(dAZ));
      }
      else
      {
        xprintf(12-8,4,0,8,"%8.1f",dAZ);
      }

      cls_Oled();
      PrintStrOled(0,22,str_oled_D_);//try use 2decimals
      PrintIntOled(0,32,(int)rint(dSD*100.0));//
      PrintIntOled(0,42,(int)rint(dHD*100.0));//
    }
    else
      break;
    if(dme_key()!=KEY_ENTER)
      break;
    data_set();//reset data storage output
  }
  return &res;
}

/**********************************
*
*
***********************************/
int f3d(void)
{
  force_compass=1;      //tvinga anv�ndande av kompass
  data_set();//reset data storage output
  f3d_data();
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}

/**********************************
*
*
***********************************/
#define COMPPIC_X (LCDMAXX-50/2)
#define COMPPIC_Y (LCDMAXY-50/2)
void DrawCompass(int angle)
{
  static struct disp_line_type l,l2,l3,l4;
  int R=20;
  angle=-angle+90;

  l.filled=l2.filled=l3.filled=l4.filled=0;

  Line(&l2);
  Line(&l3);
  Line(&l4);

  l.filled=l2.filled=l3.filled=l4.filled=1;
  l.x1=COMPPIC_X;
  l.y1=COMPPIC_Y;

  l.x2=(int)rint(l.x1-R*sin(angle/180.0*MA_PI));
  l.y2=(int)rint(l.y1+R*cos(angle/180.0*MA_PI));
  l3=l2=l;
  l2.x1=(int)rint(COMPPIC_X+5*cos(angle/180.0*MA_PI));
  l2.y1=(int)rint(COMPPIC_Y+5*sin(angle/180.0*MA_PI));
  Line(&l2);
  l3.x1=(int)rint(COMPPIC_X-5*cos(angle/180.0*MA_PI));
  l3.y1=(int)rint(COMPPIC_Y-5*sin(angle/180.0*MA_PI));
  Line(&l3);
  l4.x1=l2.x1;
  l4.y1=l2.y1;
  l4.x2=l3.x1;
  l4.y2=l3.y1;
  Line(&l4);

}
/**********************************
*
*
***********************************/
int ReadCompAndLaserOrDme(int laser_or_dme,int fix_or_relative_target, int loop_compass)      //VLG-2 lagt till argument f�r kompass-funktionen, full�sning men hittade inget b�ttre s�tt      //VLG2-14 lagt till reversed compass
{
  int ch;
  struct sensordatatype sensor;
  double dgYaw;

  sd.stored=0;//zero, data_set() is sometimes not used
  sd.SaveOk=0;//zero
  sd.OledDisplayDataValid=0;//zero

  //do
  {
    int i=0;

  start_compass:
    cls();
    cls_Oled();
    Sight_Oled(1);

    ResetMagCalCount();//reset filters
    sensor.counter=0;       //nollar f�r s�kerhets skull

    Draw_Icon(DISPLAY_GRAFIC_XY,&ico_comppic);
    putxy2(0,0,(char*)str_Compass_);
    if(rev_compass)
      putxy2(0,1,(char*)str_CompRev_);  //VLG2-14 skriver ut REV p� rad 1 om reversed

    if(ee->AzimuthType==1u) //VLG-2 Azimuth
    {
      putxy2(6,2,"'");    //nygrader
    }
    else
      putxy2(6,2,">");   //grader

    SetOledTimer(15);    //ca 15s
    do
    {
      read_angle(&sensor);        //g�r medelv�rde av inm�tta l�sningar, blir liten eftersl�pning av kompassv�rdet d� vrider enheten fort, vid m�tning nollas och tas nytt medel
      sd.gYaw=sensor.gYaw;
      dgYaw = sensor.gYaw;
      if(rev_compass)   //VLG2-14 lagt till reversed compass
      {
        //r�kna om v�rdet +180 mod 360
        float dAZ = sd.gYaw + 180.0;
        if(dAZ >= 360.0)
          dAZ -= 360.0;
        sd.gYaw = dAZ;
      }

      ch=My_getc();

      if(ch<0)//no key
      {
        if(i++==20)       //skruva p� v�rdena f�r att f� den att blinka l�ngsammare eller fortare
        {
          Sight_Oled(0);

          if(rev_compass)
            PrintStrOled(0,32,str_CompRev_);  //VLG2-14 skriver ut REV i Oled om reversed

          if(ee->AzimuthType==1u) //VLG-2 Azimuth
          {
            putint2(0,2,(int)rint(fGradToGon(sd.gYaw)*10.0),6,1);
            putint2_Oled(0,44,(int)rint(fGradToGon(sd.gYaw)),3,0);   //change x=52->44 in V2.6
            putxy2_Oled(18,44,"'");
          }
          else
          {
            putint2(0,2,(int)rint(sd.gYaw*10.0),6,1);
            putint2_Oled(0,44,(int)rint(sd.gYaw),3,0);   //change x=52->44 in V2.6
          }
          DrawCompass((int)rint(dgYaw/*sd.gYaw*/));     //VLG2-14 s� att norrpilen ritas ut r�tt
        }
        else
          if(i==33/*30*/)
            Sight_Oled(1);
          else
            if(i==46/*40*/)
            {
              Sight_Oled(0);
              i=0;
            }
      }
      else
      {
        if(ch==KEY_ENTER)
        {
          Sight_Oled(1);
          if(laser_or_dme==USELASER)
          {
            if(loop_compass)
            {
              int tmp=0;
              ChStoreDisInt(KEY_ENTER);//restore enter to trigger laser
              tmp = fhgt1pl_compass_return();       //VLG-2 gjort egen version av fhgt1pl_ f�r att f� till det med hantering av knapptryckning och returv�rde
              return tmp;
            }
            else
            {
              ChStoreDisInt(KEY_ENTER);//restore enter to trigger laser
              sd.treemethod=fix_or_relative_target;
              return fhgt1pl_();
            }

          }
          else if(laser_or_dme==USEDME)//use dme
          {
          use_dme_:
            {
              int ret;
              do
              {
                Sight_Oled(1);
                if((ret=dmef(DME_NO_WAIT_FOR_KEY,0))>0)//and now dist        //cm alt ftx10
                {
                  sd.distance=ret;      //cm alt ftx10

                  sd.distance_cm = distance_to_cm(ret);   //VLG2-2 Diameterm�tning, spara alltid sd.distance_cm i cm

                  if(Metric())
                    sd.distance=roundi(sd.distance);    //dm
                  clr_buff();
                  UseLaser=1;  //sim laser no need to hold enter key dwn
                  if(measure_angle_(0,SHOW_ALPHA_AND_H,NO_ADD_REF_H))
                  {
                    Bell(200);
                    sd.yaw=((int)rint(sd.gYaw*100.0));
                    sd.beta=sd.gamma=sd.alpha=((int)rint(sd.gPitch*18000.0/PI));

                    sd.measmethod=ONE_P_LASER_METHOD;  //sim laser (use eye hgt)
                    sd.treemethod=fix_or_relative_target; //fixed or relative target used in Trail

                    redrawAngleResult();//default laser redraw
                    if(sd.treemethod != ONE_P_LASER_METHOD_COMPASS_XY && sd.treemethod != ONE_P_LASER_METHOD_RELATIVE_XY)      //F�r ONE_P_LASER_METHOD_COMPASS_XY och ONE_P_LASER_METHOD_RELATIVE_XY g�rs det h�r i redrawAngleResult(), s� att det blir r�tt vid laserm�tningen
                      sd.height-=ee->EEtrpHeigth;//remove trpH to get to ground
                    sd.add_to_Hgt-=ee->EEtrpHeigth;//save info about ref (eyeH-trpH)
                    putint(12-6,2,sd.height,6,1);   //update new hgt
                    if(sd.treemethod!=COMPASS_METHOD&&sd.treemethod!=ONE_P_LASER_METHOD_COMPASS&&sd.treemethod!=ONE_P_LASER_METHOD_COMPASS_XY)
                      PrintIntOled(0,62,sd.height*10);        //VLG-2 uppdaterar h�jd i OLED ox�
                    copy_to_ir_str(0,sd.height);       //VLG-5 s� att h�jden uppdateras i sdBlue-structen
                  }
                }
                else
                {
                  Sight_Oled(0);
                  PrintStrOled(0,52,"---");
                }
                ch=ToggleOledTxtAndSightGetSystemKey(LASERROLLED_NONE);
                //ch=dme_key();    //om ENTER s� m�t avst�ndet med lasern och r�kna ut och visa baf-diametern
                if(ch == KEY_RIGHT && sd.SaveOk)
                {
                  ir_str_ram();
                  return ch;
                }
                else if(ch==KEY_ENTER && (fix_or_relative_target==ONE_P_LASER_METHOD_COMPASS_XY || loop_compass))
                {
                  //VLG-2 MAP TRAILXY om m�ter om s� visas b�ringen, som det g�rs f�r laser
                  goto start_compass;
                }
                else
                  if(ch==KEY_EXIT)
                    return 0;
              }
              while(1);
            }
          }
          else
            return KEY_ENTER;
        }
        else if(use_dme && ch == KEY_LEFT && (laser_or_dme==USEDME || fix_or_relative_target!=ONE_P_LASER_METHOD_COMPASS_XY || loop_compass))  //VLG-2 MAP TRAILXY lagt till villkor f�r att f�rhindra att DME k�rs ig�ng med KEY LEFT om laserm�tning
          goto use_dme_;
        else if(ch==KEY_EXIT)     //VLG-2 lagt till hantering av KEY_EXIT
          return 0;
      }
    }
    while(ch!=KEY_EXIT);
  }
  //while((ch&KEY_EXIT_BIN)!=KEY_EXIT_BIN);
  clr_buff();
  cls_Oled();
  return 0;
}

/**********************************
*
*
***********************************/
int fmeasure_compass(void)
{
  force_compass=1;      //tvinga anv�ndande av kompass
  irset();//reset ir and meas res struct
  data_set();//reset data storage output
  sd.use_ir=1;
  irset();
  sd.treemethod=COMPASS_METHOD;
  while(ReadCompAndLaserOrDme(USELASER,ONE_P_LASER_METHOD_COMPASS,1));//Laser, fixed target      //VLG-2 MAP TRAIL COORD lagt till argument   //VLG-2 �ndrat s� att den visar kompassen d� trycker ON f�r att skjuta p� nytt
  force_compass=0;      //tvinga anv�ndande av kompass
  return 1;
}

/**********************************
*
*
***********************************/
int fmeasure_compass_rev(void)  //VLG2-14 Reversed compass
{
  force_compass=1;      //tvinga anv�ndande av kompass
  irset();//reset ir and meas res struct
  data_set();//reset data storage output
  sd.use_ir=1;
  irset();
  sd.treemethod=COMPASS_METHOD;
  rev_compass=1;        //VLG2-14 Reversed compass
  while(ReadCompAndLaserOrDme(USELASER,ONE_P_LASER_METHOD_COMPASS,1));//Laser, fixed target      //VLG-2 MAP TRAIL COORD lagt till argument   //VLG-2 �ndrat s� att den visar kompassen d� trycker ON f�r att skjuta p� nytt
  force_compass=0;      //tvinga anv�ndande av kompass
  rev_compass=0;
  return 1;
}

/**********************************
Show delta heights
***********************************/
int Show2plDeltaResult(void)
{
  cls();
  putxy(0,0, (char*)hgt_str_HD1_);putint(12-6,0,sd.distance2,6,1);
  putxy(0,1, (char*)hgt_str_HD2_);putint(12-6,1,sd.distance1,6,1);
  putxy(0,2, (char*)hgt_str_H_);
  if(sd.height2 < -999){
    putint(0,2,sd.height2,6,1);
  }
  else{
    putint(1,2,sd.height2,5,1);
  }
  putint(12-6,2,sd.height1,6,1);

  //visa �ven niv�skillnaden h1 - h2
  putxy(0,4,(char*)hgt_str_Diff_);
  putint(12-6,4,abs(sd.height2-sd.height1),6,1);

  putxy(6,3, (char*)hgt_str_H_);putint(12-5,3,sd.height3,5,1);  //h�r blir problem om ut�kar till 3 tecken, portugisiska
  Sight_Oled(0);

  My_getc();
  return getchar();//beh�ver inte kunna spara v�rden h�r
}

/**********************************
Show final delta height result
***********************************/
void Show2plFinalDeltaResult(void)
{
  cls();
  Sight_Oled(0);
  putxy(0,0, (char*)hgt_str_HD1_);putint(12-6,0,sd.distance2,6,1);
  putxy(0,1, (char*)hgt_str_HD2_);putint(12-6,1,sd.distance1,6,1);
  putxy(0,2, (char*)hgt_str_H_);

  if(sd.height2 < -999){
    putint(0,2,sd.height2,6,1);
  }
  else{
    putint(1,2,sd.height2,5,1);
  }
  putint(12-6,2,sd.height1,6,1);
  putxy(6,3, (char*)hgt_str_H_);putint(12-5,3,sd.height,5,1);//line hgt  //h�r blir problem om ut�kar till 3 tecken, portugisiska
  putxy(0,4, (char*)hgt_str_DeltaH_);putint(12-6,4,sd.height3,6,1);
  copy_to_ir_str(1,abs(sd.height2));//Height first pole
  copy_to_ir_str(2,abs(sd.height1));//Height second pole
  copy_to_ir_str(0,sd.height3);//deltaH     //kopierar denna sist s� att det blir r�tt h�jd i nmea
  sd.measmethod=ONE_P_LASER_METHOD;//store enable
  sd.treemethod=0;
  sd.stored=0;//enable save
  sd.SaveOk=1;//enable save
  sd.sek=1;//
  My_getc();
  sd.use_ir=1;
  sd.gamma=sd.alpha;//angle
  sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw
  dme_key();
}

/**********************************
*
*
***********************************/
int fhgtdelta(void)
{
  int ch;
  UseLaser=1;
  irset();
  data_set();//reset data storage output
  sd.measmethod=ONE_P_LASER_METHOD;
  sd.treemethod=DELTA_METHOD_LASERSHOT;//1 p laser , don't store
  //turn on laser module

  if(s200PowerOnReady()<=0)
    goto end_delta;

  redrawAimLaserText();
  putxy(0,3, (char*)hgt_str_Height1_);
  do
    if(LaserShot(0,2,0,1,1) >= 0)
    {
      WhileEnterPressed();   //v�nta tills enter sl�ppt
      if(sd.stored)
        break;
    }
    else
      goto end_delta;
  while(1);

  sd.height2=sd.height;
  sd.distance2=Hdist(sd.distance);

  redrawAimLaserText();
  putxy(0,3, (char*)hgt_str_Height2_);

  sd.OledDisplayDataValid=0;
  sd.stored=sd.SaveOk=0;

  do
  {
    if(LaserShot(0,1,1,1,1) >= 0)
    {
      WhileEnterPressed();   //v�nta tills enter sl�ppt
      if(sd.stored)
        break;
    }
    else
      goto end_delta;
  }while(1);

  sd.height1=sd.height;//Remember h.result (height=global)
  sd.distance1=Hdist(sd.distance1);//Hdist2

  sd.height3=CalcHeightOptional(sd.distance1,sd.height1,sd.distance2,sd.height2);

  if((ch=Show2plDeltaResult())==KEY_ENTER || ch==KEY_LEFT || ch==KEY_RIGHT)
  {
    clr_buff();
    redrawAimLaserText();
    putxy(0,3, (char*)hgt_str_DeltaH_);
    sd.OledDisplayDataValid=0;
    sd.stored=sd.SaveOk=0;
    do
    {
      if(LaserShot(0,0,1,1,1) >= 0)
      {
        WhileEnterPressed();   //v�nta tills enter sl�ppt
        if(sd.stored)
          break;
      }
      else
        goto end_delta;

    }while(1);

    sd.height3=sd.height3-sd.height;
    Show2plFinalDeltaResult();
  }

end_delta:
  Sight_Oled(0);
  s200PowerOffReady(); //just in case
  UseLaser=0;
  return 1;
}

//VLG-2 MAP TARGET med DME
/**********************************
*
*
***********************************/
int fhgt1p_dme_repeat()
{
  //1p dme-m�tning
  int res;
  sd.treemethod = ONE_P_LASER_METHOD_REPEAT;
  res = fhgt1p_dme_();
  return res;
}

/**********************************
*
*
***********************************/
int fhgt1p_dme_relative()
{
  //1p dme-m�tning
  int res;
  sd.treemethod = ONE_P_LASER_METHOD_RELATIVE;
  res = fhgt1p_dme_();
  return res;
}

/**********************************

***********************************/
int calc_hgt_1pdme(int tdistance,int talpha)
{
  int neg,angle;
  long tdist,h;
  angle=0-talpha;
  if(angle<0){
    neg=-1;
  }
  else{
    neg=1;
  }

  //1-punkts m�tning med dme
  tdist=ee->EEpivot+tdistance;
  h=rounddiv(tdist*isin(angle),icos(0));
  h=h*neg;

  //l�gg till �gonh�jden och dra bort transponderh�jden f�r att f� h�jdskillnaden till "markniv�"
  h+=ee->EErefH-ee->EEtrpHeigth;
  sd.add_to_Hgt=ee->EErefH-ee->EEtrpHeigth;//save info about ref

  return h;
}


/**********************************
Redraw
***********************************/
void redrawAngleResultDme(void)
{
  int tHdist;
  sd.height=calc_hgt_1pdme(sd.distance,-sd.alpha);//1p measurment
  copy_to_ir_str(0,sd.height);
  tHdist=Hdist(sd.distance);
  copy_to_ir_str(3,tHdist);
  putint(12-6,1,tHdist,6,1);
  putint(12-6,2,sd.height,6,1);
  redrawAngleNum(sd.alpha,3);

  cls_Oled();
  PrintStrOled(0,22,str_oled_D_);
  PrintIntOled(0,32,sd.distance*10);

  PrintIntOled(0,42,sd.hdistance*10);//try use 2decimals
  PrintStrOled(0,52,str_oled_Hgt_);
  PrintIntOled(0,62,sd.height*10);
}

/**********************************
*
*
***********************************/
void redrawAimDmeText(void)
{
  cls();
  putxy(0,0, (char*)s200_strAim1_);
  putxy(0,1, (char*)s200_strAim2_);
  putxy(0,2, (char*)s200_strAim6_);
}

/**********************************
*
*
***********************************/
int fhgt1p_dme_()
{
  int ch,AddTrpH,res=0,ret;

  AddTrpH = ADD_REF_H;
  sd.distance=0;
  sd.hdistance=0;
  UseLaser=0;
  sd.stored=0;//zero, data_set() is sometimes not used
  sd.SaveOk=0;//zero
  sd.OledDisplayDataValid=0;//zero
  sd.measmethod = TRANSPONDER_METHOD;

  do
  {
    redrawAimDmeText();
  meas_1pdme_start:
    do
    {
      ch=ToggleOledTxtAndSightGetSystemKey(LASERROLLED_NONE);
      Sight_Oled(1);
      switch(ch)
      {
      case KEY_ENTER: //dme
      meas_1pdme:
        UseLaser=1; //s� att den tar vinkeln automatiskt vid entertryck

        if((ret=dmef(DME_NO_WAIT_FOR_KEY,DME_DIST))>0)            //cm alt ftx10
        {
          Sight_Oled(0);
          wait(20);
          Sight_Oled(1);

          sd.distance = ret;      //cm alt ftx10

          sd.distance_cm = distance_to_cm(ret);   //VLG2-2 Diameterm�tning, spara alltid sd.distance_cm i cm

          if(Metric())
            sd.distance=roundi(sd.distance);    //dm
        }
        else
        {
          cls();
          putxy(0,0, (char*)s200_strNoVal1_);
          putxy(0,1, (char*)s200_strNoVal2_);
          putxy(0,2, (char*)s200_strShotAgain1_);
          putxy(0,3, (char*)s200_strShotAgain2_);
          Sight_Oled(0);//blinka till med korset n�r meddelande tagits emot fr�n lasern
          PrintStrOled(0,42,"---");
          My_Bell(500);
          //wait(500);
          ch = 0;
          UseLaser=0;
        }
        break;
      case KEY_EXIT:
        res=0;
        goto end_1pdme;
      case KEY_RIGHT:
        //VLG-2 om auto_save s� ska kunna radera sista v�rdet h�r
        if(auto_save_target)      //VLG-2 autosparar v�rden, f�r map target
        {
          if(autosd.SaveOk)      //finns n�got att spara
          {
            memset(&autosd,0x00,sizeof(autosd));
            Sight_Oled(0);
            PrintStrOled(0,42,str_oled_Deleted_);
            PrintStrOled(0,52,str_oled_Deleted2_);
            xprintf(0,0,DISP_REVERSE,12,"%-12s",str_Deleted_);
            My_bell2(300,3000);
            //wait(300);
            bell(300,3000);
          }
        }
        break;
      default:
        break;
      }
    }while(ch!=KEY_ENTER);

    copy_to_ir_str(3,sd.distance);
    if(measure_angle_(0,SHOW_ALPHA_AND_H,AddTrpH))//alfa
    {
      Bell(200);

      if(UseLaser)
      {
        redrawAngleResult();
      }
      WhileEnterPressed();   //v�nta tills enter sl�ppt

      sd.gamma=sd.beta=sd.alpha;
      sd.yaw=(int)rint(sd.gYaw*100.0);//save yaw
      redrawAngleResultDme();        //kompensera f�r EYE.HGT och TRP.HGT
      UseLaser = 0;

      //VLG-2 om autospar s� spara och returnera. Eg spara f�reg�ende v�rde.
      if(auto_save_target)
      {
        if(autosd.SaveOk)      //finns n�got att spara?
        {
          struct storedatatype tsd;

          tsd = sd;
          sd = autosd;  //spara f�reg�ende punkt

          ir_str_ram(); //det ber�knas och s�tts massa saker i sd-structen vid sparningen, normalt 100-124ms f�r att spara, 794 l�ngsammast

          tsd.x0=sd.x0; //dessa s�tts vid sparningen
          tsd.y0=sd.y0;
          tsd.xf=sd.xf;
          tsd.yf=sd.yf;
          tsd.area = sd.area;
          tsd.len = sd.len;

          tsd.sek = sd.sek;     //uppdatera n�sta sekvensnummer
          tsd.id = sd.id;       //tappar �ven id-numret
          autosd = tsd; //l�gg nya punkten i k� f�r att sparas
          if(autosd.measmethod == TRANSPONDER_METHOD)
            autosd.measmethod = ONE_P_LASER_METHOD;       //byter m�tmetod s� att det blir r�tt typ p� punkten i kml och csv
          sd = tsd;       //n�dv�ndigt??
        }
        else
        {
          if(sd.sek<=1)  //p� f�rsta punkten ska gps-pos tas
          {
            wait(400);    //v�ntar lite innan g�r �ver till GPS
            SaveOledDispContenz();      //spara displayerna f�r att kunna visa data efter gps-inl�sningen
            SaveDispContenz();
            if(ReadGps()>0)
              sd.PreDefGps=1;//avoid gps when storing, have allready

            RestoreOledDispContenz();
            RestoreDispContenz();
          }

          autosd = sd;  //kommer hit efter varje ny ref-punkt
          if(autosd.measmethod == TRANSPONDER_METHOD)
            autosd.measmethod = ONE_P_LASER_METHOD;       //byter m�tmetod s� att det blir r�tt typ p� punkten i kml och csv

          //p� f�rsta punkten s� fejka i displayen och pip att den sparar
          xprintf(0,0,DISP_REVERSE,12,"%-12s",str_DataSaved_);
          wait(400);    //v�ntar lite
          bell(200,3000);
        }

        goto meas_1pdme_start;
      }
      else
      {
        if((ch=ToggleOledTxtAndSightGetSystemKey(LASERROLLED_NONE))==KEY_EXIT)
        {
          res=0;
          goto end_1pdme;
        }
        else if(ch==KEY_ENTER)
        {
          Sight_Oled(1);
          goto meas_1pdme;
        }
        else if(ch==KEY_RIGHT)
        {
          res=1;
          sd.measmethod = ONE_P_LASER_METHOD;     //s�tter om metoden f�r att det ska bli r�tt typ vid sparning i filerna
          ir_str_ram();
          wait(300);      //sparning, v�nta lite s� hinner se i displayen
        }
      }
    }//if(measure_angle_(0,SHOW_ALPHA_AND_H,AddTrpH))
  }while(!sd.stored);

end_1pdme:
  Sight_Oled(0);
  UseLaser = 0;
  s200PowerOffReady();    //just in case

  return res;
}

/**********************************
*
*
***********************************/
int fhgt_laser_dme_repeat()
{
  if(sd.type_target_point == POINT_DME_BASE)
    return fhgt1p_dme_repeat();
  else
    return fhgt1pl_repeat();
}

/**********************************
*
*
***********************************/
int fhgt_laser_dme_relative(int type)
{
  if(type==POINT_DME_BASE)
    return fhgt1p_dme_relative();
  else
    return fhgt1pl_relative();
}

/**********************************
*
* VLG2-15 BAf-funktion
***********************************/
const char *pLineStyles[2]={"|  |","----"};
int fmeasure_baf()
{
  int ch,num=0,tnum,type;
  struct meas_dia_type dia_res;
  static int ba;
  //Kr�ver minst Bios v3.2
  if(GetVerNr() < 32)
  {
    Message(str_msg_Error_,str_msg_UpdateBios_);
    return 0;
  }

  cls();
  putxy(0,0,(char*)str_Factor_);
  do
  {
  if(Metric())
  {
    putxy(12-5,1,"m2/ha");
    if((tnum=My_InputNumZero(12-5-5-1,1,ba,4))<0)
      return KEY_EXIT;
    ba=tnum;
  }
  else
  {
    putxy(12-6,1,"ft2/ac");
    if((tnum=My_InputNumZero(12-6-5-1,1,ba,4))<0)
      return KEY_EXIT;
    ba=tnum;
    tnum=(int)(0.22957*ba+.5);//conv imperial to metric
  }
   if(tnum>250)
   {
     if(Message(str_msg_BAF_,Metric()? str_msg_MaxBAF_ : str_msg_MaxBAFft_)==KEY_EXIT)
       return 0;
   }
  else if(tnum==0)
  {
    if(Message(str_msg_BAF_,str_msg_MinBAF_)==KEY_EXIT)
       return 0;
  }
  }
  while(tnum>250 || tnum==0);

  //val av streck
  putxy(0,2,(char*)str_SightType_);
  type = ee->line_style;
  do
  {
    xprintf(7,3,1,4,pLineStyles[type]);
    ch = GetSystemKey();
    switch(ch)
    {
    case KEY_LEFT:
      if(--type<0)
        type=1;
      break;
    case KEY_RIGHT:
      if(++type>1)
        type=0;
      break;
    case KEY_EXIT:
      return 0;
    }
  }while(ch!=KEY_ENTER);
  if(type!=ee->line_style)
  {
    //spara nya v�rdet till ee
    ee->line_style = type;
    SetGeoSettings();//save ee
  }

  xprintf(0,2,0,12,"            ");
   xprintf(0,3,0,12,"            ");

  putxy(0,3,(char*)str_TotNum_);
   putxy(0,4,(char *)str_TotBA_);
  do
  {
    int totba=num*ba;
    xprintf(12-3,3,0,12,"%3d",num);
    xprintf(12-6,4,0,12,"%4d.%d",totba/10,totba%10);
    switch(ch=Meas_Baf(tnum,&dia_res))
    {
    case KEY_LEFT:
      if(--num<0)
        num=0;
        bell(300,3000);
      break;
    case KEY_RIGHT:
    case KEY_ENTER:
      Bell(200);
      num++;
      break;
    }
  }
  while(ch!=KEY_EXIT);

  return 1;
}

/**********************************
*
*
***********************************/

/**********************************
*
*
***********************************/


