#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "language.h"
#include "font.h"
#include "geo.h"        //H�mta fr�n \\hagsrv00\programvaror\geo2\kod, h�mta �ven swi.c d�rifr�n
#include "nmea.h"
#include "laser.h"
#include "datamem.h"
#include "height.h"
#include "my_math.h"
#include "user.h"
#include "orientation.h"
#include "CalcDangerous.h"
#include "map3d.h"

#ifndef MYFUNCS__
#define MYFUNCS__

//fr�n MagCal.h
/**************/
// vector components
#define X 0
#define Y 1
#define Z 2

// booleans
#define true 1
#define false 0

// useful multiplicative conversion constants
#define FDEGTORAD 0.01745329251994F		// degrees to radians conversion = pi / 180
#define FRADTODEG 57.2957795130823F		// radians to degrees conversion = 180 / pi

// type definitions
// these re-define (but with no changes) those in MQX-Lite PE-Types.h for Kinetis
typedef signed char				int8;
typedef unsigned char			uint8;
typedef signed short int		int16;
typedef unsigned short int		uint16;
typedef int32_t			int32;
typedef uint32_t		uint32;
/**************/

#define MIN_LASER_DIST 5  //min 5dm

extern int force_compass;      //VLG2-2 flagga f�r att tvinga anv�ndande av kompassen //Kompassen ska alltid anv�ndas, STATUS_USE_COMPAS returnerar alltid 1. Tagit bort ikonen fr�n huvudmenyn. Kontrollerna f�r vara kvar om �ndras i framtiden
extern int point_basepoint;     //LGEO3DPILE-6
extern int auto_save_target;    //VLG-2 flagga f�r att autospara v�rden i map target
extern int no_save_angle;       //VLG-2 flagga f�r att inte kunna spara i measure_angle_() p� Send, f�r map target
extern int UseLaser;
extern int use_dme;     //flagga fr�n bios om det �r VL eller L instrument, anv�nd ist�llet f�r USE_DME direktivet i koden
extern int measured_continously;     //VLG2-8 flagga f�r att visa om skjutit kontinuerligt med lasern under m�tning
extern int gHandle;            //VLG-2 globalt file handle
extern int rev_compass;        //VLG2-14 flagga om reversed compass ska anv�ndas

enum map3dpointtypes {POINT_LASER_TARGET, POINT_LASER_BASE, POINT_DME_BASE};
enum {USELASER,USEDME,USENOLASERNODME};

#define SETUPFILENAME   "StdSet.set"
#define SETUPVER        0x3     //VLG2-14 lagt till Reversed Compass i huvudmenyn
#define MAX_NUM_FUNCTIONS  30   //S�tter max 30 funktioner i huvudmenyn
struct sysdatatype
{
  int ver;
  int nSendFileType[6];    //0=csv, 1=kml, 2=geojson (VLG2-10) resten spare

  int functions[MAX_NUM_FUNCTIONS];       //kunna v�lja vilka menyer som ska vara valbara i huvudmenyn. OBS! 3 sista ska alltid vara aktiva Memory, Settings, USB

  char str_spare[60];

  int spare0;
  int spare1;
  int spare2;
  int spare3;
  int spare4;
  int spare5;
  int spare6;
};
extern struct sysdatatype g_Sysdata;

struct SendBlueType//V2.0 use binary struct to send saved data
{
 int hdistance, distance,height;
 float gYaw,gPitch;
};

struct storedatatype
{
  int id;
  int treemethod;
  int measmethod;
  unsigned char SaveOk; //1=ok att spara i ee-minnet, 0=ska inte kunna spara
  int height,gamma,beta,yaw;
  int alpha;
  int distance;

  int hdistance;

  int add_to_Hgt;

  int height1;
  int height2;
  int height3;//delts
  int distance1;//sd1 2pl
  int distance2;//sd2 2pl

  int prev_distance;//store first point for gps location when 3d
  int prev_gamma;
  int prev_yaw;
  int prev_hgt;

  int sek;//seqvens 1,2,3... etc

  float gPitch,gRoll,gYaw,gDelta;//radians

  struct GPSType *pgps;//gps data

  double targetx,targety,targetz;
  double startx,starty,startz;
  int stored;
  int use_ir;//us ir only when hgt;
  int PreDefId;//id alreday defined
  int PreDefGps;//gps alreday defined (used in GetGps()
  int OledDisplayDataValid;

  double x0,y0,x1,y1,xf,yf,area,len;//areal
  int handlecsv,handlekml;//preopened files, fasten file handle
  int type_target_point;//VLG-2 typ f�r point_basepoint

  int distance_cm;      //VLG2-2 Diameterm�tning, spara avst�ndet i cm. Visa och skriv SD och HD i cm, csv, kml, om Metric
  int hdistance_cm;      //VLG2-2 Diameterm�tning, spara horisontella avst�ndet i cm.
  int diameter;         //VLG2-2 Diametern mm, inch*10
  int handlejson;   //VLG2-10 lagt till GeoJsonutskrift
};

extern struct storedatatype sd;
extern struct storedatatype autosd;     //VLG-2 f�r autospara h�jd
extern struct SendBlueType sdBlue;//V2.0 use binary struct to send saved data
extern struct GPSType GpsData;

#define TIMETOSLEEP 120 //s

extern int product;
extern int ver;

extern struct eedatatype *ee;

struct MenuItemType
{
 const char *s;struct disp_icon_type *ico;int (*f)(void);
};

struct MenuType
{
 void (*Redraw)();
 const struct MenuItemType *Menu;
};

//VLG-2 KML Area, struct och array f�r att spara Basepoints till polygon i kml-fil
#define TEMPAREAPOINTFN "TempAreaPoints.bin"
struct kmlareapointdatatype
{
  double dLat;
  double dLon;
  double dAlt;
};

void InitApp();
int InitSysdata();
int SaveSysdata();

int My_Disp_Draw_Icon(struct disp_icon_type *p);
int My_YesNoExit(int x,int y,char *str,int answer);
int My_PutChar(char ch);
void My_battcheck();
int My_ch_clr_buff();
int My_ChRead();
int My_getc();
void My_SetFont(int font);

int ExecMeny(const struct MenuType *m,int n,char *act_, int IsMainMenu);
void MainMenuRedraw();

int fSettings(void);
int fUsb(void);

void copy_to_ir_str(int n,int num);
void irset(void);
void ir_str_ram(void);
double fGradToGon(double deg);
int ConvToGrad(int deg);
int ConvToProc(int deg);
int SaveDispContenz();
int RestoreDispContenz();
void putxy2_Oled(int x,int y, char* str);
void data_set();
int dme_key();
int CopyFile(char *source, char *dest);

//swi-funktioner som jag anv�nder, deklarerar h�r s� jag slipper varningar
int GetStatus();
unsigned char * GetGeoSettings();
unsigned char * SetGeoSettings();
int GetObexData(char *mapname);

//VLG2-5 Kunna v�lja funktioner i huvudmenyn
#define NUM_OF_VL_FUNCTIONS        15   //VLG2-14 lagt till Reversed Compass funktion   //VLG2-15 lagt till BAF
#define NUM_OF_L_FUNCTIONS         14
#define NUM_ALWAYS_ON           3            //3 sista ska inte g� att sl� av Memory, Settings, USB
enum vl_menu_opt_index{VL_MENU_HEIGHT1PL, VL_MENU_HEIGHT3P, VL_MENU_HEIGHTDME, VL_MENU_3DVECTOR, VL_MENU_COMPASS, VL_MENU_COMPASS_REV, VL_MENU_ANGLE, VL_MENU_MAPTARGET, VL_MENU_MAPTRAIL, VL_MENU_LINECLEAR, VL_MENU_DELTAHEIGHT, VL_MENU_BAF};
enum l_menu_opt_index{L_MENU_HEIGHT1PL, L_MENU_HEIGHT3P, L_MENU_3DVECTOR, L_MENU_COMPASS, L_MENU_COMPASS_REV, L_MENU_ANGLE, L_MENU_MAPTARGET, L_MENU_MAPTRAIL, L_MENU_LINECLEAR, L_MENU_DELTAHEIGHT, L_MENU_BAF};
int fSelectFunctions();

//VLG2-2 Diameterm�tning, nya funktioner och variabler
extern int checkrollflag;       //anv�nd f�r att aktivera kontroll om lasern tiltas �t sidan
extern struct meas_dia_type dia_res;    //resultatet fr�n m�tningen sparas i structen
int ConvToInch(int mm);
int distance_to_cm(int dist);
void PrintHeightOled();
int redrawDiaHgt(int fixed_dia);        //Visa resultatet av diameterm�tningen
int My_Meas_Dia(int sd_dist_cm, int fix_dia_mm, int AddRefH);

#endif
