#include "MyFuncs.h"

int icos(int deg);

int16_t rounddiv(int32_t div,int16_t n)
{
 int32_t res;
 if(div<0l)
  res=(div-(n>>1))/n;
 else
  res=(div+(n>>1))/n;
 return res;
}

uint32_t urounddiv(uint32_t div,uint32_t n)
{
 uint32_t res;
 res=(div+(n>>1))/n;
 return res;
}

int16_t roundi(int32_t div)
{
 return rounddiv(div,10);
}


static int16_t buff[MAXBUFFn+1];
static int16_t buffstart;
static int16_t buffend;
/*
struct BuffType
{
int16_t Buff[3][MAXBUFFn+1];
int16_t Buffstart;
int16_t Buffend;
}
Buff[2];
*/



/**********************************
Horizontal distance
***********************************/
int Hdist(int tdistance)
{
 return rounddiv(1L*tdistance*icos(sd.alpha),5000);
}

/**********************************
Count number of items in buffer
***********************************/

int cntbuff(int n)
{
 return (buffend-buffstart)&n;
}

/**********************************
Clear buffer
***********************************/

int clrbuff(void)
{
 return buffstart=(buffend=0);
}

/**********************************
* FILO *
Store item in buffer
***********************************/
int storebuff(int data,int n)
{
 buff[MAXBUFFn]=UNVALID;
 buff[buffend++]=data;
 buffend&=n;
 if(buffend==buffstart)
  {
   buffstart++;
   buffstart&=n;
  }

  return 1;
}

/**********************************
Read from buffer
***********************************/
int readbuff(int n)
{
 if(buffstart==buffend){
  return UNVALID;
 }
 buffend--;
 buffend&=n;
 return buff[buffend];
}

/**********************************
Average of buffer
***********************************/
int medelbuff(int n)
{
 int start,end;
 long sum=0;
 int i,j;
 start=buffstart;
 end=buffend;
  if((j=cntbuff(n))>0)
  {
   for(i=0;i<j;i++){
    sum+=readbuff(n);
   }
  }
 i=rounddiv(sum,j);
 buffstart=start;
 buffend=end;

 return i;
}

/**********************************************
;* L?s in v?rden i FILO int buff[BUFF+1]      *
;* Om senast inl?sta <maxfel fr?n medel: spara*
;* ?ldre v?rden skrivs ?ver                   *
;* Ev st?rning sparas om den ej var st?rning  *
;**********************************************/
int filter(int data,int maxerror,int n)
{
 int medel;
  if(cntbuff(n)<=0){
   return storebuff(data,n);
  }
 medel=medelbuff(n);
 if(abs(data-medel)<maxerror){
  return storebuff(data,n);
 }
 if(buff[MAXBUFFn]==UNVALID){
  return buff[MAXBUFFn]=data;
 }
 if(abs(data-buff[MAXBUFFn])<maxerror)
  {
   clrbuff();
   storebuff(buff[MAXBUFFn],n);
   return storebuff(data,n);
  }
  buff[MAXBUFFn]=data;
  buffstart++;
  buffstart&=n;
 return 0;
}

static const int sintab[72]=
{  0, 112, 223, 335, 446, 557, 668, 779,
 889, 999,1108,1216,1324,1432,1538,1644,
1749, 1854, 1957, 2059, 2160, 2261,2360,
2458, 2554, 2650, 2744, 2836, 2928,3017,
3106, 3192, 3278, 3361, 3443, 3523,3602,
3678, 3753, 3826, 3897, 3966, 4033,4098,
4161, 4222, 4280, 4337, 4392, 4444,4494,
4542, 4587, 4631, 4672, 4710, 4747,4781,
4812, 4841, 4868, 4892, 4914, 4933,4950,
4965, 4977, 4986, 4993, 4998, 5000,5000
};
/*******************************************
;* sin(intH:intL)=>intH:intL x5000
;* in -18000<x<18000 ut 0..5000
;*******************************************/
int isin(int deg)
{
 int index,rest,tsin;
 if(deg<0){
  deg=-deg;
 }
 while(deg>9000){
  deg=abs(18000-deg);
 }

 index=deg>>7;
 rest=deg-(index<<7);

 tsin=(((long) rest)*(sintab[index+1]-sintab[index])+64)>>7;
 tsin+=sintab[index];
 return tsin;
}
/**********************************
Cosinus
***********************************/
int icos(int deg)
{
 return isin(deg-9000);
}

/**********************************

***********************************/
int calc_hgt(int tdistance,int talpha,int tbeta, int trp)
{
 int neg,angle;
 long tdist,h;
 angle=tbeta-talpha;
 if(angle<0){
  neg=-1;
 }
 else{
  neg=1;
 }

 tdist=ee->EEpivot+tdistance;
 h=rounddiv(tdist*isin(angle),icos(tbeta));
 h=h*neg;
  if(trp)
  {
  if( sd.measmethod!=TRANSPONDER_METHOD){//0=1p Laser
    h+=ee->EErefH;
    sd.add_to_Hgt=ee->EErefH;//save info about ref
   }
   else{
    h+=ee->EEtrpHeigth;
    sd.add_to_Hgt=ee->EEtrpHeigth;//save info about ref
   }
  }
 return h;
}

/**********************************

***********************************/
int calc_hgt_3(int tdistance,int talpha,int tbeta,int gamma,int trp)
{
 int h1,h2;
 h1=calc_hgt(tdistance,talpha,tbeta,0);
 h2=calc_hgt(tdistance,talpha,gamma,trp);
 return h2-h1;
}

/*****************************
c^2=a^2+b^2-2abcos(alpha)
*****************************/
unsigned int Calc2pH(int d1,int d2,int talpha)
{
 float sum;

 d1=ee->EEpivot+d1;
 d2=ee->EEpivot+d2;

 sum=(1.0*d1*d1)+(1.0*d2*d2)-(2.0*d1*d2*cos(talpha/18000.0*MA_PI));//
 sum=sqrt(sum);
 return (int)(sum+.5);
}

/**********************************
Hopt=(H1*D2+H2*D1)/(D1+D2)
***********************************/
int CalcHeightOptional(int d1,int h1,int d2,int h2)
{
 long sum;
 sum=(1L*d1*h2)+(1L*d2*h1);
 sum=sum/(d1+d2);
 return (int)sum;
}




//3d **************************************
/*

short int cntbuff_3d(int16_t device,int16_t n)
{
 return (Buff[device].Buffend-Buff[device].Buffstart)&n;
}
short int clrbuff_3d(int16_t device)
{
 return Buff[device].Buffstart=Buff[device].Buffend=0;
}

short int storebuff_3d(int16_t device,struct angle_type B,int16_t n)
{
  Buff[device].Buff[0][MAXBUFFn]=UNVALID;
  Buff[device].Buff[0][Buff[device].Buffend]=B.x;
  Buff[device].Buff[1][Buff[device].Buffend]=B.y;
  Buff[device].Buff[2][Buff[device].Buffend++]=B.z;
 Buff[device].Buffend&=n;
 if(Buff[device].Buffend==Buff[device].Buffstart)
  {
   Buff[device].Buffstart++;
   Buff[device].Buffstart&=n;
  }
 return 1;
}

struct angle_type readbuff_3d(int16_t device,int16_t n)
{
 struct angle_type B;
 if(Buff[device].Buffstart==Buff[device].Buffend)
 {
   B.x=Buff[device].Buff[0][Buff[device].Buffend]=UNVALID;
  return B;
  }
// No data
 //if(Buffstart==Buffend)
//  {
//  B.x=Buff[0][Buffend]=UNVALID;
//  }
//
// Read oldest..
 B.x=Buff[device].Buff[0][Buff[device].Buffstart];
 B.y=Buff[device].Buff[1][Buff[device].Buffstart];
 B.z=Buff[device].Buff[2][Buff[device].Buffstart];
 Buff[device].Buffstart++;
 Buff[device].Buffstart&=n;
 return B;

//  Read latest
 //Buffend--;
 //Buffend&=n;
 //B.x=Buff[0][Buffend];
 //B.y=Buff[1][Buffend];
 //B.z=Buff[2][Buffend];
 //return B;
//
}

struct angle_type medelbuff_3d(int16_t device,int16_t n)
{
 short int start,end;
 int sum[3];
 short int i,j;
 struct angle_type B;
 memset(&B,0,sizeof(B));
 memset(sum,0,sizeof(sum));
 start=Buff[device].Buffstart;
 end=Buff[device].Buffend;
  if((j=cntbuff_3d(device,n))>0)
   for(i=0;i<j;i++)
   {
    B=readbuff_3d(device,n);
    sum[0]+=B.x;
    sum[1]+=B.y;
    sum[2]+=B.z;
   }

 if(j)
 {
  B.x=rounddiv(sum[0],j);
  B.y=rounddiv(sum[1],j);
  B.z=rounddiv(sum[2],j);
 }
 Buff[device].Buffstart=start;
 Buff[device].Buffend=end;

 return B;
}


int filter_3d(int16_t device,struct angle_type B,short int maxerror,short int n)
{
 struct angle_type mB;

  if(cntbuff_3d(device,n)<=0)
   return storebuff_3d(device,B,n);

 mB=medelbuff_3d(device,n);
 if(abs(mB.x-B.x)<maxerror&&abs(mB.y-B.y)<maxerror&&abs(mB.z-B.z)<maxerror)
  return storebuff_3d(device,B,n);

 if(Buff[device].Buff[0][MAXBUFFn]==UNVALID)
  {
   Buff[device].Buff[0][MAXBUFFn]=B.x;
   Buff[device].Buff[1][MAXBUFFn]=B.y;
   Buff[device].Buff[2][MAXBUFFn]=B.z;
   return 1;
  }
 if(abs(B.x-Buff[device].Buff[0][MAXBUFFn])<maxerror&&abs(B.y-Buff[device].Buff[1][MAXBUFFn])<maxerror&&abs(B.z-Buff[device].Buff[2][MAXBUFFn])<maxerror)
  {
   struct angle_type ttB;
   clrbuff_3d(device);
   ttB.x=Buff[device].Buff[0][MAXBUFFn];
   ttB.y=Buff[device].Buff[1][MAXBUFFn];
   ttB.z=Buff[device].Buff[2][MAXBUFFn];
   storebuff_3d(device,ttB,n);
   return storebuff_3d(device,B,n);
  }

  Buff[device].Buff[0][MAXBUFFn]=B.x;
  Buff[device].Buff[1][MAXBUFFn]=B.y;
  Buff[device].Buff[2][MAXBUFFn]=B.z;
  Buff[device].Buffstart++;
  Buff[device].Buffstart&=n;

  if(!cntbuff_3d(device,n))//added
   storebuff_3d(device,B,n);

 return 0;
}
*/
int ATanAlpha(int16_t x,int16_t y,int16_t z)
{
 int res;
 float comp;

 comp=1.0*x/sqrt(1.0*y*y+1.0*z*z);
 comp=100.0*atan(comp)*180/MA_PI;
  if(comp<0.0)
   res=(int)(comp-.5);
  else
   res= (int)(comp+.5);
   return res;
}
/* Pitch Deg x100 */
float gPitch,gRoll,gYaw,gDelta;


/**********************************
Convert to feet
***********************************/
int ConvToFeet(int cm)
{
  long temp;
  temp=(long)cm*3281L;
  return rounddiv(temp,10000);
}


/*
Spherical [r, pitch, yaw]   Cartesian [x, y, z]
(X=Norr, Y=?st, Z=upp)
x = rsin(90-pitch)cos yaw
y = rsin (90-pitch)sin yaw
z = rcos (90-pitch)
Conversion from spherical to cartesian coordinates:
  */

struct angle_typef ConvertToCartesian(float dist,float pitch,float yaw)
{
  struct angle_typef tCartesian;
  pitch=pitch/180.0*MA_PI;//to radians
  yaw=yaw/180.0*MA_PI;//to radians
  tCartesian.x=dist*cos(pitch)*cos(yaw);
  tCartesian.y=dist*cos(pitch)*sin(yaw);
  tCartesian.z=dist*sin(pitch);
  return tCartesian;


}



int m_n[2];
double m_oldM[2], m_newM[2], m_oldS[2], m_newS[2];
int NumDataValues(int index)
{
  return m_n[index];
}

double Mean(int index)
{
  return (m_n[index] > 0) ? m_newM[index] : 0.0;
}

double Variance(int index)
{
  return ( (m_n[index] > 1) ? m_newS[index]/(m_n[index] - 1) : 0.0 );
}

double StandardDeviation(int index)
{
  return sqrt( Variance(index) );
}


void StoreStat(int index,int n,double x)
{
m_n[index]=n;
  if (m_n[index] == 1)
  {
    m_oldM[index] = m_newM[index] = x;
    m_oldS[index] = 0.0;
  }
  else
  {
    m_newM[index] = m_oldM[index] + (x - m_oldM[index])/m_n[index];
    m_newS[index] = m_oldS[index] + (x - m_oldM[index])*(x - m_newM[index]);

    // set up for next iteration
    m_oldM[index] = m_newM[index];
    m_oldS[index] = m_newS[index];
  }
}



