#include <ysizet.h>
void Reset_Handler(void);
#pragma section = "ROM_CONTENT"
__root const size_t rom_len @ "rom_len" = __section_size("ROM_CONTENT");
__root const size_t rom_mark  @ "rom_mark" =0xaeae;
__root const size_t rom_start @ "rom_start"=(size_t)Reset_Handler;
__root const size_t rom_ver   @ "rom_ver" =32;
__root const size_t rom_snr   @ "rom_snr" =-1;
__root const size_t rom_product @ "rom_product"=1000;
__root const size_t rom_lic @ "rom_lic"=-1;

__root const unsigned char  checksum_start    @ "checksum_start_mark" =0;
__root const unsigned char  checksum_end[4]   @ "checksum_end_mark" = {0,0,0,0xEE}; // The last byte is the actual "checksum area end mark"

typedef struct _DeviceVectors
{
  /* Stack pointer */
  void* pvStack;

  /* Cortex-M handlers */
  void* pfnReset_Handler;

} DeviceVectors;

void Reset_Handler(void);

typedef void (*intfunc)(void);
typedef union {
	intfunc __fun;
	void *  __ptr;
} intvec_elem;

void __iar_program_start(void);
int  __low_level_init(void)
{
}

/* Exception Table */
#pragma language = extended
#pragma segment  = "CSTACK"

/* The name "__vector_table" has special meaning for C-SPY: */
/* it is where the SP start value is found, and the NVIC vector */
/* table register (VTOR) is initialized to this address if != 0 */

#pragma section                      = ".intvec"
#pragma location                     = ".intvec"
const DeviceVectors __vector_table[] = {
    (void *)__sfe("CSTACK"),
    (void *)Reset_Handler,
};

void __iar_init_vfp_( void )
{
}
/**------------------------------------------------------------------------------
 * This is the code that gets called on processor reset. To initialize the
 * device.
 *------------------------------------------------------------------------------*/
void Reset_Handler(void)
{



	__iar_program_start();
}

