#ifndef DIAMETER__
#define DIAMETER__

enum {LP,LM,RP,RM};     //left-right plus-minus
#define MAX_LINE_OFFSET 23

#define MAX_LINE_STYLE 2        //anv�nd endast fulla streck och linje
enum{LINE_FILLED,LINE_BAR,LINE_SECOND,LINE_242,LINE_161};       //anv�nder bara 2 f�rsta

extern int checkrollflag;       //anv�nd f�r att aktivera kontroll om lasern tiltas �t sidan

int ConvToInch(int mm);
void init_dia_meas();
void exit_dia_meas();
void DrawLineOled(int tx, int ty, int max, int line_style);
void ClrLineOled(int tx, int ty, int max, int line_style);

int meas_dia(int return_on_enter);
int distance_to_cm(int dist);
int Sdist(int tdistance);
void PrintHeightOled();

#endif